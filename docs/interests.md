# <center>Interests</center>
<center>![Interests](assets/cyandipole.gif)</center>

<font size="2">This section will begin as a repository for ideas: things, concepts, phenomena or fields of study that I am attracted to which I will try to structure, refine and rearticulate as the course progresses.
I hope it will be an exercise in building a taxonomy of "exciters" in order to find an interesting directions for projects.</font>
<br>
<br>
<font size="2">The first part of organising these items is an exercise in brainstorming  or free association followed by categorisation. This approach is cumbersome but gives value in finding processes of inquiry for projects.
I hope to develop a tool - where I can input things that give me motivation and visualise the links and shared characteristics between them to produce a web of interest where interesting proposals can be located.</font>

### Navigating interests
Using a coloured mind map I decided to split the items I am attracted to into:

* immeasurable qualitative experiences
* and types of scientific measured and rational enquiry.
* Many items on my list are conduits or interfaces of information to and from these fields on enquiry. I called these either technologies or arts.
* I have called some items that are natural occurrences Phenomena (which from my point of view have no apparent design behind them).
* I also felt I should add Values to include a political compass in relation to any projects that might come out of the exercise.

<iframe
  src="https://embed.kumu.io/c47847646e06fa8bee2714076c159d17"
  width="940" height="600" frameborder="0"></iframe>



**Below are some themes that are emerging from my enquiries during this course**

## Science and Experience - thinking and feeling - measurable and immeasurable

>  “The notion that science and spirituality are somehow mutually exclusive does a disservice to both.”

*-Carl Sagan*

I feel that I navigate reality everyday by moving between measuring and reasoning,  experiencing qualitative immeasurable feelings and designing responses that work towards values or beliefs.

Scientific inquiry and technological objects can incite mystical experiences...

![Machine Art Exhibition MOMA](assets/MachineArtMOMA.jpg)

<font size="2">*View of “Machine Art,” Museum of Modern Art, New York, 1934*</font>

...and mystical experiences and symbols can incite rational ideas.

<iframe width="480" height="270" src="https://www.kickstarter.com/projects/2093788053/instant-archetypes-a-new-tarot-for-the-new-normal/widget/video.html" frameborder="0" scrolling="no"> </iframe>

<font size="2">*Superflux - contemporary tarot cards*</font>

In Term 3 I found this diagram by Neri Oxman

![](assets/krebs.png)


**References**

* [Scientific discoveries made in dreams by Epoch Times](https://www.theepochtimes.com/5-scientific-discoveries-made-in-dreams_1380669.html)

* ['Pillow-Talk: Seamless Interface for Dream Priming Recalling and Playback' Edwina Portocarrero, David Cranor V, Michael Bove](https://dam-prod.media.mit.edu/x/files/wp-content/uploads/sites/10/2013/07/4p375-portocarrero.pdf)

* [Superflux Tarot Cards](http://superflux.in/index.php/work/synbio-tarot-reading/#)

## Discrete and Continuous thinking

Wave Particle duality shows us that a phenomena can be detected and modelled as waves (continuous) and particles (discrete).

Calling something a particle involves border definition.

Wave modelling attributes a cyclical movement rather than a border.


![Fourier Transformations](wa/assets/fourier.gif)

*The Fourier series is a way to represent a function as the sum of simple sine waves*




## How systems evolve

![information structure evolution](assets/infosystemsmall.png)

### Nature is dynamic and equilibrium is fleeting
Living systems are constantly evolving and rearranging themselves and cosmology presents dynamic models of the  universe. Humans are more than Newtonian machines and our brains behave differently to silicon chips.
Economic and political systems are organisms trying to order themselves - they too keep changing. The dustbowl and the great depression demonstrated instability in nature affecting economic and political systems.

Living systems change. Politics must reflect this.


<iframe width="560" height="315" src="https://www.youtube.com/embed/8vxYIy8vL_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Determinism and agency
Chaos theory in mathematics demonstrates determinism in nature and 'the butterfly effect' tells us that whatever you do - no matter how small - matters and contributes to what happens in the future sometimes with huge affects.

We feel helpless and anxious if we think of ourselves as part of an unpredictable Newtonian machine but nature is more than Newtonian. Instead of feeling helpless can we feel that we have agency in our actions knowing that what we do DOES affect the future? Can this knowledge help us to take responsibility rather than feel oppressed?

<div style="max-width:560px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/sean_gourley_on_the_mathematics_of_war" width="560" height="315" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>


 <iframe width="560" height="315" src="https://www.youtube.com/embed/qfp5tKeSQAc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Sustainability and dynamic systems
In week 5 Jose Luis articulated well the anxiety we feel about facing climate change.
What does dynamic sustainability look like?

What are the critical "nodes" of influence that can steer systems to a dynamic sustainability?
What tools are we going to need? - what are the key technologies and indicators?

### Chaos and AI

### Linked Proposals:
* [A Toolkit for dealing with chaos](proposals/#toolkit)


**References:**

* http://www.kierandkelly.com/

* [Keith Bennett (2010) 'The chaos theory of evolution']( https://www.newscientist.com/article/mg20827821-000-the-chaos-theory-of-evolution/)

* [Scoones, I., Leach, M., Smith, A., Stagl, S., Stirling, A. and Thompson, J. (2007) Dynamic Systems and the Challenge of Sustainability, STEPS Working Paper 1, Brighton: STEPS Centre](http://steps-centre.org/wp-content/uploads/final_steps_dynamics.pdf)

* [D.C. Wahl (2006) Understanding complexity: A prerequisite for sustainable design from ‘Design for Human & Planetary Health’]((https://medium.com/age-of-awareness/understanding-complexity-a-prerequisite-for-sustainable-design-fd45990e3bd6)

* https://sustainability.asu.edu/media/wrigley-lecture-series/complex-systems-theory/

* [Carlos E. Perez (2017) 'How to Explain Deep Learning using Chaos and Complexity' Medium](https://medium.com/intuitionmachine/how-to-explain-deep-learning-using-chaos-and-complexity-33de81c321de)

* [DAVID GRAEBER DAVID WENGROW (2018) 'How to change the course of human history(at least, the part that’s already happened)'](https://www.eurozine.com/change-course-human-history/)

* [Postnormal Times Website 'Ignorance and Uncertainty](https://postnormaltim.es/essentials/ignorance-and-uncertainty)

* Tegmark, Max (1998) 'Is the 'theory of everything' merely the ultimate ensemble theory?'


## Inclusive Systems

I believe that human processes and systems that are open and inclusive are the key to boosting innovation and improving resilience if not actual sustainability.

The United Nations recognise inclusive societies and institutions as instrumental in sustainable development.  Sustainable Development Goal 16 is to:

> 'Promote peaceful and **inclusive** societies for sustainable development, provide access to justice for all and build effective, accountable and inclusive institutions at all levels'.

 My starting points are governance,  education and economic systems but I am also interested in learning from systems of expression like the media, art, design, music and storytelling.

  <iframe
  src="https://embed.kumu.io/9adb5414650f055a3e39d3e0acef187d"
  width="940" height="600" frameborder="0"></iframe>



### What is an inclusive system?

Research into 'Inclusion' seems to mostly be coming from the field of education. Christopher McMaster from University of Canterbury writes that *the core elements
that enable inclusive change are: relationships, advocacy, a sense of identity, shared experiences, and transparency*.

### Open, closed and isolated systems:
In thermodynamics:

* an open system can exchange both energy and matter with its surroundings.
* A closed system, can exchange only energy with its surroundings, not matter.
* An isolated system is one that cannot exchange either matter or energy with its surroundings.

The concept of **transparency** as a way of working uses the idea of information about data and processes being **open** to scrutiny and accountability.

Part of the attraction to the MDEF course is it's open nature - sharing learnings and opening up to scrutiny to refine ideas.



### Why inclusive systems in highly complex and chaotic environments?

>Chaos is caused by weak negative feedback underdamping the population’s “Volatility”!... Any driven damped system will only be able to self-stabilize (i.e. self-organize its own balancing stability) if it has the ability to “fine-tune” its way to equilibrium

*- Kieran D. Kelly, Experimental Computer Scientist, and Specialist in Complex Nonlinear Systems and Dynamics.*

### Making information and education more inclusive - everyone is a designer, scientist and artist

Art, Design, Science and Engineering all have their exclusivity issues. One thing keeping someone from being any one of these things is whether or not they feel they belong in that field, whether it is their role to be these things.
There is personal preference but we are consciously or not assigned roles by the stories we are told about ourselves and whether or not information is made comprehensible to us. Some people will not consider leaving the roles they are assigned at a young age - this is top down design based on a system of inherited privilege. This makes outputs less rich and statistically we are less likely to gain insight if fewer people of similar backgrounds are working in each area.

One area where this doesn't happen as much is music - where many breakthroughs in music practices came from the most excluded, underprivileged and even oppressed people.

#### Educational tools

* Learning by making (wordless learning) https://www.techwillsaveus.com/shop/education/

* User Interface technology

* Interface technologies for people with Autism and Attention Deficit Disorder

* Mobile phones in developing countries

* Mobile phone innovations by and for refugees

* Mobile phones in disasters

* Disrupting exclusive university systems - MOOCs and youtube

* Communities of education and practice


### Building respect and representation

#### The value of work
Current economic systems are not fit for the average working person let alone people with disabilities or other disadvantages.
Can looking at work as an isolated system without money or time help us to reverse engineer a system that suits the dynamics of the system better?

##### Taking money out of the equation / what is work? / what is money?
Mariana proposed the idea of work being an investment and money being a "joker card" for this. If we consider only what goes in and out of our bodies, can we find another mediating system other than money? What would this system look like? What are it's properties.

##### Who should be designing jobs
Business, finance, tax and employment laws have aged to encourage the syphoning off of profits and taxes for a minority of people - who then continue to have the most agency to create jobs (and finance politics) and not necessarily the most innovative, sustainable, healthy ideas. Meanwhile the rest of the people (workers), struggle to secure basic needs let alone employ people for ideas they have.

##### Work and social roles
Worse still whole "classes" of people are led to believe it is not their place to test the limits of the financial system.

An anthropological take on evolution of the nature of work finds that some jobs seem to exist to uphold the current system.

<iframe width="560" height="315" src="https://www.youtube.com/embed/K0t50D4lQrs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On numbers alone, I speculate that most struggling workers would value their health, sanitation, housing and education highly. I do not believe that they would vote for company bosses to earn more than 10 times the amount of t he lowest wage in their company.

##### Homing in on particular jobs
Understanding the human resource that goes into systems such as water and sanitation could help people appreciate their resources (human and natural)

##### Helping people navigate careers and education
Understanding work can also help people understand how best they can live the kind of lives they want and how they can contribute https://80000hours.org/

##### Telling stories about how it feels to navigate work
I found in exploring this topic as a lived embodied design that I have my own narrative linked to this desire to communicate how work feels and is valued. I could explore the topic using narrative tools such a film.

##### Humans are not machines
Breakthroughs in technology (agricultural, mechanical, cybernetics) seem to invoke systems of humans mimicking the technology. The free market born out of Objectivism embedded a faith in stable states that are shown historically and mathematically not exist as systems become complex.

We also know that humans' physiology responds to time and energy very differently to logic circuits in semiconductors.

### Linked Proposals:
* 'ADDesk'
* Interfaces for people with Autism
* Work investment plotter



**References:**

* Mechanisms for Inclusive Governance - Raymond L. Ison Philip J. Wallis (2016)
* Elements of Inclusion: Findings from the Field, Christopher McMaster University of Canterbury (2014)
* https://files.eric.ed.gov/fulltext/EJ1040134.pdf
* Hardy, I., and S.Woodcock.2015. “Inclusive Education Policies: Discourses of Difference, Diversity and Deficit.” International Journal of Inclusive Education
* http://www.kierandkelly.com

## Metamaterials - bending light and sound using geometry

### Structural properties

#### Mechanical metamaterials

* [Hasso Plattner Institute_HCI Potsdam, Germany have developed some 3D printed metamaterial mechanical devices](https://www.thingiverse.com/HassoPlattnerInstitute_HCI/collections/metamaterial-mechanisms)
    <iframe width="560" height="315" src="https://www.youtube.com/embed/lsTiWYSfPck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* [Alexandra Ion, Johannes Frohnhofen, Ludwig Wall, Robert Kovacs, Mirela Alistar, Jack Lindsay, Pedro Lopes, Hsiang-Ting Chen, and Patrick Baudisch Hasso Plattner Institute, Potsdam, Germany (2017) 'Metamaterial Mechanisms'](http://alexandraion.com/wp-content/uploads/2016UIST-Metamaterial-Mechanisms-authors-copy.pdf)

* [Johannes Frohnhofen's Metamaterial Mechanisms Editor](https://johannes.frohnhofen.com/metamaterial-mechanisms/)

Digital Mechanical Metamaterials
Using "mechanical computation" in 3D printed objects, i.e., no electronic sensors, actuators, or controllers typically used for this purpose. The resulting objects can be 3D printed in one piece and thus do not require assembly.

<iframe width="560" height="315" src="https://www.youtube.com/embed/A0lJUzcKDsY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

http://chi2017.acm.org/


#### Transformable metamaterials

* Hasso Plattner Institut again Alexandra Ion, Robert Kovacs, Oliver S. Schneider, Pedro Lopes, Patrick Baudisch
    <iframe width="560" height="315" src="https://www.youtube.com/embed/t12kHV001hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

##### Origami strategies

* 'Topological kinematics of origami metamaterials' Bin Liu, Jesse L. Silverberg, Arthur A. Evans, Christian D. Santangelo, Robert J. Lang, Thomas C. Hull & Itai Cohen (2018)  Nature Physics

* 'Geometry of Transformable Metamaterials Inspired by Modular Origami' Yunfang Yang and Zhong You (2017) Journal of Mechanisms and Robotics

* Extruded cubes put together to utilise hinges and stiffness to make a range of shapes. 'A three-dimensional actuated origami-inspired transformable metamaterial with multiple degrees of freedom.' **Johannes T.B. Overvelde**, Twan A. de Jong, Yanina Shevchenko, Sergio A. Becerra, George M. Whitesides, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2016) Nature Communications

  <iframe width="560" height="315" src="https://www.youtube.com/embed/AbCb1TtYTbg?start=14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* 'Rational design of reconfigurable prismatic architected materials' Johannes T. B. Overvelde, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2017)  Nature

  <iframe width="560" height="315" src="https://www.youtube.com/embed/7A_jPky3jRY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
'
### Wave Interaction

In the 1960s 'negative refractive index' was proposed - could we get light to bend the opposite way to how it does in nature?

The structure of materials interact with waves that travel through materials (like air and water) and electromagnetic wave radiation (that doesn't a need material).
Perfect lenses were proposed in the late 1990s and Sir John Pendry demonstrated this in early 2000 using a metamaterial - structures much smaller than the wavelengths of light - in the world of **nanotech**.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ogNKrQCH1Kk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Acoustic metamaterials

University of Sussex Interact Lab looked into:
* a [3D printable modular acoustic metamaterial building system in 2017](http://www.sussex.ac.uk/broadcast/read/39354)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/BcVxRyvipcU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* They also looked into dynamic acoustic control using metamaterials and transducers.

  <iframe width="560" height="315" src="https://www.youtube.com/embed/zixzyeF25SY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


  Modular Acoustic Filters using Acoustic Voxels:  is available here:
  http://www.cs.columbia.edu/cg/lego/

  <iframe width="560" height="315" src="https://www.youtube.com/embed/7JbN9vXxGYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Steve Cummer's team at Duke University were looking into Acoustic Holograms in 2016:

* [ Yangbo Xie, Chen Shen, Wenqi Wang, Junfei Li, Dingjie Suo, Bogdan-Ioan Popa, Yun Jing, and Steven Cummer (2016) 'Acoustic Holographic Rendering with Two-dimensional Metamaterial-based Passive Phased Array'](https://arxiv.org/ftp/arxiv/papers/1607/1607.06014.pdf)

* ["it should be possible to manufacture entire 3D printed fields of sound that don’t rely on a transducer array controlled by electronics, but rather a grouping of cells that work together to control the acoustic waves."](https://3dprint.com/154311/duke-3dp-acoustic-holograms/)

* The printed material is acrylonitrile butadiene styrene plastic with density of 1180 kg/m3  ['Systematic design and experimental demonstration of bianisotropic metasurfaces for scattering-free manipulation of acoustic wavefronts' Junfei Li, Chen Shen, Ana Díaz-Rubio, Sergei A. Tretyakov & Steven A. Cummer (2018) Nature Communications](https://www.nature.com/articles/s41467-018-03778-9)

 ['Enhancing the Sound Absorption of Small-Scale 3D Printed Acoustic Metamaterials Based on Helmholtz Resonators' Cecilia Casarini,  Ben Tiller, Carmelo Mineo, Charles N. MacLeod, James F.C. Windmill and Joseph C. Jackson](https://pureportal.strath.ac.uk/files-asset/82476858/Casarini_etal_IEEE_Sensors_2018_Enhancing_the_sound_absorption_of_small_scale_3D_printed_acoustic_metamaterials.pdf)

[Disney Research Labs Acoustic filters into everyday objects](https://www.disneyresearch.com/publication/acoustic-voxels/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/gyMta5Eyo0A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Magnetically controlled metamaterial shapes
*  [A team led by USC Viterbi developed 3-D printed acoustic metamaterials that can be switched on and off remotely using a magnetic field](https://viterbischool.usc.edu/news/2018/04/3-d-printed-active-metamaterials-for-sound-and-vibration-control/) https://www.3dnatives.com/en/metamaterials-3d-printed180420184/ Kunhao Yu  Nicholas X. Fang  Guoliang Huang  Qiming Wang (2018) 'Magnetoactive Acoustic Metamaterials'

    <iframe width="560" height="315" src="https://www.youtube.com/embed/aV07hCF7-AQ?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  * Tunable Field responsive mechanical metamaterials (FRMMs) made of polymeric tubes filled with 'magnetorheological fluid'
  Julie A. Jackson et al (2018)'Field responsive mechanical metamaterials'  Science Advances

    <iframe width="560" height="315" src="https://www.youtube.com/embed/iNaMxIad7Io?start=14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





#### Optical and EM metamaterials

Invisibility shields (which has been the holy grail of metamaterials).
Materials would have to be of the nano scale in order to use their geometry to bend light. Currently methods to manufacture these Photonic metamaterials are very expensive and difficult to do.

Expensive to make for the visible light range but for longer wavelengths applications include:

1. Making radar satellite dishes cheaper
2. Optimising solar collection
3. Optimising IR (heat) collection or dissipation


* https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html


### Use of diatom frustules as optical metamaterials


>"Several animals and plants, mainly insects, birds and ﬂowers, developed, through billions of years of evolution and as constitutive part of their organisms, **sub-micron, pe-riodic or quasi-periodic architectures which substantially act as photon-ic crystals**, giving rise, by means of selective transmittance and reﬂectance at different wavelengths, to so-called structural colors. The interaction of diatom frustules with light is even more articulate, and consists of three main phenomena: **light conﬁnement, selective transmission and photoluminescence.**

> "It seems likely that the structure of the frustule provides the diatom with capabilities, not only to **focus light, but also to tune the reception of light in an optimal way**. It aligns with novel developments in antenna technology where electromagnetic bandgap structures are used to create low loss dielectric structures based on periodicity of super shapes to prevent propagation of certain frequencies, and increase isolation between antennas, and this can be tailored on angle of incidence and polarization. Also in ﬂuid dynamics (see Section 4)shape sdeﬁned by Eqs. (1) and (2) have been used (Wang, 2008; Legay and Zilian, 2008). Eqs. (1) and (2)have also been used in many studies in nanotechnology (e.g. Tassadit et al., 2011; Macías et al., 2012; Forestiere et al., 2015), in **optimizing electro-osmotic pumps based on asymmetric silicon microchannel membranes** (Parashchenko et al., 2014) and in the study of dielectric properties of the skin (Huclova et al., 2010).

*[Diatom Frustule Morphogenesis and Function: A Multidisciplinary Survey - Edoardo De Tommasi, Johan Gielis, Alessandra Rogato](https://www.researchgate.net/publication/318567831_Diatom_Frustule_Morphogenesis_and_Function_A_Multidisciplinary_Survey)*

3d printed
https://www.youtube.com/watch?v=vOOvdheuAFs



**References:**


* Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood

* Biologically enabled sub-diffractive focusing
E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu


* 'Comparing optical properties of different species of diatoms'(2015)
Maibohm, Christian; Friis, Søren Michael Mørk; Su, Y.; Rottwitt, Karsten



### Deep Learning to design metamaterials

* [Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018]


### 3D printing metamaterials

aerogel


<iframe width="560" height="315" src="https://www.youtube.com/embed/pNsnvRHNfho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



### Possible Projects

* Low cost glasses
* Earphones / Acoustic filter earmuffs (see above)
* Soundscape environment
*


## Continua: Waves, Fields and Fluids

http://www.studioovervelde.com/wp-content/uploads/2018/07/My-Movie-46.mp4

###  Fields and Fluids in Chaos

### Sound field experiences


**References:**

* Sound Field Synthesis (2014) Academic Rudolf Rabenstein, Sascha Spors, Jens Ahrens, Press Library in Signal Processing  Volume 4, 2014, Pages 915-979, Chapter 32

* http://diy3dprinting.blogspot.com/2016/09/ninjaflex-3d-printed-metamaterial.html

* http://interact-lab.com/our-research/


## Updates from interesting people

<a class="twitter-timeline" data-lang="en" data-width="400" data-height="600" data-theme="light" data-link-color="#00ff00" href="https://twitter.com/srazamataz/lists/mdef-interests?ref_src=twsrc%5Etfw">A Twitter List by srazamataz</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

**Mapping Tools used:**

* Kumu
* Draw.io
* http://arborjs.org/halfviz/#/python-grammar
* https://pp.bme.hu/ar/article/view/8215
* https://gephi.org/
* https://rawgraphs.io/
* https://npm.runkit.com/d3-force - forces
