python notes

http://fab.academany.org/2019/labs/barcelona/local/codeclub/intropython/


* use anaconda interface OR pip in your command prompt not both

* right hND SIDE SHOWS PACKAGES dependencies
* left hand side has environments  where you can group dependencies
environments are really folders where you save stuff

* to run scripts go to "anaconda prompt" in windows
in your terminal or anaconda prompt

conda activate nameofenvironment - tells python where to look for variables

## create environment
to install python on anaconda
https://medium.com/@GalarnykMichael/install-python-on-windows-anaconda-c63c7c3d1444

writing files
save files as .py files in atom

call them from your terminal

to find where your python files are use
> ➜ where python

the very first line of the file should be:
#!path/to/python, i.e.: #!/Users/macuser/anaconda2/bin/python. Then, we can make the file executable by:
> chmod +x app.py

And run it by:
> ./app.py

> print ('Hello')


argument parsing

import name of the library (by default installed with anaconda)

in terminal go to C:\Users\saira\Documents\Git\saira.raza\docs\python


The anaconda environment enables your computer to store libraries

| Library | What is it   | associated code |
| ------------- |:-------------| -----|
| argparse     | Argument parser The program defines what arguments it requires, and argparse will figure out how to parse those out of sys.argv.| to set up in the code... import argparse      parser = argparse.ArgumentParser()      parser.parse_args())    to call from command:   python program.py --help (or python program.py -h)                 usage: program.py [-h]               optional arguments:            -h, --help  show this help message and exit |
| Tkinter     |  interface to the Tk GUI toolkit shipped with Python. |    |
| wxPython |   open-source Python interface for wxWindows http://wxpython.org.   |  |
| JPython | port for Java which gives Python scripts seamless access to Java class libraries on the local machine http://www.jython.org.   |  |
| RequestException |      |  |
| BeautifulSoup | Data scraping from websites     | raw_html = simple_get('https://www.bbc.com/news') html = BeautifulSoup(raw_html, 'html.parser') |
| randint |   generate pseudo random numbers   |  |
| requests, json | de facto standard for making HTTP requests in Python  - data is available generally in JSON format. Json is done by packing data in between {}:     |  |
| imageio |   creating a gif?   |  |
| urllib |      |  |
| re |  [provides regular expression matching operations](https://docs.python.org/3/library/re.html) similar to those found in Perl.  Both patterns and strings to be searched can be Unicode strings (str) as well as 8-bit strings (bytes). However, Unicode strings and 8-bit strings cannot be mixed: that is, you cannot match a Unicode string with a byte pattern or vice-versa; similarly, when asking for a substitution, the replacement string must be of the same type as both the pattern and the search string. |  |
| serial |      | create a serial object: ser = serial.Serial(PORT, BAUDRATE) |
| datetime |      |  |
| glob |      |  |
| sys |      |  |
| numpy |   fundamental package for scientific computing with Python   |  |
|  |      |  |
|  |      |  |
|  |      |  |
|  |      |  |

We need the requests library which installed through anaconda





| function | ![Code](http://fab.academany.org/2019/labs/barcelona/local/clubs/codeclub/intropython/)  | Command |
| ------------- |-------------| -----|
| Using an anaconda environment |#!/Users/macuser/anaconda2/bin/python     chmod +x app.py| ./app.py|
| "for loop" operation |for i in range(6):  print ('Hello') | -----|
| While loop | a = 0  while a < 10:  a = a + 1  #alternatively a += 1   print(a) | -----|
|Conditionals | a = 1 if a > 5: print("This shouldn't happen.")  else:   print("This should happen.")| |
| | | |
| | | |
| | | |





> internet.py -t entersearch term



## Python and physical equipment
http://fab.academany.org/2019/labs/barcelona/local/clubs/codeclub/pythonreal/


the serial monitor in arduino is a python code
Serial
install using pipi or anaconda

grp usb?
ports  is a command apparently

a way for python to interact with the real world

you can open and close ports

e.g serial.println

youbcan do this in your terminal

">>" means whatever you output you can put into anothor port
or a file
e.g >>expample.txt



"serial - write

raspberry pi has an i square c  - which lets you do this.
so sensors on rasperry pi can log data like a server.

## serial
http://fab.academany.org/2019/labs/barcelona/local/clubs/codeclub/pythonreal/
 one serial port, also known as... USB!.

 Import pyserial through anaconda



 How do you access your code through anaconda


 conda activate sairapython
