# <center>Fab Academy</center>
<center>![](assets/3dprinting.gif)</center>

<br>
<center><a href="https://mdef.gitlab.io/saira.raza/fa/fab1/"><img src="../assets/mars.gif" width="82" height="86" title="Version control" alt="Week 1"></a> <a href="https://mdef.gitlab.io/saira.raza/fa/fab2/"><img src="docs/assets/dna.png" width="82" height="86" title="computer aided design" alt="Week 2"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab3"><img src="../assets/tool.png" width="82" height="86" title="Computer-controlled cutting" alt="Week 3"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab4/"><img src="docs/assets/milling.gif" width="82" height="86" title="Electronics production" alt="Week 4"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab5"><img src="https://media.giphy.com/media/nHyt5zmBphwl2/200w_d.gif" width="82" height="86" title="3D printing and scanning" alt="Week 5"> </a><a href="https://mdef.gitlab.io/saira.raza/fa/fab6/"><img src="../assets/circuit.png" width="82" height="86" title="Electronics design" alt="Week 6"></a></center>
<br>

<center><a href="https://mdef.gitlab.io/saira.raza/fa/fab7/"><img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/assets/cnc.gif" width="82" height="86" title="computer-controlled machining" alt="Week 7"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab8/"><img src="../wavelet.gif" width="82" height="86" title="embedded programmings" alt="Week 8"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab9/"><img src="../currentcoil.png" width="82" height="86" title="molding and casting" alt="Week 9"></a><a href="https://mdef.gitlab.io/saira.raza/fa/fab10/"><img src="../assets/lightcone.gif" width="82" height="86" title="input devices" alt="Week 10"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab11/"><img src="../assets/bmotor.gif" width="82" height="86" title="output devices" alt="Week 11"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab12/"><img src="../assets/field poles.png" width="82" height="86" title="applications and implications" alt="Week 12"></a></center>
<br>

<center><a href="https://mdef.gitlab.io/saira.raza/fa/fab13/"><img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/fa/assets/cyandipole.gif" width="82" height="86" title="networking and communications" alt="Week 13"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab14/"><img src="assets/wavelet.gif" width="82" height="86" title="mechanical design" alt="Week 14"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab15/"><img src="assets/currentcoil.png" width="82" height="86" title="interface and application programming" alt="Week 15"></a><a href="https://mdef.gitlab.io/saira.raza/fa/fab16/"><img src="../assets/spider.gif" width="82" height="86" title="machine design" alt="Week 16"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab16/"><img src="assets/vortex.gif" width="82" height="86" title="wildcard week" alt="Week 17"></a>
<a href="https://mdef.gitlab.io/saira.raza/fa/fab18/"><img src="assets/field poles.png" width="82" height="86" title="invention, IP, and income" alt="Week 18"></a></center>
<br>
<br>




## Resources

* http://fabacademy.org/2019/schedule.html
* http://fab.academany.org/2019/labs/barcelona/local/

## Software used

- Eagle
- Rhino 5 and 6
- Grasshopper for Rhino
- Fab modules
