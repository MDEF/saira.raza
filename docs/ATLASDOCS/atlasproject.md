

# <center>Atlas of Weak Signals</center>

<center>![](assets/)</center>



## Anthopocene


### conflicts of anthopocene

- plastic glomorate - n beaches - minerals and plastic - stone - its just plastic. extractivism - it being ok to keep taking
PLASTIC BEACHES
PLASTIC ISLANDS
plastic is a product of oil
- access and ownership of water and sand
- food  - resource haevy and needs to be nutritious - fertilisers and transport and machinery - food insecurity
- refugees
- politics on a geological scale - - -long term decision making - like weve never done before
children need to sue adults
**generational accountability** and **future rights**
but theis needs education no?
worlds first commissioner for futur generations in wales. gret thunberg
- changing things on purpose like the iron in the sea to change acidity
like the film Sunshine
who gets to decide it - religious nuts.
- beyond anthropocism - beyoind hierarchy - the geometry of this . human emotions are not unique.
 water wars, animal extinction,
Donna Harraway - staying with the trouble
no species - acts alone

### this week
- climate conscience inter0species solidarity
- long termism
- carbon neutral lifestyles - what habits insentive ways of living
- fighting anthropocene conflicts - all the list above


 ## Fighting anthropocene conflicts

 future scenarios

 post from each weak


## Nano biological technology

### Context
In 2050
The temperature and acidy of the seas has risen.

Fortunately diatoms have mutated to adapt to the new conditions and their part in driving the biological pump of oceans and as resilient agents of carbon capture has become more apparent.

###
### Capturing atmospheric carbon dioxide





Biologists have selectively cultivated biofilms that thrive near wastewater treatment plants and process engineers have developed floating osmosis packs to treat waswater entering the sea.

Humans have started using diatoms and other sorts of algae to clean water of organic and inorganic waste whilst photosynthesising carbon dioxide into oxygen, oil and chitin.

Particular species have been genetically modified that ooze nutritious oils.

Living diatoms also enhance wet, living organic photovoltaic cells




Cities have begun to implement mass
Diatom Algae are the last group of algae to have evolved about 200 million years ago,
Diatoms are also easier to digest and hence are the main food for newly hatched fish and shrimp.

Unlike other algae Diatoms do not accumulate in the water as  they get eaten

10-20 degree c
https://books.google.es/books?id=UtuTFyk6pQ4C&pg=PA15&lpg=PA15&dq=diatom+water+treatment&source=bl&ots=F7Vl7URmki&sig=ACfU3U0etI7acIvOjuPJpRGeK-VMFigOZw&hl=en&sa=X&ved=2ahUKEwjs-u2wppHhAhVF4OAKHTnBAXYQ6AEwBnoECAkQAQ#v=onepage&q=diatom%20water%20treatment&f=false

nh4+ no3- important nutrients from water treatment plant effluent, septic tanks, farming combustion
organic degredation of organic matter by bacteria also
phosophorus

algae extract co2 from water and release oxygen into water

Currently, diatoms are present on a planetary scale and dominate the other marine phytoplankton species in cold and turbulent waters. A hotter and more stratified future, models predict an overall decline of these siliceous micro-algae (with the exception of the Southern Ocean), although the authors acknowledge adaptations of these species to climate change and ocean acidification may contradict these predictions.

Diatoms
have long been known to be abundant in turbulent, nutrient-rich waters, but observations and simulations indicate that they are dominant also in meso- and submesoscale structures such as fronts and filaments, and in the deep chlorophyll maximum.

Model simulations project a
decline in the contribution of diatoms to primary production everywhere outside of the Southern Ocean. We argue that we need
to understand changes in diatom diversity, life cycle and plankton interactions in a warmer and more acidic ocean in much more
detail to fully assess any changes in their contribution to the biological pump.

larger, highly silicified, fast sinking shapes with resting spores grow in limited nutrient and  light - this creates aging cells.
Hight stickiness, environmental streeor and high Transparent exopolymer particles (TEP)enable diatoms to aggregate and sink faster.
These with the presence of Large grazing zooplankton increase carbon transfer / export from CO2 in the water to cells inside large zooplankton

s we burn fossil fuels and atmospheric carbon dioxide levels go up, the ocean absorbs more carbon dioxide to stay in balance. But this absorption has a price: these reactions lower the water’s pH, meaning it’s more acidic. And the ocean has its limits. As temperatures rise, carbon dioxide leaks out of the ocean like a glass of root beer going flat on a warm day. Carbonate gets used up and has to be re-stocked by upwelling of deeper waters, which are rich in carbonate dissolved from limestone and other rocks.

In the center of the ocean, wind-driven currents bring cool waters and fresh carbonate to the surface. The new water takes up yet more carbon to match the atmosphere, while the old water carries the carbon it has captured into the ocean.


## Fluid mechanics of the sea - stratification and turbulence affects carbon dioxide upgtake into the ocean
https://earthobservatory.nasa.gov/features/OceanCarbon

The concentration of carbon dioxide (CO2) in ocean water (y axis) depends on the amount of CO2 in the atmosphere (shaded curves) and the temperature of the water (x axis). This simplified graph shows that as atmospheric CO2 increases from pre-industrial levels (blue) to double (2X) the pre-industrial amounts (light green), the ocean CO2 concentration increases as well. However, as water temperature increases, its ability dissolve CO2 decreases. Global warming is expected to reduce the ocean’s ability to absorb CO2, leaving more in the atmosphere…which will lead to even higher temperatures. (Graph by Robert Simmon.)

Diagram of carbon dioxide exchange in the ocean's mixed layer. In the short term, the ocean absorbs atmospheric carbon dioxide into the mixed layer, a thin layer of water with nearly uniform temperature, salinity, and dissolved gases. Wind-driven turbulence maintains the mixed layer by stirring the water near the ocean’s surface. Over the long term, carbon dioxide slowly enters the deep ocean at the bottom of the mixed layer as well in in regions near the poles where cold, salty water sinks to the ocean depths. (NASA illustration by Robert Simmon.)

The warmer the surface water becomes, the harder it is for winds to mix the surface layers with the deeper layers. The ocean settles into layers, or stratifies. Without an infusion of fresh carbonate-rich water from below, the surface water saturates with carbon dioxide. The stagnant water also supports fewer phytoplankton, and carbon dioxide uptake from photosynthesis slows. In short, stratification cuts down the amount of carbon the ocean can take up.

The ocean does not take up carbon uniformly. It breathes, inhaling and exhaling carbon dioxide.
