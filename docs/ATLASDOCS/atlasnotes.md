

# <center>Atlas of Weak Signals with Ramon</center>

<center>![](assets/)</center>


trends
megatrends
weak signals - their analysis often leads to wildcards
wild cards - black swans - change perceptions of things - 9/11

evaluation on quality of response


## life in times of surveillance capiltalism

2012 swansong of old capitalism

the foundational myth of the internet - highly
no permission
you can broadcast
no learning cureve
no money

i = tcp/ip (1970s protocol) +http (bernerslee way of finding things in a network through localisation address URLs _)

mosaic first browser in 1992
allowed anyone to set up a server and connect throuhg a url


structure was non hierarchical structure "small pieces loosely joined" by david weinberger

identity
separation of online identity to you

net neutrality
no priority of typres of information



**the finite nature of frequency for radio tv**

web 2.0

universities collaborating and sharing resources drove the development  of internet
1994 - home connection
1997 - platform development boost
2004 - different ways of coordination and bottom up participation
political, deesigning linux, wikipedia - new behaviours beginning of the invitation to contribute - youtube, flicker
our understanding of the thing changed

at this time...
> internet is a mythilogical creature
morozov --

andre staltz - the "web" structure began dying in 2014
became highly centralised

surveillance capitalism
the age of surveillance capitalism - shashana zuboff

making money from information

attention monopoly
data exytractivism
data markets
sell data and services
produce intelligence

a market of intelligence and data


### attention monopoly
hijacking new spaces and time to sell you stuff and use your time and data
like buttons making it easy to give data

creating a self reinforcement loop
notifications - a promise of something good - it makes you

"social validation feedback loop - exploitling vulnerability in human psychology"
- sean parker

infinite scroll !

pull to refresh  - it LOOKs like a mechanical action and sort of feels like it

data driven measurement of profit  - MUSIC

competing with the machine to stop stuff.

>"sleep is our competition"
Reed Hastings

creates
- clickbait
BUZZFEED
 but then doesnt the data become less meaningful
 fake news
  reach - how many of your friends see your news!

  extreme personalization
  shadow identity

  filter bubble - eli pariser

  the value of effort

**data extractivism**
  endless
  and endless appetitie


### THis weeks signals

**- attention protection**
- dismantling filter bubbles
- a circular data economy
- the truth wars
- redesign social


#### ideas

reclaiming your time and space - time well spent project

not needing to be liked





## Anthopocene

labour growth progress paradigm


mindset:
  - resources are entless
  - externalities are irrelevant

  geological impact of the atomic bomb

  impact of all people or organisms is not the same
  what if the pr

  TRADE made impacts bigger

  Culture v Nature is being challenged because we ARE nature - culture is sculping nature

  the great acelleration over the past 60 years:
  use of resources
  wars put loads of money into tech

  optimists are picking 11 billion on development as this causes fall of population
  female reights reduce population

  9 billion by 2050


1968 first picture of earth

the word growth was associated with progress not resources


the limites to growth by the club of rome

Bruno Latour - the globe is bigger than the planet

63% of co2 in the last 25 years  with thee explosion of neoliberalism

david wallace-wells - the uninhaitable earth

Co2 levesl
sea levels - YES- the artict preserve methane.
species extinction - 60% extinction since 1970 - the beginning of the 6th extinction


paris
limit temp rise to 2 degree by..
we are at 1.1 degrees

limit emissions to zero during second part of the century.
no more emissions
capturing more c02 out

deforesting
carbon sepostration

2020 - pledge for diferent countries -
EY 40% reduction by 2030


### conflicts of anthopocene

- **plastic glomorate** - n beaches - minerals and plastic - stone - its just plastic
PLASTIC BEACHES
PLASTIC ISLANDS
plastic is a product of oil
- access and ownership of water and sand
- food  - resource haevy and needs to be nutritious - fertilisers and transport and machinery - food insecurity
- refugees
- politics on a geological scal - - -long term decision making - like weve never done before
children need to sue adults
**generational accountability** and **future rights**
but theis needs education no?
worlds first commissioner for futur generations in wales
gret thunberg
- changing things on purpose like the iron in the sea to change acidity
like the film Sunshine
who gets to decide it - religious nuts.
- beyond anthropocism - beyoind hierarchy - the geometry of this . human emotions are not unique.
 water wars, animal extinction,
Donna Harraway - staying with the trouble
no species - acts alone

### this weeks signals

- climate conscience
- inter species solidarity
**- long termism**
**- carbon neutral lifestyles** - what habits insentive ways of living
- fighting anthropocene conflicts - all the list above

 **extractivism** - it being ok to keep taking



 ### fighting anthropocene conflicts

 future scenarios

 post from each weak.





## Life after AI and the end of work

### labour
experitise to make money


### capital

money made without work
money to make money

people with means of production


IMF data
capiatl has become more important and labour less

housing plays a part in making capital more important.

more and more people

the more capital you have the easier it is to make money



2015 - 99% have the same wealth as the 1%



* information companies dont need as many people - not work that distrtibute money throughout society

* the gig economy informal employment through app - job per job booking, no insurance , no contract, scraps of a system - fast reaction to supply and demand basis

* automation - not many well paid jobs - amazon

### ideas during the lecture

fluid mechanics models to control drones

most common jos

2013 the future of employment study on jobs that most likely to be utomated/

## life after work
john maynard keyenes

work might become a scarce resource

3 hours a day 15 hour weeks


 4 day week

 work-life boundaries physically different


geometry>automation>roles


the useless class - harare
fewer people will design the algorithms -


 - the luddites
workers against automation - for being left out.

human desire - of finding things we want to do
like yoga teachers - we will find new things to need


old jobs are usually low skill jobs and incoming jobs are high skill.

ai

## post work society

decouple wealth with how you make a living


## universal basic income

you get paid because you are alive


robo tax


b-mincome - 2000 families in low rent low skill  in barcelona

finland 200 people got 600 euro a month

**fully automated luxury communism**
what if we all owned the robots


## what will people do?

* renaissance of creativity - everyone is an artist or a maker - go back to craftmanship
* the care economy
* couch potato
 * AI as a slave
 alexa is a snoop
 uses data which is un giverned

 we are working for alexa

 algorithic bias

- too creative for ai
gereative algoriths

it feels like pop music is already written by an ai


- creative cooperation in art, instruments,
GAN networks
its putting out of our control some parameters - "randomness"
holly herndon

### Weak signals this week are:

* **technology for euqality**
* fighting ai bias
* imagining new jobs
* making UBI work - how to not become couch potatoes
* human machine creative collborations.




## idenity

### kill the heteropatriarchy
addressing priviledge and bias


### decoding sexuality
non hetero


representing races and cultures, philosophies in narratives
like:
margaret atwood
ursula k le guin
Octavia Butler
it needs to be authentic though


invisible women - caroline perez

------------#

 <!---Surveillance Capitalism
We heard about how the internet, what we used it for and the use of the data we put into it has developed. Among the weak signals of where this information ecosystem is going, we explored in groups: attention protection, dismantling filter bubbles, a circular data economy, truth wars and the redesign of social media.
### Attention protection
As a group we examined how attention is becoming a precious resource individually and as societies and countries.
We imagined a world where attention was maximised by allowing people to work in optimised environments in their homes. Social meetings are controlled by a centralised system connected to sensors embedded into our bodies. Advertising is controlled in a way as to
## The Anthropocene
## Work
## End of the Nation State
## Identity -->
