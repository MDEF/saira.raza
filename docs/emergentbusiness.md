
# <center>Emergent Business Models</center>

<center>![](assets/)</center>

In this module with Ideas for Change we were looking again at impact but by considering system of existing organisations and innovations. we were asked to answer some key questions to perform an analysis

### Pinning down the goal

#### What do I want to change?

A paradigm change in material science and the material world is happening and we should can play a part in its development. We need more accessible knowledge and tools for Digital fabrication and AI and ways to engage in designing new forms of materials.

#### Which system/s does your innovation belong to?

Research and design collaborations (Human-human and human-non-human)

#### What variable of the system do you want to change/impact?

Representation in the development of technologies


## Analyse previous innovations

The innovations I chose were:

- Cheap/ free design/development interfaces like apps - a product
- Online journals, magazines and documentation to share experiences and data -  aknowledge infrastructure
- Fab Labs - a movement and capacity building innovation

The most important enablers of these were:
- coding languages, open source tools and knowledge, many people having a smart phone
- A shared focus, language and method of inquiry
- buy-in / Investment into fab labs

And adjacent systems I was able to identify from these were:

- desktop software, hardware design, retail, integration with other types of software
- universities, cultural organisations and spaces
- development of digital fabrication tools and materials

## Applying lessons from other innovations

The essential element of my innovation is:

- Focus and buy in from a wide range of people

Can you use key elements that previous innovations changed or is there something else?

- I felt that permission and space to make and share experiences of making is key to getting wider involvement and therefore representation in the development of technology

What internal or external efforts could you minimize?

- get buy in to make existing design software freely available. - - ---- Investment, development and promotion of open source tools.
- Sharing of results to promote buy-in investment.

I found that it was refreshing to look at my idea from this pragmatic point of view.

This exercise also forced me to commit to an impact. This is quite hard to do when you are taking an embodied design approach to make something.

For the next exercise we tried to think of our Impact in terms of a story or testimonial. Here are the results:

![](assets/Newspaper.jpg)


I really appreciated the outlook we were given to think in terms of changes that existing systems can make. It feels bold to suggest these without a fleshed out idea, especially when you are trying to be grounded in research, learning new skills and being speculative all at the same time.

I tend to keep direction open until I feel I have enough knowledge to and drive to connect them to things that I can imagine myself doing.

Being forced to pick an impact could lead you into all sorts of field and skills needed. But its an important step to map out if you are really driven to make impact.

The drive of my project and attending this course was to trying doing new things, learn new skills and to use my imagination towards a cluster of issues I would like to address.

Inclusion and representation underlies all the issues I am targeting in some way because I believe that in open systems good ideas gain traction fast and I believe that far more people want sustainable futures than to cause irreparable damage.




<!--

## Minimise internal efforts/ Maximise external efforts

* Focus on essential product feature
  * Identify the most essential feature
    * migrate ideas from other industries
  * look at perceptions about a thing and work with it rather than against it.


* empower users




1. Define your impact variable
Describe






2. analysis previous  in previous innovations
3. refine your idea.


leveraging the system


## session 2

the field of possibilities
elements
rules
agents - the more people you have thinking the more solutions you can generate.


from hierarchy to netwroking  - -

what does a technology make abundant and what does it make sparse

you can leverage these

data gathering and analysis - collaborative platforms
opendata

distributing trust
- transparent
-inclusive
-decentralised
-accountable
-bottom up

cultures of disributed trust


confidence in opening up your project

trust in the common goal - commons



technological possibilitieS:
change elements: roles,trust
change rules: open, networked not hierarchival


agents:


ai morality
a dialogue with ai

it is not pasive so we HAVE to send it data


leveraging capabilities -
upskilling to meet new demmands
pentagrowth analysis
connect networks - social, mobile and most of all thnings
sharing knowledge
collect inventory
empower users
enable partners
share knowledge


traditional businesses work on vertical integration - aiming for monopoly

connecting - to ineractions of value

more users also help growth
pooling and testing ideas
making partners
and ecosystems

diverse businesses working together grow faster together
you get buy in by enabling busjinesses - sharing market
intel had to make a chip compatible with arduino - so making your thing embeddabke makes it more useful makes

knowledge that isnt used dies quickly - protecting knowledge is costlyreviews

even papers cant be published fast enough

self sustaining knowledge systems  - volunteered donations

maker designs

engaged knowledge communities can operate outside of copyright

reinvent a business from a new network
piggybacking on other networks
like
ambient internet
pervasive? ai

WHAT IS ALREADY AVAILABLE leveraging other networks

- making fablabs more inclusive
- less expensive
-  turning people into nodes

compatibility - pigybacking to start

tesla only has patents on electrical parts

let others produce the parts of a new system



how will you project GROW

a network of material needs
