# Interests

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta property="og:image" content="http://www.music-map.com/elements/objects/og_logo.png">
 <link rel="stylesheet" href="/elements/objects/styles_7_1.css">

<script src="https://pagead2.googlesyndication.com/pagead/js/r20181107/r20100101/osd.js"></script><script src="https://pagead2.googlesyndication.com/pub-config/r20160913/ca-pub-4786778854163211.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
    // Fix for Firefox autofocus CSS bug
    // See: http://stackoverflow.com/questions/18943276/html-5-autofocus-messes-up-css-loading/18945951#18945951
</script>

 <title>Music like The Beatles - Similar Bands and Artists</title>

<style>#s0:hover { background-color: rgb(239,209,202); z-index: 100; } #s1:hover { background-color: rgb(239,209,202); z-index: 100; } #s2:hover { background-color: rgb(229,212,224); z-index: 100; } #s3:hover { background-color: rgb(219,202,199); z-index: 100; } #s4:hover { background-color: rgb(239,209,202); z-index: 100; } #s5:hover { background-color: rgb(239,209,202); z-index: 100; } #s6:hover { background-color: rgb(194,227,194); z-index: 100; } #s7:hover { background-color: rgb(232,242,202); z-index: 100; } #s8:hover { background-color: rgb(234,192,199); z-index: 100; } #s9:hover { background-color: rgb(214,212,222); z-index: 100; } #s10:hover { background-color: rgb(204,234,202); z-index: 100; } #s11:hover { background-color: rgb(237,212,222); z-index: 100; } #s12:hover { background-color: rgb(224,202,212); z-index: 100; } #s13:hover { background-color: rgb(197,234,202); z-index: 100; } #s14:hover { background-color: rgb(229,192,242); z-index: 100; } #s15:hover { background-color: rgb(197,227,219); z-index: 100; } #s16:hover { background-color: rgb(227,192,237); z-index: 100; } #s17:hover { background-color: rgb(234,202,199); z-index: 100; } #s18:hover { background-color: rgb(224,212,234); z-index: 100; } #s19:hover { background-color: rgb(239,209,202); z-index: 100; } #s20:hover { background-color: rgb(199,192,244); z-index: 100; } #s21:hover { background-color: rgb(239,209,202); z-index: 100; } #s22:hover { background-color: rgb(239,209,202); z-index: 100; } #s23:hover { background-color: rgb(239,192,222); z-index: 100; } #s24:hover { background-color: rgb(239,209,202); z-index: 100; } #s25:hover { background-color: rgb(222,242,237); z-index: 100; } #s26:hover { background-color: rgb(239,209,202); z-index: 100; } #s27:hover { background-color: rgb(192,197,146); z-index: 100; } #s28:hover { background-color: rgb(207,234,202); z-index: 100; } #s29:hover { background-color: rgb(239,209,202); z-index: 100; } #s30:hover { background-color: rgb(194,212,219); z-index: 100; } #s31:hover { background-color: rgb(239,209,202); z-index: 100; } #s32:hover { background-color: rgb(202,219,202); z-index: 100; } #s33:hover { background-color: rgb(242,154,192); z-index: 100; } #s34:hover { background-color: rgb(192,202,234); z-index: 100; } #s35:hover { background-color: rgb(222,242,222); z-index: 100; } #s36:hover { background-color: rgb(239,209,202); z-index: 100; } #s37:hover { background-color: rgb(222,202,239); z-index: 100; } #s38:hover { background-color: rgb(207,242,224); z-index: 100; } #s39:hover { background-color: rgb(204,219,202); z-index: 100; } #s40:hover { background-color: rgb(207,227,234); z-index: 100; } #s41:hover { background-color: rgb(202,219,239); z-index: 100; } #s42:hover { background-color: rgb(204,227,227); z-index: 100; } #s43:hover { background-color: rgb(222,192,234); z-index: 100; } #s44:hover { background-color: rgb(214,227,242); z-index: 100; } #s45:hover { background-color: rgb(199,192,204); z-index: 100; } #s46:hover { background-color: rgb(192,194,194); z-index: 100; } #s47:hover { background-color: rgb(194,227,224); z-index: 100; } #s48:hover { background-color: rgb(194,227,194); z-index: 100; } </style><link rel="preload" href="https://adservice.google.es/adsid/integrator.js?domain=www.music-map.com" as="script"><script type="text/javascript" src="https://adservice.google.es/adsid/integrator.js?domain=www.music-map.com"></script><link rel="preload" href="https://adservice.google.com/adsid/integrator.js?domain=www.music-map.com" as="script"><script type="text/javascript" src="https://adservice.google.com/adsid/integrator.js?domain=www.music-map.com"></script><link rel="preload" href="https://pagead2.googlesyndication.com/pagead/js/r20181107/r20180604/show_ads_impl.js" as="script"></head>

<body style="">
<table class="universe">
 <tbody><tr class="heaven">
  <td class="headline">

<table style="width: 100%;"><tbody><tr><td>
   <a class="project" href="/">Music-Map</a><span id="the_title" class="the_title" style="background-color: rgb(239, 209, 202);">The Beatles</span>
</td><td style="text-align: right">
   <a class="questionmark" href="/info.php"><span style="font-size: 26px">?</span></a>
</td></tr></tbody></table>

  </td>
  </tr><tr>
   <td class="content">

<link rel="stylesheet" href="/elements/objects/relator.css">

<div class="map_info">
 <div class="map_info_text"><p>People who like The Beatles might also like these artists.</p><p>The closer two names are, the greater the probability people will like both artists.</p><p>Click on any name to travel along.</p></div>
 <div class="advert"><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Map Responsive --><ins class="adsbygoogle" style="display: block; height: 600px;" data-ad-client="ca-pub-4786778854163211" data-ad-slot="6623484194" data-ad-format="auto" data-adsbygoogle-status="done"><ins id="aswift_0_expand" style="display:inline-table;border:none;height:600px;margin:0;padding:0;position:relative;visibility:visible;width:200px;background-color:transparent;"><ins id="aswift_0_anchor" style="display:block;border:none;height:600px;margin:0;padding:0;position:relative;visibility:visible;width:200px;background-color:transparent;"><iframe width="200" height="600" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;width:200px;height:600px;"></iframe></ins></ins></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>
</div>

<div id="gnodMap">
 <a href="http://www.gnoosic.com/discussion/the+beatles.html" class="S" id="s0" style="display: block; left: 229.5px; top: 323px;">The Beatles</a>
<a href="the+rolling+stones.html" class="S" id="s1" style="display: block; left: 199.265px; top: 358.267px;">The Rolling Stones</a>
<a href="pink+floyd.html" class="S" id="s2" style="display: block; left: 143.913px; top: 303.941px;">Pink Floyd</a>
<a href="led+zeppelin.html" class="S" id="s3" style="display: block; left: 145.009px; top: 339.224px;">Led Zeppelin</a>
<a href="the+doors.html" class="S" id="s4" style="display: block; left: 61.081px; top: 331.391px;">The Doors</a>
<a href="the+who.html" class="S" id="s5" style="display: block; left: 179.52px; top: 382.019px;">The Who</a>
<a href="bob+dylan.html" class="S" id="s6" style="display: block; left: 158.469px; top: 407.724px;">Bob Dylan</a>
<a href="queen.html" class="S" id="s7" style="display: block; left: 340.865px; top: 378.057px;">Queen</a>
<a href="radiohead.html" class="S" id="s8" style="display: block; left: 197.922px; top: 252.848px;">Radiohead</a>
<a href="jimi+hendrix.html" class="S" id="s9" style="display: block; left: 81.6148px; top: 378.79px;">Jimi Hendrix</a>
<a href="freddie+mercury.html" class="S" id="s10" style="display: block; left: 32.5024px; top: 406.641px;">Freddie Mercury</a>
<a href="simon+-26+garfunkel.html" class="S" id="s11" style="display: block; left: 77.7296px; top: 434.476px;">Simon &amp; Garfunkel</a>
<a href="neil+young.html" class="S" id="s12" style="display: block; left: 91.3839px; top: 489.425px;">Neil Young</a>
<a href="creedence+clearwater+revival.html" class="S" id="s13" style="display: block; left: 43.3793px; top: 461.789px;">Creedence Clearwater Revival</a>
<a href="paul+simon.html" class="S" id="s14" style="display: block; left: 60.0732px; top: 517.333px;">Paul Simon</a>
<a href="coldplay.html" class="S" id="s15" style="display: block; left: 412.475px; top: 124.15px;">Coldplay</a>
<a href="oasis.html" class="S" id="s16" style="display: block; left: 393.217px; top: 152.084px;">Oasis</a>
<a href="red+hot+chili+peppers.html" class="S" id="s17" style="display: block; left: 308.166px; top: 254.914px;">Red Hot Chili Peppers</a>
<a href="nirvana.html" class="S" id="s18" style="display: block; left: 361.649px; top: 226.95px;">Nirvana</a>
<a href="the+beach+boys.html" class="S" id="s19" style="display: block; left: 334.574px; top: 646px;">The Beach Boys</a>
<a href="david+bowie.html" class="S" id="s20" style="display: block; left: 289.194px; top: 551.908px;">David Bowie</a>
<a href="the+black+keys.html" class="S" id="s21" style="display: block; left: 298.713px; top: 180.07px;">The Black Keys</a>
<a href="the+strokes.html" class="S" id="s22" style="display: block; left: 302.634px; top: 35.9586px;">The Strokes</a>
<a href="tame+impala.html" class="S" id="s23" style="display: block; left: 127.58px; top: 110.842px;">Tame Impala</a>
<a href="the+killers.html" class="S" id="s24" style="display: block; left: 352.68px; top: 85.6717px;">The Killers</a>
<a href="muse.html" class="S" id="s25" style="display: block; left: 359.502px; top: 113.666px;">Muse</a>
<a href="the+eagles.html" class="S" id="s26" style="display: block; left: 347.711px; top: 618.45px;">The Eagles</a>
<a href="ac-2fdc.html" class="S" id="s27" style="display: block; left: 429.19px; top: 445.641px;">Ac/dc</a>
<a href="green+day.html" class="S" id="s28" style="display: block; left: 466px; top: 226.492px;">Green Day</a>
<a href="the+kinks.html" class="S" id="s29" style="display: block; left: 1.6582px; top: 137.487px;">The Kinks</a>
<a href="billy+joel.html" class="S" id="s30" style="display: none;">Billy Joel</a>
<a href="the+white+stripes.html" class="S" id="s31" style="display: none;">The White Stripes</a>
<a href="electric+light+orchestra.html" class="S" id="s32" style="display: none;">Electric Light Orchestra</a>
<a href="u2.html" class="S" id="s33" style="display: none;">U2</a>
<a href="aerosmith.html" class="S" id="s34" style="display: none;">Aerosmith</a>
<a href="mumford+-26+sons.html" class="S" id="s35" style="display: none;">Mumford &amp; Sons</a>
<a href="the+smiths.html" class="S" id="s36" style="display: none;">The Smiths</a>
<a href="metallica.html" class="S" id="s37" style="display: none;">Metallica</a>
<a href="guns-27n-27roses.html" class="S" id="s38" style="display: none;">Guns'N'Roses</a>
<a href="fleetwood+mac.html" class="S" id="s39" style="display: none;">Fleetwood Mac</a>
<a href="gorillaz.html" class="S" id="s40" style="display: none;">Gorillaz</a>
<a href="elton+john.html" class="S" id="s41" style="display: none;">Elton John</a>
<a href="foo+fighters.html" class="S" id="s42" style="display: none;">Foo Fighters</a>
<a href="maroon+5.html" class="S" id="s43" style="display: none;">Maroon 5</a>
<a href="journey.html" class="S" id="s44" style="display: none;">Journey</a>
<a href="daft+punk.html" class="S" id="s45" style="display: none;">Daft Punk</a>
<a href="abba.html" class="S" id="s46" style="display: none;">Abba</a>
<a href="bon+jovi.html" class="S" id="s47" style="display: none;">Bon Jovi</a>
<a href="bob+marley.html" class="S" id="s48" style="display: none;">Bob Marley</a>

</div>

<script>var NrWords=49;var Aid=new Array();Aid[0]=new Array(-1,4.68372,4.61588,4.41416,3.96962,3.45449,3.38198,3.03396,2.71984,2.65559,2.44776,2.20728,1.94559,1.75071,1.66476,1.49933,1.4578,1.33617,1.2843,1.22544,1.14284,1.00931,0.963153,0.945814,0.935178,0.931786,0.908705,0.905821,0.838991,0.837137,0.794347,0.783718,0.745575,0.744774,0.739726,0.707738,0.678263,0.66301,0.659616,0.627984,0.622968,0.621769,0.614251,0.595991,0.564858,0.558605,0.550121,0.539731,0.539642);Aid[1]=new Array(4.68372,-1,3.50329,4.34869,4.82865,5.14312,4.35523,1.30194,1.64562,4.06592,4.17839,2.85343,3.36066,3.05219,2.78171,0.336864,0.568909,0.79459,0.503978,0.351405,0.878313,0.954711,0.416663,-1,0.328649,0.304996,0.641867,1.14572,0.276582,0.644687,0.411359,0.575532,0.203015,0.662032,0.77196,-1,0.210884,0.387171,0.761182,0.527703,0.203652,0.37062,0.301274,0.153604,0.375221,0.192033,0.152738,0.447016,0.32033);Aid[2]=new Array(4.61588,3.50329,-1,6.77716,4.95251,3.68903,3.17001,2.06423,3.54047,3.13363,2.93247,1.76174,2.19857,1.57743,1.56615,0.971468,0.575014,1.3715,1.2253,0.151274,0.971335,0.649198,0.344212,1.43311,0.353795,1.04623,0.667372,1.03149,0.401658,0.172641,0.340678,0.385331,0.508262,0.730667,0.567655,-1,0.32067,1.53955,0.908527,0.479528,0.78951,0.310682,0.509388,0.142279,0.334766,0.680156,0.137679,0.38867,0.501633);Aid[3]=new Array(4.41416,4.34869,6.77716,-1,5.1301,4.23727,3.38443,1.97731,2.45142,3.93808,3.20404,1.87927,2.4108,2.03165,1.75096,0.454651,0.462217,1.56187,1.31929,0.142413,0.590824,1.52342,0.546333,-1,0.309807,0.718771,0.586245,1.97822,0.361028,0.240814,0.44951,1.03647,0.274156,0.492052,1.05639,-1,0.216365,1.5836,1.37394,0.462119,0.429406,0.381773,0.902107,0.103488,0.549381,0.359336,0.0866835,0.582138,0.421742);Aid[4]=new Array(3.96962,4.82865,4.95251,5.1301,-1,5.39296,4.55296,0.84682,2.15413,4.55508,4.6839,2.87986,3.4066,2.82498,2.69687,0.23807,0.302077,0.731386,0.931571,0.184654,0.598315,0.67515,0.404278,-1,0.277983,0.382019,0.23449,0.482986,0.149024,0.4358,0.153983,0.470258,0.160933,0.28729,0.277229,0.142342,0.486264,0.56157,0.37846,0.270285,0.339058,0.131782,0.187821,0.0550301,0.141614,0.223019,0.0756237,0.166777,0.458357);Aid[5]=new Array(3.45449,5.14312,3.68903,4.23727,5.39296,-1,4.49133,0.847263,1.72662,4.36767,5.29696,3.2548,3.80701,3.05081,3.30189,0.166986,0.368048,0.39825,0.308645,0.28536,0.396718,0.280399,0.147584,-1,0.188736,0.243734,0.291859,0.48978,0.188466,0.635024,0.258846,0.336961,0.284115,0.318305,0.307025,-1,0.14231,0.267997,0.242806,0.218274,0.148533,0.203985,0.231661,0.0392909,0.24383,0.0720301,0.0809903,0.208541,0.116221);Aid[6]=new Array(3.38198,4.35523,3.17001,3.38443,4.55296,4.49133,-1,0.500672,1.85517,3.88264,4.32511,3.25244,4.56492,2.72912,3.24877,0.209262,0.251485,0.426332,0.413852,0.298372,0.758776,0.411279,0.229419,0.147892,0.144222,0.165216,0.216719,0.21411,0.14234,0.350267,0.276931,0.311322,0.131226,0.30246,0.126667,-1,0.310003,0.245021,0.157265,0.343497,0.111111,0.214897,0.127698,0.0335718,0.0990332,0.0932887,0.0621654,0.128259,0.768269);Aid[7]=new Array(3.03396,1.30194,2.06423,1.97731,0.84682,0.847263,0.500672,-1,0.552426,0.447854,0.540256,-1,0.209208,0.509158,0.254818,1.23676,0.803274,1.0702,0.89811,0.38076,1.70404,-1,0.26767,-1,-1,1.61694,0.715069,5.81141,1.04178,-1,3.36473,0.345115,1.28051,0.980009,5.03974,-1,0.269931,1.44653,4.94267,0.53544,-1,2.66955,0.806822,0.733572,3.1794,0.856019,1.1269,4.4395,0.301003);Aid[8]=new Array(2.71984,1.64562,3.54047,2.45142,2.15413,1.72662,1.85517,0.552426,-1,1.57931,1.54092,0.965553,1.36037,0.771787,0.989717,1.94162,1.31011,1.54016,1.55761,0.118024,0.732278,0.886338,1.28592,1.31167,1.10287,3.44583,0.051302,0.133194,0.278928,0.0852089,0.0605099,0.906431,0.0731194,0.699239,0.0895193,-1,1.02427,0.522546,0.144787,0.1395,1.22383,0.0565404,0.580391,0.118836,0.0327965,0.713525,-1,0.0606393,0.154099);Aid[9]=new Array(2.65559,4.06592,3.13363,3.93808,4.55508,4.36767,3.88264,0.447854,1.57931,-1,4.4363,3.39718,3.66885,3.26159,3.25134,0.0843134,0.162504,0.805271,0.598676,0.0893122,0.320058,-1,0.179374,-1,-1,0.171463,0.155576,0.387859,0.106979,0.196668,0.107936,0.405814,0.103233,0.123602,0.190979,-1,0.0949019,0.364481,0.279379,0.182584,0.180035,0.0962473,0.215141,-1,0.108356,0.147109,-1,0.110589,0.631154);Aid[10]=new Array(2.44776,4.17839,2.93247,3.20404,4.6839,5.29696,4.32511,0.540256,1.54092,4.4363,-1,3.92417,4.18721,3.2553,4.0333,0.0661326,0.124158,0.248423,0.2093,0.0488601,0.141205,0.0424242,0.0240074,-1,-1,0.144007,0.050882,0.102178,0.0665118,0.0330642,0.0536725,0.0410974,0.0168644,0.0671692,0.0590915,-1,0.0209757,0.207681,0.0483986,0.0417322,0.0514884,0.0667337,0.0458549,0.023864,0.0279827,0.0284703,0.0358412,0.0537289,0.062787);Aid[11]=new Array(2.20728,2.85343,1.76174,1.87927,2.87986,3.2548,3.25244,-1,0.965553,3.39718,3.92417,-1,3.67721,3.63374,4.65287,0.193631,0.163488,0.20198,0.140977,0.616015,0.36476,-1,0.0790771,-1,-1,-1,0.361118,-1,-1,-1,0.523975,-1,-1,0.216396,-1,-1,0.231259,0.144771,0.0732244,0.751641,-1,0.369283,0.0712271,-1,-1,0.0670482,0.462266,0.103934,0.168201);Aid[12]=new Array(1.94559,3.36066,2.19857,2.4108,3.4066,3.80701,4.56492,0.209208,1.36037,3.66885,4.18721,3.67721,-1,3.08312,4.10839,0.0939581,0.152583,0.263599,0.26751,0.150062,0.400966,-1,0.10513,0.122701,-1,0.0779658,0.28855,0.108407,-1,0.159784,0.12159,0.168026,0.0917706,0.169997,0.0632959,-1,0.165511,0.155557,0.083628,0.319057,0.06361,0.13757,0.0938965,-1,-1,-1,-1,-1,0.168896);Aid[13]=new Array(1.75071,3.05219,1.57743,2.03165,2.82498,3.05081,2.72912,0.509158,0.771787,3.26159,3.2553,3.63374,3.08312,-1,3.18167,0.120063,0.110663,0.270442,0.162783,0.321137,0.211055,-1,0.099648,-1,-1,0.0978286,1.04319,0.593163,0.116133,0.397263,0.431949,0.154845,0.420897,0.123392,0.384688,-1,0.0480775,0.17751,0.371993,0.686886,-1,0.362571,0.190483,-1,0.432617,0.106049,-1,0.298831,0.211861);Aid[14]=new Array(1.66476,2.78171,1.56615,1.75096,2.69687,3.30189,3.24877,0.254818,0.989717,3.25134,4.0333,4.65287,4.10839,3.18167,-1,0.0530848,0.0883149,0.130849,0.106244,0.216176,0.178824,-1,0.0386411,-1,-1,0.0598075,0.147168,0.0564267,0.0311953,0.145529,0.288361,0.0441699,0.135085,0.0958987,0.0499699,-1,0.061027,0.0768985,0.0353848,0.418946,0.0623247,0.162959,0.0479335,0.0251033,-1,-1,0.0675535,0.0366791,0.126902);Aid[15]=new Array(1.49933,0.336864,0.971468,0.454651,0.23807,0.166986,0.209262,1.23676,1.94162,0.0843134,0.0661326,0.193631,0.0939581,0.120063,0.0530848,-1,5.21831,1.72658,0.587423,0.0786598,0.174437,-1,0.769013,0.271387,4.01806,3.9304,0.302153,0.493745,1.35857,-1,0.204072,0.339767,-1,3.76164,0.332105,-1,0.252961,0.44583,0.455177,-1,0.664214,0.174097,0.854253,4.99709,0.215297,0.807636,0.215599,0.335223,0.237552);Aid[16]=new Array(1.4578,0.568909,0.575014,0.462217,0.302077,0.368048,0.251485,0.803274,1.31011,0.162504,0.124158,0.163488,0.152583,0.110663,0.0883149,5.21831,-1,1.40624,0.907308,0.110669,0.287059,-1,1.15824,0.296163,3.2063,2.71773,0.154789,-1,0.881226,-1,-1,0.546278,-1,2.59604,0.341278,-1,0.855826,0.328078,0.487756,0.215723,0.394929,0.146811,0.880591,0.995844,0.134062,0.246378,0.0816129,0.330099,0.129381);Aid[17]=new Array(1.33617,0.79459,1.3715,1.56187,0.731386,0.39825,0.426332,1.0702,1.54016,0.805271,0.248423,0.20198,0.263599,0.270442,0.130849,1.72658,1.40624,-1,5.29287,0.0985411,0.280451,-1,1.04119,-1,1.39966,1.99704,0.319789,0.885741,1.6457,0.0860621,0.233441,0.908819,0.126506,0.991872,0.555247,-1,0.25044,2.47493,0.846854,0.285714,1.42148,0.178049,3.13936,0.584103,0.233469,0.771954,0.0812022,0.41519,0.711664);Aid[18]=new Array(1.2843,0.503978,1.2253,1.31929,0.931571,0.308645,0.413852,0.89811,1.55761,0.598676,0.2093,0.140977,0.26751,0.162783,0.106244,0.587423,0.907308,5.29287,-1,0.0750599,0.401838,-1,0.576497,-1,0.463887,0.981681,0.110829,0.907275,1.72955,0.10297,0.168031,1.00543,0.0428672,0.502511,0.5264,-1,0.49123,3.02444,1.07463,0.184276,0.716106,0.156434,2.3413,-1,0.19007,0.27832,-1,0.403575,0.35831);Aid[19]=new Array(1.22544,0.351405,0.151274,0.142413,0.184654,0.28536,0.298372,0.38076,0.118024,0.0893122,0.0488601,0.616015,0.150062,0.321137,0.216176,0.0786598,0.110669,0.0985411,0.0750599,-1,-1,-1,0.194201,-1,0.104476,0.0460476,-1,-1,0.0864324,1.27082,0.466625,0.133901,-1,0.0873803,-1,-1,0.229538,0.0273647,0.0556552,0.453213,-1,0.327247,-1,0.0919199,-1,0.128736,0.666515,-1,-1);Aid[20]=new Array(1.14284,0.878313,0.971335,0.590824,0.598315,0.396718,0.758776,1.70404,0.732278,0.320058,0.141205,0.36476,0.400966,0.211055,0.178824,0.174437,0.287059,0.280451,0.401838,-1,-1,-1,0.425424,-1,0.357139,0.379155,-1,0.270101,0.189737,0.731447,0.406158,0.406476,1.08885,0.340685,0.198026,-1,1.29837,0.131368,0.209192,0.592131,-1,0.50787,0.211065,-1,0.118632,0.526661,0.325726,0.162671,0.170084);Aid[21]=new Array(1.00931,0.954711,0.649198,1.52342,0.67515,0.280399,0.411279,-1,0.886338,-1,0.0424242,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,5.98979,1.81294,1.9782,-1,-1,0.556191,-1,-1,-1,9.74628,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);Aid[22]=new Array(0.963153,0.416663,0.344212,0.546333,0.404278,0.147584,0.229419,0.26767,1.28592,0.179374,0.0240074,0.0790771,0.10513,0.099648,0.0386411,0.769013,1.15824,1.04119,0.576497,0.194201,0.425424,5.98979,-1,1.6965,3.11912,2.39797,-1,0.131293,0.357666,0.22853,-1,6.72022,-1,0.144201,0.0541635,-1,0.907871,0.119555,0.098938,-1,0.85382,-1,0.666481,-1,-1,0.575546,-1,-1,0.116251);Aid[23]=new Array(0.945814,-1,1.43311,-1,-1,-1,0.147892,-1,1.31167,-1,-1,-1,0.122701,-1,-1,0.271387,0.296163,-1,-1,-1,-1,1.81294,1.6965,-1,-1,-1,-1,0.0493472,-1,-1,-1,-1,-1,-1,-1,0.168933,0.590457,-1,-1,-1,1.62379,-1,-1,0.0448381,-1,-1,-1,-1,-1);Aid[24]=new Array(0.935178,0.328649,0.353795,0.309807,0.277983,0.188736,0.144222,-1,1.10287,-1,-1,-1,-1,-1,-1,4.01806,3.2063,1.39966,0.463887,0.104476,0.357139,1.9782,3.11912,-1,-1,6.00438,0.139489,0.255235,1.1238,0.14536,-1,1.31556,-1,1.24257,0.185028,1.34864,0.441276,0.227747,0.228344,-1,0.744551,-1,1.14817,0.962993,-1,0.43503,-1,0.253454,0.107632);Aid[25]=new Array(0.931786,0.304996,1.04623,0.718771,0.382019,0.243734,0.165216,1.61694,3.44583,0.171463,0.144007,-1,0.0779658,0.0978286,0.0598075,3.9304,2.71773,1.99704,0.981681,0.0460476,0.379155,-1,2.39797,-1,6.00438,-1,-1,-1,1.38192,-1,0.149685,1.68748,-1,1.04809,0.318862,-1,0.292265,0.974404,0.462184,-1,1.15006,0.117625,1.99624,0.746244,-1,1.01902,0.126017,0.30253,0.087525);Aid[26]=new Array(0.908705,0.641867,0.667372,0.586245,0.23449,0.291859,0.216719,0.715069,0.051302,0.155576,0.050882,0.361118,0.28855,1.04319,0.147168,0.302153,0.154789,0.319789,0.110829,-1,-1,-1,-1,-1,0.139489,-1,-1,0.472937,-1,0.168259,0.760754,0.0684994,0.7513,0.362755,0.525785,-1,0.0399858,0.2222,0.380505,2.41039,-1,0.573589,0.24897,-1,2.43242,-1,0.60994,0.387245,0.19648);Aid[27]=new Array(0.905821,1.14572,1.03149,1.97822,0.482986,0.48978,0.21411,5.81141,0.133194,0.387859,0.102178,-1,0.108407,0.593163,0.0564267,0.493745,-1,0.885741,0.907275,-1,0.270101,0.556191,0.131293,0.0493472,0.255235,-1,0.472937,-1,0.899461,0.104571,2.90855,0.382773,-1,0.43631,6.4046,-1,-1,2.95739,7.66191,0.25403,-1,2.40921,0.836028,-1,3.44533,0.407583,0.289088,5.269,0.281436);Aid[28]=new Array(0.838991,0.276582,0.401658,0.361028,0.149024,0.188466,0.14234,1.04178,0.278928,0.106979,0.0665118,-1,-1,0.116133,0.0311953,1.35857,0.881226,1.6457,1.72955,0.0864324,0.189737,-1,0.357666,-1,1.1238,1.38192,-1,0.899461,-1,0.0828435,0.265755,0.43786,-1,0.476425,0.438923,-1,0.125338,0.849788,0.844527,0.130646,0.809142,0.135006,1.56292,0.891148,0.228594,0.306296,-1,0.540019,0.13091);Aid[29]=new Array(0.837137,0.644687,0.172641,0.240814,0.4358,0.635024,0.350267,-1,0.0852089,0.196668,0.0330642,-1,0.159784,0.397263,0.145529,-1,-1,0.0860621,0.10297,1.27082,0.731447,-1,0.22853,-1,0.14536,-1,0.168259,0.104571,0.0828435,-1,-1,0.248484,-1,0.0584535,-1,-1,0.321217,-1,-1,-1,-1,-1,0.0592675,-1,-1,-1,0.149829,-1,-1);Aid[30]=new Array(0.794347,0.411359,0.340678,0.44951,0.153983,0.258846,0.276931,3.36473,0.0605099,0.107936,0.0536725,0.523975,0.12159,0.431949,0.288361,0.204072,-1,0.233441,0.168031,0.466625,0.406158,-1,-1,-1,-1,0.149685,0.760754,2.90855,0.265755,-1,-1,0.0448092,-1,0.275148,4.14877,-1,-1,0.357353,3.21203,0.548882,-1,6.68439,0.174685,-1,4.49319,-1,0.477378,4.20596,0.0636664);Aid[31]=new Array(0.783718,0.575532,0.385331,1.03647,0.470258,0.336961,0.311322,0.345115,0.906431,0.405814,0.0410974,-1,0.168026,0.154845,0.0441699,0.339767,0.546278,0.908819,1.00543,0.133901,0.406476,9.74628,6.72022,-1,1.31556,1.68748,0.0684994,0.382773,0.43786,0.248484,0.0448092,-1,0.0593762,0.164281,0.111913,-1,0.231613,0.21606,0.172666,0.144927,0.906083,-1,0.683701,-1,-1,0.282553,-1,-1,0.128327);Aid[32]=new Array(0.745575,0.203015,0.508262,0.274156,0.160933,0.284115,0.131226,1.28051,0.0731194,0.103233,0.0168644,-1,0.0917706,0.420897,0.135085,-1,-1,0.126506,0.0428672,-1,1.08885,-1,-1,-1,-1,-1,0.7513,-1,-1,-1,-1,0.0593762,-1,0.13736,-1,-1,-1,-1,-1,1.1584,-1,0.656548,-1,0.0534596,-1,0.35647,1.15827,-1,-1);Aid[33]=new Array(0.744774,0.662032,0.730667,0.492052,0.28729,0.318305,0.30246,0.980009,0.699239,0.123602,0.0671692,0.216396,0.169997,0.123392,0.0958987,3.76164,2.59604,0.991872,0.502511,0.0873803,0.340685,-1,0.144201,-1,1.24257,1.04809,0.362755,0.43631,0.476425,0.0584535,0.275148,0.164281,0.13736,-1,0.468663,-1,0.360789,0.345851,0.46761,0.329025,0.168141,0.226275,0.586682,0.534232,0.247305,0.225374,0.223109,0.593325,0.267889);Aid[34]=new Array(0.739726,0.77196,0.567655,1.05639,0.277229,0.307025,0.126667,5.03974,0.0895193,0.190979,0.0590915,-1,0.0632959,0.384688,0.0499699,0.332105,0.341278,0.555247,0.5264,-1,0.198026,-1,0.0541635,-1,0.185028,0.318862,0.525785,6.4046,0.438923,-1,4.14877,0.111913,-1,0.468663,-1,-1,-1,1.14129,7.28007,0.218297,-1,3.57332,0.378132,-1,4.44621,0.125228,0.169158,7.0301,0.117986);Aid[35]=new Array(0.707738,-1,-1,-1,0.142342,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0.168933,1.34864,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0.141686,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);Aid[36]=new Array(0.678263,0.210884,0.32067,0.216365,0.486264,0.14231,0.310003,0.269931,1.02427,0.0949019,0.0209757,0.231259,0.165511,0.0480775,0.061027,0.252961,0.855826,0.25044,0.49123,0.229538,1.29837,-1,0.907871,0.590457,0.441276,0.292265,0.0399858,-1,0.125338,0.321217,-1,0.231613,-1,0.360789,-1,0.141686,-1,0.0712289,-1,-1,-1,0.0619528,0.122812,-1,-1,0.14275,0.0982426,0.0348351,0.110376);Aid[37]=new Array(0.66301,0.387171,1.53955,1.5836,0.56157,0.267997,0.245021,1.44653,0.522546,0.364481,0.207681,0.144771,0.155557,0.17751,0.0768985,0.44583,0.328078,2.47493,3.02444,0.0273647,0.131368,-1,0.119555,-1,0.227747,0.974404,0.2222,2.95739,0.849788,-1,0.357353,0.21606,-1,0.345851,1.14129,-1,0.0712289,-1,2.26343,0.0879278,0.298222,0.330249,1.12557,0.191356,0.458406,0.391771,0.0993214,0.970032,0.241314);Aid[38]=new Array(0.659616,0.761182,0.908527,1.37394,0.37846,0.242806,0.157265,4.94267,0.144787,0.279379,0.0483986,0.0732244,0.083628,0.371993,0.0353848,0.455177,0.487756,0.846854,1.07463,0.0556552,0.209192,-1,0.098938,-1,0.228344,0.462184,0.380505,7.66191,0.844527,-1,3.21203,0.172666,-1,0.46761,7.28007,-1,-1,2.26343,-1,-1,-1,2.97438,0.563862,-1,3.58812,0.174491,0.127724,6.21506,0.178572);Aid[39]=new Array(0.627984,0.527703,0.479528,0.462119,0.270285,0.218274,0.343497,0.53544,0.1395,0.182584,0.0417322,0.751641,0.319057,0.686886,0.418946,-1,0.215723,0.285714,0.184276,0.453213,0.592131,-1,-1,-1,-1,-1,2.41039,0.25403,0.130646,-1,0.548882,0.144927,1.1584,0.329025,0.218297,-1,-1,0.0879278,-1,-1,-1,0.664189,-1,-1,0.532435,-1,0.975136,-1,0.164539);Aid[40]=new Array(0.622968,0.203652,0.78951,0.429406,0.339058,0.148533,0.111111,-1,1.22383,0.180035,0.0514884,-1,0.06361,-1,0.0623247,0.664214,0.394929,1.42148,0.716106,-1,-1,-1,0.85382,1.62379,0.744551,1.15006,-1,-1,0.809142,-1,-1,0.906083,-1,0.168141,-1,-1,-1,0.298222,-1,-1,-1,-1,0.606274,-1,-1,5.82073,-1,-1,0.42721);Aid[41]=new Array(0.621769,0.37062,0.310682,0.381773,0.131782,0.203985,0.214897,2.66955,0.0565404,0.0962473,0.0667337,0.369283,0.13757,0.362571,0.162959,0.174097,0.146811,0.178049,0.156434,0.327247,0.50787,-1,-1,-1,-1,0.117625,0.573589,2.40921,0.135006,-1,6.68439,-1,0.656548,0.226275,3.57332,-1,0.0619528,0.330249,2.97438,0.664189,-1,-1,0.0959903,0.124241,3.59427,0.0749535,0.472212,3.50066,-1);Aid[42]=new Array(0.614251,0.301274,0.509388,0.902107,0.187821,0.231661,0.127698,0.806822,0.580391,0.215141,0.0458549,0.0712271,0.0938965,0.190483,0.0479335,0.854253,0.880591,3.13936,2.3413,-1,0.211065,-1,0.666481,-1,1.14817,1.99624,0.24897,0.836028,1.56292,0.0592675,0.174685,0.683701,-1,0.586682,0.378132,-1,0.122812,1.12557,0.563862,-1,0.606274,0.0959903,-1,0.312851,0.181136,0.448684,0.0771673,0.317301,0.197243);Aid[43]=new Array(0.595991,0.153604,0.142279,0.103488,0.0550301,0.0392909,0.0335718,0.733572,0.118836,-1,0.023864,-1,-1,-1,0.0251033,4.99709,0.995844,0.584103,-1,0.0919199,-1,-1,-1,0.0448381,0.962993,0.746244,-1,-1,0.891148,-1,-1,-1,0.0534596,0.534232,-1,-1,-1,0.191356,-1,-1,-1,0.124241,0.312851,-1,-1,-1,-1,0.408093,-1);Aid[44]=new Array(0.564858,0.375221,0.334766,0.549381,0.141614,0.24383,0.0990332,3.1794,0.0327965,0.108356,0.0279827,-1,-1,0.432617,-1,0.215297,0.134062,0.233469,0.19007,-1,0.118632,-1,-1,-1,-1,-1,2.43242,3.44533,0.228594,-1,4.49319,-1,-1,0.247305,4.44621,-1,-1,0.458406,3.58812,0.532435,-1,3.59427,0.181136,-1,-1,-1,-1,4.58306,-1);Aid[45]=new Array(0.558605,0.192033,0.680156,0.359336,0.223019,0.0720301,0.0932887,0.856019,0.713525,0.147109,0.0284703,0.0670482,-1,0.106049,-1,0.807636,0.246378,0.771954,0.27832,0.128736,0.526661,-1,0.575546,-1,0.43503,1.01902,-1,0.407583,0.306296,-1,-1,0.282553,0.35647,0.225374,0.125228,-1,0.14275,0.391771,0.174491,-1,5.82073,0.0749535,0.448684,-1,-1,-1,0.196275,-1,0.250216);Aid[46]=new Array(0.550121,0.152738,0.137679,0.0866835,0.0756237,0.0809903,0.0621654,1.1269,-1,-1,0.0358412,0.462266,-1,-1,0.0675535,0.215599,0.0816129,0.0812022,-1,0.666515,0.325726,-1,-1,-1,-1,0.126017,0.60994,0.289088,-1,0.149829,0.477378,-1,1.15827,0.223109,0.169158,-1,0.0982426,0.0993214,0.127724,0.975136,-1,0.472212,0.0771673,-1,-1,0.196275,-1,0.19237,-1);Aid[47]=new Array(0.539731,0.447016,0.38867,0.582138,0.166777,0.208541,0.128259,4.4395,0.0606393,0.110589,0.0537289,0.103934,-1,0.298831,0.0366791,0.335223,0.330099,0.41519,0.403575,-1,0.162671,-1,-1,-1,0.253454,0.30253,0.387245,5.269,0.540019,-1,4.20596,-1,-1,0.593325,7.0301,-1,0.0348351,0.970032,6.21506,-1,-1,3.50066,0.317301,0.408093,4.58306,-1,0.19237,-1,0.0996856);Aid[48]=new Array(0.539642,0.32033,0.501633,0.421742,0.458357,0.116221,0.768269,0.301003,0.154099,0.631154,0.062787,0.168201,0.168896,0.211861,0.126902,0.237552,0.129381,0.711664,0.35831,-1,0.170084,-1,0.116251,-1,0.107632,0.087525,0.19648,0.281436,0.13091,-1,0.0636664,0.128327,-1,0.267889,0.117986,-1,0.110376,0.241314,0.178572,0.164539,0.42721,-1,0.197243,-1,-1,0.250216,-1,0.0996856,-1);
window.Pop=new Array(918933,362850,939744,770447,361686,240523,296584,455596,900946,231971,138879,113936,162695,122542,103098,650369,275039,673158,555632,41197,185429,205220,310956,146965,345384,488674,104815,315057,435422,42572,107041,245541,50866,224301,169030,257899,185284,645806,224700,91150,214577,85886,301611,263384,111267,303670,50840,147707,119117);
NrWords=49;var Len0=11;</script>
<script src="/elements/objects/related.js"></script>
<script src="/elements/objects/relator.js"></script>

<div id="search_template" style="display: none">
 <form id="search_form" class="search" action="map-search.php" method="get">
  <input class="typeahead" type="text" name="f" id="f" maxlength="95" autocomplete="off">
  <button class="search_button"><div class="mglass"></div></button>
 </form>
</div>

<script src="/elements/objects/jquery-minified.js"></script>
<script src="/elements/objects/bootstrap_typeahead_only.js"></script>
<script src="/elements/objects/typeahead.js"></script>

   </td>
  </tr>
</tbody></table>
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-7382239-3', 'auto');
ga('send', 'pageview');

</script>


<iframe id="google_osd_static_frame_5658399154337" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe></body>


<iframe id="google_shimpl" style="display: none;"></iframe>



/******************************************************************************
* Animated item relationship distance map
*
* Part of Gnod, the Global Network of Dreams. (c) Marek Gibney
*
* Animates html elements with id's "s0", "s1",...
*
* Tries to keep the distances between them as given by an array
* gnodMap.aid[][], where aid[4][7] is the proposed distance
* "s4" should have to "s7".
*
* The element with id "s0" allways stays in the center.
*
* settings can be made by altering several values in gnodMap, e.g.
*  gnodMap.scale=0.4; // makes the map smaller
*
* Margin between the items can be created by putting them into a
* container with padding.
*****************************************************************************/

/**
 * A moving HTML element
 */
function mg_2d_element(id)
{
 this.id=id;
 this.x=0;
 this.y=0;
 this.speedX=0;
 this.speedY=0;
 this.element=document.getElementById("s"+this.id);
 // this.name=this.element.innerHTML;
 this.width =this.element.offsetWidth;
 this.height=this.element.offsetHeight;
 this.inertia=0.7;
 this.fixed=false; // if the element is pinned

 this.update=function(damper)
 {
  this.element.style.left=this.x-this.width /2+"px";
  this.element.style.top =this.y-this.height/2+"px";

  if(!this.fixed)
  {
   this.x+=this.speedX/damper;
   this.y+=this.speedY/damper;  
  }
  this.speedX*=this.inertia;
  this.speedY*=this.inertia;
 }
}

/**
 * Given two rectangular items, this function checks if they overlap.
 * If so, it moves item1 away from item2.
 * @param item1 A rectangular item. needs to have x,y,width and height properties
 * @param item2 A rectangular item. needs to have x,y,width and height properties
 * @param force A force of 1 means the items will de-overlap in 1 step. A force of 0.5 means they will move 50% of the way needed to de-overlap.
 */
function repelItem(item1,item2,force)
{
 // Distance of the item centers:
 var dx=item1.x-item2.x;
 var dy=item1.y-item2.y;

 // Extents of the "shall not overlap" parts of the items from their centers:
 var extentsSumX=(item1.width +item2.width) /2;
 var extentsSumY=(item1.height+item2.height)/2;

 // Calculate how far we would have to move into each direction to escape the overlap:
 // If the number is negative, it means there already is a gap between the items
 // and the number states the distance to item2 in that direction. Pretty cool, uh?
 var right = -dx+extentsSumX;
 var left  =  dx+extentsSumX;
 var down  = -dy+extentsSumY;
 var up    =  dy+extentsSumY;

 // If there already is a gap between the items, there is no overlap:
 if(left<0 || right<0 || up<0 || down<0) return;

 // Calculate the horizontal and vertical movements nedded to get out:
 var moveX=right;
 if (left<right) moveX=-left;
 var moveY=down;
 if (up<down)    moveY=-up;

 // We mainly go into the direction of the shortest escape, which is either on the x
 // or on the y axis. The movement on the other axis will be scaled down proportionally.
 // This movement should be somewhat similar the movement of a physical object.
 // (Overlapping physical objects are a strange thought anyway)
 xy_ratio=Math.abs(moveX)/Math.abs(moveY);
 if (xy_ratio<1) moveY*=xy_ratio*xy_ratio;
 else            moveX/=xy_ratio*xy_ratio;

 if(!item1.fixed)
 {
  item1.x+=moveX*force;
  item1.y+=moveY*force;
 }
}

function GnodMap()
{

 this.aid=null;   // aid[i1][i2]: proposed similarity between the two items i1, i2.
 this.maxItems=0; // max. number of items to draw. if set to 0, dimension of aid[] is used.

 // adjustable constants ------------------------------------------------------

 // window size and scaling
 this.left             =0;     // left border of drawing area
 this.top              =0;     // top border of drawing area
 this.bottom           =0;     // bottom border, automatically detected if not set.
 this.right            =0;     // right border, automatically detected if not set.
 this.padding          =1;     // padding in px added to the boundaries. defaults to 1px to remove extends by rounding
                               // errors which would force a map restart on iOS platforms by triggering onresize
 this.offsetX          =0;     // x displace central item
 this.offsetY          =0;     // y displace central item
 this.scaleFactor      =1.4;   // scaling in respect to calculated window space
 this.scaleByCenterDist=-1;    // scaling in respect to mean target center distance (mean aid[i][0])


 // timing
 this.frameDelayInitial=25;    // initial value of increasing delay at each timestep
 this.slowdownCycle    =300;   // number of timesteps after that delay is increased

 // physics
 this.inertia          =0.7;   // part of velocity kept in one timestep
 this.damperInitial    =1;     // initial value of increasing damper that cool down motion with time
 this.damperFactor     =1.002; // factor the damper is increased by in one timestep
 this.damperMax        =100;   // max. value of damper
 this.springForce0     =0.025; // force between each item and the central item s0 trying to keep them at aid[][]-distance
 this.springForce      =0.005; // force between each item pair except the central item s0
 this.centeringForce   =0.1;   // force that pulls the center of gravity towards the central item

 // overlap avoiding
 this.repelDelay       =150;   // nr. of timesteps without repulsion
 this.repelIncrease    =0.01;  // amount the repelling force that keeps items non overlapping increases each timestep
 this.repelMax         =0.5;   // max amount of repelling force.

 // ----------------------------------------------------------------------------

 items=null;                  // holds the items
 this.nrItems=null;                // number of items to layout
 var maxX, maxY, minX, minY; // bounds of drawing area
 var scaleX,scaleY;          // scale to make anything fit
 var cogX, cogY;             // center of gravity of all items
 var cycle=0;                // refresh cycle counter
 var frameDelay;             // delay between redraws
 var damper;                 // increasing damper to cool down motion
 var repel;                  // increasing force that repells items to not overlap each other

 this.limitNrItems=function()
 {
  var limit=99999;
  if ((maxX-minX)<600) limit=30;
  if ((maxX-minX)<400) limit=20;
	if (limit>this.items.length) limit=this.items.length;
  nrItems=limit;
  for (var i=0;i<this.items.length;i++)
  {
    if (i>=nrItems) this.items[i].element.style.display="none";
    else            this.items[i].element.style.display="block";
  }
 }

 /**
  * update minX,minY,maxX,maxY to window size
  */
 this.updateBoundaries=function()
 {
  minX=0;
  minY=0;
  maxX = document.getElementById("gnodMap").clientWidth;
  maxY = document.getElementById("gnodMap").clientHeight;
 }

 /**
  * returns the mean item dimensions (width, height)
  */
 this.getMeanItemSize=function()
 {
  var meanW=0, meanH=0;

  for (var i=1;i<nrItems;i++)
  {
   meanW+=this.items[i].width;
   meanH+=this.items[i].height;
  }
  meanW/=nrItems;
  meanH/=nrItems;

  return {width: meanW, height: meanH};
 }


 /**
  * sets scaleX, scaleY in relation to window size, proposed item distance, mean item size, parameters.
  */
 this.updateScale=function()
 {
  var scale            =this.scaleFactor*(1+this.scaleByCenterDist*this.meanTargetCenterDistance());
  var meanSize         =this.getMeanItemSize();

  if (!scale) scale=0.2; // otherwise the map will collapse if there are only 2 items

  scaleX=(maxX-minX-meanSize.width )*scale;
  scaleY=(maxY-minY-meanSize.height)*scale;
 }

 /**
  * update boundaries to window size and place items at initial positions
  */
 this.resetItemPositions=function()
 {
  this.updateBoundaries();

  this.items=new Array();
  this.items[0]=new mg_2d_element(0);

  // center item0
  this.items[0].x=minX+(maxX-minX)/2;
  this.items[0].y=minY+(maxY-minY)/2;

  if (this.offsetX) this.items[0].x+=this.offsetX;
  if (this.offsetY) this.items[0].y+=this.offsetY;

  for (var i=1;i<this.aid.length;i++)
  {
   this.items[i]=new mg_2d_element(i);
   this.items[i].inertia=this.inertia;

   //initially place elements on a small circle:
   this.items[i].x=this.items[0].x+Math.sin(i);
   this.items[i].y=this.items[0].y+Math.cos(i);
  }

	this.limitNrItems();

  this.updateScale();

  cogX=this.items[0].x;
  cogY=this.items[0].y;
  cycle=0;
  frameDelay=this.frameDelayInitial;
  damper=this.damperInitial;
  repel=0;
 }

 /**
  * recenter items by moving their center of gravity towards item[0].
  */
 this.recenterItems=function()
 {
  var forceX=(this.items[0].x-cogX)*this.centeringForce;
  var forceY=(this.items[0].y-cogY)*this.centeringForce;
  for (var i=1; i<nrItems; i++)
  {
   if(!this.items[i].fixed)
   {
    this.items[i].x+=forceX;
    this.items[i].y+=forceY;
   }
  }
 }

 /**
  * update item positions and cogX, cogY.
  *
  * first checks all items against boundaries and push them back if needed
  */
 this.updateItems=function()
 {
  cogX=0; cogY=0;

  for (var i=0; i<nrItems; i++)
  {
   var w=this.items[i].width /2;
   var h=this.items[i].height/2;
   if (this.items[i].x+w>maxX) this.items[i].x=maxX-w;
   if (this.items[i].x-w<minX) this.items[i].x=minX+w;
   if (this.items[i].y+h>maxY) this.items[i].y=maxY-h;
   if (this.items[i].y-h<minY) this.items[i].y=minY+h;

   cogX+=this.items[i].x;
   cogY+=this.items[i].y;

   this.items[i].update(damper);
  }
  cogX/=nrItems;
  cogY/=nrItems;
 }

 /**
  * update position of all items
  *
  * * pull items toward target distance stored in aim[][]
  * * push items to not overlap each other.
  */
 this.layoutItems=function()
 {
  for(var i1=1; i1<nrItems; i1++)
  {
   for(var i2=0; i2<nrItems; i2++)
   {
    if (i2==i1) continue;

    this.adjustItemDistance(this.items[i1],this.items[i2]);
    if (repel>0) repelItem(this.items[i1],this.items[i2],repel);
   }
  }
 }

 /**
  * move two items towards their proposed distance in aim[][].
  */
 this.adjustItemDistance=function(item1, item2)
 {
  var targetDistance=this.aid[item1.id][item2.id];
  if(targetDistance<=0) return;

  var dx=item1.x-item2.x;
  var dy=item1.y-item2.y;

  var forceFactor;
  if   (item2.id==0) forceFactor=this.springForce0;
  else               forceFactor=this.springForce;

  // measure distance in both axes independently in respect to window width and height
  var wdx=dx/scaleX;
  var wdy=dy/scaleY;
  var distanceInWindowSpace=Math.sqrt(wdx*wdx+wdy*wdy);
  var force=(targetDistance-distanceInWindowSpace)*forceFactor;

  item1.speedX+=dx/distanceInWindowSpace*force;
  item1.speedY+=dy/distanceInWindowSpace*force;
 }


 /**
  * do one layout step.
  */
 this.layoutStep=function ()
 {
  this.layoutItems();
  this.recenterItems();
  this.updateItems();

  if (damper<this.damperMax)                        damper=damper*this.damperFactor;
  if (cycle>this.repelDelay && repel<this.repelMax) repel+=this.repelIncrease;
  if (cycle>this.slowdownCycle)                     frameDelay++;

  cycle++;

  // schedule next layout step
  var thisMap=this;
  setTimeout(function(){thisMap.layoutStep()},frameDelay);
 }

 this.sum_of_squared_errors=function()
 {
  var error0=0, error=0;
  for(var i1=0; i1<nrItems; i1++)
  {
   for(var i2=i1+1; i2<nrItems; i2++)
   {
    var targetDistance=this.aid[i1][i2];
    if(targetDistance<=0) continue;

    var dx=this.items[i1].x-this.items[i2].x;
    var dy=this.items[i1].y-this.items[i2].y;

    // measure distance in both axes independently in respect to window width and height
    var wdx=dx/scaleX;
    var wdy=dy/scaleY;
    var distanceInWindowSpace=Math.sqrt(wdx*wdx+wdy*wdy);
    var tmp=targetDistance-distanceInWindowSpace;
    error+=tmp*tmp;
    if (i1==0) error0+=tmp*tmp;
   }
  }
  return {error: error, error0: error0};
 }

 this.meanTargetCenterDistance=function()
 {
  var r=0;

  for(i1=1; i1<nrItems; i1++) r+=this.aid[i1][0];
  r/=(nrItems-1);

  return r;
 }

 this.keyPress=function(event)
 {
  if (!event) event=window.event;
  if (!event.ctrlKey) return;
	var key=null;
  if (event.which) key=event.which;
  else             key=event.keyCode;
  if (key!=105) return;

  var errors=this.sum_of_squared_errors();

  var info="Infos:\n";
  info+="Cycle: "+cycle+"\n";
  info+="Mean target center distance: "+this.meanTargetCenterDistance()+"\n";
  info+="Sum of Squared Errors of all items: "+errors.error+"\n";
  info+="Sum of Squared Errors of center item: "+errors.error0+"\n";
  alert (info);
  return false;
 }

 /**
  * find needle in sorted array haystack
  */
 function search(needle, haystack) {
  var low=0;
  var high=haystack.length-1;

  while (low<=high)
  {
   var mid   = parseInt((low+high)/2);
   var value = haystack[mid];

   if      (value>needle) high=mid-1;
   else if (value<needle) low =mid+1;
   else    return mid;
  }

  return -1;
 }

 /**
  * replace values of aid[][] with equally dense values in the range 0.0-1.0
  * the smallest item becomes 1.0, the largest 0.0.
  */
 this.equalizeAid=function()
 {
  var values=new Array();

  //build array of all used values
  for (var i=0; i<nrItems; i++)
   for (var j=0; j<nrItems; j++)
    if (this.aid[i][j]>0) values.push(this.aid[i][j]);

  values.sort();

  //assign each aid[][] the rank of its value in values[] normalized to 0.0...1.0
  for (var i=0; i<nrItems; i++)
   for (var j=0; j<nrItems; j++)
    if (this.aid[i][j]>0)
    {
     this.aid[i][j]=1-(search(this.aid[i][j], values)/values.length);
    }
 }

 /**
  * initially place every item and start refresh timer.
  */
 this.init=function ()
 {
  // Even though init() gets called on window.onload, in IE it sometimes
  // gets started with a screen size of 0. If so, we try again later:
  this.updateBoundaries();
  if (maxX<1 || maxY<1)
  {
   var me=this;
   setTimeout(function(){me.init()},500);
   return;
  }

  nrItems=this.aid[0].length;
  if (this.maxItems && nrItems>this.maxItems) nrItems=this.maxItems;

  this.equalizeAid();
  this.resetItemPositions();

  setTimeout(function(){gnodMap.layoutStep()},10); // let's go.
 }

 // ----------------------------------------------------------------------------
 // install event handlers.
 //
 // instead of calling init directy, we attach it to the window.onload event
 // especially when the map is loaded, the screensize might not be set
 // before that event

 var me=this;

 document.onkeypress=function(event)
 {  
  return me.keyPress(event);
 }

 window.onresize=function()
 {
  me.resetItemPositions();
 }

 window.onload=function ()
 {
  me.init();
 }
}

gnodMap=new GnodMap();



<iframe
  src="https://embed.kumu.io/772beabe81724f831329943130dc16a3"
  width="940" height="600" frameborder="0"></iframe>
