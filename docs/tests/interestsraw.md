# Interests

This section will start as a blank space for me to deposit ideas or configurations of information that I find interesting. \
It is hoped to be an exercise in the taxonomy of "exciters" in order to find an interesting direction for inquiry.
It will begin as a repository for concepts, philosophies, phenomena or fields of study that I am attracted to which I will try to structure. As the course progresses I hope to expand, refine and rearticulate these interests.

The first part of organising these items is an exercise in brainstorming  or free association followed by categorisation. This approach is cumbersome but gives value in trying to design a process.  navigation of information - hopefully to find links between words, their perceived definitions and categories and my motivations.

Below I hope to develop a tool - where I can input anything that gives me motivation and visualise the links and shared characteristics between them to produce a web of interest where interesting proposals can be located.

### Navigating between Science and Mysticism
Using a coloured mind map I decided to split the items I am attracted to into immeasurable qualitative experiences and types of scientific enquiry. This reflects the boundaries between which I feel I (and I think every human) navigates their exploration of reality - moving between measuring and experiencing - digital and analogue, particulate knowledge and waveform knowledge (analogous to enquiry into the nature of light)

Many items on my list are conduits or interfaces of information to and from these fields on enquiry. I called these either technologies or arts.

I have called some items that are natural occurrences Phenomena (which from my point of view have no apparent design behind them).

I also felt I should add values as my projects to include a political compass in relation to any projects that might come out of the exercise.

![interests map](assets/connected map.png)

<iframe
  src="https://embed.kumu.io/a68b7cfe97811f085cc3f335a1fc2472"
  width="940" height="600" frameborder="0"></iframe>


## Themes

### How information systems evolve

![information structure evolution](wa/infosyst.jpg)

https://www.newscientist.com/article/mg20827821-000-the-chaos-theory-of-evolution/

Human systems got more and more complex with technological advancements in transport and trade.
Can times of war be described in mathematical terms as Chaos?(rooted in a cause, highly dynamic and unpredictable and eventually resulting in the emergence of spontaneous order).

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/sean_gourley_on_the_mathematics_of_war" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

the dustbowl and the great depression demonstrated the instability of natural and economic systems.

Determinism -
the butterfly effect - what you do now matters.
 we are not living in a newtonian world.
 systems change. politics must reflect this, people need to know this to accept this and we need a strategy or story to help us deal with this information. We feel helpless and axious if we think of ourselves beholden to an unpredicatable newtonion machine.  

 <iframe width="560" height="315" src="https://www.youtube.com/embed/qfp5tKeSQAc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

how can we feel empowered under chaos?
we need tools to cope with anxiety and trauma.
economic systems are pushing environmental chaos.
we need to think about it being already too late to stop huge climate change.
Heating doubles when the polar ice caps melt.





What comes after order and organisation?
How can we describe the current times?

What is the link between AI and Chaos Theory?
Fuzzy logical
Neuro fuzzy logical


Values



<iframe width="560" height="315" src="https://www.youtube.com/embed/ABWYOcru7js" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Everyone is a designer, scientist and artist - making information and education more inclusive

Art, Design, Science and Engineering all have their exclusivity issues. One thing keeping someone from being any one of these things is whether or not they feel they belong in that field, whether it is their role to be these things.
There is personal preference but we are consciously or not assigned roles by the stories we are told about ourselves and whether or not information is made comprehensible to us. Some people will not consider leaving the roles they are assigned at a young age - this is top down design based on a system of inherited privilege. This makes outputs less rich and statistically we are less likely to gain insight if fewer people of similar backgrounds are working in each area.

One area where this doesn't happen as much is music - in fact most breakthroughs in music happened from the most excluded, underprivilege even oppressed people.


| Item / Point of attraction / Nodes of focus  | Categories/ tags  | What is interesting about it | Intellectual or Personal or beyond explanation interest|
|-------------------------------------|---------------------------------------------------------------|------------------------------------|-----------|
| Inclusion | Philosophy; Strategy | | personal |
| Tools that make you feel immeasurable unquantifiable concepts like experiences, memories, dreams, talisman - like Adam Curtis films, James Turrel's arts, music, surrealism, , some language)  | Technologies   |   help you feel immeasurable feelings that we can | Personal|
| Visual ways to understand data| Technology  | helps me process information | personal |
| Attention   | Human Processes | perception and attention go hand in hand. People need attention to survive and thrive, Not being able to pay attention impacts your decisions and learning | Personal   |
| Interfaces  |        |      |    |
| Symbols   |        |      |    |
| Linguistics   |        |      |    |
| Record Players  |        |      |  Personal |
| Hubble ultra deep field  |        |      |    |
| Astronomy   |        |      |    |
| Maps  |        |      |  Personal  |
| Particle Physics  |        |      |    |
| Nucleotides   |        |      |    |
| Neuroscience  |        |      |    |
| Volcanoes   |        |      |    Personal  |
| Consciousness and the self  |        |      | Personal    |
| The value of work  |        |      |  Personal   |
| Our experience of Light: transparency, reflection, colour  |        |      |  Personal   |
| wave particle duality  |        |      |    |
| ethnography |        |      |    |
| movement  |        |      |    |
| storytelling |        |      |    |
| apocalypse |        |      |    |
| Fractals  |        |      |    |
| Diatoms  |        |      |    |
| Water management  |        |      |    |
| Emergencies  |        |      |    |
| Immeasurable unquantifiable phenomena like experiences, memories, dreams, talisman | Phenomena; |   help you feel immeasurable feelings that we can ||
| Patterns of information structure and processing  | Phenomena; |   help you feel immeasurable feelings that we can ||


If I can tag these items and write a script on this tool
http://arborjs.org/halfviz/#/python-grammar

References:
https://pp.bme.hu/ar/article/view/8215
https://gephi.org/
https://rawgraphs.io/
https://npm.runkit.com/d3-force - forces
