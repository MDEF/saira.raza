

# <center>Bad Analogies</center>

<font size="2"><center>![an image here](assets/)</center>

<p>One of the things that this course has helped me to do is weed out bad analogies that are tempting to make when you don't have much time to think deeply about them.</p>
<p>I wanted to keep these together somewhere for reference </p>
<p>The substance of stories are roles that perform functions in relation to one another and actions that have consequences. The actions of the roles and how their environment responds gives the role a character, an essence that becomes a symbol (sometimes whose name even spawns a word) that stands for a combination of behaviours and experiences and defining moment.</p>
They contain a lot of dimensions of information that humans easily visualise, remember and love transmitting to each other and from generation to generation, imprinting more of their nature into them with each telling - like DNA evolving. Might story structures even tell us something of the architecture of our neurones and how we process all information? </p>
<p> I will use this space to inspect fictional narratives that speculate about the nature of the universe and the future for the dimensions of information they contain, what political messages and questions they raise and to see whether any symbols contained in them echo across genres and time.</p>
<p> I would like to ultimately compile the findings into a short history of science fiction told through an animated video.</p></font size>

## Fictions linked to topics discussed so far



### Understanding time and space
* Back to the future
* Arrival
* Star Trek Next Generation
* Interstellar
* a shit film called "the butterfly effect"



### Synthetic Biology

#### Gene Modification

* Okja
* Alien Covenant

#### Mutation
* Annihilation
* The Fly
* X-Men
* Spiderman

#### Cloning

* Moon

#### Food

* Soylent Green
* Idiocracy


### Anthropocene

* Planet of the Apes

### Post Paris Futures

#### Accelerators

* Total Recall
* Sunshine
* Snowpiercer

#### De-growth

* The White king

#### Capitalists

* The Fifth Element
* Total Recall
* Moon
* Alien
* Water world
* Mad Max
* Brazil

#### Extinctionists

* Wall-E
* Passengers
* Alien Covenant

#### Mutulatists

### Information Interfaces

* Tron
* The Thing
* The Matrix

### Symbols, language and information processing

* Arrival
* Pi

### Artificial Intelligence


#### The nature of information and coherence
* The Library of Babel - Borjes

#### Hacking Human Consciousness

* The Eternal Sunshine of the Spotless Mind
* The Manchurian Candidate
* Inception

#### Hacking AI systems

* Minority Report
* Ghost in the Shell

#### AI dominating human systems

* Terminator 3
* The Matrix

#### The AI's experience of consciousness

* Pinoccio
* Alien covenant
* Blade Runner
* AI
* Ghost in the Shell

#### Ethics in AI design

* Metalhead (Black Mirror)
* Terminator
* Blade Runner 2049
* 2001 A Space Odyssey

#### Reinforcement Learning

* Hang the DJ (Black Mirror)

#### Sentience of systems

* Solaris
* The Matrix (agent smith)



## Future Studies

### Linear time
* Terminator 2
* Back to the Future series
* Primer
* Bill & Ted's Bogus Journey

### Non Linear time
* The Butterfly Effect
* Arrival

### Afrofuturism

* Black Panther
* Space is the Place


### Utopias


### Dystopias

* Mad Max
* The Handmaids Tale
* Snowpiercer

### Posthumanism

* Black Mirror (various)

### Visions of the future from the second industrial revolution
* Jules Verne


### The Golden age of Sci-Fi


### Oligopolisation

* Terminator (Skynet)


<br>
<br>




<div class="container">

  <div class="right">
  </div>

  <div class='spacer'>

    <a href="https://mdef.gitlab.io/saira.raza/fictions/aliencovenant" class='wide blue'(text);>
      <h2> Alien Covenant </h2>
    </a>



    <a href="https://www.google.es" class='wide orange'>
      <h2>Arrival</h2>
    </a>

    <a href="https://www.google.es/" class='box redgay'>
      <h2>Tron</h2>
    </a>

    <a href="https://www.google.es" class='box lime'>
      <h2>The White King</h2>
    </a>

    <a href="https://www.google.es" class='box bluefish'>
      <h2>Sunshine</h2>
    </a>

    <a href="https://www.google.es" class='box yellow'>
      <h2>Annihilation</h2>
    </a>

    <a href="https://www.google.es" class='wide gallery'>
      <h2>Gallery</h2>
    </a>

    <a href="https://www.google.es" class='box redgay'>
      <h2>Browser</h2>
    </a>

    <a href="https://www.google.es" class='box orange'>
      <h2>The Thing</h2>
    </a>

    <a href="javascript://" class='wide blue cal_e'>
      <h2 class="top cal_i">Total Recall</h2>
    </a>

    <a href="javascript://" class='box lime'>
      <h2>Pi</h2>
    </a>

    <a href="javascript://" class='box blue'>
      <h2>Black Mirror "Metalhead"</h2>
    </a>

    <a href="javascript://" class='box bluefish'>
      <h2>Solaris</h2>
    </a>

    <a href="javascript://" class='box redgay'>
      <h2>Task</h2>
    </a>

    <a href="javascript://" class='box magenta'>
      <h2>Something Else</h2>

    </a>
    <a href="javascript://" class='wide yellow'>
      <h2>Media</h2>
    </a>

  </div>

</div>


<div class="square"></div>
