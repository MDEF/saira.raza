

<html>
<head>
<title>REPOSITORY OF DIAGRAMS</title>

<style>
body {
font-family: monospace;
background-color: #eee;
margin: 40px;
}
.box {
width: 400px;
height: 250px;
float: left;
padding: 0px 40px 80px 0px;
}
.screenshot {
width: 400px;
height: 250px;
overflow: hidden;
}
.overlay {
position: fixed;
top: 0px;
left: 0px;
pointer-events: none;
}
</style>

</head>
<body>

<div class="overlay">
<img src="https://mdef.gitlab.io/saira.raza/assets/transpheadline.png/" style="width:100%; height:100%;"></div>

<div class="box">
<p>
# REPOSITORY OF DIAGRAMS
<br/>
–<br/>
In its ruggedness and lack of concern to look comfortable or easy, Brutalism can be seen as a reaction by a younger generation to the lightness, optimism, and frivolity of today's web design.<br/>
–<br/>
<a href="">Submit</a></p>
</div>

<div class="box">
<p>
# REPOSITORY OF DIAGRAMS
<br/>
–<br/>
In its ruggedness and lack of concern to look comfortable or easy, Brutalism can be seen as a reaction by a younger generation to the lightness, optimism, and frivolity of today's web design.<br/>
–<br/>
<a href="">Submit</a></p>
</div>


<div class="box">
<div class="screenshot">
<a href="https://cdn.iopscience.com/images/2040-8986/19/8/084004/Full/joptaa7009f1_lr.jpg" style="width:400px;"/></a>
</div>
<p>Disslist</p>
</div>

<div class="box">
<div class="screenshot">
<a href="http://middleplane.com"><img src="http://brutalistwebsites.com/_img/middleplane.com.jpg" style="width:400px;"/></a>
</div>
<p>Nelson Heinemann</p>
</div>

<div class="box">
<div class="screenshot">
<a href="http://middleplane.com"><img src="http://brutalistwebsites.com/_img/middleplane.com.jpg" style="width:400px;"/></a>
</div>
<p>ISSN 2631-388X MIDDLEPLANE</p>
</div>

<div class="box">
<div class="screenshot">
<a href="http://37signals.com"><img src="http://brutalistwebsites.com/_img/37signals.com.jpg" style="width:400px;"/></a>
</div>
<p>37signals <a href="http://brutalistwebsites.com/37signals.com">Interview</a></p>
</div>

</body>
</html>



<!--
this is the rotating animation thing
<body id="firstPage">

  <section class="header">
    <article>
      <svg id="spin-cycle" height="1000" width="1000">
         <circle cx="500" cy="500" r="250" stroke="black" stroke-dasharray="40 80" stroke-width="15" stroke-linecap="round" fill="none" />
      </svg>
      <h1>SAIRA RAZA</h1>
      <p>MDEF 2019</p>
    </article>
  </section>
  <section class="main">
    <article style="padding: 80px 0px">
      <img src="https://s26.postimg.org/5orvjmvnd/hackjob_phone.gif" /> <img src="https://s26.postimg.org/72ex175ix/hackjob_computer.jpg" border="0" /> <img src="https://s26.postimg.org/5orvjmvnd/hackjob_phone.gif" />
      <h2>TERM 1 <em>TERM2</em> TERM 3</h2>
      <p>The way we interact with technology is changing at a rapid pace. Mobile devices, countless browsers: it's a bunch of baloney. My websites are custom-crafted to give you the exact tools necessary to totally avoid this evolving internet landscape. The future is here and I'm ignoring it, thanks.</p>
    </article>
    <article class="brags">
      <div>
        <h3>t1</h3>
        <p>There are countless screen sizes out there, but I don't know what they are, or even care, to be honest. All of my sites are created and tested exclusively in Internet Explorer 8, ensuring that the layout of your site looks perfect on Internet Explorer 8.</p>
      </div>
      <div style="border-left: 5px solid black;">
        <h3>t2</h3>
        <p>I love developing sites around the many amazing content management systems on the market. I'm a big fan of the Freewebs, Geocities, and LiveJournal platforms, and I specialize in making custom sparkle GIFs to suit any layout.</p>
      </div>
      <div style="border-left: 5px solid black;">
        <h3>t3</h3>
        <p>There's a world full of competition vying for a piece of your business, but you don't want just anyone wasting your valuable time. That's why I make sure only the most dedicated of </p>
      </div>
    </article>
  </section>

</body>
-->
