<center> # 10. An Introduction to Futures</center>

with Elisabet Roselló Román

<center>![Interests](assets/lightcone.gif)</center>



## Understanding of time

### Linear

Linear timelines
back to the future
the epochs of evolution - modern western idea

### Non linear
Breakthroughs in Physics such as quantum theory and Chaos theory in Maths affect our cultural understanding of time and of the future.

The future cone -(like a projector)
probability possibility and plausibility preferable

### what the hell is time these days?

There is not a general acceptance of what time looks like because we are in it .

#### Cones
vertex between two cones is where we are.
big bang is the bottom cones


#### Dators First Law
>"The future" cannot be "predicted" because "the future" does not exist

*Jim Dator*

jules verne was a nerd
people who write fictions project stuff that is actually  happening

postfuterear.com

self fulfilling prophecies
http://www.postfuturear.com/


* [Max Tegmark (1997)'On the dimensionality of spacetime' Letter to the Editor, Classical and Quantum Gravity, ](https://space.mit.edu/home/tegmark/dimensions.pdf)

#### VUCA states - volatility, uncertainty, complexity and ambiguity.

This is a space where no normals exist - is this chaos?

This is a space where 'black swans' appear  - unpredictable appearance that provokes changes.. new ideas that are not phenomenological they are extend over many systems? climate change keeps producing these like ice shelves melting
Examples include
* the occcupy movement
* 2008 was one


#### Dators 2nd Law

> Any useful idea about the futures should appear to be ridiculous.

try ridiculous



## Objects, scenarios and timelines in a future crossing of axes


<img src="assets/w10/posthumanaxis.jpg" width="175" height="250" title="Week 1" alt="Week 1">
<img src="assets/w10/Queeraxis.jpg" width="175" height="250" title="Week 1" alt="Week 1">
<img src="assets/w10/map.jpg" width="275" height="400" title="Week 1" alt="Week 1">






<iframe width="560" height="315" src="https://www.youtube.com/embed/g6BK5Q_Dblo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**References:**
* [Constructor Theory](http://constructortheory.org/)



## Futures from histories

### Apocalyptical thinking - Thatcher and Mad Max

Elisabet talked about how within our lifetimes apocalypse has come to signify the end of Capitalism. During Margaret Thatchers leadership in the UK Communism was still pushed as a dystopian system. "there is no alternative" international
communism was seen as dystopia
capitalism drove apocolyptic visioms
the other side were elysieum (movie)

When the Berlin wall fell down, for a generation it might have felt easier to imagine the end of the world rather than the end of Capitalism.
The aesthetic of overrun industrial buildings can be fetishised into a symbol of the failure of capitalism.

abandonned things in detroit

The same can be said from a post USSR point of view of

https://www.newstatesman.com/culture/art-design/2016/08/ruin-porn-art-world-s-awkward-obsession-abandoned-soviet-architecture

#### The worst version of humans collectivised - hunger games, 1984, the handmaids tale

#### Transhumanism - agenda and idealism we have a moral need to transform human evolution  - bionic arms, cyborgs elon musk

#### Posthumanism

#### Retrofuturism
People were more in love with the future in the past


## History of futures

### Ancient futures

MAYA - how easily accepted things are?
expectations / desires and fears
oracles

4 horsemen of the apocalypse  - british museum images


### Modern Futures
* different understanding of timelines no words for future - Arrival
* the concept of progress
* utopias 500 year old concepts - political construct
 - thomas moore - new world utopias of the puritans? pilgrim fathers. submarine inventor was a socialist utopian.

 2nd industrial revolution
 jules verne
 Albert rorids
 the world beyond - william morris - critique of modernisatoin - rural

 paleofutures.com

cinema
segundo chomon el hotel electrico
collective fears and hopes after wars changed
dark side of robots starts after ww1 and 2
futurism in italy - like transhuman
soviet films
metropolis
city of skyyscrapers 1925  - bcn references
sexual robots

### golden age of sci fi

aesthetic gets ramped up  
los angeles airport
jetsons  
dune
high rise
HassoPlattnerInstitute_HCI
lovecraft

HassoPlattnerInstitute_HCI

### postfuturism
cyber punks
actual Punks
philip k dick


bruce sterling -  
ghost in the shell   - Posthumanism
matrix cyber punks


### Retrofuturism
steampunk
la cite des enfents perdu


### year 2000
plasticy decadence
shitty plastic things


### technoutopianism
future needs tech

### Transhumanism
transcending our boundaries
that weird book where  - celestine prophecy

x men?
singularity
anti aging

a worker for google is spearheading Transhumanism
### Digital nostaliga, vapourwave and fashwave

current Retrofuturism

aldous huxley's brother was the head of the UN!?

neuromancer

last decade transhumanism is stronger

### postcolonial Futures

* Afrofuturism
last decade its grown a lot

gulf futuriam

fatima al qadiri


#### Afrofuturism
working more with music

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z8nppTSY-Rs?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### Gulf futurism
sopohia al Mariana
reza negrani

#### Queer afrofuturism
alien metaphor

existenz!  or anything by cronenberg

#### pakistan afrofuturism
miles davis, sun ra




#### Futures as prototypes
kevin Kelly
https://kk.org/

#### Design Fictions!!
superflux


### Posthumanism

rosi braidotti

"A Cyborg Manifesto" is an essay written by Donna Haraway

cyborg for empowering beyond gender

movement from philosophy
connected to speculative realism

thinking outside of anthropocentrism  
postcoloniality - non western countries reclaiming their roots.

gentrification is a bit like this.

 - trying to be more autonomous.
de-colonialisation

new bdies

jason hopkins freaky bodies


### left accelerationism alt woke
 - post capitalism fututre

 -

** Magic
arthur c clarke and Magic
magic machines!
the electric hotel - objects move like Magic **


wendell bell 1997

1. time is Continuous - the future is going to be different.. its about change

Eleanora massini

in the 1990s the cone was proposed
 dunne and raby  vesrion  updated this

 http://cargocollective.com/wongxianshun/following/all/wongxianshun/We-Are-As-Gods

 joseph voros blog

 quantum cones?  http://www.dileepnanotech.com/quantum_entanglement_quantum_communication3.html

we cannot imagine something we do not know.

from circles to cone "unknown unknowns"

probability v possibilities
desirability v palusibility

acknowledged or possibilities - knowing and ignoring

xeno - futures (xeno means strange  - fear o f the strange)
http://www.postfuturear.com/


postfururear cones show importance of Narratives


knowledge and unknown

known unknown is climate change
elephant in the room
unknown unknowns -  asteroid / cthulu


## Narratives

Narratives are something I've been thinking about in a [side project on Fiction](https://mdef.gitlab.io/saira.raza/fictions/).

It was mentioned by a guest speaker that stories give us strategies so narratives give us a constructive way to think of what to do in relation to the future.

Lack of consensus between narratives so we cant do anything.
2001 for a solution states

### Types of narratives

#### Narratives provoke strategies
structures:
* structures mega Narratives
* grand Narratives
* images
* hype and advised
* scientific knowledge is a narrative!
* interesting field - **hyperstitions** (really new - narratives that have agencies ) , hauntology e.f slenderman Alex Williams . creepypasta - like big foot - specifically  fiction feeds off reality really fast.

### Tools of narratives

#### utopias and dystopias

Eleanora Masini
https://www.sciencedirect.com/science/article/pii/S0016328706000462

#### narratives of hanges go in waves.
  patterns are breaking down in waves.
  weather forecasting is breaking down

  hype cycle

  dr wendy schultz

#### ethnographic Futures

####   **experiential futures** and speculative design  
ex futures - creates an experience includes smells and lights

stuart candy situation lab
https://situationlab.org/about/

Diegesis
diegesis - fiction that provokes a story
e.g methril, sonic scredrivers .. the object doesnt explain the story

**Design Fictions**
conveys research processes with speculative techniques.

**spec design**  - more close to artistic goals - generating discussion after
dunne & raby
united microkingdoms

superflux  

the goal is the object not the process.
provocative objects.
includes films?

critical design - more critical not commercially oriented.

dunne and raby manifesto.
https://www.researchgate.net/figure/Dunne-Raby-A-B-A-Manifesto-2009_fig4_282460352
Sophia is a speculative design object.

wizard of oz

dunbar coin

how is borges a narratives its not!?


## Change

who studied:
Hegel, Marx, Foucault
future studies
intelligence studies
indirectly
Sociology and anthro
economy
phys
phil
theory of change

**change has been interpreted as waves and lines**

### Diffusion of innovations
1962
Innovators
early adopters
early majority
late majority
laggards

> The future is already here - its just not evenly distributed

- William Gibson, Science Fiction Writer (The Economist 2003)


Steward Brand  - father of hacking
graph of change
Updated by Grantcraft



Culture is a slow changer
link to changes of social norms and **behaviour change**
confirmation bias - openness to accept things we believe. we find proof to back our beliefs
culture is hard to change

evolution
innovations are mixed with previous innovations
mixing different sources not linear
**non-linearity**
betul kacar future shaped by pasts that could have been
the same happens


### Trends
cultures, behavioural phenomena
representation of complex elements of **dynamics** in human and non human systems. You have to be familiar to what trends look like so you have tools to work with. and be critical of them.

Hype comes from hyperbole
Don't believe the HYpe
Terry Flew definition of hype

#### Fad
emerges and dies really quickly

#### Taxonomies of trends
secular and cyclical trends
 - by shape - cycles, ven diagramms - cool graphics online to check out
 - micro and macrotrends
 - duration
 - they have interrelations
  - drivers (triggers, catalysts, constructors, bloacker) you also get laws that do this
  - signals or general indicators (innovations, unexpected changes, black swans)


#### Dynamics pf trends or change

Emergence
growth / survival
Stagnation
Death



### Systems thinking
Nets nodes and dependencies

In design there has been a shift from user-centred to complexity thinking
useful to do before interventions to get the desired net effects. Probably as an effect of the increasing complexity of the times.

what is a system?
set of elements that are interelated
ecosystems - systems of systems - 1990s misunderstanding of the net effects of these.

Dick Gently? Douglas Adams


" A sound of thunder" Ray Bradbury
butterfly effect
theres a shit film called the butterfly effect.

* feedback loops
* Hierarchies
* behavior over time

### Horizon scanning
POlitics
INformation and communication
Nature
Conflicts
health
arts& cultre


### Mega trends

non everything global is good

* Oligoppolisations
Big companies are getting bigger

 * State-Nation Crises (catalonia, scotland) - nation-state Brexit, Criticism of the UN
* non-state actors (or power)
companies, NGOs (conspiracy theories?) like amazon have more wealth than some countries. Some people do too
* feminism
* Queeer movement
* Climate Change
* Environmental awareness
extinction rebellion (really current) in London
* Hyper reality -
* Post-truth era
personal trust over science or experts
chnanging politics
* user, human-centrism
anthropocism
* Transhumanism
* new intimacy and privacy structures
 selfies
* postdigial - moderating our digital presence


seems to be a breakdown of trust in general - megatrends intersecting (post-truth, state-nation crises)  into conspiracy theories rampant confirmation bias and weird symbol guides



extended present
 - nothing new, old ideas (yesterdays tomorrowa, predictions. deterministic.
   future studies isn ot this. it is not predications it is forescast - probabilisitic. essentially pluralistic.
   we become relevant if we look at more than one future.
   it gives us choice
   dont worry about it because we are taking care of it.
    population exponential growth
    moores law

    to make a forecast:
    what do we know
    what do we NOT know
    what do we need to learn about them
    - all about learning
the above is about finding trends
a measurment of change over time
has to be quantifyable
there has to be time frequesncy.

trends:
* emergence
* take off
*slow down up to structural limits
* stagnates / rebirth/ decline

at emergence and stagnation qualitative stuff happens

structural limits coincide with  material / information shifts?


#### Future scenarios

how a futures comes to be and the effects
* consistent parts
* coherent

we dont make a scenario to get it right - its to make you understand ge implications

* plausibile - -without using the present as a criteria
the less likely futur is one that is like the prssnent
* questions the present.

mont fleur, south africa
 logic of scenarios


 pick 2 drivers as axes for forecasting but its reductive

 what kind of changes are we considering:
 * stability
 * Constant change


betting money just on trends is risky and dangerous - sitting ducks
the future is more than sum of it parts
 there might be bias at work.

 black elephants - capacity to believe what we're already being told  - matching beliefs - climate change denial, changing economic models.
the oil industry


 belief v data that affects our decisions about now affect our ideas about the future
 brainwashing
 conditioning
 e.g immigration

 colonising futures
 people need to be able to make their own choices

people dont often want to take responsibility for their future collectively.



#### Familiar Futures

future image:
jetsons
daisy -

Blade runner / her /

novelty rather than radical change

Data is overated when talking about the future

Where does novelty come from?

museum of water


during emrgence imagination becomes more useful than analysis  - not to foretell but to DECIDE WHAT TO TAKE SERIOUSLY. working on assessing potential.


### outliers are not noise

they may be a black swan
focus doesnt identify black swans and black elephants
they can be oppootunities

cognitive biases support existing models

our brains save us from thinking
#### and use linearity
sapiens - harare

non linear thinking?

#### dichotomy
if something is true the opposite is false

but there is quantum states - both and none
multiplicity

#### induction
finding patterns in what you see

but what about what you dont see - black swans

the death of death - a book
jose luis disortno? futurist -


women robots
sophia


Alpha go - netflix
after deep blue defeated kasparoff

posthumanism asks us to reasses what it means to be human.

cyborgs

machines are not cruel as far as we know

Unthought futures
the things we create devolve into the exact opposite - e.g the EU
- how is our prerception of the future be limted
- does our worldview change the understanding of change? brains are physically changed by learning languages.
we learn that time is linear through language.
world views created by cultures save us from thinking. we teach kids to form lines.
-


contradictions

complexity - cannot be addressed in a linear way
short term influence
a system is complex if it has components that relate in different ways

chaos
something trival can bring havc to a city.


jellyfish - like warm acid water - great for them is climate change
blocking powerplants
they kill fish

jellyfish a weird.


hyperconnectivity - a tool for the seeds of chaos - we are global.
the worldview has changed
impacts scaled up.
there are thresholds of impact.
that diagram about opportunities (tomas' model)

accelerated
expansive
escalating
simultaneous

postnormal change:
speed
scope
scale


the future can be cliche
could we need to unlearn knowledge
do you think because of what you see or do you observe what you know.

watch out for confirmation of your expectations.

challenge why you think, ask and say stuff

### Unthought
trump is the unthought.
what lies outside our world view

but it is not unthinkable.

beliefs sit in worldviews
the belief in always being the hero
the belief in honor


some midtools.
strategies for futuring:
start with
* extended present - look for black elephant
* familiar futures - black swan
* unthought futures (worldviews) - black jellyfishes
and repeat and look for them throughout the process
and look for chaos contradictions and complexities throughout.




futures for muslim societies

profiling is failing.

black jellyfish - escalators under certain conditions that we dont believe they exist.


practical dynamics


### Research in Design for futures


 - product centred
 - methodic design emergence of participation in design, 60s onwards
 - user-centred since the 80s
 - human collective-centrred enzo manzini 00s onwards
 -service design
 -strategic design - transition design


### Types of reasoning
- deductive - top down logic - sciencce
- inductive reasoning -
 - abductive

experiential futures

 liam young


Research in design for futures.
speculative:
 - an emerging issue, a critic trend
 small research
 prection of scenarios
 prototype development
 exhibition
 data gathering

 A

 Causal Layered Analysis
 links myths/images to discourse to science to litany (official)
Problem > causes > worldview >

#### Scenario Building

* 2 axis matrix or shoemaker

* dators 4 futures
continuation
transformation
discipline/saturation
decline or collapse

* simple extrapolation of trends i alinear way


























## Applying this weeks learning

### Punk ideology in inclusive making
I've been finding punk politics coming up through discussions over the weeks. It's DIY protocol-breaking approach to aesthetics and musicmaking were very **inclusive and resonated with me during our exploration of design by experience.

Punk aesthetics accentuated their exclusion from society - shocking, loud and messy.

### Using Futures Theory in my research projects.
I would like to look deeper into futures theory so I can apply this to my research into emergency water and sanitation

### Tying research with experiences with speculative design

There is a clear research stage in speculative design. I could use speculative design as a path for my research practice as a means to gather data as well as a method

#### Experiential Futures

From the offset I have been trying to link experiences with the measurable, data and theory.
This method is something I'd like to explore - it ties together my interests in light, music and stories.
creating experiences
i'm really drawn to this
* multimedia
* emersive theatre
* film in 3d
* documentaries in 3d
* multimedia film and music Adam curtis
 liam young
 keichi matsuda
Punch drunk
black mirror

ties into the value gained from experiential design methods - all that data can be gained by people other than the designer.


Causal Layered Analysis
### The role of contradictions in complex systems

> Contradictions, which are irreconcilable views and perspectives, cannot be resolved: they can only be transcended. In other words, contradictions have to be synthesized and necessitate the formulation of a new position that incorporates most of the various different positions. Contradiction often provide the first signs that a system is moving towards complexity, chaos and eventually postnormality.

* https://postnormaltim.es/essentials/3cs






### Representation!
the narratives in science fiction are way too Old
be active participate in storutelling
https://hiveminer.com/Tags/peshawar,starfleet

### Geometry and Dynamics and Systems

#### Behaviour change theory and non linearity

#### Dynamics, fluids, flows of social phenomena
The insight we learned is really significant to the inquiry into information evolution
from mapping of my interests to researching technology to researching social systems

### Drivers of change as starting points for projects


### unnknown unknowns. teaching people about this
really important for people to know this regarding critical thinking. best way to teach critical thinking



## Resources

### Websites

* postnormaltim.es

nearfuturelab




### Books
