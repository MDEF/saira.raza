# 08. Living with Ideas

This week we tried using some experiential and speculative design methods with Oscar, Angella Mackey and David McCallum.

> Material speculation emphasizes the material or mediating experience of specially designed artefacts in our everyday world in order to speculatively and critically inquire through design.
A counterfactual in material speculation is a virtual or tangible artefact or system rather than a statement or text.
The counterfactual artefact is also an embodied proposition that, when encountered, generates possible world accounts to explain its existence.



## Using experiences of counterfactual artefacts to inform design

**The brief:**
We were all asked to bring in an ordinary household object to modify. We each picked an object that was not ours and trying to modify it to fit a fictional scenario with a "What if.." question as a starting point.

** My objects: **
I was drawn to a sleeping mask and a Rubik cube puzzle.
The sleeping mask made me wonder about the possibilities of using or optimising what happens to our bodies during sleep.
I liked the idea of the Rubik cube puzzle functioning like a security feature that contains something very precious - this is mentioned in fictional stories such as Hellraiser, Chronos, Theseus and the Minotaur, Stargate and many others. In these stories solving the puzzle often unleashes something extremely powerful..good or bad. The stories reflect humans innate curiosity and desire to solve problems but are often cautionary tales.

** The Question: **
What if we could optimise the functions of our sleep o become psychologically and physically healthier ?
Our bodies naturally use sleep to maintain the function of our bodies but in the quest for being more content and productive will we try to optimise our productivity in the time we spend unconscious sleeping.

**My Response:**
**The Sleep Keeper:** An Eyemask with an activator that stimulates the brain and a controller/data storage device

![Sleep Keeper](assets/w8/sleepkeeper.jpg)

**Experiencing the object:**

* I tried to set the sleep cycle using the programmer / data storage. and then have a nap wearing the eyemask.

* The data storage was not something i could keep easily on my person and I have to put it to one side. This made me worried that my precious data was not safe because I could not see it and someone might take it.

* I felt that I wanted a data storage / controller attached to my body preferably on my eye mask so I would notice if it was not on me.

**Reflection:**

* The thinking behind the function of the object was quite rushed and the 'experiencing' stage would have been easier if the object had a more important purpose.

* Going through the ritual of going to sleep was important because it is integral to the uptake of the object.

* The size, feel and location of the object was easier to do by designing around the experience.

In researching my interests this week I came across a project called [Pillow talk](https://www.edwinaportocarrero.com/PILLOW-TALK) by Edwina Portocarro and David Cranor
>"It consists of two devices, one of which is a pillow embedded with a voice recorder that is activated upon squeezing together several conductive patches at the corner of the pillow. This interaction minimizes the steps necessary to record a fresh memory of a dream immediately upon awakening.
After the dream is recorded into the pillow, the audio file is transmitted wirelessly to a jar containing shimmering LEDs to display the "capture" of a new memory, and electronics in the jar can play back the recordings through a small speaker under its lid when it is opened. "

Although this device was about recording dreams rather than controlling dreams the physical exploration of the form felt really familiar to my experience. A paper documenting their design process can be found [here](https://dam-prod.media.mit.edu/x/files/wp-content/uploads/sites/10/2013/07/4p375-portocarrero.pdf).

![Pillow Talk jar](assets/w8/pillowtalkjar60.jpg)

*"After the dream is recorded into the pillow, the audio file is transmitted wirelessly to a jar containing shimmering LEDs to display the "capture" of a new memory, and electronics in the jar can play back the recordings through a small speaker under its lid when it is opened." - www.edwinaportocarrero.com*

https://www.media.mit.edu/videos/edwina-portocarrero-defense-2017-08-01/

## Building an artefact, system and situation for a speculative material

We used a greenscreen app to experiment with different ways we could use a speculative material (one that could display digital images and video).

**Brief:** Think of an item of clothing and a situation where you could use a speculative material that can display digital images and video.

**My Response: Protest Balaclava**

I thought very broadly that the technology could be used to either blend in or stand out and make a statement.
A place where you might want to make a statement is at a protest. You might also want to show unity with others at a protest which you could arrange with this technology (like audience light shows at sporting events).



<img style="float: left; padding: 0px 10px 0px 0px;" src="assets/w8/olympics.jpg">
 *Olympic games 2012 light display using audience members as pixels*

<br>
<br>
<br>


I thought an item of clothing you might wear at a protest to make a statement or to remain anonymous might be a balaclava although these are frightening because you can't see facial expressions and balaclavas have connotations of violence - so the object might be counter productive.

![balaclava](assets/w8/balaclavasmlr.png)
![balaclava2](assets/w8/balaclava3.png)

**Experiencing the object:**
* I spent a lot of time learning to use the technology. It was interesting to try new ways of using the technology.
* I also experienced how it felt having someone else use me as a canvas for their experiments which could be something that could happen with this material if it existed. It felt slightly intrusive. This might be especially dangerous if  they can project images onto where your face should be.




## Magic Machines - Materialising an abstract idea

**Brief:**
Today we chose as a starting point a value from a list of values that represented abstract needs - we designed speculative objects that addressed these abstract ideas of needs - trying to meet them or to tackle them - usually both.

**Need:** Acceptance
**Implied action needed:** the need to be appreciated
How I feel about this definition: I don't think acceptance equated to the need for appreciation - to me it is the need for inclusion and validation.

**Name of Machine:** Role Model Projector

![Projector](assets/w8/rolemodel.jpg)

**Function:** The Role Model Protector is an embodied interactive AI that is able to:

*  converse with you using sound and use a human form of your choosing to use body language.
* It can project videos and images onto a surface to show you examples of real people and fictional characters who look like you or are from similar backgrounds who face similar challenges to you - reflecting parts of your experience back to validate them.
* It can project several unspecific scenarios of positive feedback to use reinforcement learning to learn that you are accepted by many different types of complex characters
* It can access and reinforce real memories of positive feedback.

**What is the 'magic' in the machine?:**
The magic is the ability to hack human ideas of the self.

**Reflection on the design process:**

* I found that I had to spend a lot of time thinking about the need and what it meant to me. Then I had to think about the causes of the need. I felt that research would have been beneficial to the process if more time was available.

* I felt I was lacking some skills in fast prototyping.

## How do we value work?
An inquiry into how people value work with a view to question current wealth/ salary distribution.

### Desk research on theories surrounding work (when seen as an investment)
From the point of view of the worker, work can be seen as an investment for the things they need and value. For people who struggle to survive on the returns of this investment it can feel like slavery.

** Work is rewarded...**

- by exchanging material things that people need for their work (barter)

- by giving them money, "a joker card" that they can exchange for skills and things they want and need

- The "3 Dimensions of Work Satisfaction" model by Jan Collins (from the finance sector) suggests that emotional, intellectual and physical returns whilst doing the job are in a sense a type of reward.

** We can get some returns from doing a job **

From the world of HR and business management there are some theories into what motivates people to work.

The Hackman and Oldham job characteristics model proposes that
Motivating Potential Score (MPS)  = ((skill variety + task identity + task significance) / 3)  x autonomy x feedback
This suggests that motivating factors could be

* Skill variety
* Task identity
* task significance and
* autonomy

Herzberg's Motivation-Hygiene theory names as motivating factors:

* achievements,
* recognition,
* work itself,
* responsibility,
* advancement
* and growth

These all could be considered as returns we can get from work.


 **<font size="3">What do people want in exchange for work from outside of their job?</font size>**

** Money **  (access to a virtual value system) is how we normally expect for work.

[Maslow's hierarchy of needs](https://medium.com/thrive-global/maslows-hierarchy-of-needs-5b021e203e34) is a theory from the 1940s that sets out ** psychological needs of individuals**  which are:

* Physiological needs (basic needs for survival like food)
* Safety
* Love / belonging
* Esteem
* Self-actualisation

** Returns supporting values**  might also describe what people want to get out of work. The World Value Survey maps values from different countries on the following factors:

  - survival
  - self expression
  - reason
  - and tradition

 <iframe width="560" height="315" src="https://www.youtube.com/embed/ABWYOcru7js?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

** Preferences based on personality and psyche** : The five-factor model (FFM), and the OCEAN model of personality traits defines five broad groups of descriptors commonly used to describe the human personality and psyche: openness to experience, conscientiousness, extraversion, agreeableness, and neuroticism.

** Dimensions of wellness ** might also be able to describe the returns we seek. There are various models of wellness. Some of these dimensions include:

* Physical.
* Emotional.
* Intellectual.
* Social.
* Spiritual.
* Environmental.
* Occupational (this could include educational returns outside of your current job)


** What we invest into doing work / earning money **

 - The Collins model also suggests that workers make physical, emotional and intellectual *investments *into their jobs.
 - time
 - choice

**What could investments into our jobs look like in the future?**

 <iframe width="560" height="315" src="https://www.youtube.com/embed/D7bRgE8le-o?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*<font size="2"> Above: 360 film about the future of work Merger by Keichi Matsuda.  "An international city in 2030. Automation and big corporations are taking over.
A top accountancy graduate from an elite university has made it her mission to help small business in their brutal, winner-takes-all world.
She works night and day in her control centre, dedicating herself to the efficient workflows that will help her clients survive. But her obsession with productivity and drive for total control has come at a cost.
As the potential of the Artificial Intelligence networks around her grow, she finds herself at a crossroads. Should she remain in the physical world? Or upload herself to the network and become the ultimate worker?" </font size>*

### 1. Gathering experiential data - Actions as investments - Why do you do things and what do you get in return?

Gathering data enables a starting point to inquiries.
I started by trying to get some experiential data on the links between the things we want/value and the things we do for them.

#### 1a. A Diary of everything I did in 20 hours, why and what it cost me.

I kept a diary of every activity I did for 20 hours and then attributed costs and rewards for these activities - analysing each action as an investment.
I recorded the time spend, costs and purposes of each action and arranged the data in the mind map below.

  ![Actions as investments](assets/w8/actions as investments.jpg)


**Observations:**

* I found that money was the cost of a lot of my actions. Money is ubiquitous and current economic systems have had quantitative and empirical theories to back them. I am not an economist and tools of economic analysis are not within my remit. If I am to try and suggest a new way of valuing work **I could try and look at the non-economic costs of work**.

* **Watching your own behaviour is a direct way to start mapping variables**.

* In starting my enquiry into returns from my actions. I was able to unpick my decision process which led to understanding that ** values govern my decisions**.

* I realised that ** there are emotional costs** to actions


#### 1b. My body as the system - a system without money

I modelled myself doing my last role as a researcher as a closed system (one where matter can not come in or out).

 ![Body as a system](assets/w8/bodyassystem.png)

**Observations:**

* It felt strange classing values as a return because values guided the other returns I wanted so I think these are separate things.

#### 1c. Another body and another system
In order to look at work in general I need to look across several jobs
I interviewed my flat mate about her job

The questions and answers were as shown below:


| Question       | Answer          |
| ------------- |:-------------|
| 1. What values guided you to choose your job?     | giving back, trying to do something useful. Being the best I can be for the greater good. being a member of society |
| 2. What do you invest into your job emotionally, intellectually and physically     | there's a large physical component to the job and also a large communication element which is emotionally draining. Sometimes I have to be a role model I gave up relationships for it. I can't drink alcohol the day before I work|  
| 3. What do you get back whilst doing your job ? | Independence. Creative Freedom. Teamwork / people. Meaninful workIts amazing that I can earn a regular average wage doing work like this - so stability is a huge benefit.    |   
| 4. What do you get back from the wider world for doing your job? what sorts of things do you work to get from outside of your job? | Respect from my profession. Survival. Political role. Belonging and self esteem.I get to live in barcelona, I get to independence and I hope to be able to invest back into my craft  |   


**Observations:**

She mentioned **a new parameter for data** - "sacrifices" - things you have to give up that you will not get back.

#### 1d. Data Strings to see flows of 'investments'/costs to wants/needs across more than one job

To capture experiences of a wide number of jobs I formulated an updated set of parameters to map jobs as investments, based on my initial inquiries.

I thought I could map:

- Values that guide your work
- Sacrifices you make to do your job
- Investments you make into your job
- Returns you get from your job
- Returns you seek from the wider world

I added some theoretical options for each of these parameters using post its to enable the list to not be exhaustive in case the user felt there was anything not covered.

This led to the following data string board

![Datastrings](assets/w8/datastrings.jpg)

**Observations:**

- I received **new options** to help me refine the tool. I was able to add items to sacrifices that I might not have thought of such as "comfort". I realised that this method was really a way of collecting more ideas on the parameters that describe the investment of work.

-  Visualising more than one job together **made me imagine new ways of showing data** on how investments and returns of different jobs are interrelated.

- By doing a data string for myself I realised that I have changed careers several times to try and 'tweak' the investments and returns of work. The desire to explore this subject may be due to the need for me to gain some clarity on personal experiences. **I found something about my motivations for doing the project.** I realised that maybe my search for finding a way of understanding how different jobs feel and are interrelated is in one way a search for validation of all the different roles I have worked in.

### 2. Materialising the concept of work
I wanted to look for an analogy that might materialise the concept of work as something you invest in with a hope in getting a return. I would want to use that analogy to help people understand how different jobs feel to different people and that your returns require someone else's investment into a job not just money. I wanted to remove the sense of entitlement that money can give you.

#### 2a. Understanding what money is

* Numismatist Joe Cribb has looked at the different metaphors that money has been used for - including: **Justice, Order, Time and Power as metaphors for money**.

* **Time as currency - What if work was rewarded in time**

  <iframe width="560" height="315" src="https://www.youtube.com/embed/5K0s6eveTsI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* A money-free economy may still need a unit of exchange: in The Great Explosion by Eric Frank Russell, the Gands use **favor-exchange** based on "obs" (obligations)


#### 2b. Work and machines
* In physics a 'machine' is any device where you can apply a force to move some object and work done by the machine is the force times the distance (in the direction of force). Machines do work so they seem like a good literal place to start.
Can we make a machine where you have to invest time and sacrifice something in order to get something else you want or need  - mediated by some sort of other concurrent symbolic system (representing money)?

![Speculative Work Machine](assets/w8/Machine.png)

Machines have been used to demonstrate economics before.

<iframe width="560" height="315" src="https://www.youtube.com/embed/iUrhENgn_m0?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*<font size="2">In 1949, the economist Bill Phillips constructed a machine that modelled the British economy – using water.</font size>*

![Chester Model](assets/w8/einstein60.png)
*<font size="2">
David Harold Chester devised this mechanical model for teaching macroeconomics.</font size>*


#### 2c. Work as a strategic game
* A very obvious and overused analogy is a game where you have to stay on track to do something to ultimately get what you want (like many platform video games)



## Reflection on the week

### Finding bad practices
* I found myself asking bad questions which led to some overengineered responses.
* I made a some bad unresearched analogies about the function of the brain being something that functions like a computer.
* The protest balaclava went from an idea to a terrible idea very fast.

### Questioning objectivity
These methods oppose the objectivist view of reason being the source of all valid decision-making data and supports the view that human experiences are unique.

### Using time as an agent
Experiencing phenomena and understanding take us humans time - time helps build context and improves chances of insight.

#### Unpacking behaviour and motivations
Taking the time to record experiences helps us unpack information contained in them

#### Experimenting intuitively
The explorations of this week reinforced my wish for my outputs of explorations being made objects. This is because they spark interesting avenues to explore in the process of building them.

#### Dynamic and generative processes
We were advised to try jumping between levels of examination - from personal to cultural to global or from material, to the body to the system. This method should be dynamic and generative.

### Tools to work across scales of influence
Tomas explained the thinking behind the path of inquiry within the MDEF course. We can intervene by aligning systems rather than creating new systems to insight changes

### The importance of leveraging your interests

You might not know why you are interested in something but by inquiring through experience you may find some sub-conscious reasons behinds your interests. I would like to try some first person design projects using interests that I am trying to organise [here](https://mdef.gitlab.io/saira.raza/interests/).


## References

### Websites

* http://wearethe99percent.us/main/page_home.html http://wearethe99percent.us/main/page_graphics_info_graphics.html
* https://medium.com/thrive-global/maslows-hierarchy-of-needs-5b021e203e34
* http://www.openlearningworld.com/books/Job%20Design%20and%20Enrichment/Job%20Design%20and%20Enrichment/Approaches%20to%20Job%20Design.html
* https://medium.com/@georgekao/money-metaphors-63244056ef19

### Research Papers

* HOME ARCHIVE MARCH + APRIL 2016 A SHORT GUIDE TO MATERIAL SPECULATION: ACTUAL ARTIFACTS FOR CRITICAL INQUIRY Ron Wakkary, William Odom, Sabrina Hauser, Garnet Hertz, Henry Lin
* What Motivates Us for Work? Intricate Web of Factors beyond Money and Prestige - Nadja Damij, Zoran Levnajić,  Vesna Rejec Skrt, Jana Suklan, Matjaz Perc (2015)
* Maslow AH. Personality and motivation Harlow: Engl Longman; 1954
* Herzberg F. Workers’ needs: the same around the world. Ind Week. 1987;21(9):29–30.
* Ongan, Serdar. (2009). The Economy Machine. 11.
* A MECHANICAL MODEL FOR TEACHING MACROECONOMICS By: David Harold Chester
* 'Money as Metaphor 1: Money is Justice', in Numismatic Chronicle, (2005); '2: Money is Order', in Numismatic Chronicle, (2006); '3: Money is Time', Numismatic Chronicle (2007); '4: Money is Power', Numismatic Chronicle (2009) - Joe Cribb

### Books

* Bullshit Jobs - A Theory By David Graeber
