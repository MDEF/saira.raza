# 11. From Bits to Atoms

**Course materials:**
[https://bitsandatoms.gitlab.io/site/](https://bitsandatoms.gitlab.io/site/)


## Fast Prototyping - Scribble Sound

**Challenge:** As a team, design a kinetic machine to learn new processes. Master some mechanical outputs and inputs.

**Team Scribble Sound:**
Julia Quiroga, Saira Raza, Silvia Ferrari, Gábor Mándoki, Oliver Juggins and Ryota Kamio

**Team output site:**
[https://mdef.gitlab.io/scribble-sound/](https://mdef.gitlab.io/scribble-sound/)

**Concept**

The original concept was a machine that allowed you to draw onto a moving surface which was then read by a light sensor that would return a sound depending on some coding.

**Mechanisms we thought about included:**

* A turntable with concentric circles where each circle played a different part of a song and you could alter these by drawing or adding templates.
* a sandbox that allowed detection of different heights of sand that would alter the sounds played in different areas of the sandbox (no moving parts).
* A conveyor belt with different sections (like on a stave of sheet music) that you could draw onto and the drawings would be read and transformed into sound.

**The concept we chose was the conveyor belt mechanism.**

**My individual contribution to this project was to:**

* Put together a first draft schematic of equipment needed
* Get advice from Santi on what available motors, fittings and power sources to use for the mechanism.
* assemble the motor-axle-bearings assembly and motor's power source circuit.
* Preparation of laser cutter for first cut of housing.
* Attaching spindles to axles for first prototype.




### Other inspiring Ideas:

We saw some inspiring projects of creating machines including:

* Theremins (interesting control design and actuator design)
* Sonification of ordinary objects
* Bubble surfaces to create interesting shapes

Some other ideas I like include:

* Kinetic artworks involving propellers and optics
* 3D printing projects involving Brains scans
* Art about data like the art of [Thijs Biersteker](http://thijsbiersteker.com/)


## Introduction to Fab Academy

I was excited to hear about the upcoming Fab Academy and that it has:

* Cross disciplinary approach to making
* Self directed, self motivated, personal technical projects
* Distributed Educational model
* 20 weeks, 19 topics and a final project
* Everyone tunes in on Wednesday afternoon spain time
* Makers collaborative community and international shared experience
* Teaching community (online opensource outputs)

#### Specialist academies

Fablab also runs some specialist academies including:
* Academany project - a platform for teaching for distributed courses
* Bio Academy
* Fabriacademy (fabrics)


#### Tips for completing Fab Academy:

* Get references from fab academy, check out http://archive.fabacademy.org/ to see if you can build on existing projects
* Lectures from last year are available on Vimeo http://archive.fabacademy.org/archives/2017/master/lectures/index.html
* Don't worry about aesthetics in the first instance
* This years programme is documented here: http://fab.academany.org/2018/
* You can prepare for the course and learn code or software in advance using the course docs on http://fab.academany.org/2018/  and  http://fab.academany.org/2018/labs/barcelona/
* Documentation takes a long time so start early and get used to it.
* Make it personal and expressive - so you enjoy making things.

#### Assessment
* Is based on documentation because it adds to a repository of processes, fails, experiences.
* Assessors from other fab labs check your work and evaluate. Its a blind assessment.
* Documentation needs to make process replicable but not step by step. Must include files, results. Explain the particulars.
* As part of the MDEF we will have from June until the end of the next cycle to complete the final assessment.



## How can I use the ideas from this week

### Fab labs in refugee camps
Santi mentioned that there is a fab lab in a refugee camp which I was interested to hear about as I have been researching innovations made by refugees this year.

### Fractals in construction
I have been interested in the mechanical properties of fractals during this course and would be interested in exploring how accessible digital fabrication can help us to experiment with these on our scale.


### Augmented record players
This has been an idea I have been thinking about from early in the course to explore the experiential properties of record players - in the vein of Kinetic / Machine Art
