# 04. Exploring Hybrid Profiles

## Learning from existing hybrid designers

This week we visited designers who work by combining interests and expertise.

### Workshops
<font color="fuchsia">**[Sara Gonzalez de Ubieta]( http://www.deubieta.com/)**</font>
is currently designing footwear for companies, is cultural director of a museum, directing a masters course, lecturing and is also a tutor on another course. She began her career as an architect and her interest in shoemaking led her to gaining knowledge in classic techniques in the field which enabled her to see where best she could find new techniques through experimentation.

Sara invited us to spend some time trying and find ways to turn building insulation material into footwear using some basic materials. This was very enjoyable and we were able to feel something of what motivates Sara to do what she does - experimenting, deconstructing and constructing.

 ![Sara explaining](assets/w4/Sara.jpg)   ![Sara with shoe](assets/w4/sarawithshoe.jpg)  ![Square shoe](assets/w4/squareshoe.jpg)
![Insulation Shoes](assets/w4/shoeslabelled.jpg)

**[Domestic Data Streamers](https://domesticstreamers.com/)** are a communication company that wanted to stand out amongst the "scrolling culture" of today. They find innovative ways to find and represent data that tell stories. They pride themselves on being cross-disciplinary - taking methods from psychology and sociology as well as product design and engineering. Pol and Marta explained how each of their careers took different paths but were able to weave together their expertise at different times to build the practice. The practice's openness to use different expertise has allowed them individually to gain experience and education across different disciplines and bring these back to the practice.

They invited us to tell a story about hidden data in objects that we had on us. The results were very engaging and sometimes moving as people tried to convey qualities of memories and feelings into some sort of structure.

![Domestic](assets/w4/domestic.jpg)

![Domestic Studio](assets/w4/domesiticstudio.jpg)

### Points of attraction from these practitioners.

It was easy to feel the skills, knowledge, attitudes or ways of working that resonated to where I wanted to be or what I find energising. I have summarised these below.

| Practitioners:  | [Sara Gonzalez de Ubieta]( http://www.deubieta.com/) | [Domestic Data Streamers](https://domesticstreamers.com/) | [Ariel Guersenzvaig](http://www.elisava.net/en/center/professorate/ariel-guersenzvaig) (from reading only) |
| ------------- | ------------- | ------------- | ------------- |
| **Profile:**  | From architecture to shoe making and material experimentation  | From product design to data enabled communication.  | From usability consultant to design researcher on ethical digital transformation.  |
| ***Attractive Skills:*** | Learning through making and rapid prototyping  | Getting people to access the unseen (immeasurable/ indescribable) through their work  |   |
| ***Attractive Knowledge:***  | Traditional practices that we largely relying on  | Grounding in R&D <br> <br> Use of ethnographical methods to understand contexts well |   |
| ***Attractive Attitude:***  | Dedication to background research (you can’t know what’s new unless you know what has been done) <br> <br> Doing counterintuitive things| Committed to mutating fields of knowledge and problem solving <br> <br> Being comfortable not knowing what you can call yourself as you start your practice.  |    ||


## Defining who you are

### Using points of attraction to existing practitioners
We were asked to try and use the skills, knowledge and attitudes that we liked of the designers we visited as a starting point to develop some  points that we wished to define our own practice. Below are 3 points that I felt set something of a framework for a direction.

* Committed to <font color="fuchsia"> combining fields of interests in research and experience </font>
* <font color="fuchsia">Grounded in research </font> - of people, environments, systems, materials, tools and techniques available and of first, second and third hand experiences.
* <font color="fuchsia">Making real things </font> that test a system

This exercise has been very useful and has extended to a separate page on this website where I am collecting "attraction points" and working on finding relationships between them.


### Focus areas
We then considered what areas needed the most work

| Achieved (or almost) | Yet to be achieved |
| ------------- | ------------- |
| Commitment to combining fields of research and experience | Making real things that work - currently only achieved in a few fields and looking to learn new techniques and tools
  |  | Grounded in research – looking for new tools e.g data science, GIS |

### A personal development plan for term 1

We looked at all the courses in term 1 to see what we could use from each module to improve on our focus areas.

| Week  | Content| Goal 1: Making real things that work  | Goal 2: Grounded in research –more tools – e.g data science, GIS | Goal 3: Commitment to combining fields of research and experience|
| ------------- | ------------- | ------------- | ------------- | ------------- |
| W1_MDEF Bootcamp  |   |  | use this opportunity to understand the extended classroom that is Poble Nou and Barcelona  | Learn as much as you can from your peers  |
| W2_Biology Zero  |   | Participate in Labs to understand the viability of being able to participate in Biology, work with Biologists or use Biology in projects  | Practice the scientific method and ways to present data  | Research crossovers between biology and other fields of interest and practitioners who use biology in other fields  |
| W3_Design for the Real Digital World  |   | Learn how to use the digital fabrication tools available at IAAC  | Research how other designers respond to similar design problems. Learn from your peers  | Learn how your peers approach problems and ways of working that worked best|
| W4_Exploring Hybrid Profiles in Design  |  | See how practicing designers produce things  | See what tools practicing designers use | Ask practitioners what ways of working work best  |
| W5_Navigating the Uncertainty  | sessions organized with experts are aimed to give students the framework to understand the resistance to change in society and find ways to overcome them.  | Learn tools that help to visualise relationships between types of data and to illustrate interests | Find the journals and communities that align with the directions of inquiry that resonate |  |
| W6_Designing with Extended Intelligence  | explores the development of artificial intelligence and its close connection with design from its very beginning.   | Try to find out what we can do to design AI ourselves - how far can we be involved, what is there that is open source. What can I design using AI | How can we learn about AI systems - are there any recommended resources to understand  the tools needed? |   |
| W7_The way things work  | Hacking everyday objects | <font color="fuchsia">This will be a key week for developing making skills |   |  |
| W8_Be Your Own System  | challenges for quickly embodying concepts, and addressing them through lived experiences.  |  | Develop data skills - how to record experiences |   |
| W9_Engaging Narratives  |  | Communication for behaviour change | What research is there into storytelling? <font color="fuchsia">Narratives are used for recording experiences and are a source of qualitative data | Narratives are excellent bridges between different fields of expertise |
| W10_An Introduction to Futures  | Futures Theory and Futures Thinking  |   |   |   |
| W11_From Bits to Atoms  |    | <font color="fuchsia">This will be a key week to learn how information can be made material  | What journals can we follow |  |
| W12_Design Dialogues  |   |   |  |   ||

## Direction
As a class we considered three focus areas to drive direction of projects: Vision, Reflection and Experience.

We were presented with the idea of the renaissance man who has vision and uses analytical skills as well as making skills.
Working in this way is more open and allows multiple entry points to answering a question.

We read a paper about the experience of one designer who tried to respond to a problem from a first, second and third person's point of view.
I found the paper interesting as I have worked projects using a second point of view approach which had several limitations including difficulties in ensuring there is good representation of different sorts of users sometimes stemming from cultural barriers. The first person approach, much like a business approach, could be more efficient way of producing a fast prototype, is a familiar method of design and may be better at getting honest feedback.
This direction is appealing in that it is driven by producing working prototypes using available resources within a system and then invites the system to adjust around it.

## My Vision

We pooled our ideas on what we thought a vision was and each tried to articulate our vision for our future design selves to each other.

My attempt so far reads as:

><p align="center">
"A future where decision making for managing global resources is inclusion-focussed and rights-based".
</p>
