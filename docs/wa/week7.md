# 07 The way things work

**This week we learned about communication from inputs to outputs - through electronic circuits, flow diagrams, wifi protocol and code.**

Guillem Camprodon, Victor Barberan and Óscar González helped us to break down components of readily available appliances to see how ordinary things work. It was an empowering exercise to literally break open 'black boxes' that we may get used to as being unbreakable. In breaking them open we were given some ideas and skills to help us:

* avoid 'reinventing the wheel' and save time
* save buying new materials when making systems
* build fast and cheap prototypes
* avoid wasting existing components.
* promote experimentation to come up with new interesting systems with unexpected results.

## Inputs and Outputs, Sensors and Actuators

### What's inside a printer?

![printer parts](assets/w7/printerpartslabelled.jpg)

I enjoyed being able to open and find components first hand.
Victor helped us find numbers of components using a camera
![checking parts](assets/w7/checkpartsml.jpg)


### Sensors

We were introduced to the huge number of sensors that are available and that we can construct today.
WE looked at standard electronic components as well as some DIY sensors.

![sensors](assets/w7/sensorskit70.jpg)

With the help of a relatively cheap Arduino we can detect very small signals and changes in signals and counting in milliseconds enables effective amplification of these. This opens up a wide number of things that can be turned into sensors such as textiles  

<iframe width="560" height="315" src="https://www.youtube.com/embed/uPoKn4mbrQk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Actuators
We were shown a number of interesting experimental actuators as inspiration for machines we could create.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y5kZO8SSxVw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### New ways of understanding data

Collisions from the ATLAS collider at CERN turned into sound

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FATLASexperiment%2Fvideos%2F609292082821487%2F&show_text=0&width=476" width="476" height="476" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>


"[S I G N U M](https://victormazon.com/wrks/workshops/signum/) is a portable device designed to provide a clean, line level & micro controller friendly output signal to various input sensors"

<iframe src="https://player.vimeo.com/video/142831757" width="640" height="360" frameborder="0" allowfullscreen></iframe>

The Rite of Spring by Keiichi Matsuda (Arcade) and James Alliban (Arcade). 50 lasers, each linked each one to an instrument - by a small piezo (contact microphone), which ran to a circuit board, and then to a laser diode. No computers were used

<iframe src="https://player.vimeo.com/video/72695667" width="640" height="346" frameborder="0" allowfullscreen></iframe>



### Musical Instruments

I was interested in trying to build a musical instrument because they are interesting and entertaining from a control point of view (as a musician) and from an output point of view (as a listener).
However electrical signals produced from an Arduino often need an amplification stage in order to be audible through a speaker.

**Hydraulophones - underwater instruments**

<iframe width="560" height="315" src="https://www.youtube.com/embed/GWmiBVndVMY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**Vocal chords**

Endoscopic view of vocal chords at work

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z_ZGqn1tZn8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Talking robot vocal chords

<iframe width="560" height="315" src="https://www.youtube.com/embed/HmSYnOvEueo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Simulating vocal chords using gel

<iframe width="560" height="315" src="https://www.youtube.com/embed/lOJAWOK1RTs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Computers

Computers are not binary by default and silicon transistor based computers are not the only type of computers. You can make analogue computers with gears. We choose binary to represent 2 states.
We used to use the decimal system (working with 10 states) but the voltages required could not be reproduced accurately.
Quantum Computers use quantum bits or qubits, which can be in superpositions of states.

![Babbbage's Analytical Engine](assets/w7/babbage70.jpg)

*Babbage's Analytical Engine completed in 1822*


### Bits and compression
1 bit is an answer to a yes no question or a 1 or a 0 signal.
Therefore the number of possibilities of answers to a series of questions is 2 ^ iterations of questions.
Each 8 bit pixel contains 2^8  x 2^8 x 2^8  possible outcomes.

**Compression** is when a series of 1s and 0s are packaged because the 1s and 0s re repeating or if there is a pattern that can be generalised as a smaller pattern.

### Arduino


### ESP8266

The ESP8266 is a cheap Wi-Fi microchip with full 'TCP/IP stack and microcontroller capability' (internet capability)
This small board requires a 3.3V input. It only has one analogue input channel.

![ESP8266](assets/w7/ESP8266.png)


## The Internet

>When wireless is perfectly applied the whole earth will be converted into a huge brain, which in fact it is, all things being particles of a real and rhythmic whole. We shall be able to communicate with one another instantly, irrespective of distance. Not only this, but through television and telephony we shall see and hear one another as perfectly as though we were face to face, despite intervening distances of thousands of miles; and the instruments through which we shall be able to do his will be amazingly simple compared with our present telephone. A man will be able to carry one in his vest pocket.

*Nikola Tesla, 1929*

Television was thought to be a technology that would eventually allow 2 way visual communication but only with internet was this technology brought to the masses.

![Videodrome](assets/w7/videodromeagain.jpg)  ![Poltergeist](assets/w7/Poltergeist232.jpg)

*Above: David Cronenberg's Videodrome (left) and Steven Spielberg's Poltergeist (right). 2 way TV communication only made it as far as surreal and horror fiction in the 1980s.*

With fibre optics you can send information at high speeds. The speed is fast enough that many senders can package information label it and send it down fibre optics networks at the same time. This felt to me analogous to using cars rather than trains (as telephone or telegraph lines were used) - allowing many more journey options and less scheduling.

**The cloud is not a cloud** Information is stored in servers and travels through the internet through networks of fibre optics.  These networks can be centralised, decentralised or a distributed network.

The first server was built by Tim Berners Lee for CERN to save documentation and CERN httpd  was the first web server software. This led to his development of html and the http protocol, storage servers and browsers.
He decided to opensource html which was key to improving it and led to the development of different browsers.


### Opensource

The internet was based on opensource code adhering to Copyleft rules:

0. start at zero: freedom to run programme as you wish for any purpose.
1. freedom to study how the programme works - access to source code mandatory and you can modify as you wish. (like prose , motifs in arts)
2. freedom to redistribute to help your neighbours. you can control and not distribute? you can sell it too
3. freedom to copy - can distribute other peoples work.

The internet is an example of an **inclusive system which is self-improving**. Opensource and Copyleft principles encourage more people to try solving problems and help to improve the system. Opensource Linux operating systems are used by  large companies such google, instagram, duck duck go, tesla cars and github.

### Network protocols

Modern computer networking protocols send and receive messages in the form of packets — messages with sections of information that are assembled at their destination. There are protocols for Internet, wireless networks, network routing

#### MQTT
>MQTT is a machine-to-machine (M2M)/"Internet of Things" connectivity protocol. It was designed as an extremely lightweight publish/subscribe messaging transport.

![MQTT](assets/w7/mqtt60.png)

## Joining Inputs to Outputs

The aim of this week was to produce a range of inputs and outputs that would be controlled by Arduinos, ESP8266s and raspberry pi servers.

### Electronic Circuits - hardware inputs and outputs

We were shown how to use Arduino example coding to test the control of components. Having decided on working with sound outputs our team tested some speakers using an Arduino blink test.

Gabor continued working with Arduinos on finding ways to control elements like pitch.

I looked for actuators that could produce sounds.
I found a 12v peristaltic pump. Inspired by videos of human vocal chords I wondered if it might produce some airflows that could make sounds. I connected the pump to an Arduino through a relay switch and its own power source.

![Pump Circuit](assets/w7/Peripumpcircuit.jpg)

This did not produce much sound by itself. The airflow flow was too low - even to blow air bubbles through water or soap bubbles through air.

Guillem suggested that I borrow an air compressor and try to experiment with this. I found that the airflows produced made a strong horn sound when blown across a glass bottle so I fixed the bottle and pipe together. This was connected to the air compressor via 24v servo valve which needed its own 24V power source. A relay controlled the power source to the servo valve.

![bottle blower](assets/w7/bottleblower.jpg)


### Flow charts - software inputs and outputs

Flow charts help to set out a map of the logic required before coding.

![flow chart](assets/w7/flowchart.jpg)

Victor used https://nodered.org/ which has a flow diagram interface to programme a raspberry pi server from which my actuator was to receive signals.

### Code

The relay and valve were tested using an Arduino programmed with a blink test.

The Arduino was later replaced by an ESP8266.
The ESP8266 was programmed to connect via Wifi to an MQTT server (a raspberry pi).
This server enabled input signals from various devices to operate  the relay switch through the ESP8266.
The relay stayed on for a short time if it received any input at all.

## Reflection
This week strengthened my desire to make functioning objects as part of my work to:
* 1. keep breaking 'black boxes' to unpick and learn about the information and technology that is contained in things.
* 2. learn ways to be more resourceful to enable me to design less wasteful and more useful things.

Machines have always been something I have wanted to understand in a hands on way and I would really like to use this year to delve deep into what can be understand about technology this way.

I hope to become more fluent in electronics and coding as this course progresses.

Learning about the development of the internet demonstrated the benefits of open inclusive systems of design which is a direction I continue to wish to explore.


## References

### Websites
Module website: https://hackmd.io/ys7lcCDJSeq69meSeGnIXg#References

### Reading List

* Understanding Media The Extensions of Man By Marshall McLuhan
* Handmade Electronic Music: The Art of Hardware Hacking, Collins, Nicolas, 2006
* Designing for Emerging Technologies: UX for Genomics, Robotics, and the Internet of Things, Follett, Jonathan, 2014
* Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy, O’Neil, Cathy, 2016 (ISBN: 553418815)

Module reading list: https://www.goodreads.com/review/list/70336932-pral2a?order=d&per_page=infinite&print=true&shelf=mdef-the-way-things-work&sort=rating&utf8=%E2%9C%93&view=covers

### Connected communities

* http://archive.fabacademy.org/
* https://hackoustic.org/
* https://www.instructables.com/
