<head>
<title></title>
<style>

.img-shadow{
  display: block;
  margin-left: auto;
  margin-right: auto;
  vertical-align: middle;
  border-style: none;
  box-shadow: 10px 10px lime;
  margin-bottom: 40px;
  margin-top: 30px;
}

img {
  vertical-align: middle;
  border-style: none;
  box-shadow: 10px 10px lime;
  margin-bottom: 40px;
  margin-top: 30px;
}
.col-a {
width: 30%;
float: left;
padding-right: 5px;
}

.col-b {
max-width: 65%;
float: left;
padding-right: 5px;
}

#biotable {
border-collapse: collapse;
width: 100%;
max-width:1000px;
font-size: 0.7rem;
}

#biotable td, #biotable th {
  border: 1px solid #ddd;
  padding-top: 4px;
  padding-right: 4px;
  padding-bottom: 4px;
  padding-left: 8px;
  min-width: 180px;
}

#biotable th {
  text-align:center;
  vertical-align: middle;
}

#biotable td {
text-align: left;
padding-left: 8px;
}



#biotable img {
    vertical-align: middle;
    margin-bottom: 4px;
    margin-top: 4px;
    box-shadow: none;
    display: inline-block;
    max-width: 200px;
  }
}

</style>
</head>


# 02. BIOLOGY ZERO

## Reflections

### The evolution and structure of matter

<p align="center">
</p>

![mindmap](assets/w2/mindmap.jpg)

**Living and synthetic matter as the cutting edge of matter**
<p> I had never seen Biology presented in the context of all matter before and looking at the journey of known matter from the Big Bang to human self-awareness helped me appreciate how Biology studies the newer forms of matter we know of and synthetic matter is the newest kind of known matter that exists in the universe.  </p>

**The shape and charge of molecules make life happen**
<p>  I was struck by how the building blocks of all life fall into only 4 functional groups of charged macromolecules whose shape and charge determine how they interact and form cells. </p>

**Order, complexity and what seems like chaos exist on many scales**, suggesting the possibility of some sort of similarity across everything in space time and everything actually being ordered but seeming chaotic when we cannot perceive the order.

<!--
*[Here](https://learn.genetics.utah.edu/content/cells/scale/)* is a depiction by the Genetic science Learning Center, University of Utah of the bewildering sizes of dissimilar looking microscopic entities  
![gif of scales](https://media.giphy.com/media/n7C9uT8dAYH4s/giphy.gif)
--->

<div class="block">
<span class="floatright">
<img alt="gif of scales" src="https://media.giphy.com/media/n7C9uT8dAYH4s/giphy.gif">
</span><br><br>
<em><a href="https://learn.genetics.utah.edu/content/cells/scale/">Here</a></em> is a depiction by the Genetic science Learning Center, University of Utah of the bewildering sizes of dissimilar looking microscopic entities
</div>


**Geometric similarity appears across different scales in systems concerned with growth**.


<div class="block">
<span class="floatright">
<img src="../assets/w2/fractalbacteria.jpg" width="100%">
</span>
<br><br>
Jim Hasseloff Lab at the Department of Plant Sciences at the University of Cambridge genetically modified bacterial cells with different coloured proteins and found self similar fractal order during growth of bacteria cells
</div>

This image by Bruce D. Malamud, Kings College London shows self-similar river network from the Shaanxi province in China. The scale is 300 km across with colours representing elevation.

<div class="block">
<span class="floatright">
<img src="../assets/w2/DrainageMalamud.jpg" width="100%">
</span><br><br>

</div>





### Synthetic Biology
 ><p align="center">
 "If consciousness is natural why are synthetic organisms unnatural?".
 </p>

This question was put to us by our teacher. I hypothesised that nothing is unnatural but not everything is sustainable. This leads some questions
 - Do we want to prolong individual human lives or do we want to become more efficient organisms needing less resources and producing less waste?
 - If we want to sustain life on earth, what evolutionary jumps should we assist?

We may be forced to address these issues for human survival - but we may not have time to address them if we wait. As human population increases and our activity becomes more complex it becomes harder to predict the wider implications of our choices.


### Genetics
 ><p align="center">
 "Life is about copying".
 </p>

The simplistic definition of life as consisting of birth, reproduction and death implied to me that **information and the transcription of information is what makes cells live** - enabling a script or order of instructions to be carried out. It was poignant to learn how this potential for the life is stored in a protected DNA double helix, chromatins, a superhelix and then chromosomes, locked and unlocked by molecular machines triggered by chemical messengers i.e charge and structure.

![Crick](assets/w2/crick.png)

<p align="center">
Notes by Francis Crick on the central dogma. Early draft for article published as: Crick, F.H.C. (1958): On Protein Synthesis.
</p>

<br><br>
<div class="block">
<span class="floatright">
<figure>
<iframe width="560" height="315" src="https://www.youtube.com/embed/7Hk9jct2ozY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
</figure>
</span>
Coming from a background of mechanical engineering, I found it startling to see in the animations of Drew Berry - proteins inside our cells behaving like mechanical machines.
</div>
<br>

<div class="block">
<figure class="floatright">
<img style="-webkit-user-select: none;margin: auto;background-color: hsl(0, 0%, 90%);transition: background-color 300ms;" src="https://i.imgur.com/Uc4UyfT.gif"></figure>

Here is a gif of the synthesis of ATP the biological unit of energy showing a molecular world that looks like factory machinery.
<br><br>
Source: https://imgur.com/gallery/Uc4UyfT

</div>

<br>


It was amazing to use the polymerase chain reaction and electrophoresis to isolate a gene from our DNA samples in our lab - knowing that this machinery was occurring at such a small scale in the solution.


 ![DNAlab](assets/w2/Pipetting DNA Samples.jpg)

<!---  
![Transillumination](assets/w2/TransilResult.jpg)
<p>
All living things alive today come from information from another once-living thing - a common ancestor called Luca (last universal common ancestor)
</p>  -->

<div class="block">
<span class="floatright">
<figure>
<img alt="Transillumination" src="../assets/w2/TransilResult.jpg">
</figure>
</span>
<br><br>
<strong>Questions about genetic engineering</strong>
<br><br>
We already have <a href="http://parts.igem.org/Main_Page">
repository of genetic parts</a> (instructions for getting living cells to do things) and we have reached a point where we can create DNA from scratch so we don't need to copy it any more.
<br><br>
If we were to change anything in the DNA of a living thing, the change will be very fast in evolutionary terms.
<br><br>
What would be the impact of the changes to the lifetime of the organism, to the species, ecosystem, climate and geology?
<br><br>
Will ecosystem be able to adapt in time to sustain species at the current rate or will we need sacrifice some in favour of others?
</div>

### Optics

<p> Nothing inherently has colour. Molecular structures alter the path and/ or frequency of light which we can perceive as different colours (different frequencies). We use this property in Spectrophotometry to detect more or less concentration of a substance (or structure) in a solution.
X ray crystallography uses how different structures scatter x rays in different patterns.</p>

<p>
Light, molecular structures and the red, blue and green cone cells behind our retinas give us a colourful experience of our surroundings. In Microscopy we can alter light hitting or leaving structures and the detection abilities of our eyes via lenses, sensors and computers </p>


### Microbiology

<div class="row">

<div class="col-a">
<img src="../assets/w2/Isolation.jpg" max-width="100%">
<figcaption><strong>Isolation of microorganisms</strong>
</figcaption>
</div>

<div class="col-b">
<img alt="Isolation" src="../assets/w2/Identification.jpeg" width="100%">

<figcaption>The shape of colonies can help to identify species.
<strong>Identification techniques using microscopy</strong>
Colony morphologies (source: https://microbiologyonline.org)
</figcaption>
</div>

</div>


<!--
**Isolation of microorganisms**
<center>
![Isolation](assets/w2/Isolation.jpg)
</center>
**Identification techniques using microscopy**
The shape of colonies can help to identify species.
![Identification](assets/w2/Identification.jpeg)
<p align="center">
Colony morphologies (source: https://microbiologyonline.org)
</p>
--->




<p>
Gram staining can also help to identify different types of membranes in bacteria.
</p>


**Microbiomes** are non-homogenous communities of microorganisms resulting from the aggressions of certain environments. They can be used as a tool to indicate environments such as water resources of a certain quality, and humans that eat certain foods.

**Growing microorganisms** is possible by providing the best nutrients and conditions for the organism to grow and then trying to separate them from any other organisms present.

Spirulina can be cultivated using household equipment and ingredients such as Sodium Bicarbonate, Potassium Nitrate, sea salt, Potassium Phosphate and Iron Sulphate (from iron in vinegar) to mimic conditions in volcanic soda lakes where they are naturally found.


![Lab Spirulina](assets/w2/DIY Spirulina.jpg)


On learning that human urine can be used as a source of Potassium, Nitrogen and  Phosphate for spirulina, I wondered if an illuminated, temperature controlled, greenhouse-bioreactor / urinal could be designed.

<!---
![Urinal](assets/w2/spirulinaurinal.jpg)
![Windowbox](assets/w2/windowbox.jpg)
--->

<img alt="Urinal" src="../assets/w2/spirulinaurinal.jpg" width="80%">

<img alt="Windowbox" src="../assets/w2/windowbox.jpg" width="80%">

Flamingos eat spirulina in their natural environment and use their beaks as a filter

<row>
<div class="row">
<img alt="Flamingo" src="../assets/w2/flamingonet.jpg" width="40%">
<iframe width="400" height="315" src="https://www.youtube.com/embed/3YkP60bMTDs?controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="">
</iframe>
</div>
</row>


### The Scientific Method


![Scientific Method](assets/w2/scientificmethod.jpg)


### Some scientific techniques used in Biology

<div style="overflow-x:auto;">
<table id="biotable">

<tr><th>Technique</th>
<th>What it's used for</th>
<th>DIY Equipment</th>
<th>Professional Equipment</th>
</tr>

<tr>
<td>Media for growing cultures</td>
<td>Optimises conditions for growth often using pH and nutrients needed by cultures</td>
<td><img src="../assets/w2/DIYmedia.jpg" alt="DIY Media" width="200"></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td>Sterilisation by autoclave</td>
<td>Killing microorganisms that might contaminate the samples.</td>
<td>Pressure cooker for equipment, bunsen burner for instruments, alcohol for hands <img src='../assets/w2/pressurecooker.jpg' alt='Pressure cooker' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td>Stricking</td>
<td>Isolation and identification of cells</td>
<td>Spreading out samples into growing medium in smaller concentrations.</td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> Indicators</td>
<td>Enabling observation of specific cells or structures or conditions</td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> Spectrophotometry</td>
<td>to measure concentration of a substance in a medium</td>
<td><img src='../assets/w2/Spectrophotometer.jpg' alt='DIY Spectrophotometer' width="200"></td>
<td></td>
</tr>
<tr>
<td> Incubation</td>
<td>To optimise growth conditions for cultures using heat</td>
<td><img src='' alt=''  width='200'></td>
<td></td>
</tr>
<tr>
<td> Microscopy</td>
<td>To observe forms that we cannot see with a naked eye</td>
<td><img src='' alt=''  width='200'></td>
<td><img src="../assets/w2/Profmicroscope.jpg" alt="Professional Microscope" width="200"></td>
</tr>
<tr>
<td> Centrifuge</td>
<td>Separates molecules by size</td>
<td><img src='../assets/w2/drillcentrifuge.jpg' alt='Drill Centrifuge' width='200'></td>
<td><img src='../assets/w2/professionalcentrifuge.jpg' alt='Professional Centrifuge' width='200'></td>
</tr>
<tr>
<td> Serial dilution using a pipette</td>
<td>Changes concentrations of solutions</td>
<td><img src='../assets/w2/serialdilution.jpg' alt='DIY Transillum width='200'></td>
<td><img src='' alt=''  width='200'></td>
</tr>
<tr>
<td> Stoichiometry and titration</td>
<td>To determine the amount mass or mols) of a molecule or element in a sample using ratio of elements or molecules in a chemical reaction</td>
<td><img src='' alt='' width='200'></td>
<td><img src='https://www.sigmaaldrich.com/chemistry/stockroom-reagents/learning-center/technical-library/mass-molarity-calculator.html' alt='Mass molarity calculator' width='200'></td>
</tr>
<tr>
<td> Isolation</td>
<td>separation of a strain from a natural, mixed population of living microbes</td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td>Identification</td>
<td></td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> Gram staining </td>
<td>to indicate different types of membranes in bacteria</td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> PCR Polymerase Chain Reaction</td>
<td>to copy genes</td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> PCR machines (thermal cycler)</td>
<td>Facilitates temperature-sensitive reactions</td>
<td>Waterbaths at different temperatures <img src='' alt='' width='200'></td>
<td>This is a small one<img src='../assets/w2/PCR_cyclic_incubator.jpg' alt='PCR Cyclic Incubator' width='200'></td>
</tr>
<tr>
<td>Electrophoresis</td>
<td>To separate charged macromolecules like DNA and RNA by size, or to estimate the size of DNA and RNA fragments or to separate proteins by charge</td>
<td>Dangerous but possible<img src='' alt='' width='200'></td>
<td><img src='../assets/w2/electrophoresis.jpg' alt='Small Electrophoresis' width='200'></td>
</tr>
<tr>
<td> Transillumination</td>
<td>To improve visibility of certain structures</td>
<td><img src='../assets/w2/underuv.jpg' alt='DIY Transillumination' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>
<tr>
<td> X-ray crystallography</td>
<td>to obtain the three-dimensional structure of crystallised proteins</td>
<td></td>
<td></td>
</tr>
<tr>
<td> CRISPR-Cas9</td>
<td>to edit genes within organisms</td>
<td><img src='' alt='' width='200'></td>
<td><img src='' alt='' width='200'></td>
</tr>

</table>
</div>

<!--

|  Technique |  What it's used for | DIY Version |Professional  version
| ------------- |-------------| -----|------|
| Media for growing cultures | Optimises conditions for growth often using pH and nutrients needed by cultures | ![DIY Media](assets/w2/DIYmedia.jpg) ||
| Sterilisation by autoclave |Killing microorganisms that might contaminate the  samples. |Pressure cooker for equipment, bunsen burner for instruments, alcohol for hands ![DIY Media](assets/w2/pressurecooker.jpg)  | |
| Stricking |Spreading out samples into growing medium in smaller concentrations. Isolation and identification of cells |    |   |
| Indicators |Enabling observation of specific cells or structures or conditions| ||
| Spectrophotometry |to measure concentration of a substance in a medium | ![DIY Spectrophotomer](assets/w2/Spectrophotometer.jpg)||
| Incubation |To optimise growth conditions for cultures using heat| ||
| Microscopy |To observe forms that we cannot see with a naked eye| |![Professional Microscope](assets/w2/Profmicroscope.jpg)|
| Centrifuge |Separates molecules by size| ![Drill Centrifuge](assets/w2/drillcentrifuge.jpg) |![Pro Centrifuge](assets/w2/professionalcentrifuge.jpg)|
| Serial dilution using a pipette |Changes concentrations of solutions|![Seial dilution](assets/w2/serialdilution.jpg) ||
| Stoichiometry and titration | To determine the amount (mass or mols) of a molecule or element in a sample using ratio of elements or molecules in a chemical reaction| [Mass molarity calculator](https://www.sigmaaldrich.com/chemistry/stockroom-reagents/learning-center/technical-library/mass-molarity-calculator.html)  ||
| Isolation | separation of a strain from a natural, mixed population of living microbes | ||
| Identification |  | ||
| Gram staining  |to indicate different types of membranes in bacteria| ||
| PCR Polymerase Chain Reaction   |to copy genes|  ||
| PCR machines (thermal cycler) |facilitates temperature-sensitive reactions| waterbaths at different temperatures| ![DIY PCR](assets/w2/PCR_cyclic_incubator.jpg) This is a small one |
| Electrophoresis |To separate charged macromolecules like DNA and RNA by size, or to estimate the size of DNA and RNA fragments or to separate proteins by charge| Dangerous but possible | ![Small Electrophoresis](assets/w2/electrophoresis.jpg) |
| Transillumination |To improve visibility of certain structures |  ![DIY Transillum](assets/w2/underuv.jpg) ||
| X-ray crystallography | to obtain the three-dimensional structure of crystallised proteins | none|  |
| CRISPR-Cas9 | to edit genes within organisms | none| -  |

--->



## A question that biology could answer

Diatoms are unicellular algae that produce biomineralized silica exoskeletons known as frustules from silicon dissolved in seawater. They are the only organism on the planet with cell walls composed of transparent, opaline silica. Diatom frustules have high surface areas and bend light to enhance light uptake which is advantageous for photosynthesis. Once the organism has died the functional structure remains as a biomineral shell. Diatom frustules have the highest strength-to-weight ratio of all reported natural biomaterials.
Below are some images of diatoms that live in alkaline conditions.


![Diatoms](assets/w2/Alkalidiatoms.png)


<p align="center">
Images of Diatoms in Alkaline, Saline Lakes by Robert E Hecky and Peter Kilham, Department of Zoology, Duke University
</p>

### Question
Can growing containers that mimic diatom frustule structures or frustules of dead diatoms improve photosynthesis in other underwater microorganisms?

### Hypothesis
Porous glass growing containers improve growth in spirulina.

### Methodology

1. Research diatoms that live in highly alkaline environments and existing microscopy of these diatom structures (if there are any).
2. Research available porous glass materials and objects.
3. Pick 2 different objects or glass types.
4. Fabricate 2 growing containers from 2 different porous glasses chosen, preferably with the same shape (test tubes would be ideal) and find one test tube for the Diatomaceous Earth (a powder of the silica frustules of dead diatoms).
5. Prepare fresh spirulina and weigh 4 equal measures of equal concentration using spectrophotometry.
6. Use 1 measure as a control to find the average length of the spirulina before growing using electrophoresis? or photograph through microscopy.
7. Put equal volumes of equal concentration spirulina into the two porous glass containers and the third into a test tube with a small measured amount of diatomaceous earth.
8. Place all 4 samples in separate beakers of the same broth containing food (Sodium Bicarbonate, Magnesium sulphate, Potassium nitrate, Citric acid, Salt, Urea, Calcium chloride, Iron sulphate, Ammonium sulphate)
8. Incubate in sunlight for 2 days.
9. Remove samples from broth and measure concentration using spectrophotometer.
10. Measure lengths of spirulina using electrophoresis or photograph using microscopy (should be around 500 microns)
11. Compare with control.




## How did I learn this week
* Demonstrating abstract unseen theory into examples of DIY methods and observable results in our laboratory allowed the scientific theory to be experienced physically within my means at my scale and to be memorable as a part of my experience of my environment.
* Having to measure and quantify results helped to repeat and reinforce my understanding of scientific methods.
* Visiting a working laboratory with world class equipment enabled me to perceive how feasible it is to make breakthroughs in Biology
* Seeing animated visualisations of molecular mechanisms helped me to understand the movements involved in processes such as transcription.
* Learning some key definitions enabled me to research around topics such as growing cultures and increased my understanding of how available they are.

## How this week relates to my work
> <p align="center">"Good science is good questions".</p>


<p>The field of Synthetic Biology asks what properties that organisms have can we use in different environments.</p>


<p>The scientific method is reductive and continues to map what we know the universe is not so that we can start to see its nature in patterns.</p>


<p> My work is also motivated by good questions which drives mapping of technologies and behaviours with a view to design tools. My methods are somewhat scientific as they are highly iterative</p>

## References

Genetic Learning Science Center, University of Utah
*Cell Size and Scale*
accessed 20/10/2018
<https://learn.genetics.utah.edu/content/cells/scale/>

University of Cambridge *Fractal patterns spontaneously emerge during bacterial cell growth*
<https://www.cam.ac.uk/research/news/fractal-patterns-spontaneously-emerge-during-bacterial-cell-growth>

The Fractal Foundation *Fractal Rivers* <https://fractalfoundation.org/OFC/OFC-1-4.html>

Cell Biology By the Numbers *How Big are the Molecular Machines of The CentraL Dogma?*
<http://book.bionumbers.org/how-big-are-the-molecular-machines-of-the-central-dogma/>

E.O.Wilson Biodiversity Foundation, Drew Berry, The Walter and Eliza Hall Institute of Medical Research *DNA animations by wehi.tv for science-art exhibition* <https://youtu.be/7Hk9jct2ozY>


Robert E. Hecky2 and Peter Kilham”
Department of Zoology, Duke University, Durham, North Carolina
*Diatoms in Alkaline Saline Lakes: Ecology and Geochemical Implications* <https://aslopubs.onlinelibrary.wiley.com/doi/pdf/10.4319/lo.1973.18.1.0053>

https://www.quantamagazine.org/how-cells-pack-tangled-dna-into-neat-chromosomes-20180222/
