# 09. Engaging Narratives

My appreciation of narratives was enriched this week from workshops by professionals who work on tools that assist people to tell stories.
We heard from Heather Corcoran from Kickstarter on using short pitch videos and Bjarke Calvin from Duckling who advised on using images to tell stories.

We were reminded how during this course we are looking at how aligning ideas can be as powerful as creating things to instigate change and that narrative can be used as a tool to align ideas.


## Shareholder primacy

A learning for me from Heather Corcoran was the concept of Shareholder Primacy which has resulted in laws that enable shareholders to sue companies they have invested in if they do not act in the interests of shareholders. This can prevent companies to act in the interest of local communities or the wider world.

The Kickstarter funding model is guided by beliefs by investors that certain ideas should exist in the world. This reminded me of the Copyleft manifesto which gives permission for innovators to improve on ideas they see potential in. I wondered what a world without copyright would look like. How would people who come up with ideas earn money? [The WIRED documentary about Shenzhen](https://www.youtube.com/watch?v=SGJ5cZnoodY) showed how fast manufacturing enabled designers to respond quickly to markets by quickly refining products to reach optimal designs and prices.  If ideas people were easily connected to resource people they could both profit from ideas - could more accessible manufacturing services make designing more attractive to more people?

## Narratives in new media

Bjarke Calvin reminded us that user journeys online are like narratives.
He also highlighted how documentaries are becoming more interactive with online platforms - increasing the pathways to understanding issues. He called this use of media as **Insight Media**. Bjarke introduced us to MIT's [Open Doc Lab](http://opendoclab.mit.edu/) who are exploring new ways of presenting documentaries. This field is trying to look among other things at *inclusivity* in narratives.

In exploring this subject I found an **immersive art** project by [Hyphen-Labs](http://www.hyphen-labs.com/) which resonated the ideas I was looking at last week on trying to express multiplicity in experiences. "NeuroSpeculative AfroFeminism (NSAF) is a cross-platform project grounded in several applications of technology including product design, virtual reality (VR), and social-psychological/cognitive impact/biometric/fMRI research". It sits somewhere between Experience Design, Virtual Reality and Product Design.

<iframe src="https://player.vimeo.com/video/208264686" width="640" height="349" frameborder="0" allowfullscreen></iframe>

## Reality as objective, subjective and intersubjective

The topic of objectivity v subjectivity has always been the background to the inquiries I/we have followed in this course. Bjark introduced us to the term intersubjective reality which includes things like language and stereotypes to try and come to an understanding between subjective experiences. Narratives fall within these.

> "Intersubjective perceptions refer to shared perceptions of the psychological characteristics that are widespread within a culture."

<font size="2">*Chi-Yue Chiu et al (2010) Intersubjective Culture - The Role of Intersubjective Perceptions in Cross-Cultural Research.*</font size>

This idea is the reason I think stories are interesting. The shared links we have to myths from around the world that seem to be universal is fascinating. Archetypes and roles in fiction guide our behaviours and build a sense of our selves. Stories give us strategies.

I wondered if money is also an intersubjective tool - linking in with the inquiries I made last week into analogies for money.

Social media sounds like an ideal place to share how we subjectively think reality is. However the result of this tool seems to have become an overreliance on stereotypes. People are conforming to each other and in doing so we risk losing stories,  perspective and authenticity.


## Interesting tools in film documentaries

I've referenced Adam Curtis films in my reflections on this course before. His use of tv and news footage with careful music synchronisation helps to create a multimedia barrage of intersubjectivity. **TV and music are excellent reference points for intersubjective reality**.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wcy8uLjRHPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Art teaches us about other peoples experiences and we can learn something about other peoples versions of facts through **dramatisations**. In Joshua Oppenheimer's "The Act of Killing" the subject of the film was allowed to direct his story using dramatisation and on a meta level this became a documentary on the nature of the director of the drama.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LLQxVy7R9qo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## The structure of stories

Bjark gave us a linear method to structure stories:

1. head   - lead line to draw people into the story
2. Body - with a turning point of no return. make it about one idea
3. Reflection
4. Possibly side stories.
5. Give people a reason to care
6. Build Evidence

One documentary that I found really effective was this one about magician / debunker James Randi. It had all these elements but a plot twist at the end too.

<iframe width="560" height="315" src="https://www.youtube.com/embed/s9ZWaS_FjNU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Visual tools

Visuals are now the mainstream medium and platforms to share visual stories have exploded recently.
Bjarke shared with us his platform [Duckling](https://www.duckling.co/) and invited us to use it to construct a narrative.

Our class results can be seen [here](http://duckling.me/interest/emergentfutures)


## Pitching ideas

Heather Corcoran showed us how video is used to pitch ideas on the Kickstarter platform. They usually contain the following elements:

* 15 second snapshot of the product and what it does
* The why and how of the mission 15 seconds
* Introduce who you are 15-30s
* Deep dive into the project

We looked at the style of presentations and how clarity, a call for help, authenticity and building intrigue keep people interested.

Video can be backed up by a written page, rewards and project updates.
Building a community around your project is one of the advantages of telling stories online.


## A narrative on the interconnectedness of work experiences

At the end of the week we produced a narrative around our areas of interest. I worked on the idea from the previous week on the interconnected experiences of work. The narratives of individual experiences were linked but I felt that they weren't necessarily linear. I was interested if there were non-linear ways of telling stories.

I wanted to use voice because I think the way people talk about their experiences in life through voice are something we tune into easily.

 I interviewed myself based on the 5 questions I had developed last week:

1. What values guide your work?
2. Have you had to sacrifice anything in order to do your job?
3. What do you invest into your job (Physically, emotionally, intellectually)?
4. What returns do you get from doing you job (Physically, emotionally, intellectually)?
5. What do you work to get from outside of your job (what do you buy)?

For the last question I found that rent and entertainment were my main returns from working in my last job.
I then interviewed my classmate who is an Events/ Music producer who works to produce the kinds of things I spend my earnings on.
I also interviewed my friend who is a landlord who works to maintain a property. I plotted the results in a diagram to see if I could show the interconnectedness of the stories. I found the answers looked more like a field of experiences rather than a linear narrative. I attached recordings of the responses onto my map to see if I could make an interactive narrative and the results so far are below:

<div style="width: 100%;"><div style="position: relative; padding-bottom: 44.13%; padding-top: 0; height: 0;"><iframe frameborder="0" width="5490px" height="2423px" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://view.genial.ly/5c00cd054a56bc02026e7829" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe> </div> </div>

### Future development

#### 3D environments with sound fields
I felt that these stories could be interesting in a 3 dimensional environment - with a field of sound.

![Infinity Room](assets/w9/yayoi-kusama.jpg)

<font size="2">*Yayoi Kusama's Infinity Mirror Room*</font size>


<iframe src="https://player.vimeo.com/video/66167082?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<p><font size="2"><a href="https://vimeo.com/66167082">*Light Leaks*</a> *by* <a href="https://vimeo.com/kylemcdonald">*Kyle McDonald*</a>.</font size></p>



I would like to look into whether or not it is possible to 3D print acoustic metamaterials that would help to form such a thing by focussing acoustics at certain points in a space.

<iframe width="560" height="315" src="https://www.youtube.com/embed/pNsnvRHNfho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### 3D sound controllers
I also found some interesting 2D interfaces for exploring sound and wondered if these might be something that could be turned into 3D interfaces - so fields of light sensors that trigger sounds.

* [Weblandia - interactive map of sounds for Koka Nikoladze's homepage](http://nikoladze.eu/)
* [Olivia Jack - Pixelsynth - Browser-based synthesizer for creating sounds from images and drawings](https://ojack.github.io/PIXELSYNTH/)


## References

* http://opendoclab.mit.edu/

* https://www.widewalls.ch/sound-art/

* MIT Tech review https://www.technologyreview.com/

* https://thecreativeindependent.com/guides/a-creative-persons-guide-to-thoughtful-promotion/#summary
https://www.flickr.com/photos/joseduarteq/

* Sapiens: A Brief History of Humankind by Yuval Noah Harari

* Zlatev, Jordan & P. Racine, Timothy & Sinha, Chris & Itkonen, Esa. (2008). Intersubjectivity: What makes us human?. The Shared Mind: Perspectives on Intersubjectivity. 10.1075/celcr.12.02zla.

* Embodiment of intersubjective time: relational dynamics as attractors in the temporal coordination of interpersonal behaviors and experiences
Julien Laroche1,2*, Anna Maria Berardi2 and Eric Brangier2

* https://www.nasa.gov/content/orbiting-rainbows-phase-ii/_test

* https://www.nasa.gov/larc/scientists-recreate-earth-s-northern-lights
