

# <center>Shenzhen</center>

<center>![](assets/)</center>


## Shanzai

Shanzai is a way of fabricating by improvisation.
> the shanzhai method delivered “hardware memes”—gadgets quickly designed and built out of easily sourced and readily interchangeable parts.


*- An Xiao Mina and Jan Chipchase*


Lax copyright laws + Network of factories =
High experimentation + Fast Turnover of prototypes = many variants on a theme i.e Hardware memes.

David Li at Szoil introduced us to Shenzen's significance in the field of innovation.

Knowledge, Tech and Production drive innovation.
### Key Elements of the city

**Huge infrastructure** to accommodate all kinds of people living and travelling around Shenzhen including a state of the art subway.

**Huaqiangbei market**, where hundreds of factories maintain storefronts

**Urban Villages**, where original residents have developed facilities for communities as well as leasable space.

**Budding maker spaces** for residents to innovate and learn skills

**Universities** with courses preparing students for the fabrication industry

**Links to external markets** via road or high speed train to Hong kong and by air and sea to other countries.





## Circular Economy
#### Observations:

A huge amount of product being made for markets around the world.

Sustainability was a term used mostly to describe economies rather than resources.

The designs that I saw outside seemed quite market driven. I saw an emphasis on beauty products and a number of mirror designs, I also saw a number of products that played with creating novel ways of interfacing with gadgets.

Sustainability also was used to address concerns in air pollution.

We saw on a huge construction side that ideas such as reusable shuttering are being implemented. Long term thinking is being implemented in this significant industry.

Products are going out in huge quantities but the lifecycle of product is not something that was discussed to any depth during our visit.

We did find out that plastic and metal waste from factories is sold on for reuse.






## Applying learnings

<font size="4" color="blue">Is copyright overrated</font>

The idea of ideas belonging to a person can feel counterproductive in these "VUCA" times of wicked problems.

If the free market was supposed to be the great equaliser killing copyright will accelerate it.

<font size="4" color="blue">Everyone is a designer</font>

I heard more than once on our trip, people working in manufacturing referring to the expertise in Shenzhen not being about design. I think this attitude can be challenged. Everyone is a designer and the more we acknowledge the value of our experiences feeding into decisions, the more chance we have of finding solutions that suit our needs.


**References**

- [Inside Shenzhen’s race to outdo Silicon Valley by An Xiao Mina and Jan Chipchase (2018) MIT Technological Review](https://www.technologyreview.com/s/612571/inside-shenzhens-race-to-outdo-silicon-valley)
- http://oma.eu/publications/project-on-the-city-i-great-leap-forward
- https://hackaday.com/2016/02/07/bunnies-guide-to-shenzhen-electronics/
- https://www.bunniestudios.com/blog/
- https://www.coindesk.com/blockchains-killer-app-making-trade-wars-obsolete
- https://www.technologyreview.com/s/609038/chinas-ai-awakening/

**Books**

'Makers' Chris Henderson
