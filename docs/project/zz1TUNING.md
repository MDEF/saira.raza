<head>

<style>
<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">

@import url('https://fonts.googleapis.com/css?family=Archivo+Black&display=swap');

body {
font-family: monospace;
font-size: 9px;
background-color: #eee;
margin: 10px;
}

.box {
width: 400px;
float: left;
padding: 0px 40px 80px 0px;
}

.screenshot {
width: 400px;
overflow: hidden;
}

</style>
</head>

<body>

# Tuning data to find materials using machine learning

Some examples showing how machine learning  can find structures that meet specific functions.


<div class="box"><div class="screenshot"><img src="https://media.nature.com/original/nature-assets-draft/srep/2016/160223/srep22018/extref/srep22018-s2.gif" style="width:400px;"/></div><p>**Fig 2: ** Direct observation of negative-index microwave surface waves</p><p><a href="https://www.nature.com/articles/srep22018?proof=true&draft=journal#supplementary-information">J. A. Dockrey, S. A. R. Horsley, I. R. Hooper, J. R. Sambles & A. P. Hibbins (2016)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="http://static-movie-usa.glencoesoftware.com/source/10.1073/30/5b86b8f6d75cf5ffaa01144171cc80f6a83ffb5d/pnas.1600521113.sm01.gif" style="width:400px;"/></div><p>**Fig 3: ** </p><p><a href="https://www.pnas.org/content/113/10/2568">Dexin Ye, Ling Lu, John D. Joannopoulos, Marin Soljačić, and Lixin Ran (2016)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="http://news.mit.edu/sites/mit.edu.newsoffice/files/images/metasurfaces.gif" style="width:400px;"/></div><p>**Fig 5: ** From a randomly patterned metasurface, new technique quickly evolves an ideal pattern to produce a lens with desired optical effects</p><p><a href="http://news.mit.edu/2019/mathematical-tune-metasurface-lenses-0520">Zin Lin, Victor Liu, Raphaël Pestourie, Steven G. Johnson (2019)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://spectrum.ieee.org/image/MzAyMjg5NQ.gif" style="width:400px;"/></div><p>**Fig 6: ** </p><p><a href=""></a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://media.nature.com/original/nature-assets/srep/2016/160902/srep32577/extref/srep32577-s3.gif" style="width:400px;"/></div><p>**Fig 7: ** Color map animation of the magnetic field evolution in the optical diode under fundamental spatial mode excitation from the right</p><p><a href="https://www.nature.com/articles/srep32577">Francois Callewaert, Serkan Butun, Zhongyang Li & Koray Aydin (2016)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://pubs.rsc.org/en/Content/Image/GA/C7ME00062F" style="width:400px;"/></div><p>**Fig 8: ** </p><p><a href="https://pubs.rsc.org/en/content/articlelanding/2017/me/c7me00062f#!divAbstract">Karim R. Gadelrab,  Adam F. Hannon,  Caroline A. Rossa  and  Alfredo Alexander-Katz (2017)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://journals.aps.org/prx/supplemental/10.1103/PhysRevX.1.021016/Animation3.gif" style="width:400px;"/></div><p>**Fig 9: ** </p><p><a href="">W.C. Chen, J. J. Mock, D. R. Smith, T. Akalin, and W. J. Padilla (2011)</a></p></div><br>



</body>
