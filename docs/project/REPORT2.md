**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦</font></center>
<br>

<!--- -->
<!--- --><!--- --><!--- --><!--- -->
# <center>Area of Interest & State of the Art</center>


<!---
<center><font size="4" color="blue">How can makers/ users/ all of us, our networks and AI use our strengths to collaborate to design material things we all need?</font></center>

<center><font size="4" color="blue">What can an outsider to the fields of making and material design make using digital fabrication and AI?</font></center>

<center><font size="4" color="blue">What could future intersections of the maker movement and material design look and feel like?</font></center>

-->



## Making Metadevices in a Fab Lab today

Metamaterials can be fabricated in a fab lab for the radio, wifi and microwave frequencies and acoustic metamaterials can be made that alter ultra sound waves such as those used for medical scanning and absorb low range sound waves from exhaust pipes. Digital fabrication has encouraged testing of shapes that were previously too expensive to make for speculative experiments. With the availability of 3D printing experiments in acoustic metamaterials can be done relatively quickly - allowing exploration of an area that was perhaps not considered as critical to the biggest problems or opportunities previously faced by researcher. Now however we see these experiments forming new taxonomies of metamaterials that we know have equivalent mechanisms and taxonomies and forms in other fields.

### 3D Printing

There are many examples of 3D printed metamaterials for acoustic applications.
They come in a number of strategies - Labyrinthine, spiral and resonant.

A recent configuration was this spiral acoustic "silencer" by Boston University

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/Fd1D42dVxS0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

The Human Computer Interaction Lab at the Hassner Platner Institute  have been looking into fabrication of mechanical and [digital mechanical metamaterials](https://pdfs.semanticscholar.org/47de/919aefe501e3b90ec61cbb80fb2c5e399ee6.pdf) and recently Alexandra Ion of the institute has researched [Mechanical Meta Devices](http://alexandraion.com/wp-content/uploads/2019CHI_Understanding_Metamaterial_Mechanisms.pdf)

## Design interfaces for digital fabrication - Human Computer Interaction

A host of new design interfaces keep appearing to cater for makers who have access to a maker space.

The Human Computer Interaction Lab at Hasso Plattner Institute have been exploring many design interfaces for makers. Among these are [this opensource interface to design mechanical metadevices](https://jfrohnhofen.github.io/metamaterial-mechanisms/) by Johannes Frohnhofen which uses a voxel environment.

<center><iframe width="600" height="400" src="https://jfrohnhofen.github.io/metamaterial-mechanisms/" allowfullscreen></iframe></center>

[Platener](https://hcie.csail.mit.edu/research/platener/platener.html) is a software system that converts functional objects (tools and prototypes) into parts you can cut on a laser cutter. The lab's recent [Kyub](https://hpi.de//en/baudisch/projects/kyub.html) project helps to break down 3D objects of any shape into smaller shapes that you can cut on a laser cutter.
[Grafter](https://hpi.de//baudisch/projects/grafter-remixing-3d-printed-machines.html) is designed to help makers "re-mix" existing mechanical 3D printed parts in a modular way.

<!--
[Stefanie Mueller](https://hcie.csail.mit.edu/stefanie-mueller.html) head of the [Human Computer Interaction Lab at Hasso Plattner Institute](https://hcie.csail.mit.edu/)
 UC San Diego  https://www.youtube.com/watch?v=MtmcSwzInGY -->

[inFORM – Dynamic Shape Display](http://tangible.media.mit.edu/project/inform/) by Tangible Media Group at MIT uses language of 3D shape to enable users to interact with digital information in a tangible way.

[EmotiveModeler](https://emotivemodeler.media.mit.edu/) by Philippa Mothersill at MIT is a design interface that tries to form a Qualitative Taxonomy of Emotive Shapes to facilitate the design of forms using python scripts for Rhino.

In wider interaction design, the work of [The Object Based Media Group](https://obm.media.mit.edu/) research **immersive displays** and how interface technologies could change everyday life. They ask "Can the physical world be as connected, engaging, and context-aware as the world of mobile apps?". In doing so they engage in the "everydayness" of the present and future to materialise a tool for discussing perception.

This year [Dan Novy from the group defended his thesis on programmable hallucinations](https://www.media.mit.edu/videos/obm-dan-novy-defense-2019-01-31/) bringing the materiality of human brains into the exploration of interaction design.

> It seems intuitively correct to say that the hardware is already capable of anything that the software might cause it to do. But wouldn’t it be interesting if this were not true? That is, if the interaction of hardware and software yielded more than the sum of the parts?

*-[Neil Steiner and Peter Athanas (2005) Hardware-Software Interaction: Preliminary Observations](http://www.ccm.ece.vt.edu:8444/papers/steiner_2005_RAW05_hsi.pdf)*



## Making with AI

Cloistered supercomputers and quantum computers of today sound a world away from Fab Labs and maker spaces but developers of hardware and software information technologies are exploring ways that makers can integrate machine learning into their projects. This opening up of these technologies from research laboratories and industries showing a willingness and commitment towards a cultural change around trust and knowledge of AI.

### AI Hardware for makers

[NVIDIA Jetson](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/) is a suite of CUDA architecture + Linux 'System-on-Modules' (SOM) starting from $129, with CPU, GPU, PMIC, DRAM, and flash storage that can be programmed to use the [CuDNN deep neural network library](https://devblogs.nvidia.com/accelerate-machine-learning-cudnn-deep-neural-network-library/) and a range of deep learning frameworks that incorporate CuDNN. These SOMs can be put into embedded programming by makers.

[Google's AIY Projects Team](https://aiyprojects.withgoogle.com/) are producing a line of AI kits to let makers use google intelligence in their projects. Their intelligent camera kit called [Vision Kit](https://aiyprojects.withgoogle.com/vision/#project-overview) and intelligent speaker kit called [Voice kit](https://aiyprojects.withgoogle.com/voice/) let makers do some limited customisation on TensorFlow which their compiler turns into code usable by the kits.
They have also released 2 removable SOMs that contain eMMC, SOC, wireless radios, and the Edge Tensor Processing Unit (TPU) chip designed for fast on-device ML inferencing for IoT devices and embedded systems.

### AI customisation software

It is encouraging to find that opensource development culture is so active in the lucrative field of AI. Today opensource machine learning tools run into the [tens of thousands](https://medium.mybridge.co/amazing-machine-learning-open-source-tools-projects-of-the-year-v-2019-95d772e4e985) with documentation and even more free learning tools.

TensorFlow, Caffe and Keras are opensource deep learning frameworks that have opened up the field to the willing learners and have been adopted in academic research. NVIDIA has released TensorIT and the open source [DIGITS training system](https://devblogs.nvidia.com/digits-deep-learning-gpu-training-system/) to let users train and visualise custom neural networks.

### Cloud computing

You don't even need a high spec computer to build and run a deep learning network as some applications let you build and run neural networks remotely online. These include platforms such as Google Cloud and even quantum cloud computing platforms such as [IBM Watson](https://dataplatform.cloud.ibm.com/docs/content/wsj/getting-started/welcome-main.html), [Rigetti's Quantum Clud Services](https://www.rigetti.com/qcs) and [D Wave's Leap](https://cloud.dwavesys.com/leap/) (which can be trialled for free).

### Hoarding data for AI

Researchers are finding the benefits of sharing trained datasets to enable faster testing of machine learning techniques.
New formats of data sets are being trialled for different functions and sectors but collecting data to feed AI algorithms is currently not something that makers seem to be exploring.


## Finding materials using AI

Researchers are using a range of software, hardware and services to use machine learning algorithms to find materials tailored to perform in specific ways. Researchers typically use licensed simulation software such as COMSOL to produce labelled datasets and then train a neural network to seek There have been a number of instances where opensource frameworks have been employed but higher end research extends to supercomputer services.

The Materials Genome Initiative (MGI) U.S government funded project launched in 2011 produced no usable output at the time however progress in the field is being well documented in reviews such as [Keith T. et al (2018)](https://www.researchgate.net/publication/326608140_Machine_learning_for_molecular_and_materials_science) and [Mitsutaro Umehara et al(2019)](https://www.nature.com/articles/s41524-019-0172-5)

### Generative design of metamaterials using deep learning

Data-driven inverse design of metamaterials has been explored academically almost exclusively in photonics. Yet the process could be made transferable to any other type of wave field or scale as long as simulation software exists for it and the outputs can be made dimensionless. Several research papers describe deep learning methods used to find metadevices including Deep Neural Networks (DNN), Converse Neural Networks, (CNNs), Generative Adversarial Network (GAN)   and Binary Particle Swarm Optimization (BPSO) .

The input geometries are relatively simple compared to biological and chemical materials.

This year the Sandia National Laboratories released Mirage - a software for the inverse-design of optical metamaterials but this software is only available to scientists with US government contracts. **There is not yet a generalised and opensource deep learning design tool for metamaterial design.**  


### Co-designing with AI to capture pollutants

Machine Learning is being used in Material science to tackle reducing greenhouses gases at the molecular scale through the design of structures called Zeolites and Metal organic frameworks that can contain carbon dioxide molecules.  Recently it was proposed that Metamembrane structures could be used for separating   molecular compounds (Restrepo-Flórez, 2016). Exploration of this technology using machine learning has not yet been published.


 <!--Artificial intelligence aids materials fabrication today. In the future could AI pour through millions of research papers to extract “recipes” for producing materials.
 http://news.mit.edu/2017/artificial-intelligence-aids-materials-fabrication-1106
 Larry Hardesty
 Zeolites and metal organic frameworks are being explored to capture carbon by..
 Metamembranes --->


### Opensource machine learning for material design

Datasets for material science machine learning applications is hard to find.

>One noteworthy difference between materials mechanics and other, more traditional machine learning domains is the comparative expense of obtaining training data, either by experimental gathering or via simulation. Such simulations can be prohibitively expensive, which may require new methods of synergizing materials simulations to machine learning, for example via multi-fidelity models for generating data for machine learningource machine learning for material design

Bock et al. (2019)

Many researchers are working in the field of computational material science and tools are being shared.

Quantum ESPRESSO is an integrated suite of Open-Source computer codes for electronic-structure calculations and materials modeling at the nanoscale.

MAST-ML is an open-source Python package designed to broaden and accelerate the use of machine learning in materials science research.  Currently this package has 3 datasets: dilute solute diffusion,  perovskite stability (these materials have applications in fuel cells, solar cells, lasers, memory devices) and a metallic glasses dataset.

MatCALO is  an intelligent, cognitive assistant system that supports materials scientists in developing novel material that. uses python as a sophisticated library.

The Materials Data Facility (MDF) is a data sharing service that also operates using Python code.

Data mining is being employed by a number of applications. Project Chemeia is a “A synergistic AI integrated architecture for augmenting high value dark-data” to produce up to date, comprehensive, and reliable data sets for high-value R&D work.

**Libraries**

[Materiom](https://materiom.org/search) are building an open source repository of recipes and data on materials made from abundant sources of natural ingredients and waste. They work with researchers to find promising combinations of these materials.


<!--
Biorelate Limited and Intellegens Limited “Chemeia: A synergistic AI integrated architecture for augmenting high value dark-data”.

Taxonomies

Software
MatCALO, an intelligent, cognitive assistant system that supports materials scientists in developing novel materials. uses python as a sophisticated library

Functional materials
BO ZHU, MELINA SKOURAS, DESAI CHEN, WOJCIECH MATUSIK, ´ MIT CSAIL Two-Scale Topology Optimization with Microstructures (http://news.mit.edu/2018/automating-materials-design-0202)  https://arxiv.org/pdf/1706.03189.pdf

-->
<!---
Algorithm finds best carbon dioxide 'sponges' from a trillion recipes
Researchers borrowed from Darwinian principles to identify the promising carbon capture metallic-organic frameworks (MOFs). Angus Bezzina reports.

Metal organic frameworks
Zeolites
Barcelona Supercomputing Center -->



## Information from making things

The word Technology comes from two ancient Greek words – ‘Techne’ meaning ‘skill and/or art’ (different sides of the creative process) – and ‘Logia’ – meaning ‘speaking’ or ‘discourse of’. Technology is literally rooted in and forms part of a wider discourse on creation and expression.

Practical knowledge is knowledge gained by doing. It leads to actionable outputs but also contributes to theoretical knowledge. It can utilise actions and reactions of our bodies that we may not be aware of.

>Body schema is a self-organizing system that realizes an action without bodily awareness. It converts the perceptions of environment into the appropriate action toward environment, and facilitates skill fully coping with situations. It also produces new actions through interactions with unfamiliar situations. It is closely related to, but is not equivalent to the physical body. In short, body schema plays a key role in ̳doing without representing

*[Shogo Tanaka (2011)](https://www.researchgate.net/publication/316862553_The_notion_of_embodied_knowledge)*

Seymour Papert linked practical knowledge to the construction of material things as a way to construct knowledge systems by making meaningful things.

>The word constructionism is a mnemonic for two aspects of the theory of science education underlying this project. From constructivist theories of psychology we take a view of learning as a reconstruction rather than as a transmission of knowledge. Then we extend the idea of manipulative materials to the idea that learning is most effective when part of an activity the learner experiences as constructing a meaningful product.

*[Seymour Papert (1986)](https://books.google.co.uk/books/about/Constructionism.html?id=0N8-HAAACAAJ&redir_esc=y)*



## Meta design culture - increasing opportunities for co-design

The term meta design has been used to describe the opening up of design to more than the roles it currently occupies. It seeks to find new ways of co- designing and helping users to design. It accepts that designs are never finished - because no one is qualified to determine this. In this way it embraces the evolution of designs of things, unfinished-ness and augmentation. It facilitates the adaptation of things and materials.
> Metadesign represents a cultural shift from design as “planning” to design as “seeding.” By promoting collaborative and transformational practices of design that can support new modes of human interaction and sustain an expansion of the creative process, metadesign is developing toward new ways of understanding and planning with the goal of producing more open and evolving systems of interaction... Rather than a new model  of design, metadesign represents a constructive mode  of design: an enhancement of the creative process at the convergence of “art” and “science.”

*[Elisa Giaccardi, 2006](https://www.academia.edu/2113875/Metadesign_as_an_emergent_design_culture)*

To be clear, the term “meta” here does not refer to metamaterials but to the surrounding information beyond design – the structure, the culture, the rules. . Research continues to find tools that might assist in this cultural change by organisations such as [https://metadesigners.org](https://metadesigners.org) who are due to open The Metadesign Research Centre in Swansea later this year. Professor John Wood continues to publish thoughts on the field such as [this paper on relationalmoney](https://metadesigners.org/Relational-Money-2018).

>Metadesign supports change in order to fit new needs and opportunities that arise during the use of artefacts.

*[Elisa Giaccardi and Gerhard Fischer (2008) Creativity and Evolution: A Metadesign Perspective](http://l3d.cs.colorado.edu/~gerhard/papers/ead06.pdf)*

To enable open cross-cultural systems of learning and problem solving Metadesign encourages the practice of Embodied Design methodologies.

>Embodied design is a pedagogical framework that seeks to promote grounded learning by creating situations in which students can be guided to negotiate tacit and cultural perspectives on phenomena under inquiry; tacit and cultural ways of perceiving and acting

(Abrahamson, 2013)


### Representing experiences through making and documenting making


The Digital Fabrication revolution is a revolution because it starts to affect everyone and different ideas become part of the design “system” to interface with the material world.

Meta design aspires to enable users to design the things they need and to co-design with people with different expertise. In doing so Design becomes inductive, additive and expansive to enable the scale and resolution to find ways for all the parts of the system to evolve.

Representing experiences and contributing latent information in in exchange between us and the materials we use and the things we make- give the technosphere more data.

<!-- The Digital Fabrication revolution is a revolution because  it starts to affect everyone and different ideas become part of the design "system" and technosphere.

Meta design drives towards all users being able to design.

Fab labs seek to bring equipment and technical literacy to more and more communities to share knowledge.

Representing experiences and contributing latent information in in exchange between us and the materials we use and the things we make- give the technosphere more data.

Design becomes inductive, additive, expansive. We may get more resolution on what the technosphere is and even what the symboloshpere is.

### The case for open systems of knowledge sharing

Open systems of knowledge sharing involve more numbers of problem solvers and more variety of experiences in innovation.

Businesses have realised this and we see large corporations like Google opening up their technologies for other businesses to piggy-back off. There are a number of research organisations right here in Barcelona that are investigating the application of these systems like  [Open Systems Lab](https://www.opensystemslab.io/)  and [System Innovation](https://systemsinnovation.io/) open systems for digital innovation for industry and society.

Some innovations attract problem solvers to form communities to accelerate the development of the product. Platforms like Github and Wikipedia encourage this kind of collaboration for the greater good. -->

<!--
> The pragmatic roots of embodied thinking lie in the brain-body-niche system of conscious environmental adaptation.

*[Jerry Diethelm (2019) Embodied Design Thinking](https://www.sciencedirect.com/science/article/pii/S2405872619300127)*

https://designresearch.aalto.fi/groups/edg/project/heat-harvest/
>Agents do not pop into existence (emerge) from complex brain dynamics, already armed with powers of intentionality and will. Rather, agent and environment are co-dependent sides of the same coin.”
[Michael Silberstein and Anthony Chemero (2011) “Dynamics, Agency and Intentional Action”](https://www.humanamente.eu/index.php/HM/issue/view/15.27)
The lexicon of open design systems have emerged from industrial Design
Daniel Charny curator of 'The Power of Making' explored the theme of
Design terms are changing all the time  - Cocreation and collaboration is being explored these days

### Visualising sound waves
## Getting subjective information by representing experiences
### through making and documenting making
-->

<!---
#### The unfinishedness of inclusive design

Metadesign principles embrace design as never finished, additive, evolving and increasing in complexity similar to the evolution of lifeforms.
Presentation of unfinished object that function in a coherent way gives value to the information revealed by the process of making, inviting "mutations" of a design response by the user / audience / machine in the present and in the future - making space for what we do not yet know.
-->

### Codesigning with AI and sensor networks for sustainable energy
AI is helping to find new materials and design devices that could change our reliance on non-renewable material sources but these devices are only part of a system. As part of the emerging system of sensors and user/designers can AI help to find ways to deliver the energy and environment we need?

<!---http://www.anthropocenemagazine.org/AI/-->


## The Anthropocene illusion - Can our relationship to the things we make and waste be different?

>My interpretation of the Anthropocene illusion was the tendency for humans to put themselves at the center of things, so that when we think of the climate problems and other environmental problems, we usually think of these as due to human impact. Of course, at some level they are due to human impact, but there are many other things going on in the world that embed humans in a way such that they are not really acting completely independently and as free agents in doing these things, but there are much greater forces let loose in the world. These are the forces of the technosphere.

*[- Philosopher Erich Hörl talking to PK Haff in an interview for technosphere-magazine.hkw.de (2016)'Technosphere and Technoecology'](https://technosphere-magazine.hkw.de/p/Technosphere-and-Technoecology-qzjFDWgzxX2RDEDg9SN32j)*

Peter Haff proposed that in the past, being able to see ourselves not at the centre of the physical world through Copernican and Darwinian theory revolutionised how we look at our place in the world and that the technosphere could be the third. The drivers for this kind of change must be cultural as much as technical.

<center>![](assets/anthropocenerevolution.jpg)</center>

Narratives about the important position of humans in the universe run deep in modern culture, politics and design. Is there a space for this to change in a post-technological age?

We have seen how material things have helped us to develop computers and the quest to build better ones have driven the discovery of new materials. Is it conceivable for us to now extend rights and agency to these things that have evolved with us?

>“.. all earthlings are kin in the deepest sense, and it is past time to practice better care of kinds - as - assemblages (not species one at a time). Kin is an assembling sort of word. All critters share a common “flesh,” laterally, semiotically, and genealogically. Ancestors turn out to be very interesting strangers; kin are unfamiliar (outside what we thought was family or gens), uncanny, haunting, active.”

 - Donna Haraway (2015) Anthropocene, Capitalocene, Plantationocene, Chthulucene: Making Kin

### Xenodesign – embracing otherness

> Speculative realism challenges this anthropocentrism, and seeks to expand philosophical thinking to consider perspectives and notions of the “other,” including that which lies beyond human experience and perception. Object-oriented ontology, a subset of speculative realism, uses flat ontologies to understand reality. This means all objects — living and non-living entities, sometimes even fictional objects — are considered to have the same degree of being-ness in the world...In times when robots are granted citizenship and residency rights, forests and mountains are given the same legal status as humans to protect them from ecological disaster, and in which we now understand that humans are to a large extent other-than-human, hosting more microbial cells in our body than human cells, our attitudes toward design and human-centeredness need to be re-examined. A resistance of reduction in design is necessary, shaped by cross-audience sensitivity, multi-entity empathy, and a reconsideration of agency. “Becoming-with each other,” as Donna Haraway would say.

*[Johanna Schmeer - Xenodesignerly Ways of Knowing](https://jods.mitpress.mit.edu/pub/6qb7ohpt)*


#### Object-Centered Design - Seeing a spirit in objects and embedded politics in designing material things

> Buildings are haunted, not just by their past, but also by their future

*[Timothy Morton in an interview by Nicholas Korody "Timothy Morton on haunted architecture, dark ecology, and other objects"](https://archinect.com/features/article/149934079/timothy-morton-on-haunted-architecture-dark-ecology-and-other-objects)*

Object Orientated aesthetics play with the agency of objects and technology.

<!---
Narratives about the important position of humans in the universe run deep in modern culture, politics and design. Is there a space for this to change in a post-technological age?

We have seen how material things have helped us to develop computers and the quest to build better ones have driven the discovery of new materials



Donna Harrawy writes of the notion of Kinship is

> .. all earthlings are kin in the deepest sense, and it is past time to practice better care of kinds - as - assemblages (not species one at a time). Kin is an assembling sort of word. All critters share a common “flesh,” laterally, semiotically, and genealogically. Ancestors turn out to be very interesting strangers; kin are unfamiliar (outside what we thought was family or gens), uncanny, haunting, active.

*[ - Donna Haraway (2015) Anthropocene, Capitalocene, Plantationocene, Chthulucene: Making Kin](http://environmentalhumanities.org/arch/vol6/6.7.pdf)*


culture-  nature link breaking
post- anthropocentrism

using human terms to talk about non human things
Latour -
jane bennet

humans created it but do not master it.
geometric imagination

some objects need humans
art - objects are terrible at this
art needs us
humans are observers of the world
the world is always a surplus
harman
tim morton
-->


[Simone Rebaudengo](http://www.simonerebaudengo.com/) is a future interaction and product designer who is looking at the ways we might interact with AI and things. He explores Non Anthropocentricism through design with an emphasis on the mundane and transports you quite easily to the everydayness of a future. In doing so his work effectively communicates the political information coded into the design of objects.

He is also writing [a story about power and intention coded and passed on through the design of objects set in "a future where more things are becoming like people and viceversa."](https://everythingissomeone.github.io/)

Mark Foster Gage has been fascinated with mystery and disrupting existing aesthetics which invites people to be brought together by realities that are larger than the differences in their realities.
he is a creator of compelling "wow!" experiences" releasing the viewer from codes of designthat we have become desensitised to.

<center><a href=""><img src="https://static.wixstatic.com/media/b3085a_3645413148ea40e1a7ee28c145d184a2~mv2.jpg/v1/fill/w_940,h_672,al_c,q_85,usm_0.66_1.00_0.01/b3085a_3645413148ea40e1a7ee28c145d184a2~mv2.jpg" width="300" height="300" title="Nicola Formichetti Store NYC, 2012" alt="TERM 1"></a></center>

He embraces the limits of technology and applies them to found objects. I like that his designs invoke the viewer to imagine - something which is vital in resetting sights on the future.

He also loves maths and embraces the intricacies that processing power can reveal through the process of digital "kitbashing". His work feels like the objects might have genetically mutated to construct new forms as living things do - [as we are designing them to do]().

<center><a href=""><img src="https://static.wixstatic.com/media/b3085a_b889e358eb20412090b5678898ac797a~mv2_d_2213_2600_s_2.jpg/v1/fill/w_533,h_626,al_c,q_90,usm_0.66_1.00_0.01/b3085a_b889e358eb20412090b5678898ac797a~mv2_d_2213_2600_s_2.webp" width="300" height="300" title="Nicola Formichetti Store NYC, 2012" alt="TERM 1"></a></center>


The other side of object oriented ontology is exploring how much like machines we already are and how assimilated into non-human systems we are.

>‘all beings’, ‘all entities, things, or objects are machines’

*[Andrew Ball (2016) Book Review - Onto-Cartography: An Ontology of Machines and Media by Levi Bryant
](https://www.academia.edu/37951273/Book_Review_-_Onto-Cartography_An_Ontology_of_Machines_and_Media_by_Levi_Bryant)*



<!--- Adrian Ivakhiv.
## Information processing: Resolution, scale, noise and tuning

Our ability to control machines with more precision are a result of higher processing power which enable smaller packets of movement to be controlled as well as improvements in mechanical properties and designs of machines.

Higher resolution images contain more smaller pixels and use more memory to process.
Our ability to process geometries and simulations of physical processes is improving with computing power.
Its like increasing the resolution on the observable world.
Which can feel like moving up a scale of observation.
We tune neural networks to match conditions.  
-->



<!---
## Changing paradigms


> How might we move away from existing hardware and software platforms which reduce by narrowing, constricting, fragmenting and simplifying the human experience to those which enhance by revealing, accommodating, humanizing and defending, **Experiences which reveal**: allowing user engagement beyond manicured profiles and personas, creating rich social environments. **Experiences which accommodate**: allowing non-prescriptive engagement and expression. **Experiences which humanize**: focusing on more meaningful forms of interaction and feedback with others.**Experiences which defend**: protecting spaces and periods of time from fragmentation, giving space for meaningful in-person relationships to form.

[Michael Andrea(2019)How Technology Reduces Us, and What We As Designers Can Do About It](https://jods.mitpress.mit.edu/pub/hsxloiq4)
-->
<!---
## Re-designing ontology

> It  has  been  proposed  that  information  is  the  union  of  three  components.  Three  types  of information: symbolic, spatial and semantic.

[A communication model based on symbolic, spatial and semantic information fractions GERARDO FEBRES](https://www.researchgate.net/publication/301618553_A_communication_model_based_on_symbolic_spatial_and_semantic_information_fractions)

A Proposal about the Meaning of Scale, Scope and Resolution in the Context of the Information Interpretation Process Gerardo L. Febres https://arxiv.org/ftp/arxiv/papers/1701/1701.09040.pdf

Christian Nold (2018)Practice-based ontological design for multiplying realities(https://www.researchgate.net/publication/327824440_Practice-based_ontological_design_for_multiplying_realities)

## Building and telling new narratives about the Anthropocene
### New ways of describing how we connect
https://internet-atlas.net/
 -->
<!---
#### Molding and casting Mechanical metamaterials
#### 3D Printing mechanical metamaterials
http://archive.fabacademy.org/2018/labs/fablabulb/ga_digital_mechanics.html
### EM Antennae



simone r

http://www.simonerebaudengo.com/

check if you have time
[Jamer Hunt 2019 Anticipating Future System States]
(https://jfsdigital.org/articles-and-essays/vol-23-no-3-march-2019/anticipating-future-system-states/?fbclid=IwAR2hO1KMXr51IqpWjuk6VnJC85mohjN9ax6mpoVKPyXg9NaIPRpeZGqZbME)
-->
