<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Chivo&display=swap" rel="stylesheet">
</head>
  <!---
**<font face color="blue" size="6"><center>M̷̧̨̛͚̆̄̇̌à̶̢̳̖̠̬̝̫͔̊͜͜͠k̶̛̰͇͔̤͆͆́̐̉͋i̵̮̬̋͆̽̈̕n̸̨̰̟͙͖̫̟̩̈̈́̈̚g̵̻̝̣͖̗̭͚̎́̿͘͝͝ ̶͓̜̙̝̞̼͈͐f̶͇̹͇̜̖͉͕̠͐̒̒̓̄͘r̸͓̜̯̞̠̈͑̓̆͐̚̚̚ͅo̴̳̺̝͆̒̓́̑̍͊͝m̴̧͇̘̯̭̫͎̐̏ ̸̦̦̭̝̭̰͓̂̋̋̈́̇͜n̵͎̙̠̂ǒ̵̡͓̮͉͐̒́i̷̲̘̒̈́̈́͒̄͆̅͝ṣ̵̡͕̞̠͓͉̪͐̇̅̌̎e̵̯̗͋͛̎̀͊͐̾̃̚͠</center></font>**-->
<!--- <center><font size="4" color="blue">Investigating metamaterial design as a novice maker</font></center>
̧͇̘̯̭̫͎̏ ̸̦̦̭̝̭̰͓̂̋̋̈́̇͜n̵͎̙̠̂ǒ̵̡͓̮͉͐̒́i̷̲̘̒̈́̈́͒̄͆̅͝s̵͐̇̅̌̎  ̰͓͜n̵͎̙̠̂ǒ̵̡͓̮͉͐̒́i̷̲̘̒̈́̈́͒̄͆̅͝s̵͐̇̅̌̎
<br>-->

<!---
**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>** -->
<!---  <center><font size="3" color="blue">𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦</font></center><br>-->
<!---
# Overview-->
<!--- -->
<center>

![](assets/1.png)
![](assets/2.png)
![](assets/3.png)
![](assets/4.png)
![](assets/5.png)
![](assets/6.png)
![](assets/7.png)
![](assets/8.png)
</center>


<div class="canva-embed" data-design-id="DADjz265WFY" data-height-ratio="0.7500" style="padding:75.0000% 5px 5px 5px;background:rgba(0,0,0,0.03);border-radius:0px;"></div><script async src="https:&#x2F;&#x2F;sdk.canva.com&#x2F;v1&#x2F;embed.js"></script>




  <!--- <font face="Montserrat" color="black" size="5" background color="transparent"><center>   </center></font>
**<center>𝙼𝚊𝚔𝚒𝚗𝚐 𝚏𝚛𝚘𝚖 𝚗𝚘𝚒𝚜𝚎** 𝚒𝚜 𝚊 𝚙𝚛𝚘𝚓𝚎𝚌𝚝 𝚛𝚎𝚊𝚌𝚑𝚒𝚗𝚐 𝚝𝚘𝚠𝚊𝚛𝚍𝚜 𝚒𝚗𝚌𝚕𝚞𝚜𝚒𝚟𝚎, 𝚝𝚛𝚊𝚗𝚜𝚍𝚒𝚜𝚌𝚒𝚙𝚕𝚒𝚗𝚊𝚛𝚢 𝚌𝚞𝚕𝚝𝚞𝚛𝚎𝚜 𝚘𝚏 𝚖𝚊𝚔𝚒𝚗𝚐 𝚊𝚗𝚍 𝚌𝚘-𝚍𝚎𝚜𝚒𝚐𝚗.
𝙸𝚗𝚜𝚙𝚒𝚛𝚎𝚍 𝚋𝚢 𝚜𝚌𝚒𝚎𝚗𝚝𝚒𝚏𝚒𝚌 𝚛𝚎𝚜𝚎𝚊𝚛𝚌𝚑 𝚘𝚗 𝚝𝚑𝚎 𝚞𝚜𝚎 𝚘𝚏 𝚍𝚎𝚎𝚙 𝚕𝚎𝚊𝚛𝚗𝚒𝚗𝚐 𝚝𝚘 𝚍𝚎𝚜𝚒𝚐𝚗 𝚖𝚎𝚝𝚊𝚖𝚊𝚝𝚎𝚛𝚒𝚊𝚕𝚜 𝚊𝚗𝚍 𝚘𝚏 𝚍𝚒𝚐𝚒𝚝𝚊𝚕 𝚏𝚊𝚋𝚛𝚒𝚌𝚊𝚝𝚒𝚘𝚗 𝚒𝚗 𝚋𝚘𝚝𝚑 𝚝𝚑𝚒𝚜 𝚜𝚌𝚒𝚎𝚗𝚝𝚒𝚏𝚒𝚌 𝚛𝚎𝚜𝚎𝚊𝚛𝚌𝚑 𝚊𝚗𝚍 𝚒𝚗 𝚖𝚊𝚔𝚎𝚛 𝚌𝚞𝚕𝚝𝚞𝚛𝚎, 𝚒𝚝 𝚒𝚜 𝚊 𝚑𝚊𝚗𝚍𝚜-𝚘𝚗 𝚒𝚗𝚟𝚎𝚜𝚝𝚒𝚐𝚊𝚝𝚒𝚘𝚗 𝚘𝚏 𝚕𝚎𝚊𝚛𝚗𝚒𝚗𝚐 𝚊𝚗𝚍 𝚕𝚒𝚗𝚔𝚒𝚗𝚐 𝚝𝚑𝚎 𝚝𝚘𝚘𝚕𝚜 𝚘𝚏 𝚝𝚑𝚎𝚜𝚎 𝚌𝚞𝚕𝚝𝚞𝚛𝚎𝚜 𝚏𝚛𝚘𝚖 𝚊𝚗 𝚘𝚞𝚝𝚜𝚒𝚍𝚎𝚛𝚜 𝚙𝚘𝚒𝚗𝚝 𝚘𝚏 𝚟𝚒𝚎𝚠.
𝙸𝚗 𝚍𝚘𝚒𝚗𝚐 𝚜𝚘 𝚒𝚝 𝚏𝚒𝚗𝚍𝚜 𝚜𝚝𝚛𝚞𝚌𝚝𝚞𝚛𝚎𝚜 𝚘𝚏 𝚒𝚗𝚏𝚘𝚛𝚖𝚊𝚝𝚒𝚘𝚗 𝚝𝚛𝚊𝚗𝚜𝚏𝚘𝚛𝚖𝚒𝚗𝚐 𝚊𝚗𝚍 𝚝𝚛𝚊𝚗𝚜𝚕𝚊𝚝𝚒𝚗𝚐 𝚒𝚗 𝚊𝚗𝚍 𝚘𝚞𝚝 𝚘𝚏 𝚖𝚊𝚝𝚎𝚛𝚒𝚊𝚕𝚒𝚝𝚢, 𝚞𝚜𝚊𝚋𝚒𝚕𝚒𝚝𝚢 𝚊𝚗𝚍 𝚖𝚎𝚊𝚗𝚒𝚗𝚐. -->


<!---
#### <a name="making">Making</a>

Humans have made material things from [as far back as we have been human](https://www.nature.com/news/oldest-stone-tools-raise-questions-about-their-creators-1.17369).
The material things we make all contain information and making material things can be seen as a kind of coding of materials or inversely a materialisation of information.
[Making things helps us to construct meaningful knowledge](http://namodemello.com.br/pdf/tendencias/situatingconstrutivism.pdf) and enables us to put information back into our environment so that we can influence it.


We now make materials that produce, transfer and store *bits* (basic units of logic) and we are living in an age when these technologies have blossomed and continue to evolve with us. -->

  <!---
<a class="thumbnail" href="#thumb"><img src="http://www.dynamicdrive.com/cssexamples/media/tree_thumb.jpg" width="100px" height="66px" border="0" /><span><img src="https://mdef.gitlab.io/2018/saira.raza/PROJECTDOCS/assets/Hilbertevolution.jpg" /><br />
Schematic timeline of information and replicators in the biosphere from [Michael R. Gillings, Martin Hilbert, Darrell J.Kemp (2016)](https://www.researchgate.net/publication/289707073_Information_in_the_Biosphere_Biological_and_Digital_Worlds)</span></a>
-->
<!---
<center>![](assets/Hilbertevolution.jpg)</center>

*<center> Schematic timeline of information and replicators in the biosphere from [Michael R. Gillings, Martin Hilbert, Darrell J.Kemp (2016)](https://www.researchgate.net/publication/289707073_Information_in_the_Biosphere_Biological_and_Digital_Worlds) </center>*

#### <a name="data">Data</a> driven material design

Today bits are being produced, shared and stored in amounts and formats that can seem like meaningless noise but advancing processing techniques such as machine learning are revealing new patterns of information about the material world. Data driven methods of finding materials to match properties are a opening a new paradigm in material science.

<center>![](assets/DraxlScheffler.jpg)</center>

*<center>'The Development of the paradigms (new modes of thought) of materials science and engineering' - [Claudia Draxl and Matthias Scheffler (2018)](https://arxiv.org/ftp/arxiv/papers/1805/1805.05039.pdf)</center>* -->

  <!---
<a class="thumbnail" href="#thumb"><img src="http://www.dynamicdrive.com/cssexamples/media/ocean_thumb.jpg" width="100px" height="66px" border="0" /><span><img src="http://www.dynamicdrive.com/cssexamples/media/ocean.jpg" /><br />So real, it's unreal. Or is it?</span></a> -->



<!---
#### <a name="meta">Metamaterials</a> and digital fabrication

Increasingly we are also making atoms from bits - using computers to design and fabricate material things - in ways that we were not able to make before.

The increasing affordability of digital fabrication tools since the 1990s has enabled researchers to fabricate more easily a class of materials called **metamaterials**. These materials exhibit properties rarely found in nature as a result of their repetitive fabricated structures rather than their molecular structures. These rare properties include transforming waves (electromagnetic, acoustic, seismic) in their surroundings in ways we don't see normally and applications of these properties  (invisibility cloaking, black holes, and passive energy harvesting from heat and sound) have been until now the stuff of science fiction. Metamaterials also open up new possibilities to improve efficiencies of existing essential technologies. Metamaterials are now being designed using data-driven, machine learning methods.

<center>![](assets/sei.png)</center>

*<center>Analogies between electromagnetic (nano-scale) metamaterial and (decameter scale) metacity - [Brulé, et al.(2017)](https://hal.archives-ouvertes.fr/hal-01635890/document)</center>*


**<font size="3" color="blue"><center>How accessible to a novice are these new tools and methods of data-driven material design and digital fabrication?</center></font>**

**<font size="3" color="blue"><center>How can a novice 'maker' design metamaterial devices from the noise of data that surrounds us?</center></font>**

From an outsiders point of view, this research project tries to:

* organise information about metamaterials and ways to fabricate them and test them.
* replicate metamaterials designed by scientific researchers in a maker space.
* construct a scanning sensor using low cost hardware and freely available information.
* find and gather documented tools and toolchains needed to design a metamaterial using data-driven methods.

In doing so it follows the journey of bits generated by sensors in a maker space, through algorithms that might be designed using opensource tools, to replicable metamaterial designs to finally the transformation of bits to atoms using digital fabrication.


<font face="Chivo" color="blue" size="5" background color="lime"><center>[Metamaterial taxonomy](https://mdef.gitlab.io/2018/saira.raza/project/metamaterialmap/)</center></font>

<font face="Chivo" color="blue" size="5" background color="lime"><center>[A review of machine learning tools and methods in metamaterial design](https://mdef.gitlab.io/2018/saira.raza/project/machinelearning)</center></font>

<font face="Chivo" color="blue" size="5" background color="lime"><center>[Bibliography](https://mdef.gitlab.io/2018/saira.raza/project/bibliography/)</center></font>


<center>coming soon..</center>

<font face="Chivo" color="blue" size="5" background color="lime"><center>Metamaterial Zoo</center></font>

<font face="Chivo" color="blue" size="5" background color="lime"><center>Making metamaterials in a maker space</center></font>

<font face="Chivo" color="blue" size="5" background color="lime"><center>Making a low cost scanning sensor</center></font> -->











  <!--- <center><img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/PROJECTDOCS/assets/projhomepage.png" width="739" height="707" border="0" usemap="#map" />
<map name="map">
<!-- Image map code generated at gaf210.imvustylez.net
<area shape="rect" coords="253,332,482,383" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT1" />
<area shape="rect" coords="245,137,399,213" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT2" />
<area shape="rect" coords="463,196,617,272" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT4" />
<area shape="rect" coords="463,196,617,272" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT3" />
<area shape="rect" coords="457,444,611,520" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT5" />
<area shape="rect" coords="247,524,401,600" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT6" />
<area shape="rect" coords="72,355,226,431" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT7" />
</map></center> -->


<!-- Image Map Generated by http://www.image-map.net/
<img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/projhomepage.png/" usemap="#image-map">

<map name="image-map">
    <area target="_blank" alt="Context" title="Context" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT1/" coords="261,327,487,386" shape="rect">
    <area target="_blank" alt="Interests" title="Interests" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT2/" coords="247,139,404,218" shape="rect">
    <area target="_blank" alt="Futures" title="Futures" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT4/" coords="476,205,611,266" shape="rect">
    <area target="_blank" alt="Intervention" title="Intervention" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT3/" coords="463,461,621,497" shape="rect">
    <area target="_blank" alt="Results" title="Results" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT5/" coords="251,536,397,588" shape="rect">
    <area target="_blank" alt="Reflection" title="Reflection" href="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/REPORT7/" coords="" shape="rect">
</map>-->
