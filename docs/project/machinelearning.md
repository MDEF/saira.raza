**<font face color="blue" size="5"><center>[░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)</center></font>**
<center><font size="3" color="blue">

[𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)
</font></center>
<br>

# Machine learning to design metamaterials

## Researched Machine learning methods for finding metamaterials

Below are all the methodologies found during the project for designing metamaterials using machine learning. Many other inverse design methods were found that did not employ machine learning and are not included here.

<iframe class="airtable-embed" src="https://airtable.com/embed/shr53SklTZpDwo9Ku?backgroundColor=red&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

## Machine learning tools for designing metamaterials

Below is a visualisation of the tools found for using machine learning to specifically find metamaterials.

<iframe
  src="https://embed.kumu.io/1539253674034d7fcc2554cbf16ecd2f"
  width="940" height="600" frameborder="0"></iframe>


## Paths to building a dataset

Deep learning works better with more data. The three strategies found to get more data are shown below. These are first hand data from experiment, data generated computationally from generated shapes and simulated responses and second hand data from other people's experiments of either of these methods.

 <center>![](assets/aidesigner2.png)</center>



<!---

### Simulation packages

### Using Existing datasets


#### Mining existing publications

### Collecting data by experiment

-->

  <!--- --> <!--- -->



<!---

# <center>Learning NN</center>
http://blog.rhino3d.com/2017/08/machine-learning-for-rhino-and.html

https://provingground.io/2017/08/01/machine-learning-with-lunchboxml/


## Grasshopper

### ThePipeInstaller

For sharing geometry between programmes (like simulators)

https://www.food4rhino.com/app/thepipe-geometry-exchange

## Types of Machine learning referenced in materials maper

Convolutional Neural Networks

Deep Learning Networks

resNets
Bodirectional


## Literature search

Link to spreadsheet of papers



A very rich paper is https://pubs.acs.org/doi/suppl/10.1021/acs.nanolett.8b03171/suppl_file/nl8b03171_si_001.pdf


## State of the art tools and toolchains referenced for Materials Design
-->
