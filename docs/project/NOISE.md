<html>
<style>

body {
font-family: monospace;
background-color: #eee;
margin: 40px;
}

.box {
width: 400px;
height:250px;
float: left;
padding: 0px 40px 80px 0px;
}

.screenshot {
width 400px;
height: 250px;
overflow: hidden;
}
.dead {
position: fixed;
top: 0px;
left: 0px;
pointer-events: none;
}

</style>
</head>

<body>

<div class="dead">
<img src="" style="width:100%; height:100%;">
</div>

<div class="box">
<p>
"Generating Noisy Data"
<br>
"Ways in which computers generate shapes, simulate physics and how AI can find shapes to fit conditions"
</p>
</div>

<div class="box">
</div>

<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza">
<img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/tools.png/ style="width:400px;">
</a>
</div>
<p>caption</p>
</div>

<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza">
<img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/tools.png/ style="width:400px;">
</a>
</div>
<p>caption</p>
</div>

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="css,result" data-user="jhnsnc" data-slug-hash="KNyOqV" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Perlin Noise Texture Generator">
  <span>See the Pen <a href="https://codepen.io/jhnsnc/pen/KNyOqV/">
  Perlin Noise Texture Generator</a> by Chris Johnson (<a href="https://codepen.io/jhnsnc">@jhnsnc</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
