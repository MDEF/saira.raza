**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘐𝘯𝘷𝘦𝘴𝘵𝘪𝘨𝘢𝘵𝘪𝘯𝘨 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭 𝘥𝘦𝘴𝘪𝘨𝘯 𝘢𝘴 𝘢 𝘯𝘰𝘷𝘪𝘤𝘦 𝘮𝘢𝘬𝘦𝘳</font></center>
<br>

# <center>Sustainability Model - Change, Impact and Innovation</center>
  <!--- --> <!--- --> <!--- -->

The impact or change I am aiming for is inclusion and accessibility to the knowledge and tools of the learning methods of science and making.

## Software and hardware development
Many of the information bottlenecks I encountered to using scientific and making tools were related to toolchains not being standardised or affordable.
We have seen how major organisations researching AI applications such as Google are already reaching out to Makers to help them to embed certain AI applications into the things they make and also are developing sandboxes where people can learn how AI works. This is also being done in the field of quantum computing to create better engagement and understanding of the technology.  Could accessible material science learning tools be a reality in the future once more data becomes available? Eventually could these two fields and their tools become synergised through interfaces?

## Interface designers
Accessible interfaces also help to engage more people. Materiality is an interesting topic for interface designers to explore to link the language of scientists with that of makers – with theory to materiality. Could learning interfaces incorporate material interactions as a learning tool?

## Physical spaces for Learning through making
Maker Spaces have helped to physically spread out digital fabrication tools. Organisations like MIT’s Center for Bits & Atoms Fab Lab Program already have the tools and community to work together to make “making” more accessible through simplifying toolchains by innovations such as the Mods interface for linking software to hardware and the Machines that Make project that share designs for making machines using equipment available in a Fab Lab.
Within MIT the How to Make Almost Anything course is open to students from across disciplines as is Fab Academy run across all Fab Labs and managed centrally.
Fab Foundation’s work is already working towards the vision of this Making from Noise intervention. Their mission is:
to provide access to the tools, the knowledge and the financial means to educate, innovate and invent using technology and digital fabrication to allow anyone to make (almost) anything, and thereby creating opportunities to improve lives and livelihoods around the world. Community organizations, educational institutions and non-profit concerns are our primary beneficiaries. The Foundation’s programs focus on: education (.edu), organizational capacity building and services (.org), and business opportunity (.com).
It could be interesting to have hackathons at fab labs which look at different material specialisms to find ways that current and future problems could be addressed via making.
Hackathons have already been held at Fab Labs but an AI Material science hackathon remains to be explored.
Fab labs are also open to spin off specialist courses. Examples in Barcelona include the Fabricademy which looks are developing skills in Digital fabrication for Textiles.


## Buy-in to copyleft and free tools

Price will affect engagement with these technologies. Buy in by the largest tech companies like google into free tools such as Sketchup for 3D modelling provides publicity and often comes with an added novice-friendly interface design.

Copyleft technologies also encourage collective problem- solving and testing. Platforms such as Github encourage building on existing solutions instead of reinventing the wheel.

Currently digital fabrication tools are getting easier to use, metamaterial fabrication can piggyback off this sector perhaps as addons to CAD programmes.






<!---

## What change? Innovating to encourage more representation (and hopefully less waste)


## Historical system changes: What other innovations have changed representation in technology




## Leveraging systems - working through different systems

5 levels of exponential growth


## Fostering adoption from users - minimising behaviours we are asking to change
 -->
