**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘐𝘯𝘷𝘦𝘴𝘵𝘪𝘨𝘢𝘵𝘪𝘯𝘨 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭 𝘥𝘦𝘴𝘪𝘨𝘯 𝘢𝘴 𝘢 𝘯𝘰𝘷𝘪𝘤𝘦 𝘮𝘢𝘬𝘦𝘳</font></center>
<br>

# <center>Intervention</center>

  <!--- --> <!--- -->

<!---
## Intervening a system

><center>“A system is a set of things—people, cells, molecules, or whatever—interconnected in such a way that they produce their own pattern of behavior over time”</center>-->

<!---
  *- Donella Meadows (2008) Thinking in Systems*
>PLACES TO INTERVENE IN A SYSTEM (in increasing order of effectiveness):
>- Constants, parameters, numbers (subsidies, taxes, standards).
>- Regulating negative feedback loops.
>- Driving positive feedback loops.
>-. Material flows and nodes of material intersection.
>- Information flows.
>- The rules of the system (incentives, punishments, constraints).
>- The distribution of power over the rules of the system.
>- The goals of the system.
>- The mindset or paradigm out of which the system — its goals, power structure, rules, its culture — arises.**

  *- Donella Meadows~ Leverage Points: Places to Intervene in a System*


*[Jamer Hunt 2019 Anticipating Future System States](https://jfsdigital.org/articles-and-essays/vol-23-no-3-march-2019/anticipating-future-system-states/?fbclid=IwAR2hO1KMXr51IqpWjuk6VnJC85mohjN9ax6mpoVKPyXg9NaIPRpeZGqZbME)*
-->
<!---  
## Designing to impact social practices

Social practices are a kind of cultural system and are changing all the time.

> practices emerge, persist and disappear as links between their defining elements are made and broken’

(Shove et al. 2012:21),
-->

## From tools and experiences of Digital Fabrication and Machine Learning to open, human - non-human, co-design cultures

<center>![](assets/pathofspec.jpg)</center>


### Changing the social practice of Making

I will take a 3-pronged approach in response to three distinct currently observed effects of the information age. These will be:

* a materialising approach turning bits to atoms
* an analysing approach to make information useful
* and an experiential approach to share my subjective experience of making.

Material things in making to examine:

- Metamaterials
- Digital Fabrication tools in a fab lab
- AI opensource resources

These three advancements exemplify the intricate ways information transforms in an out of materiality.

Skills to examine:

- Learning
- Making
- The role of the maker

Images to examine:

- Innovation
- Agency
- Inclusion


## INTERVENTION 1: MAKE A Metamaterial device and its test equipment in a Fab Lab


|||
| --- |-----|
| **Context to explore:** | Makers and digital fabrication |
| **Preferred Action:** | 	Make, Learn|
| **Exploration method:**| Constructionist, embodied design to find information|
| **Information to gain:**| Practical knowledge of metamaterials, practical knowledge on how to use digital fabrication|
| **Social practice elements involved:** |	Roles, maker, fixer, consumer, exhibition viewer, watcher |
| **Social practice element to change:**| 	Changing roles from user to maker to scientist to designer (constructing to inferring, measuring, testing) |
| **Intended system entry point:** | Mindset of where you get your information from |
| **Intended affect on the social practice system:**| People will look outside of the maker community for ideas therefore opening it and makers up to new ideas |
 ||-	The product will materialise a  subjective unique interpretation of information available to the system of things ||

The How to Make Almost Anything course is a hands-on introduction to the tools widely available for making. It is an intensive 18 week theory and making course. Theory classes are taught by Prof. Neil Gershenfeld Director, The Center for Bits and Atoms and followed up by the Fab Academy teachers in Barcelona. Every week a new tool is examined and making happens outside of lesson time.

The results of each week are documented on a shared repository which forms part of the Fab Lab networks resources that they build on.

This intervention will try to apply the tools of each week to either metamaterials, deep learning or the equipment needed to test metamaterials.


## INTERVENTION 2: STRUCTURE KNOWLEDGE AND TOOLS of metamaterials, digital fabrication and AI

|||
| --- |-----|
| **Context to explore:** |Information processing, AI |
| **Preferred Action:** |Logging, hoarding, mapping, visualising data |
| **Exploration method:**| Researching and hoarding data, mapping toolchains, labelling, co-design with AI|
| **Information to gain:**|Structural information from analysis |
| **Social practice elements involved:** |Skills, actions, knowhow, discourse, norms	 |
| **Social practice element to change:**| Documenting to spatial mapping or diagramming	 |
| **Intended system entry point:** |  Information flows|
| **Intended affect on the social practice system:**| Speed up, clarify information flows to make them actionable |
 || ||





## INTERVENTION 3 – DOCUMENT EXPERIENCES of making and designing metamaterials


|||
| --- |-----|
| **Context to explore:** | Experiences of information environment that you can’t measure|
| **Preferred Action:** | Expressing the experience of digitalfabrication, metamaterials and AI, Convey the experience of interacting with the objects of digital fabrication |
| **Exploration method:**| Experiencing information by making – Constructivist methods|
| **Information to gain:**| Experiences |
| **Social practice elements involved:** |Images, visions, inspiration   / things	 |
| **Social practice element to change:**| Change the idea learning to make as being linear and instructional logical to being about networks and asking and learning things you never did before / look at the things that push or pull people out of making	 |
| **Intended system entry point:** | The goals of the system. |
| **Intended affect on the social practice system:**|  Steer the vision of digital fabrication as a  tool for people who already have a propensity to make to those who perhaps  don’t / Encourage the development of easier  access to information, hardware designed for makers. |
 ||Examine the actions of making to encourage to present barriers or motivators in interactions with tools to provide material for designing for better inclusion  ||





<!---  Social Practice: I take a 3 pronged approach to the social practice of Making in the 21st century.
My material will be metamaterials material science
My tools will be Digital Fabrication in a fab lab  and AI
These three advancement exemplify the intricate ways information transforms in an out of materiality.

With a view to change
The mindset or paradigm out of which the social practice of making arises
The goals of the system
The distribution of powerflows

### INTERVENTION 1.0 - Materialising Information flows

Context to explore: Makers and digital fabrication
Preferred Action: Make, Learn
Exploration method: Constructionist, embodied design to find information
Information to gain: practical knowledge of metameterials, digital fabrication
Social practice elements involved: Materials, Skills, Roles, maker, fixer, consumer, exhibition viewer, watcher (Making)
Social practice element to change: Role of a maker, DIY recently scientifically designed materials outside of a lab, changing roles from user to maker to scientist (constructing to inferring, measuring, testing)
Intended system entry point: Mindset of where you get your information from
How will this affect the system of social practice: people will look outside of the maker community for ideas therefore opening it and makers up to new ideas
Output: Make A Metamaterial Device and test equipment

### INTERVENTION 2.
Context to explore: Information processing, AI
Exploration method: Researching and hoarding data, mapping toolchains, labelling, co-design with AI
Action: Logging, mapping, visualising data
Practice element to change: Skills, actions, knowhow ,accepted behaviours
Output: Toolchains, strategies
Aim: to structure knowledge and tools of metameterials, digital fabrication, AI

### INTERVENTION 3.

Context to explore: Constructiveist - experience
Exploration method: Experiencing information
Action: Expressing the experience of digital fabrication, metamaterials and AI
Social Practice element to change: Images, visions, inspiration   
Output: Video homage to trying to make a metamaterial and connecting with AI
Aim: To express experiential, subjective information











Making and testing a meta-device in a fab Lab
1. : make a scanner and a metadevice - gathering information noise

**Aims:**

* To experience energy in our surroundings through making - i.e through interacting with materials / geometric artefacts / and objects of digital fabrication.

* Expressing who we are though the things we make.

#### INTERVENTION 1.1 Subjective documentation shared with the fab lab community to contribute to a co-design culture
2. Experience intervention: immerse others into how it feels like - experiencing information

**Aims:**

* Expressing and sharing experiences of making and interacting with objects.


3. Knowledge intervention: find meaning - finding use in information


### INTERVENTION 2.0 Presenting a speculative user interface for people to co-design with AIs

**Aim:**

* To explore a speculative, non-anthropocentric ways of relating to AI, materials, environment, energy and shapes.
* To suggest an experience of an open design culture

#### INTERVENTION 2.1 Presentation or demonstration of how AI can help us to optimise the design of a metadevice through machine learning

**Aim:**

* To demonstrate how AI can help us reduce waste by finding optimum shapes using the banks of data we provide it.

#### INTERVENTION 2.2 Design this interface for inclusion

**Aim:**
Making the user interface accessible to as many age groups, literacies, and improving access for people with disabilities.

<iframe
  src="https://embed.kumu.io/504ae6e1712c7d7d85e26f7cc0880124"
  width="940" height="600" frameborder="0"></iframe>



1. Material intervention: make a scanner and a metadevice - gathering information noise

2. Experience intervention: immerse others into how it feels like - experiencing information

3. Knowledge intervention: find meaning - finding use in information
