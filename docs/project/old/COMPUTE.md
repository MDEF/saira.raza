<html>
<head>
<style>
<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
<style>
@import url('https://fonts.googleapis.com/css?family=Archivo+Black&display=swap');

body {
font-family: monospace;
background-color: #eee;
margin: 20px;
}

.box {
width: 400px;
height:250px;
float: left;
padding: 0px 20px 40px 0px;
}

.screenshot {
width 400px;
height: 250px;
overflow: hidden;
}
.dead {
position: fixed;
top: 0px;
left: 0px;
pointer-events: none;
}

</style>
</head>

<body>

<div class="dead">
<img src="" style="width:100%; height:100%;">
</div>

<div class="box">
<p>
<font face="Archivo Black" color="blue" size="10">TUNING NOISE</font>
<br>
<br/>
–<br/>
Ways in which computers generate shapes, simulate physics and how AI can find shapes to fit conditions
–<br/>
</p>
</div>


<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza"><img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/gold.gif/""    style="width:300px;"></a>
</div>
<p>caption</p>
</div>

<div class="box"><div class="screenshot"><a href="https://mdef.gitlab.io/saira.raza"><img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/gold.gif" style="width:300px;"/></a></div><p>Caption</p><p></p><p></p></div>
<div class="box"><div class="screenshot"><a href="https://www.nature.com/articles/srep22018?proof=true&draft=journal#supplementary-information"><img src="https://media.nature.com/original/nature-assets-draft/srep/2016/160223/srep22018/extref/srep22018-s2.gif" style="width:300px;"/></a></div><p>Direct observation of negative-index microwave surface waves</p><p>J. A. Dockrey, S. A. R. Horsley, I. R. Hooper, J. R. Sambles & A. P. Hibbins (2016)</p><p></p></div>
<div class="box"><div class="screenshot"><a href="https://www.pnas.org/content/113/10/2568"><img src="http://static-movie-usa.glencoesoftware.com/source/10.1073/30/5b86b8f6d75cf5ffaa01144171cc80f6a83ffb5d/pnas.1600521113.sm01.gif" style="width:300px;"/></a></div><p></p><p>Dexin Ye, Ling Lu, John D. Joannopoulos, Marin Soljačić, and Lixin Ran (2016)</p><p>Invisible metallic mesh</p></div>
"<div class=box><div class=screenshot><a href=https://www.seas.harvard.edu/news/2016/06/metalens-works-in-visible-spectrum-sees-smaller-than-wavelength-of-light><img src=https://www.seas.harvard.edu/sites/default/files/FocusingLensGifforWeb_0.gif style=width:300px;/></a></div><p>Light passing through the meta-lens is focused by millions of nano structures (Capasso Lab) .</p><p>Mohammadreza Khorasaninejad,Wei Ting Chen, Robert C. Devlin1, Jaewon Oh, Alexander Y. Zhu, Federico Capasso (2016)</p><p> Metalenses at visible wavelengths: Diffraction-limited focusing and subwavelength resolution imaging
</p></div>"
<div class="box"><div class="screenshot"><a href="http://news.mit.edu/2019/mathematical-tune-metasurface-lenses-0520"><img src="http://news.mit.edu/sites/mit.edu.newsoffice/files/images/metasurfaces.gif" style="width:300px;"/></a></div><p>From a randomly patterned metasurface, new technique quickly evolves an ideal pattern to produce a lens with desired optical effects. </p><p>Zin Lin, Victor Liu, Raphaël Pestourie, Steven G. Johnson (2019)</p><p>Topology optimization of freeform large-area metasurfaces</p></div>






<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza">
<img src="https://mdef.gitlab.io/saira.raza/PROJECTDOCS/assets/gold.gif" style="width:300px;">
</a>
</div>
<p></p>
</div>
