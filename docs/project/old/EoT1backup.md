# <center>Sacred Geometry in the 21st century</center>


<center>An exploration of contemporary and future scientific and mystical relationships with geometry</center>

## <center>Why?</center>
<center>Because technology is letting us see new geometric properties and construct geometry with greater ease than ever before.   </center>
<center>This may be an opportune time explore how we can interpret, use and share these breakthroughs and make them opensource / inclusive from the offset.</center>

Geometry describes and helps us conceptualise all energy and matter.
What are our contemporary 'sacred' geometries?


## How?

**<span style="background-color: lime"><font color="blue">[Looking at state of the art](#tsota)</font></span>** in:
  * scientific findings of geometry (mathematics, modelling of phenomena and processes)
  * use of geometry in art especially using new media
  * fabrication methods and technologies employing geometry

**<span style="background-color: cyan"><font color="blue">[Speculation](#speculation)</font></span>** of:
  * Future objects and scenarios that could exist in the axes of equality and rationality
  * Ridiculous futures
  * Postcolonial futures
  * Fictional, extended present Materials

**<span style="background-color: yellow"><font color="blue">Experimentation</font></span>** with:

  * 3D modelling
  * 3D printing using standard materials and equipment to make repeatable things
  * Uses of materials and adapting printers to use these
  * Audio, video and animation storytelling methods
  * Living with a speculative material / artifact / situation (design by experience)



## The journey so far


<iframe
  src="https://embed.kumu.io/56b68507e7f0968a3c34a3e24c775c6d"
  width="940" height="600" frameborder="0"></iframe>


## The State of the Art

<iframe
  src="https://embed.kumu.io/13767222bf72e56c2b1dc9ef3f135ce9"
  width="940" height="600" frameborder="0"></iframe>


### References

* 'A New Look at Gaia's Relationship with Water' - Peter Champoux (2017) Anthropology of consciousness. An exploration of the interconnectedness of the geometries of a water molecule, the geologic and geographic regions of our planet, and the universe as a whole. Water is shown as a crucial “bridge” passing from the microscopic to the stellar and interstellar.

* Cosmological and phenomenological transitions into how humans conceptualize and experience time  Nathalie Gontier

* [The Extraordinary Link Between Deep Neural Networks and the Nature of the Universe 2016] (https://www.technologyreview.com/s/602344/the-extraordinary-link-between-deep-neural-networks-and-the-nature-of-the-universe/)

## What? Possible Areas of interventions

Below is a map of interests that have developed since starting the course.
You can filter the elements by what weeks inquiries influenced them using the buttons at the bottom of the map.

<iframe src="https://embed.kumu.io/d31f3c0850a9c99c2bc32a9a425c31ef"
  width="940" height="600" frameborder="0"></iframe>


- Acoustic 3D printed metamaterials for
  - "sound field"/ soundscape installations/ environments
    * Sound Field Synthesis (2014) Academic Rudolf Rabenstein, Sascha Spors, Jens Ahrens, Press Library in Signal Processing  Volume 4, 2014, Pages 915-979, Chapter 32

    * http://interact-lab.com/our-research
  - Sacred Geometrical Artefacts
  - Acoustic earmuffs / Earphones

- Speculative optical / electromagnetic devices
    - invisibility cloaking
    - lighter more efficient motors and turbines
    - Foldable / transformable / textile lenses, antennae, magnets, conductors, computers (every electronic thing that exists)
    - Speculative narratives of neural networks




## Future scenarios and possible intervention in this area
and where it finds possible areas of intervention, using speculative approaches

![Voros section](wa/assets/w10/futurescone-voros.png)


  | Future     |      Scenario      | Interventions using preferred paths of inquiry  |
  | ------------- |:-------------:| -----:|
  | Possible      |  War, famine, water and fuel shortages |    |
  | Plausible | Political instability, greater inequality |     |
  | Probable |  Climate change, population growth, climate change refugees  |     |
  | Preferable |  Less reliance on fossil fuels, more rights-based decision making. Buy in from major world leaders in paradigm change regarding wealth distribution. More inclusive education and governance systems. More representation. | Cheaper or more engaging/ accessible educational tools    |
  | Premeditated | Costs of electronic equipment being driven down | |
  | Preposterous | Alien landing | |
  | Unknown unknowns |    |     |
  | Black Swan / Wildcard |  Asteroid   |     |



  **References:** to add too map
  * Deep learning to design metamaterials:
  Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018

  * Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood

  * Biologically enabled sub-diffractive focusing
  E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu

  * https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html

  * Magnetoactive Acoustic Metamaterials
  Kunhao Yu  Nicholas X. Fang  Guoliang Huang  Qiming Wang
  First published: 11 April 2018
