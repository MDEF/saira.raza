**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚<center></font>**
<center><font size="4" color="blue">Investigating metamaterial design as a novice maker</font></center>
<br>

<iframe
  src="https://embed.kumu.io/1775aefe8a8a25567eb71943c5d05630"
  width="940" height="600" frameborder="0"></iframe>

    <!--- <font face="Montserrat" color="black" size="5" background color="transparent"><center>   </center></font>
   <center><font size="6" color="blue"></font></center>
  Meta - unconventional - unnatural properties
  meta atoms - units
  metamolecules - assemblies of metaatoms that do unnatural things in isolation
  metamaterials - macro properties
  Metamaterials are highly regular composite structures that it exhibits properties not usually found in natural occurring materials.
  These properties include:
  - The ability to alter the path of waveforms including negative refractive index
  - localised mechanical properties within the material
  - transformable (and reversible) configurations
  <center><font size="6" color="blue">Bending waves</font></center>
  The mathematics:
  Meta materials can bend waveforms if repeating structures are less than half of the wavelength.
  http://web.mit.edu/nanophotonics/projects/Dissertation_Shu.pdf
  Meta atoms are synthetic units that enable the construction of metamaterials.
  These can be designed theoretically and fabricated at different scales to interact with different wavelength and forces.
  ## Resonance
  to change refractive index
  ### Split ring resonators (SRRs)
  ## Leak
  ### Leaky wave metamaterials
  ## Acoustic
  Audible  1.7cm - 17m
  http://advances.sciencemag.org/content/2/2/e1501595
  Space coiling
  Transmission type
  absorption
  active control
  HELMHOLTZ RESONATORS
  resonance-cavity-based
  space-coiling metamaterials.
  file:///C:/Users/saira/Downloads/Sound_Insulation_in_a_Hollow_Pipe_with_Subwaveleng.pdf
  https://phys.org/news/2015-09-one-way-tunnel-acoustic.html
  ![timeline of acoustic metamaterials](assets/AMMtimeline.png)
  *This figure is from the thesis [Electromagnetic Inspired Acoustic Metamaterials: Studying the Applications of Sound-Metastructures Interactions Based on Different Wave Phenomena by Hussein Esfahlani](https://www.researchgate.net/publication/318494810_Electromagnetic_Inspired_Acoustic_Metamaterials_Studying_the_Applications_of_Sound-Metastructures_Interactions_Based_on_Different_Wave_Phenomena).*
  https://www.youtube.com/watch?v=PZe7MBpaMcU
  ### control of resonance
  ## EM Metamaterials
  Part of the problem of designing EM materials is that you need a source device. The most readily available is Wifi and
  ### Split ring resonators
  Radio metamaterials
  (meters)
  using the Electrifi filament
  https://pratt.duke.edu/about/news/3d-printed-metamaterials
  coating each layer of 3D printed polymer with a fine mist of copper to give the lens a conductive surface.
  http://news.mit.edu/2012/new-metamaterial-lens-focuses-radio-waves-1114
  >The first double-negative metamaterial operated at around 5 GHz [56.18]. It was realized by structurally combining an array of 7 mm large double split-ring metamolecules with an array of continuous metallic wires (Fig. 56.6a)
  >The SRRs such as that shown in Fig. 1(inset) were fabricated using a commercially available printed circuit board.
  In order to test the results of the simulations, square arrays of SRRs were constructed with a lattice spacing of
  8.0 mm between elements.... Microwave scattering experiments were performed on the fabricated SRR medium, and the combined SRR/metal
  wire medium.
  https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.84.4184
  NRF24L01 2.4GHz Antenna Wireless Radio Transceiver
  The first metamaterial was a chiral mm made of twisted jute
  J.C. Bose: Proc. R. Soc. London 63, 146 (1898)
  >The metamaterial is double periodic with a square unit
  cell of 1515 mm2 see Fig. 1, which ensures that the
  structure does not diffract electromagnetic radiation for frequencies lower than 20 GHz. The overall size of the samples was approximately 220220 mm2
  . The metamaterial’s unit cell contains coaxial planar copper rosettes of fourfold symmetry in parallel planes, which are separated by very thin
  1.6 mm dielectric layers. The rosettes themselves consist of four copper semicircles of radius 2.9 mm, line width 0.8 mm, and thickness 35 m. Here we present results for four different forms of the metamaterial, corresponding to unit cells containing one, two, three, and four coaxial rosettes. A mutual counterclockwise twist of 15° introduced between adjacent rosettes makes the unit cells containing more than one rosette 3D-chiral. The metamaterial has been manufactured by lithography using standard FR4 circuit board substrates with a dielectric constant of 4.5+ 0.15i. The parameters of the simulated bilayered metamaterial correspond to those of the bilayered sample.
  All transmission and reflection measurements were performed in an anechoic chamber in the 3–17 GHz range of
  frequencies using broadband horn antennas Schwarzbeck
  BBHA 9120D equipped with lens concentrators and a vector network analyzer Agilent E8364B.
  Metamaterial with negative index due to chirality
  E. Plum, J. Zhou, J. Dong, V. A. Fedotov, T. Koschny, C. M. Soukoulis, and N. I. Zheludev
  Phys. Rev. B 79, 035407 – Published 12 January 2009
  ## steppers control
  bipolar
  h-bridge drivers for?
  https://learn.sparkfun.com/tutorials/experiment-guide-for-the-sparkfun-tinker-kit/experiment-9-driving-a-motor-with-an-h-bridge
  sparkfun - 1A
  can control 36V - high current determines the torque more than voltage
  you get chips with hbridges built in eg sparkfun L293NE
  Enable switch on these send the current at max speesd
  use pwm to vary the speed
  motor logic pin 1 and 2 (pin 1a nad 2a) - switching shape
  motor temrinal 1 and 2
  mixing
  analogue write will be speed of the motor
  **try prototyping in software **
  **always learn to build simple things first and streamline them**
  troubleshooting methods
  VCC2 - motor power supply - check amps/hr - peaking on starting motion - has to stand this. a capacitor can PROVIDE it s usually a filter of noise
  they are fast.
  so the chip separates the voltage
  **there is always a driver in front of the motor**
  ### protecting the electronics
  cities and chips
  newtonian physicals br
  a one electron signal is the perfect logic gate
  https://learn.sparkfun.com/tutorials/integrated-circuits/all#inside-the-ic
  * what output do you want - coil, LED etc?
  * should control what?
  * link to final project?
  * what device
  * what circuitry
   -->
