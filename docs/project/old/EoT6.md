# <center>Metamaterials</center>

## <center>Wave Interaction</center>

### <center>Optical and EM metamaterials</center>

[Invisibility cloaking as a use of negative refraction has been the holy grail of metamaterials largely pushing interest in metamaterials](https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html).
However metamaterials have to be of the nano scale in order to use their geometry to bend light. Currently methods to manufacture these Optical or Photonic metamaterials are very expensive and difficult to do for the visible light range but for longer wavelengths applications include:

1. Making radar satellite dishes cheaper and flatter
2. Optimising solar collection
3. Optimising IR (heat) collection or dissipation



### <center>Diatom frustules as optical metamaterials</center>


>"The interaction of diatom frustules with light consists of three main phenomena: **light conﬁnement, selective transmission and photoluminescence.**

> "It seems likely that the structure of the frustule provides the diatom with capabilities, not only to **focus light, but also to tune the reception of light in an optimal way**. It aligns with novel developments in antenna technology where electromagnetic bandgap structures are used to create low loss dielectric structures based on periodicity of super shapes to prevent propagation of certain frequencies, and increase isolation between antennas, and this can be tailored on angle of incidence and polarization.


Also in ﬂuid dynamics (see Section 4)shape sdeﬁned by Eqs. (1) and (2) have been used (Wang, 2008; Legay and Zilian, 2008). Eqs. (1) and (2)have also been used in many studies in nanotechnology (e.g. Tassadit et al., 2011; Macías et al., 2012; Forestiere et al., 2015), in **optimizing electro-osmotic pumps based on asymmetric silicon microchannel membranes** (Parashchenko et al., 2014) and in the study of dielectric properties of the skin (Huclova et al., 2010).

*[Diatom Frustule Morphogenesis and Function: A Multidisciplinary Survey - Edoardo De Tommasi, Johan Gielis, Alessandra Rogato (2017)](https://www.researchgate.net/publication/318567831_Diatom_Frustule_Morphogenesis_and_Function_A_Multidisciplinary_Survey)*

3d printed diatom frustule structures
https://www.youtube.com/watch?v=vOOvdheuAFs
http://cfb.unh.edu/phycokey/Choices/Bacillariophyceae/Centric/Centric_Unicells/ARACHNODISCUS/Arachnodiscus_Image_page.html

<iframe width="560" height="315" src="https://www.youtube.com/embed/vOOvdheuAFs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Diatom Frustules](assets/diatoms.jpg)
![Diatom Frustules](assets/emdiatom.jpg)
![Diatom Frustules](assets/diatom3.jpg)



**References:**

* 'Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii' K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood

* 'Biologically enabled sub-diffractive focusing' E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu

* 'Comparing optical properties of different species of diatoms'(2015)
Maibohm, Christian; Friis, Søren Michael Mørk; Su, Y.; Rottwitt, Karsten



### Deep Learning to design metamaterials

* [Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018]



### Possible Projects

* Low cost glasses
* Earphones / Acoustic filter earmuffs (see above)
* Soundscape environment
*


## Continua: Waves, Fields and Fluids

http://www.studioovervelde.com/wp-content/uploads/2018/07/My-Movie-46.mp4

###  Fields and Fluids in Chaos

### Sound field experiences


**References:**

* Sound Field Synthesis (2014) Academic Rudolf Rabenstein, Sascha Spors, Jens Ahrens, Press Library in Signal Processing  Volume 4, 2014, Pages 915-979, Chapter 32

* http://diy3dprinting.blogspot.com/2016/09/ninjaflex-3d-printed-metamaterial.html

* http://interact-lab.com/our-research/
