# <center>Metamaterials</center>
## <center>Mechanical metamaterials</center>

Scroll down to read about Mechanical metamaterials

Hasso Plattner Institute (HCI) Potsdam, Germany have developed some 3D printed metamaterial mechanical devices which you can see below.

They also developed a 'Metamaterial Mechanisms Editor' which is open in the other window on this screen

  <iframe width="560" height="315" src="https://www.youtube.com/embed/lsTiWYSfPck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="900" height="600" src="https://www.thingiverse.com/HassoPlattnerInstitute_HCI/collections/metamaterial-mechanisms" allowfullscreen></iframe>

You can send yourself their paper [here](http://alexandraion.com/wp-content/uploads/2016UIST-Metamaterial-Mechanisms-authors-copy.pdf) :


## <center>Digital Mechanical Metamaterials</center>

Using "mechanical computation" in 3D printed objects, i.e., no electronic sensors, actuators, or controllers typically used for this purpose. The resulting objects can be 3D printed in one piece and thus do not require assembly.

<iframe width="560" height="315" src="https://www.youtube.com/embed/A0lJUzcKDsY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
