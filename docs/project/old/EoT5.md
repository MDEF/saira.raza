# <center>Metamaterials</center>

## <center>Wave Interaction</center>

### <center>Optical and EM metamaterials</center>

[Invisibility cloaking as a use of negative refraction has been the holy grail of metamaterials largely pushing interest in metamaterials](https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html).
However metamaterials have to be of the nano scale in order to use their geometry to bend light. Currently methods to manufacture these Optical or Photonic metamaterials are very expensive and difficult to do for the visible light range but for longer wavelengths applications include:

1. Making radar satellite dishes cheaper and flatter
2. Optimising solar collection
3. Optimising IR (heat) collection or dissipation

  * https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html

### <center>Diatom frustules (nanoscopic algae-made greenhouses) as optical metamaterials</center>


>"The interaction of diatom frustules with light consists of three main phenomena: **light conﬁnement, selective transmission and photoluminescence.**

> "It seems likely that the structure of the frustule provides the diatom with capabilities, not only to **focus light, but also to tune the reception of light in an optimal way**. It aligns with novel developments in antenna technology where electromagnetic bandgap structures are used to create low loss dielectric structures based on periodicity of super shapes to prevent propagation of certain frequencies, and increase isolation between antennas, and this can be tailored on angle of incidence and polarization.


<a href="https://www.researchgate.net/figure/Schematic-representation-of-the-interplay-between-genes-and-molecules-geometry-and_fig2_318567831"><img src="https://www.researchgate.net/profile/Edoardo_De_Tommasi/publication/318567831/figure/fig2/AS:519601444356097@1500894046591/Schematic-representation-of-the-interplay-between-genes-and-molecules-geometry-and.png" alt="Schematic representation of the interplay between genes and molecules, geometry and physics, pivotal to understand form, function and development."/></a>



*from ['Diatom Frustule Morphogenesis and Function: A Multidisciplinary Survey' - Edoardo De Tommasi, Johan Gielis, Alessandra Rogato (2017)](https://www.researchgate.net/publication/318567831_Diatom_Frustule_Morphogenesis_and_Function_A_Multidisciplinary_Survey)*

#### [3d printing diatom frustule structures](https://community.ultimaker.com/files/file/2082-diatom-frustule/)


© Fractal Teapot 2018
![3d diatom build](assets/diatom-build.jpg)
![3d printed diatom](assets/3dprinteddia.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/vOOvdheuAFs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


![Diatom Frustules](assets/diatoms.jpg)
![Diatom Frustules](assets/emdiatom.jpg)
![Diatom Frustules](assets/diatom3.jpg)

© Fractal Teapot 2018  https://www.fractalteapot.com/portfolio/3d-printing-carbon-cycle/


**References:**

* http://cfb.unh.edu/phycokey/Choices/Bacillariophyceae/Bacillario_page/bacillario_key.html

* 'Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii' K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood

* 'Biologically enabled sub-diffractive focusing' E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu

* 'Comparing optical properties of different species of diatoms'(2015)
Maibohm, Christian; Friis, Søren Michael Mørk; Su, Y.; Rottwitt, Karsten


### Other things to explore

* Deep learning to design metamaterials:
Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018
