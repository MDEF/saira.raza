 <center><font size="7" color="blue">Being the technosphere</font></center>

<center><font size="4" color="blue">A metadesign approach to explore our relationship with technology, making and eachother</font></center>
<br>


  <!--- <center>![e8 lie](assets/e9liemox.png)</center>--> <!--- --> <!--- --> <!--- --> <!--- -->

## Idieas

### Scanner with maetamaterial


### Taxonomy map


### Metamaterial milled collage of antenna

### toolchain guides



made out of scrap PCBs?




## Three essential messages

1. We find information through many paths and computation is accelerating these - what can be made, what can be known.

2. Our individual relationships with tools of computation can help us express and contribute our experiences in how we culturally explore these questions - and in doing so can we open up design, art and science to eventual total participation from everyone? What kind of data could this produce today? happens when this non-anthropocentric noisy data soup? how do people feel about this today?

3. What will the ordinariness of these things be like in 100 years?

## What am I trying to do?

Remind people that we are all makers, designers, scientists and artists and that we have a stake in development of new knowledge, technologies and materials in this information boom.

What is your position on the subject?

Daniel charny was about fluidity of discipline.. swapping contexts .
