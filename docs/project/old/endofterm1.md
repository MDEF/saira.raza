# <center>Term 1</center>

<center>![Interests](assets/2bodyprob.gif)</center>


Below is a map of interests that have developed since starting the course.
You can filter the elements by what weeks inquiries influenced that using the buttons at the bottom of the map.

<iframe
  src="https://embed.kumu.io/d31f3c0850a9c99c2bc32a9a425c31ef"
  width="940" height="600" frameborder="0"></iframe>

## Sacred Geometry in the 21st century

An exploration of contemporary and future scientific and mystical relationships with geometry.

### Why?
Because technology is letting us see new geometric properties and construct these with greater ease. This may be an opportune time explore how best to share these breakthroughs and make them opensource / inclusive from the offset.



### How?

**<span style="background-color: lime"><font color="blue">[Looking at state of the art](#tsota)</font></span>** in:
  * scientific findings of geometry (mathematics, modelling of phenomena and processes)
  * use of geometry in art especially using new media
  * fabrication methods and technologies employing geometry

**<span style="background-color: cyan"><font color="blue">[Speculation](#speculation)</font></span>** of:
  * Future objects and scenarios that could exist in the axes of equality and rationality
  * Ridiculous futures
  * Postcolonial futures
  * Fictional, extended present Materials

**<span style="background-color: yellow"><font color="blue">Experimentation</font></span>** with:

  * 3D modelling
  * 3D printing using standard materials and equipment to make repeatable things
  * Uses of materials and adapting printers to use these
  * Audio, video and animation storytelling methods
  * Living with a speculative material / artifact / situation (design by experience)



## The journey so far


<iframe
  src="https://embed.kumu.io/56b68507e7f0968a3c34a3e24c775c6d"
  width="940" height="600" frameborder="0"></iframe>



## <a name="tsota">The State of the Art and references</a>
The state of the art and references in your chosen field that you have researched and looked into throughout the term

Mind Map of subjects





  Mathematical theory
  Biomimicry
  3D Printing
  metamaterials

  geometry being the key to information
  geometry making you hear things and see things
  the key 'sacred' geometries
  what are the modern sacred geometries?


### References

* 'A New Look at Gaia's Relationship with Water' - Peter Champoux
(2017) Anthropology of consciousness
An exploration of the interconnectedness of the geometries of a water molecule, the geologic and geographic regions of our planet, and the universe as a whole. Water is shown as a crucial “bridge” passing from the microscopic to the stellar and interstellar.


Cosmological and phenomenological transitions into how humans conceptualize and experience time  Nathalie Gontier

[The Extraordinary Link Between Deep Neural Networks and the Nature of the Universe 2016] (https://www.technologyreview.com/s/602344/the-extraordinary-link-between-deep-neural-networks-and-the-nature-of-the-universe/)






## <a name="speculation">Future Scenarios and possible intervention in this area</a>
  - The possible future scenarios of this area

  and where it finds possible areas of intervention, using speculative approaches



## What? Possible Areas of interventions
- Acoustic 3D printed metamaterials for
  - "sound field"" installations
  - Sacred Geometrical Artefacts
  - acoustic earmuffs

- Speculative optical / electromagnetic devices
    - invisibility
    - lighter more efficient motors and turbines
    - Foldable / transformable / textile lenses, antennae, magnets, conductors, computers (every electronic thing that exists)
    - Speculative narratives of neural networks


**Below are some interests that emerged or developed this term**

## Perception
### Science and Mysticism - thinking and feeling - measurable and immeasurable. Sacred geometry

### Discrete and Continuous thinking

Wave Particle duality shows us that a phenomena can be detected and modelled as waves (continuous) and particles (particulate).

## Systems thinking

![information structure evolution](wa/assets/infosystem2.png)

### Nature is dynamic and equilibrium is an illusion

### Chaos, Determinism and agency

### Sustainability and dynamic systems

### Chaos and AI

### Linked Proposals:
* [A Toolkit for dealing with chaos](proposals/#toolkit)

### Other paths to explore
* What comes after order and organisation?
* How can we describe the current times?
* What is the link between AI and Chaos Theory?


## Inclusive Systems

### Open, closed and isolated systems:


### Why inclusive systems in highly complex and chaotic environments?

>Chaos is caused by weak negative feedback underdamping the population’s “Volatility”!... Any driven damped system will only be able to self-stabilize (i.e. self-organize its own balancing stability) if it has the ability to “fine-tune” its way to equilibrium

*- Kieran D. Kelly, Experimental Computer Scientist, and Specialist in Complex Nonlinear Systems and Dynamics.*

### Making information and education more inclusive - everyone is a designer, scientist and artist

 breakthroughs in music happened from the most excluded, underprivilege even oppressed people.

#### Educational tools

* Learning by making (wordless learning) https://www.techwillsaveus.com/shop/education/

* User Interface technology

* Interface technologies for people with Autism and Attention Deficit Disorder

* Mobile phones in developing countries

* Mobile phone innovations by and for refugees

* Mobile phones in disasters

* Disrupting exclusive university systems - MOOCs and youtube

* Communities of education and practice


### Building respect and representation

#### The value of work
Current economic systems are not fit for the average working person let alone people with disabilities or other disadvantages.
Can looking at work as an isolated system without money or time help us to reverse engineer a system that suits the dynamics of the system better?

##### Taking money out of the equation / what is work? / what is money?
Mariana proposed the idea of work being an investment and money being a "joker card" for this. If we consider only what goes in and out of our bodies, can we find another mediating system other than money? What would this system look like? What are it's properties.

##### Who should be designing jobs


##### Work and social roles


##### Homing in on particular jobs
Understanding the human resource that goes into systems such as water and sanitation could help people appreciate their resources (human and natural)

##### Helping people navigate careers and education


##### Telling stories about how it feels to navigate work


##### Humans are not machines
Breakthroughs in technology (agricultural, mechanical, cybernetics) seem to invoke systems of humans mimicking the technology. The free market born out of Objectivism embedded a faith in stable states that were shown historically not to exist as systems become complex.

We also know that humans' physiology responds to time and energy very differently to logic circuits in semiconductors.

### Linked Proposals:
* 'ADDesk'
* Interfaces for people with Autism
* Work investment plotter



**References:**

* Mechanisms for Inclusive Governance - Raymond L. Ison Philip J. Wallis (2016)
* Elements of Inclusion: Findings from the Field, Christopher McMaster University of Canterbury (2014)
* https://files.eric.ed.gov/fulltext/EJ1040134.pdf
* Hardy, I., and S.Woodcock.2015. “Inclusive Education Policies: Discourses of Difference, Diversity and Deficit.” International Journal of Inclusive Education
* http://www.kierandkelly.com

To read:

* Water Issues from a System Dynamics Perspective ByPeter Wade, Saeid Eslamian

* Boundary matters: the potential of system dynamics to support sustainability?
Author links open overlay panelEhsanNabaviaKatherine A.DaniellbHusainNajafi

* [Mehdi Alirezaei 1, Nuri C. Onat 2, Omer Tatari 1,* OrcID and Mohamed Abdel-Aty (2017) The Climate Change-Road Safety-Economy Nexus: A System Dynamics Approach to Understanding Complex Interdependencies'](https://www.mdpi.com/2079-8954/5/1/6/htm)

* [Nasrin Khansari ; Arash Vesaghi ; Mo Mansouri ; Ali Mostashari (2015) 'The Multiagent Analysis of Social Progress in Energy Behavior: The System Dynamics Methodology'()

* [Piet Hut,1 Mark Alford, and Max Tegmark (2005) 'On Math, Matter and Mind'](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.405.1666&rep=rep1&type=pdf)


*  [Yu Y1, Xiao G2, Li G3, Tay WP2, Teoh HF1.(2017) 'Opinion diversity and community formation in adaptive networks.'Chaos: An Interdisciplinary Journal of Nonlinear Science]()




## Metamaterials - bending light and sound using geometry

Cellular structures - mechanically efficient - stiffness



### Mechanical

Geometry of Transformable Metamaterials Inspired by Modular Origami
Yunfang Yang and Zhong You

https://arxiv.org/ftp/arxiv/papers/1607/1607.06014.pdf

### Transformable materials

<iframe width="560" height="315" src="https://www.youtube.com/embed/7A_jPky3jRY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/aV07hCF7-AQ?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Rational design of reconfigurable prismatic architected materials
Johannes T. B. Overvelde, James C. Weaver, Chuck Hoberman & Katia Bertoldi
Nature volume 541, pages 347–352 (19 January 2017)


### Acoustic

[Cecilia Casarini,  Ben Tiller, Carmelo Mineo, Charles N. MacLeod, James F.C. Windmill and Joseph C. Jackson
“Enhancing the Sound Absorption of Small-Scale 3D Printed Acoustic Metamaterials Based on Helmholtz Resonators,”](https://pureportal.strath.ac.uk/files-asset/82476858/Casarini_etal_IEEE_Sensors_2018_Enhancing_the_sound_absorption_of_small_scale_3D_printed_acoustic_metamaterials.pdf)


### Magnetically controlling metamaterials


### Deep Learning for designing metamaterials

* Deep learning to design metamaterials:
Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018

### 3D printing metamaterials

* https://johannes.frohnhofen.com/metamaterial-mechanisms/

* https://www.thingiverse.com/HassoPlattnerInstitute_HCI/collections/metamaterial-mechanisms

* http://alexandraion.com/wp-content/uploads/2016UIST-Metamaterial-Mechanisms-authors-copy.pdf

* http://www.sussex.ac.uk/broadcast/read/39354

* https://www.3dnatives.com/en/metamaterials-3d-printed180420184/

*  A team led by USC Viterbi researchers developed 3-D printed acoustic metamaterials that can be switched on and off remotely using a magnetic field   https://viterbischool.usc.edu/news/2018/04/3-d-printed-active-metamaterials-for-sound-and-vibration-control/

<iframe width="560" height="315" src="https://www.youtube.com/embed/t12kHV001hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/zixzyeF25SY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/BcVxRyvipcU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
aerogel

<iframe width="560" height="315" src="https://www.youtube.com/embed/lsTiWYSfPck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/pNsnvRHNfho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Sound Manipulation

### Light Manipulation

Materials would have to be of the nano scale in order to use their geometry to bend light. Currently methods to manufacture these Photonic metamaterials are very expensive and difficult to do.

### Use of diatom frustules as Metamaterials
* Diatom Frustule Morphogenesis and Function: A Multidisciplinary Survey - Edoardo De Tommasi, Johan Gielis, Alessandra Rogato

"Several animals and plants,mainly insects, birds and ﬂowers, developed, through billions of yearsof evolution and as constitutive part of their organisms, sub-micron, pe-riodic or quasi-periodic architectures which substantially act as photon-ic crystals, giving rise, by means of selective transmittance andreﬂectance at different wavelengths, to so-called structural colors"
The interaction of diatom frustuleswith light is even more articulate,and consists of three main phenomena: light conﬁnement, selectivetransmission and photoluminescence.


**References:**
* Deep learning to design metamaterials:
Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018
* Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood
* Biologically enabled sub-diffractive focusing
E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu
* https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html
* Magnetoactive Acoustic Metamaterials
Kunhao Yu  Nicholas X. Fang  Guoliang Huang  Qiming Wang
First published: 11 April 2018

### Possible Projects
* Low cost glasses
* Earphones
* Soundscape environment


Metamaterials melting spacetime
November 7, 2018 | 6:17 pm | Posted by Giulia Pacchioni | Category: Guest post

Post by Igor I. Smolyaninov, Department of Electrical and Computer Engineering, University of Maryland.

## Continua: Waves, Fields and Fluids

http://www.studioovervelde.com/wp-content/uploads/2018/07/My-Movie-46.mp4

###  Fields and Fluids in Chaos

### Sound field experiences


**References:**
* Sound Field Synthesis (2014) Academic Rudolf Rabenstein, Sascha Spors, Jens Ahrens, Press Library in Signal Processing  Volume 4, 2014, Pages 915-979, Chapter 32
* http://diy3dprinting.blogspot.com/2016/09/ninjaflex-3d-printed-metamaterial.html
http://interact-lab.com/our-research/

## Possible project arcs

### Work - Multiplicity of experiences - Representation - Stories - Voice - Sounds - Acoustics -


**Mapping Tools:**
* http://arborjs.org/halfviz/#/python-grammar
* https://pp.bme.hu/ar/article/view/8215
* https://gephi.org/
* https://rawgraphs.io/
* https://npm.runkit.com/d3-force - forces
