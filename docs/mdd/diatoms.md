

# <center>Diatoms</center>

<center>![](assets/Diatoms-blue.png)</center>

Since week 2 of the MDEF course I have been interested in a group of algae called Diatoms.
Having first been attracted to these organisms by their varied and fascinating appearance, I came to find out that these abundant algae not only play a surprisingly significant part in some of the 'wickedest' problems we face today but also have incredible properties and behaviours that could be used to address these same problems.

The more I learnt about diatoms, the more I wanted to explore how to work with these organisms in a hope to gain some first hand about how structures and materials they produce can manipulate light, absorb other materials, cut softer materials, and incubate substances.


<iframe
  src="https://embed.kumu.io/f3e47ebd7e4b6f58d3b5b0340b446c12"
  width="940" height="600" frameborder="0"></iframe>


## Sources

### Living diatoms
Diatoms live in water and need silica, iron and zinc as well as light to grown and survive (for their short life of less than a week). Places you can find them include nutrient rich spots in the ocean surface water and freshwater, sponges, and in the Schmutzdecke (active bio layer) in slow sand water filters and septic tanks. They have a brownish colour owing to the oil they contain to store carbon however they still contain chloroplasts for photosynthesis. They also produce Polysacharides and chitin while alive.

My source of living diatoms:

![rock](assets/diatomrock300.jpg) ![foam](assets/diatomfoam300.jpg)

### Fossilised Diatoms
River and sea beds or what were river and sea beds can contain layers of soft powdery fossilised diatoms - known as Diatomaceous Earth or Diatomite.

My source of fossilised diatoms:

 ![Diatical](assets/diatical.jpg)

Also one fossilised diatom frustule has been found in a meteorite!

 ![meteorite](assets/diatommeteorite.PNG)

## Properties

### Light bending shells

In life, these beautiful unicellular organisms build an amorphous silica (glass) shells. These multi-layered hierarchical structures called frustules have incredible 'metamaterial' light bending properties. This enables them to distribute light of optimum (blue) frequencies for photosynthesis. They come in several intriguing shapes.

A number of diatoms exhibit iridescence properties and are often referred to as ‘sea opals’
[From living light to living materials Sophie Cassaignon a,b,c, Robin de Maleprade a,b,c, Nadine Nassif a,b,c, Jacques Livage ](https://cyberleninka.org/article/n/3469.pdf)


### Multi-stage pores
Multi-stage pores make diatoms excellent filters.

![Layers](assets/diatom layers.PNG)
![Hierarchical Structure](assets/diatom microstructure.jpg)
*Wavelength and orientation dependent capture of light by diatom frustule nanostructures Julien Romann et al (2015) Scientific Reports*

### Strength

Diatoms frustules were recently found to be the strongest biological material in the world owing to their honeycomb silica structure.

![Honeycomb Structure](assets/diatomhoneycomb300.jpg)

When fossilised, diatom frustules become more crystalline and abrasive quartz structures.

### Able to process into a semi conductors including silicon

Treatment with TiO2 and MGO improve the semiconducting and capacitive properties of the diatom frustule and silica frustules can be processed into silicone

Silver plating

## Influence

### Oxygen levels in the atmosphere

### Carbon capture from the atmosphere to feed most of the ocean

### Crude Oil

### Water purification
Slow sand filtration when alive AND as diatomaceous earth. Indicator of the content of the water systems both past and present.

### Nutrient Currents and ecosystems

### Ice crystal formation in clouds

## Possible Applications

AS they play such an important ecological role they should be used in something in tune with the ecosystem or highly controlled.


### Water filters

### Nanobots

### Genes for growing silicon and therefore silicon

### Living carbon capture / solar panel / biofuel devices

## Cultural and experiential factors

Diatomaceous earth was discovered in Germany in the 1800s in mines.

With improving microscopy there was increasing awareness and study into their intriguing structures.

### Art

http://leegj.com/diatomic-number/

![Drawing](assets/diatomdrawing.jpg)

![harvesting](assets/diatomcollection.jpg)

## Processing

### Clean with hypochlorite

### Doping with TiO2 and Mg

### Plating with silver



## 3D Printing Diatoms

### At IAAC

https://www.flickr.com/photos/fraguada/albums/72157624022963834

*IaaC at Santa Monica Arts, 2010:
Images showing IaaC's Contribution to Mireya Masó exhibition on Antartica. The exhibition titled: Antartica-Times of Change, depicts the changing climate of Antartica. One aspect of the exhibition included a study on Diatoms and Radiolaria, microscopic organisms that produce shells formed under whatever conditions are present at the time. The work produced by IaaC students was done in collaboration with Director Marta Malé-Alemany (Digital Fabriation) and Luis Fraguada (Digital Tools) with the assistance of Cesar Cruz Cazares.*

https://www.flickr.com/photos/32117344@N00/4353377872/in/photostream/

https://www.fractalteapot.com/portfolio/3d-printing-carbon-cycle/

## References

From living light to living materials
Sophie Cassaignon a,b,c, Robin de Maleprade a,b,c, Nadine Nassif a,b,c, Jacques Livage


Infection Dynamics of a Bloom-Forming Alga and Its Virus Determine Airborne Coccolith Emission from Seawater
 panelMiriTrainic1IlanKoren14ShlomitSharoni1MiguelFrada3LiorSegev1YinonRudich1AssafVardi

 High-flying diatoms: Widespread dispersal of microorganisms in an explosive volcanic eruption. Available from: https://www.researchgate.net/publication/260208793_High-flying_diatoms_Widespread_dispersal_of_microorganisms_in_an_explosive_volcanic_eruption [accessed Jan 30 2019].

 All New Faces of Diatoms: Potential Source of Nanomaterials and Beyond
 Meerambika Mishra,1 Ananta P. Arukha,2 Tufail Bashir,3 Dhananjay Yadav,4,* and G. B. K. S. Prasad5,*
 Author information Article notes Copyright and License information Disclaimer

 [Scientists find strongest biological material in the world in microscopic algae cells
It has been discovered that the shells of the tiny cells have the highest strength-to-weight ratio of any biological material
Doug Bolton
Tuesday 9 February 2016](https://www.independent.co.uk/news/science/diatom-caltech-strongest-natural-biological-material-strength-toughness-a6863176.html)

[Xiaoguo Liu, Fei Zhang, Xinxin Jing, Muchen Pan, Pi Liu, Wei Li, Bowen Zhu, Jiang Li, Hong Chen, Lihua Wang, Jianping Lin, Yan Liu, Dongyuan Zhao, Hao Yan, Chunhai Fan. Complex silica composite nanomaterials templated with DNA origami. Nature, 2018; DOI: 10.1038/s41586-018-0332-7](https://www.sciencedaily.com/releases/2018/07/180716151626.htm)

https://www.researchgate.net/publication/258812475_Surface-Enhanced_Raman_Scattering_on_Diatom_Biosilica_Photonic_Crystals
