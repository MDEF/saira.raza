<html>
<head>
<title></title>

<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Chivo&display=swap" rel="stylesheet">

<style>
.overlay {
position: fixed;
top: 0px;
left: 0px;
pointer-events: none;
}
.overlay2 {
position: fixed;
top: 800px;
left: 800px;
pointer-events: none;
}
.box {
width: 400px;
height: 250px;
float: left;
padding: 0px 40px 80px 0px;
}
.screenshot {
width: 400px;
height: 250px;
overflow: hidden;
}



<!--
*{margin:0;padding:0;}-->

section{
  height:100vh;
  width:100vw;
  display:flex;
  justify-content:center;
  align-items:center;
  background:yellow;
}

h1{
  font-family:gt america;
  font-stretch:extended;
  transform:scale(2,1);
  position:absolute;
  top:50px;
}

a.wonk{
  /*font-family:Helvetica;
  text-transform:uppercase;*/
  letter-spacing:5px;
  font-size:6vw;
  -webkit-text-stroke: .2vw black;
  color: transparent;
  text-decoration:none;
  display:block;
  transition:all .2s ease-in-out;
  animation:wubble 25s ease-in-out infinite alternate-reverse;
}

div{
  transform:skewY(-20deg) skewX(50deg);
}
a.wonk:hover{
  color: red;
}
@keyframes wubble{
  from{
    transform:rotate(-5deg) scaleY(1.2) scaleX(1);
  }
  to{
    transform:rotate(5deg) scaleY(1) scaleX(1.2);
  }
}



</style>
</head>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<font size="3" color="lime">
<a href="https://mdef.gitlab.io/2018/saira.raza/" class="wonk">
get me out of here
</a>
</font>

<section>
<br>

  <h1>HOME</h1>



  <div>
    <a href="#intro" class="wonk">About</a>
    <a href="https://mdef.gitlab.io/2018/saira.raza/project/overview/" class="wonk">Project</a>
    <a href="https://mdef.gitlab.io/2018/saira.raza/fa/fab1/" class="wonk">Fab academy</a>
  </div>
</section>

<body>




<div class="overlay">
<img src="assets/transpheadlineextra.png/" style="width:100%; height:100%;"></div>

<center><a href="https://mdef.gitlab.io/2018/saira.raza/aboutme/"><img src="assets/rgb-additive-colors.gif" width="300" height="300" title="About me" alt="colours"></a></center>


<!-- <center><a href="https://mdef.gitlab.io/saira.raza"><img src="assets/led.gif" width="498" height="500" title="Led" alt="colours"></a></center>--->

<div class="box">
<a href="https://mdef.gitlab.io/2018/saira.raza"><img src="https://www.av8n.com/physics/img48/sphere-ylm-3.gif" width="300" height="300" title="Home Page" alt="colours"></a>
</div>

<div class="box">
<center>
<font size="2"><a name="intro">This website</a> documents my learnings and reflections from participating in the 9 month Master in Design for Emergent Futures course at IAAC and Elisava.

The appearance and content of this website will develop with my skills and insights. I'm told the current appearance of this website is a result of 'hacking at the hierarchies of the style code' which I think reflects well my current approach to instigating change.

The 'About Me' section gives a little context for my interpretation of the course.</center></font>
</div>

<!--
<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza/fa/fab1/"><img src="assets/3dprinting.gif" style="width:400px;"/></a>
</div>
<p>Fab Academy</p>
</div>
--->


<center><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week1/"><img src="assets/mars.gif" width="50" height="50" title="TERM 1" alt="TERM 1"></a><a href="https://mdef.gitlab.io/2018/saira.raza/mdd/materialdrivendesign/"><img src="assets/smallBrainReco.gif" width="50" height="50" title="TERM 2" alt="TERM 2"></a><a href="https://mdef.gitlab.io/2018/saira.raza/fa/fab1/"><img src="assets/3dprinting.gif" width="150" height="150" title="FAB ACADEMY" alt="FAB ACADEMY"></a><a href="https://mdef.gitlab.io/2018/saira.raza/project/overview/"><img src="assets/inversedesign.gif" width="150" height="150" title="PROJECT" alt="PROJECT"></a><a href="https://mdef.gitlab.io/2018/saira.raza/resources/"><img src="assets/links.gif" width="150" height="150" title="References" alt="References"></a></center>

<center><a href="https://mdef.gitlab.io/2018/saira.raza/aboutme2/"><img src="https://media0.giphy.com/media/t0XXqlRWJ8uek/source.gif" width="82" height="86" title="About me" alt="colours"></a></center>
<br>

<center>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week1/"><img src="assets/mars.gif" width="82" height="86" title="MDEF Bootcamp" alt="Week 1"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week2/"><img src="assets/dna.png" width="82" height="86" title="Biology Zero" alt="Week 2"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week3/"><img src="assets/tool.png" width="82" height="86" title="Design for the Real Digital World" alt="Week 3"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week4/"><img src="assets/colorcube.gif" width="82" height="86" title="Exploring Hybrid Profiles" alt="Week 4"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week5/"><img src="https://media.giphy.com/media/nHyt5zmBphwl2/200w_d.gif" width="82" height="86" title="Navigating Uncertainty" alt="Week 5"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week6/"><img src="assets/ainumber.png" width="82" height="86" title="Design with Extended Intelligence" alt="Week 6"></a></div>
</center>

<center><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week7/"><img src="assets/solenoid.jpg" width="82" height="86" title="The Way Things Work" alt="Week 7"></a>
<a href="https://mdef.gitlab.io/2018/saira.raza/wa/week8/"><img src="assets/wavelet.gif" width="82" height="86" title="Living with Ideas" alt="Week 8"></a>
<a href="https://mdef.gitlab.io/2018/saira.raza/wa/week9/"><img src="assets/currentcoil.png" width="82" height="86" title="Engaging Narratives" alt="Week 9"></a><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week10/"><img src="wa/assets/lightcone.gif" width="82" height="86" title="An Introduction to Futures" alt="Week 10"></a>
<a href="https://mdef.gitlab.io/2018/saira.raza/wa/week11/"><img src="assets/vortex.gif" width="82" height="86" title="From Bits to Atoms" alt="Week 11"></a>
<a href="https://mdef.gitlab.io/2018/saira.raza/wa/week12/"><img src="assets/field poles.png" width="82" height="86" title="Design Dialogues" alt="Week 12"></a></center>
<br>
<br>

<!--
<center><a href="https://mdef.gitlab.io/saira.raza/interests/"><img src="assets/cyandipole.gif" width="82" height="86" title="My interests" alt="interests"></a> <a href="https://mdef.gitlab.io/saira.raza/fictions/"><img src="assets/colourwave.gif" width="82" height="86" title="Fictions" alt="fictions"></a> <a href="https://mdef.gitlab.io/saira.raza/proposals/"><img src="assets/field.gif" width="82" height="86" title="Proposals" alt="Proposals"></a></center>
</div>

--->

</body>
</html>
