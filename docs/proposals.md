

# <center>Proposals</center>

<center>![Proposals](assets/field.gif)</center>

# Term 2

## Hardware assembled- AI code generator (electronic lego talks to an AI who generates code for it)

Hardware that you fit together, connect to a hardware interface computer or the cloud via an app.
The app translates the layout into a series of machine logic blockcoding modules as in grasshopper with input Voltage and output result configurations available according to the .

you put these in and the app writes the code  to connect the blocks.

Atoms to AI to bits to functions

# Term 1

## Greenhouse-Algae Bioreactor-Urinal ##
Spirulina is a nutritious algae which requires light, easily found household ingredients and nutrients found in human urine to grow. Could a spirulina growing bioreactor be designed that is also a urinal?
Could the sunlight reaching the algae be maximised using light bending containers? Could a supplementary artificial light improve growth?


## ADDesk  - A Desk for someone with Attention Deficit Disorder
Pau Alsina in week 5 took us through how Foucault proposed that the design of chairs in schools were devices of power to force attention to create good citizens.

Could the power dynamic be reversed and could a chair or furniture or a room help someone who struggles with attention and wants to improve it.

It made me think about if there are places where we are coaxed into being distracted - in politics Red Herrings communications are designed to take your eye off other issues. Escapism is used a lot in entertainment.

## A new way of valuing of Work.
Can we design a blockchain interface that values your work and adjusts your pay depending on the value placed on your role by your peers?
This is what the free market and money is supposed to achieve however the focus of rewards has moved from bartering to supporting people that have the most capacity to create jobs.


## Record player for non musical/ non sonic patterns
### Why?
Because record players are an interface technology. Like information They have a mystical quality as well as a scientific one and an artistic one.

### A DNA transcribing record player / phonograph
The mechanical nature of DNA transcription reminded me of simple hand wound toys. Record players work on the premise of a stylus being pushed in 3 dimensions by a shape. Can this analogy work for DNA transcription somehow to enable us to hear the sound of transcription?

"3 billion base pairs in each cell fit into a space just 6 microns across. If you stretched the DNA in one cell all the way out, it would be about 2m long and all the DNA in all your cells put together would be about twice the diameter of the Solar System."

https://www.myminifactory.com/object/3d-print-dna-rna-building-set-transcription-and-translation-model-33824

##  [A Toolkit for dealing with chaos](https://mdef.gitlab.io/saira.raza/proposals/#toolkit)  

An education / meditation tool on:
* the inevitability of instability
* and Determinism and the butterfly effect

A tool for helping action through:
* motivation
* planning
* productivity or building habits
* careers

## Neural Network instruments

A schematic of a neural network looks similar to an electronic music hardware schematic - where "FX" knobs vary the modulation of sound waves in different stages. Neural networks are probability machines that rely on human-set limits of probability that then trigger additional stages of analysis.

The trigger limits between stages of analysis reminded me of valves.
We described this as an analogy for bias.
