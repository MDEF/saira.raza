

# <center>Fab Academy Week 8</center>
# <center>Embedded programming</center>

<center>![](assets/)</center>

<!---  Nice debugging process with the resistor led
Described the programming process you used.
Explain the key point of your code. How is it structured (loops, variables, etc). Why are you using softserial library?A nice workflow is to add comments to the code in order to be easy to read for the non-expert people.
Describe how did you install the Arduino library for Attiny, or add the link to some tutorial.
Identify relevant information in a microcontroller datasheet add link and explain what it was useful for you in order to program the board
What questions do you have? What would you like to learn more about?
The download link seems to be broken, or you moved the files to another side.
Recommendation: Instead of adding screen captures of the Arduino codes, it is much more optimal if you insert them directly into the HTML webpage as text. In this way it is easier to copy/paste, for people who want to replicate your work.   -->

This weeks activities focussed on microcontrollers - tiny programmable computer chips -  and how to programme them. I really enjoyed learning about the material side of processors. There were a lot of steps to understand and a lot of new terminology introduced so it has taken several weeks to tie these together from many different sources to get a full picture of how to approach the task at hand. I hope to continue to clarify and distil the information on this page before the course ends.

It surprised me how undeveloped the tools are for the maker community to do (and learn to do) this important function.

##### **Resources:** (a selection)
* [Fab academy class](http://academy.cba.mit.edu/classes/embedded_programming/index.html)
* [Fab Academy Barcelona lesson notes](https://hackmd.io/QF32KNJERb29hdCuDVIGVw)
* [Fab Academy Barcelona Electronics club lesson](http://fabacademy.org/2019/labs/barcelona/local/clubs/electroclub/embeddedprogramming/)
* [Fab Academy Barcelona's electronics club notes on electronics design](http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/electronics2/)
* [Fab Academy Barcelona Understanding Microcontrollers notes including how to program the FabISP to accept and transfer code](https://hackmd.io/PsZlUri7TTqB3i55c_c04Q#Understanding-microcontrollers)
* [Tutorial from highlowtech on Progamming ATtiny44 microcontroller using the Arduino software](http://highlowtech.org/?p=1695)
* [A tutorial on AVR, avr-gcc and avrdude on the fab academy website](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/makefile.html)
* [A tutorial on using the Arduino IDE to programme the ATTiny from the Fab Academy Website](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html)
* [MIT Tutorial on the FABISP](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html)


## Microprocessors

A Microprocessor is an integrated circuit which has only the CPU inside and don’t have RAM, ROM, and other peripheral on the chip someone has to design how it integrates with these things externally. These form the brains of most desktop and laptop computers.

## Microcontrollers

Microcontrollers have a CPU as well as some RAM, ROM and other peripherals all embedded on a single chip - its a tiny computer on a chip.
Features of microcontrollers include:

* **Input and Output pins** configured depending on their functionality, and carry digital binary signals or analogue variable ones. Groups of pins are known as Ports  an 8-bit port is a set of 8 pins. The microcontroller writes a value to a port, and will set the values of the individual pins accordingly. This is useful to control special kinds of devices or write data in parallel.
* **Timing abilities** - a few timers can be used in different ways
* **Communication capabilities** usually with serial ports, as in most computers so they can communicate with other devices using serial protocols.
* **Programmed in a PC or host computer and in the microcontroller itself**. Programming development environments on PCs allow editing of code and compiling of files that are understandable by the microcontroller - converting to machine language, as an object binary file, usually done through a serial port.

*from [Hernando Barragán (2004) Wiring: Prototyping Physical Interaction Design](http://people.interactionivrea.org/h.barragan/thesis/thesis_low_res.pdf)*

* Microcontrollers are dedicated to one task and run one specific program. The program is stored in ROM (read-only memory) and generally does not change.
* Microcontrollers are often low-power devices. A desktop computer is almost always plugged into a wall socket and might consume 50 watts of electricity. A battery-operated microcontroller might consume 50 milliwatts.

*from [Fab Academy Barcelona Electronics Club notes](http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/electronics2/)*


### Microcontroller Architectures

**Harvard architecture** keeps programme and data in different memories. This lets them run quickly to do simple tasks. Processors are not just the computer, they contain the architecture for the peripherals for the computer too.

We learned that the first computer bug was literally a bug!

**von Neumann architecture** moved programming and data into the same memory so they can modify each other.

### Timing - RISC and CISC type processors

RISC processors run on simple instructions and makes each instruction use the same amount of time which lets the processor run quickly.
CISC use more complex instructions.

### Memory - for operating

* static RAM is for storing data to access quickly
* dynamic RAM is not as fast as static RAM but bigger
* EEPROM is for setting - processor can remember values when you turn on power
* Flash - stores progrmmes

>**Program Memory, sometimes also called Flash Memory (also Read-Only-Memory)** is used for permanently saving the program (CODE) being executed, and it can be divided into two sections: Boot Program section and the Application Program section.

*https://hackmd.io/PsZlUri7TTqB3i55c_c04Q#Understanding-microcontrollers*

**Boot Flash Section / bootloader / boot program section** connects the microcontroller to other devices normally at the very beginning of the microcontroller wake-up and, if there is a failed of transfer of data or no request to send a firmware into the flash. It runs every time the microcontroller resets. This area of the memory is protected against deletion and can not be rewritten unless you make a special effort to as it can change the application section of the flash.

* Fused memory -

>**Programmable read-only memory (PROM)** is a form of digital memory where the setting of each bit is locked by a fuse or antifuse. It is one type of ROM (read-only memory). The data in them is permanent and cannot be changed. PROMs are used in digital electronic devices to store permanent data, usually low level programs such as firmware or microcode. The key difference from a standard ROM is that the data is written into a ROM during manufacture,

*- https://en.wikipedia.org/wiki/Programmable_read-only_memory*

You can visualise the size, type and layout of the memories that are available in the microcontroller in a diagram called a **memory map**.


### Peripherals

Ports talk from other devices to the pins on the processor and each pin is a little machine. These include:

* analogue to digital converter - voltage to number
* comparitor compares 2 voltages - faster than a to d - greater than or less than
* There can be a D to A pin - converting a number to voltage - to generate waveforms
* timers that measure time
* counters measure counts
* PWM - post with modulation - turn a pin on and off very quickly than - giving more power with a smaller circuit and less heat (like a dimmer)
* usarts - to communicate serially
* usb - implements the usb hardware protocol


### Word size registers

These are registers that talk to memory and **the number of 'bits' are the word size**. Processors with bigger registers can do more work per operation and address more memory.

The simplest processors are 8 bit. However an 8 bit processor running at 20mhx using RISC results in one instruction per 20 mhz which is one instruction every 50 nanoseconds.

Audio goes up to 20khz so for every audio 'sample'/wave cycle you can do a thousand operations with an 8 bit 20GHz processor - enough to do math you might expect to do with a bigger word size. **A fast 8 bit processor does not have to work with 8 bit numbers** - you can use bigger bit samples like 16 bit audio by programming the processor with a few instructions - the processors are fast enough to handle them

### Families of processors

* Mechanical
* Megaprocessors - large processors using transistors
* 8051 - very old and not for modern compilers
* PICs - early family of modern processor not for modern compilers
* MSP - early 16 bit
* **AVRs - what we are focusing on**  AVR and PICs are starting to merge.
* RISC-V - open design of a processor that people are starting to replicate.
* Xtensa - ESP processor
* PSoc, xCore etc - system on chip - for things like real time AI, video recognition

Processors come in **form factors** - different type of package for the same part.

Resources to search for parts:
* Octoparts is the search engines for parts
* Digikey
* adafruit sell the parts with add ons etc.


### AVR Processors

AVR was developed by students who designed a processor around the modern compiler for RISC processing rather than the other way round as was done for CISC processing. AVRs are cheap, small and very well supported by the tool chain - what you use to write the software.
They can be as small as a size of a grain of rice with more memory, peripherals and pins the larger they get - some have hundreds of pins. AVR is used in Arduino, some have added USB. They all using the same instruction sets and toolkits.

Integrated circuits come in different [packaging types](https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types).
We are going to use SOIC (Small Outline Integrated Circuit ) we can use BGA (Ball grid array) but routing is easier.

The ATTiny44a uses RISC because each instruction uses the same amount of time and they can run more quickly.

Low-power CMOS (Complementary metal–oxide–semiconductor- a technology for constructing integrated circuits) 8-bit microcontrollers are based on the AVR enhanced RISC architecture.


Ring test  - is a test that shows how fast a signal can go into a processor and back out again. Here is [a comparison of device ring test results from MIT CBA](https://pub.pages.cba.mit.edu/ring/)

## What I learned from reading the The ATtiny44A's datasheet.

http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf

The ATtiny44 processor has subsystems which each have a different subsection of the datasheet.

The data sheet is separated into peripherals and shows just how much equipment goes into each port eg A to D pin.

* The registers are how you configure the part
* instruction set
* electrical characteristics which are the specs.

### Pin configuration

Microcontrollers can come with different pin layouts and we can solder/ configure them differently. We will be using the SOIC confiuration of the ttINY44A

The ATtiny44A has 14 ports
* 1 x VCC
* GND
* 3 x Port B type
* 1 x Port B type can be used as a Reset
* 8 x Port A type

![](assets/ATTiny44apins.PNG)

And you can conect it to an Arduino

![](assets/ATTiny44arduino.PNG)


### Architecture

The ATTiny44A uses low-power CMOS 8-bit microcontrollers based on the AVR enhanced RISC architecture.

RISC uses the same amount of time for each instruction and allows it to run quickly.

### Memory - hardware part

There is no bootloader section in the flash memory, so it is not possible to use Serial Protocol to programme it.

### Capacitive Sensing!

I was surprised to read that this microcontroller can process touch sensing  by linking a library with an Application Programming Interface (API) which defines the touch channels and sensors.

### Clocks and timing - software settings

The ATTINY44 has 5 different options of devices to use as its clock options, selectable by "Flash Fuse bits" . The clock source is input to the AVR clock generator and routed to the
appropriate modules.

The default clock source setting is the Internal Oscillator running at 8.0 MHz with longest start-up time and an initial system clock pre-scaling of 8, resulting in 1.0 MHz system clock.

### Fuses - software settings

Fuses are configuration parameters that are stored in memory cells.

>Every AVR microcontroller, from the ATtiny in your thermostat to the ATMega in your Arduino, stores its configuration in a series of fuse bits. These fuse bits control settings such as the multiplier of the internal oscillator (and thus the speed of the chip)

https://hackaday.com/2012/08/30/avr-fuse-bits-explained/

There are [fuse calculators](http://www.engbedded.com/fusecalc/) for this task


## In system development - a process

In system development allows microcontrollers to be developed even though they are embedded in a system inside a device.  Below are some of the steps that are needed to enable in system development of a microcontroller:

| Function | Input | Language | Tools to write it | What is the final format |
| ------------- |----- |-------------| -----| |
| Conceptualising peoples ideas  | Ideas! | Procedural languages like C or  Basic | IDEs (integrated development environments) where you can write the code. Examples:  Arduino IDE, AVR GCC libraries, Assembly languages AVR Studio, The AVR Eclipse Plugin, BASCOM-AVR, MikroC:  |.obj |
| Run or update a task when certain files are updated,  Configure initialise and compile C code |  |  .makefile  | using the "make" command in the terminal of a computer  | .make |
| Converting Instructions so they are understood by machines, computers or microcontrollers | C |  Assembly language, or machine code, typically using base 256: instructions are encoded in units of bytes | Compiler programmes such as AVRdude,  PonyProg (Serial device programmer if you have a Serial port), Prog ISP supports almost any AVR microcontroller and allows you to write and READ the ROM memory, fuse bits, and EEPROM of the microcontroller | .hex file to be stored in the ROM memory of the microcontroller |
| In-system Programming - using a board that can be detached from a main computer to sit in the housing of a system to transfer your ideas in binary code into processors | | Libraries of code that turn c? | Programmer environments with binary tools (linking, assembly, conversions): FabISP, avrdude, Arduino, Atmel-ICE, FabPDI, GoodFET, Raspberry Pi, GCC compiler targeting AVR | Binary code like rs232|
|  |    |  | | |
|  |    |   || | |

ISP (header, pads, clip, pins)
bootloader
   Arduino, DFU, Micronucleus
JTAG, PDI
ICE

### Bootloader (Software to set out the communication lines)

A bootloader is a software hosted in the flash memory of a microcontroller that allows programming of a board through the serial port *without the need to use an external programmer.*   It receives new program information externally via some communication means (in the case of FabISP it is recommended to do it through ISP 6 pins from a programmer called an AVG MII because Arduinos don't translate the information very reliably) and writes that information to the program memory of the processor.
More in this [Arduino guide to developing bootloaders](https://www.arduino.cc/en/Hacking/Bootloader)

### Firmware (Software for control)

Provides the low-level control for a device's specific hardware.

### Programmers (Hardware)

Programmers are small boards  that hold small amounts of code that you can embed into a system allowing you to add and change functionality of a microcontroller. An ISP programmer is a specialized hardware that can "reflash" microcontrollers that don't have a bootloader section or "reflash" the bootloader into the microcontroller.

You do this by attaching it to a computer with a programming environment and coding in a number of languages. The software turns this into binary code and you upload this into a programmer which then you programme to transfer programmes to the microcontroller. Its function is to hold and transfer code in binary.

### Languages

From the person to the microcontroller there is a chain of language conversions necessary to work through all the tools we current need for programming a microcontroller in an embedded system.

#### Assembly languages
These languages assemble more abstracted languages so they can be converted easily into binary languages that microcontrollers use.
- Hex codes
- instruction set, opcodes
-  mnemonics, directives, expressions
-  gavrasm
-  avr-as
-  inline

#### C

Includes **Compilers**
* GCC - opensource compiler includes
  * Make files
  * bit operations
* avr is a compiler

#### .make files - a way to call a series of commands from your terminal
http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/makefile.html

The 'make' command on Mac/Linux automatizes dealing with multiple configurations and commands when compiling a software. Make reads automatically a Makefile file in the folder where you launch it or you can specify it with make -f filename.
**YOU CALL .MAKE FILES FROM THE COMMAND TERMAL IN WINDOWS 10**

A good Makefile not only compiles the C code, it includes an option for transferring the hex file into the target chip (avrdude is usedfor the transfer only.)

The first four lines are for configuring the Makefile are:

* Name of the file to compile
* Extension of the file to compile
* Microcontroller to program
* Frequency of the board to program
The other lines initialize the process and compile the C script with avr-gcc.

We need write a C code for them, compile it with avr-gcc and send it to the microcontroller with avrdude.

## Host communication

### rs232

RS232 is simplest way to talk to a computer. This weeks board uses this host communication - it uses bit timing. It involves the sending data one bit at a time, over a single communication line and it is SERIAL communication

RS232 Serial transmission is good for long distance communications, whereas parallel is designed for short distances or when very high transmission rates are required.

### USB - Hardware with protocol
 USB protocol enables communication between "proper" computers and many devices

### FTDI - Hardware and protocol to transmit binary code and power through serial signals
 FTDI is a protocol developed by Future Technology Devices International for a specific connector with 6 pins that can easily be included onto a PCB board. FTDI develop, manufacture, and support devices and their related software drivers for **converting RS-232 or TTL serial transmissions to USB signals**.

### IDEs - software to write and send code

These are development environments - softwares where you can code, compiler, convert and send code out of the ports to other devices like microcontrollers.

<!---
* ISP Header - the 2x3 pin header that connects the FabISP to our newly created chip.
* FTDI Header - the 1x6 pin header that connects to the special FTDI USB cable
* A 2x3 pin ISP ribbon cable completed in an earlier week's assignment
* The special FTDI USB cable (USB -> 1x6 pin FTDI header)
AVR - The name of the Amtel microcontroller we are programming
ISP - In System Programmer
ICP - In Circuit Programmer
FTDI - Future Technology Devices International - The company that makes a special usb cable - and drivers that let us write to the 6 pin output like a standard serial port.
GNU compiler suite (gcc avr-gcc command line util, etc)
C library for the AVR (libraries )
The AVRDUDE uploader
Several other useful tools
PCB - Printed Circuit Board
CrossPack for AVR Development - A collection of AVR dev tools bundled for OSX:
--->

### Putting them together
![](assets/barragan.PNG)

*[Hernando Barragán's version](http://people.interactionivrea.org/h.barragan/thesis/thesis_low_res.pdf)*


![](assets/toolchain3.PNG)
*My version including a programmer*


### In system programming (ISPs) - A process - In system programmers (also ISP) are a piece of hardware that do in system programming.

This is really the key term in this weeks task

>In-system programming (ISP), also called in-circuit serial programming (ICSP), is the ability of some programmable logic devices, microcontrollers, and other embedded devices to be programmed while installed in a complete system, rather than requiring the chip to be programmed prior to installing it into the system.

- Wikipedia (when you have 59 million search results you have to start somewhere!)


In system programming software includes:
avrdude
Arduino

In system programmers include:
FabISP
AVR2

### Serial programming

Serial interfaces stream their data, one single bit at a time. These interfaces can operate on as little as one wire, usually never more than four.

### Serial Peripheral Interface (SPI) - Hardware

is an interface bus commonly used to send data between microcontrollers and small peripherals such as shift registers, sensors, and SD cards. It uses separate clock and data lines, along with a select line to choose the device you wish to talk to.
https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi. USB (universal serial bus), and Ethernet, are a couple of the more well-known computing serial interfaces. Other very common serial interfaces include SPI, I2C,

#### Synchronous serial interfaces - Hardware
Examples of synchronous interfaces include SPI, and I2C.

#### Asynchronous serial interfaces

GPS module, Bluetooth, XBee's, serial LCDs all use Asynchronouor communication. Asynchronous Serial Interface, or ASI, is a method of carrying an MPEG Transport Stream (MPEG-TS) over 75-ohm copper coaxial cable or multimode optical fiber.[1] It is popular in the television industry as a means of transporting broadcast programs from the studio to the final transmission equipment before it reaches viewers sitting at home.

### What would I like to learn more about?

I would love to spend more time on coding.

The physicality of the processor and its geometry is really interesting and I will definitely look into this more.


## Programming the ATTINY 44

**Resources again**

* [Code by Neil](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.program.png)
* [Tutorial by Hilowtech](http://highlowtech.org/?p=1695)
* [Fab Academy Barcelona's Electronics club Tutorial](http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/embeddedprogramming/)
* [Lengthy FabISP programming instructions from MIT](http://fab.cba.mit.edu/classes/863.16/doc/tutorials/fabisp_programming/#windows)

**Problem: Many weeks into trying to get this task done Eduardo found that my Hello world board was missing key components.**


#### Hardware on the FabISP

[Here is some documentation by Ali Shtarbanov on 'Demystifying the FabISP and Designing the FabOptimus' ](http://fab.cba.mit.edu/classes/863.16/doc/tutorials/FabISP/FabISP_Demystified.html)

**PORT:** **The FabISP programmer can be programmed through the ISP port the first time it is programmed because it has a blank flash memory**. Afterwards it can use its USB port

**JUMPER to be moved after first programme is loaded** PB3 pin has RESET capability and both the Flash and the EEPROM can be reprogrammed by serial SPI bus while RESET is pulled to GND (Section 19.5). This means **we want the PB3 pin to be set to LOW when we reprogram the ATtiny for the first time** (either to RST or to GND) and the rest of the time to be set to HIGH, using the R5 pull-up resistor. Again, this line has a jumper (SJ1) in between the PB3 and the RST pin of the ISP in order to be pull-up once reprogrammed.

The rest of the pins are directly connected as in the above table regarding the necessary instructions for Serial Programming.

**COMUNICATION LINES** v speed

Parallel communication requires many more input/output (I/O) lines -  **serial communication needs less inputs and outputs** (and the potential of something going wrong in my case!) so it is worth sacrificing speed for at least for this task.

**6 pin ISP header**

How should we package the data to the ATTiny44? - Serial Peripheral Interface (SPI)

Rules for SPI:

- only one side generates the clock signal (usually called CLK or SCK for Serial ClocK).
- The side that generates the clock is called the “master”, and the other side is called the “slave”.
- There is always only one master (which is almost always your microcontroller), but there can be multiple slaves.
- When data is sent from the master to a slave, it’s sent on a data line called MOSI, for “Master Out / Slave In”.
- If the slave needs to send a response back to the master, the master will continue to generate a prearranged number of clock cycles, and the slave will put the data onto a third data line called MISO, for “Master In / Slave Out”.

The lines connect to the ATTiny 44 as per the data sheet

Symbol	Pins	I/O	Description
MOSI	PA6	I	Serial Data In
MISO	PA5	O	Serial Data Out
SCK	PA4	I	Serial Clock


#### How do we code using SPI protocol?

From https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi :

There are two ways you can communicate with SPI devices :

1. Software-based commands shiftIn() and shiftOut() commands that will work on any group of pins, but will be somewhat slow.

2. Use the SPI Library, which takes advantage of the SPI hardware built into the microcontroller which is much faster but it will only work on certain pins.

- You will need to select some options when setting up your interface. These options must match those of the device you're talking to; check the device's datasheet to see what it requires.
- The interface can send data with the most-significant bit (MSB) first, or least-significant bit (LSB) first. In the Arduino SPI library, this is controlled by the setBitOrder() function.
- The slave will read the data on either the rising edge or the falling edge of the clock pulse. Additionally, the clock can be considered "idle" when it is high or low. In the Arduino SPI library, both of these options are controlled by the setDataMode() function.
- SPI can operate at extremely high speeds (millions of bytes per second), which may be too fast for some devices. To accommodate such devices, you can adjust the data rate. In the Arduino SPI library, the speed is set by the setClockDivider() function, which divides the master clock (16MHz on most Arduinos) down to a frequency between 8MHz (/2) and 125kHz (/128).
* If you're using the SPI Library, you must use the provided SCK, MOSI and MISO pins, as the hardware is hardwired to those pins. There is also a dedicated SS pin that you can use (which must, at least, be set to an output in order for the SPI hardware to function), but note that you can use any other available output pin(s) for SS to your slave device(s) as well.

### Using the FabISP an In System Programmer (ISP)

Eduardo explained that the FabISP is a robust device specifically designed to load programmes onto microcontroller such as the ATTiny range. You can use other programmers to load code onto an ATTtiny however the configuration of larger devices like the Arduino can struggle to communicate effectively with small microcontrollers. The FABISP is run itself by an ATTiny44 so the compatibility is for our task is perfect. So despite several days of trying to use the Adruino as a programmer I finally bit the bullet and tried to programme the Fab ISP.


#### Programming the FabISP using Windows 10

* [Fab Academy Tutorial](http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html)

Methods found:

1. Use the hardware AVRISPM2, and software package (CrossPack software for a mac and Win AVR for windows) to programme the ISP  https://hackmd.io/PsZlUri7TTqB3i55c_c04Q#Electronics-basics.

This method requires downloading and installing the following software:
* [WinAVR](https://sourceforge.net/projects/winavr/files/) command line development tool-chain (avr-gcc, avrdude, etc)
* [Drivers for the AVR programmer & SPI interface](https://learn.adafruit.com/usbtinyisp/drivers)  [on this page](https://hackmd.io/PsZlUri7TTqB3i55c_c04Q#Basic-hardware-checks)
In Device Manager on Windows
- PLUG IN ANOTHER FABISP??? not sure where that came from but apparently not only do we need an AVRMK2 programmer but ANOTHER already rogrammed FabISP - not sure where an average person is supposed to whip out one of these frm
    - Locate "FabISP" under "Other Devices"
    - Right click on the "FabISP"
    - Select "Update Driver Software.."
    - Select "Browse my computer"
    - Navigate to the drivers folder you downloaded at step 4 and click on the folder.
    - Hit "ok" to install the drivers for the USBtiny / FabISP
* [FabISP firmware to set up the fab ISP](http://academy.cba.mit.edu/classes/embedded_programming/firmware.zip)
* These files for echo hello-world the embedded programming class page that :
    - [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c)
    - [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make)

2. [Use Libraries to connect the Arduino to the ATTiny44 processor on the Fab ISP](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html) which references http://highlowtech.org/?p=1695. This is not recommended as it is less reliable.

I downloaded the FTDI drivers for Windows 10 called Flip 2.4.7 [here](https://www.ftdichip.com/Drivers/VCP.htm)
with [instructions for installation to Windows 10 here](https://www.ftdichip.com/Support/Documents/InstallGuides/AN_396%20FTDI%20Drivers%20In


#### **Overview For Programming an ATTiny44 using a FabISP on Windows 10 in Barcelona Fablab**

You can't do this task without borrowing hardware off other people so don't even try doing this on your own.

1. **In Barcelona Fablab you HAVE TO first programme your FabISP with an AVR MK2 programmer (hardware) from the fab lab that has a USB port.**  Bigger programmers tend not to work well as programming small microcontrollers.

2. Next download all the software to use the AVRMK2 and your FABISP (drivers and firmware).
There is this zip file that contains the firmware which is uploaded using a make file  http://fab.cba.mit.edu/classes/863.16/doc/tutorials/fabisp_programming/fabISP_0.8.2_firmware.zip but the ISP driver apparently needs you to borrow and already programmed FabISP (?!) in order to install the driver - implying that you have to know someone who has done this before in order to access this knowledge (a bit like Scientology).

3. Through the MK2 programmer you need to
    a. burn the bootloader of your FabISP from the Arduino IDE
    b. load the firmware into your FabISP from your terminal using .makefiles shared by MIT. You have to make sure the MK2 hard ware is referenced and the FabISP is referenced correctly

4. Then test the FabISP is read by your computer in device settings in Windows 10.

5. Remove the jumper resistor from you Fab ISP to turn it into a programmer

6. Plug the FabISP into your computer and connect the FabISP to your board  using a 6 pin ISP headers and a 6 wire ribbon. From Arduino IDE you can select TinyUSB as the programmer, download Attiny board manager, select the ATTiny you want to programme and burn the bootloader of your ATTiny.

7. Then finally call the .makefile and .c file from your terminal and that should run WinAVR onto your board. If this doesnt work I found no information on addressing the errors.


### Using an Arduino as an ISP programmer
**Resources:**
* http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/arduino_isp.html
* http://highlowtech.org/?p=1695

#### Toolchain

![](assets/toolchain.PNG)

#### Turning the Arduino board into an “in-system programmer” (ISP):

** Even more Resources:**
* Instructions from http://highlowtech.org/?p=1706
* Arduino tutorial: https://www.arduino.cc/en/Tutorial/ArduinoISP

**Official instructions to use Arduino as an ISP Programmer and bootloader**
    - Run the Arduino development environment.
    - Open the ArduinoISP sketch from the examples menu.
    - Select the board and serial port that correspond to your Arduino board.
    - Upload the ArduinoISP sketch to the Arduino

  "Pin 10 is used to reset the target microcontroller.
  On some Arduinos (Uno,...), pins MOSI, MISO and SCK are the same pins as
  // digital pin 11, 12 and 13, respectively. That is why many tutorials instruct
  // you to hook up the target to these pins."

**!PROBLEM 'Serial was not declared in this scope' error in software**

I asked Santi our teacher to see if he could spot any problems and he spotted that my resonator was shorting.

![](assets/soldertrouble.jpg)

I re-soldered this and gouged out a piece of the board just to make sure it didn't connect. However I don't think this was anything to do with programming the Arduino to run as an ISP.

.. to be continued..

##### Trying the online Arduino IDE

It's actually not fully online because you need to download the [Arduino Create Agent plugin](https://create.arduino.cc/getting-started/plugin?page=2)for your browser to connect your board

Then you can go to https://create.arduino.cc/ to use libraries straight off the cloud.

![](assets/arduinoeditor.png)

 - I plugged in my Arduino into my laptop USB port.
 - In the Arduino Editor in my browser I selected the Arduino Uno board and port on the dropdown above the sketch.
 - I looked for ArduinoISP in the Examples tab. I downloaded it and had to open it again for some reason. I copied and pasted it into the sketch.
 - I checked it using the tick button and I uploaded it by clicking the arrow button. And...
 More errors

<!-- ![](assets/success.png)

SUCESSS! At last! My arduino should function as an ISP now.


##### USB Drivers for Atmel mega16u2 needed updating

>The ATmega16U2 chip on your Arduino board acts as a bridge between the computer's USB port and the main processor's serial port. Previous versions of the Uno and Mega2560 had an Atmega8U2. It runs software called firmware (so named because you couldn't change it once it had been programmed in the chip) that can be updated through a special USB protocol called DFU (Device Firmware Update).
.
- https://www.arduino.cc/en/Hacking/DFUProgramming8U2


https://www.microchip.com/developmenttools/ProductDetails/flip

-->

##### Port problem in Windows 10

Arduino IDE was unable to load ArduinoISP onto the Arduino in neither the desktop nor online Arduin IDE.
After uploading a new version of the Arduino IDE and nearly uploading a new driver for the ATmega16U2 chip I read that [the error it could be a windows problem and to go into device manager and reconnect the port.](https://stackoverflow.com/questions/25718779/arduino-nano-avrdude-ser-opensystem-cant-open-device-com1the-system)

SUCCESS!!

![](assets/isparduino.gif)

This is merely an example sketch uploaded to an Arduino so far!

###### Configure SPI clock (in Hz).

By default, the ATtiny's run at 1 MHz (the setting used by the unmodified "ATtiny44)
You need to do an extra step to configure the microcontroller to run at 20 MHz – as we have an external 20 MHZ resonator on the Hello Button + LED board.
Once you have the LED + Button board connected to your FabISP, select the "ATtiny44 (external 20 MHz clock)" from the Boards menu.

#### Installing ATtiny support into Arduino 1.6.4

i installed ide 1.6

I installed the ATtiny support board by referencing https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json in the board manager of the IDE.


####  Connecting the ATtiny directly to an Arduino (to be used as a programmer)


- **connect a 10 uF capacitor between reset and ground on the Arduino board** the stripe on the capacitor that’s marked with a negative sign (-) should go to ground. The capacitor prevents the Arduino board from resetting (which starts the bootloader), thus ensuring that the Arduino IDE talks to the ArduinoISP (not the bootloader) during the upload of sketches. (The capacitor is needed if you’re using an Arduino Uno, and might also be necessary for an Arduino Duemilanove.)

* I  connected the Arduino (programmer) to the ISP header on the  ATtiny board as shown in the following diagram found on this page
http://highlowtech.org/?p=1706 referenced in http://highlowtech.org/?p=1695.

![](assets/arduinotoisp.PNG)

* As we are using a circuit board with an ATtiny, the board already has an ISP header configured as the below as per the circuit.   We connect  MISO, MOSI, SCK, RESET, VCC, and GND of the programmer (Arduino!) to the ISP header on the board which are routed to the corresponding pins on the ATtiny - Use the dot in the corner of the ATtiny to orient it properly.


![](assets/circuit.png)![](assets/ispboardtoarduino.jpg)

![](assets/ArduinoISPconnect.jpg)

Settings on Arduino IDE:
Board: Arduino Uno
Programmer: AVR ISP? or Arduino ISP? neither are working
Make sure the UNO is using the right programmer Tools > Programmer > Arduino as programmer

#### Burn the Bootloader



####  Programming the ATtiny (using instructions from [here](http://highlowtech.org/?p=1695))

Next, we can use the ISP (for me this is the Arduino in ISP mode) to upload a program to the ATtiny.
Still in the online Arduino Editor I..
- Opened the Blink sketch from the Examples > Basics menu.
- Changed the pin numbers from 13 to 0. - this sketch doesn't have pin numbers so I changed LEDBUILTIN to 0
- In the library search I searched for the ATTiny board manager library again https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json but found one called Tiny SPI. As mentioned before we can use an SPI Library to take advantage of the SPI hardware built into the ATTiny44 which is fast but only works on certain pins (hope we have them connected correctly!)
- As this library is not a board manager the ATTiny44 is not coming up on my board options. I decided to uninstall the arduino ide and install the newest version. then
Select “ATtiny” from the Tools > Board menu and the particular ATtiny you’re using from the Tools > Processor menu. (In Arduino 1.0.x, these options are combined in just the Tools > Board menu.)
- Select the appropriate item from the Tools > Programmer menu (e.g. “Arduino as ISP” if you’re using an Arduino board as the programmer, USBtinyISP for the USBtinyISP, FabISP, or TinyProgrammer, etc).
Upload the sketch. It was not uploading so i tried changing the clock speed to 1mz. the code was fine but still it would not upload.
- I found I needed to **power the board separately using an FTDI cable**.

- You should see “Done uploading.” in the Arduino software and no error messages. If you then connect an LED between pin 0 and ground, you should see it blink on and off. Note that you may need to disconnect the LED before uploading a new program.

Needed to include the SoftwareSerial library in the updated blink sketch. ?


#####  Configuring the fuse bits of the microcontroller the ATtiny to run at 8 MHz
By default, the ATtiny’s run at 1 MHz. You can configure them to run at 8 MHz instead, which is useful for faster baud rates with the SoftwareSerial library or for faster computation in general. To do so,
- once you have the microcontroller connected, select “8 MHz (Internal)” from the Tools > Clock menu. (In Arduino 1.0.x, select the appropriate 8 MHz clock option from the main Tools > Board menu.) Warning: make sure you select “internal” not “external” or your microcontroller will stop working (until you connect an external clock to it).
##### Burn bootloader
 Then, run the “Burn Bootloader” command from the Tools menu. This configures the fuse bits of the microcontroller so it runs at 8 MHz. Note that the fuse bits keep their value until you explicitly change them, so you’ll only need to do this step once for each microcontroller. (Note this doesn’t actually burn a bootloader onto the board; you’ll still need to upload new programs using an external programmer.)

 The following Arduino commands should be supported:
 ATtiny Microcontroller Pin-Outs
 pinMode()
 digitalWrite()
 digitalRead()
 analogRead()
 analogWrite()
 shiftOut()
 pulseIn()
 millis()
 micros()
 delay()
 delayMicroseconds()
 SoftwareSerial (has been updated in Arduino 1.0)

##### Send Blink programme






References
arduino-tiny: alternative approach to ATtiny support for Arduino
TinyWireM & TinyWireS: Wire (I2C / TWI) library for the ATtiny85 (using USI)
Servo8Bit: Servo library for the ATtiny45/85.
Alternative: ATmega328P on a Breadboard
If the ATtiny isn’t quite powerful enough but you still want to use a bare microcontroller instead of a full Arduino board, see this tutorial on using an ATmega328P on a breadboard. It allows you to use all the same functions and libraries as the Arduino Uno, but with just a microcontroller and a few small components.

High-Low Tech Group :: MIT Media Lab

mk2
### even more tutorials:

http://archive.fabacademy.org/2018/labs/barcelona/students/javier-hernandez/week4.html
http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html
http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html#windows


install windows drivers for the FABISP


load up the fabisp and remove the jumper

student mode make file adapt the firmware

go into you firmware folder in terminal

ls to check the files


http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html#windows

winAVR

install win avr


go to termina folder
ls
make cla

always access makefiles from terminal


> Next you need to compile the firmware.
Type:
make clean    
If you are successful - you will see this response from the system:
akaziuna@Titan:~/Desktop/firmware$ make clean
rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex
main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s
Type:
make hex

https://stackoverflow.com/questions/559816/how-to-export-and-import-environment-variables-in-windows

( system path is in C:\Program Files (x86)\Atmel\Flip 3.4.7\bin)
control panel
system panel
systems properties
environment variavles
edit use variables
path
C:\Program Files (x86)\Atmel\Flip 3.4.7\bin




environment variable s
click on path


download winavr
http://www.ladyada.net/learn/avr/setup-win.html

AVR GNU Toolchain
