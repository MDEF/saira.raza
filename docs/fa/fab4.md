

# <center>Fab Academy Week 4</center>
# <center>PCB Milling and soldering</center>

<center>![](assets/)</center>

##### **Resources:**

- [Fab Academy Mods tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mods.html)
- [Fab Academy SRM20 tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
- [Fab Academy BCN Week 4 tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html)

This week I made a little in-circuit programmer called the [helloISP44](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) designed by Neil Gershenfeld who kindly provided us with the design and [trace](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png) and outline files from which I machined the board.



<!---
more pictures more detail about controlling the z write about fab modules)


You should check the links of your images, all of them are broken…
Missing screenshots of the programming process, together with the commands
Missing final board picture.
Included a ‘hero shot’ of your board
Needs documentation.
Did you have any problems? Drivers problems, solder problems, etc … It is as important to document the successes as the errors. Remember that if you document errors, you have to document how you tried to solve them.
Nice gif images! I love them. But you should explain also with words ;)
Explain how you generated the CAM files for the SRM-20, with words and pictures. Also, include those files in your archive.

We don’t use FR4 in the Fablabbcn, we use FR1. Please take a look of the refenrece (https://support.bantamtools.com/hc/en-us/articles/115001671734-FR-1-PCB-Blanks-) and check Neil’s class again, and correct your documentation.
No content…
I recommend you take a look at Assignments and Assessment doc
Missing screenshot where shows the computer recognize the FabISP
Explain what you learn from the group assignment test.
Please add link to person you mention (ex:Kris)
Will be nice to see a picture of the problem “small amount of solder in one place”
In general the work is done and it is seen in the images, but you need to be much more specify in your documentation.
  -->

<!---

make an in-circuit programmer by milling the PCB, optionally trying other processes
Shown how you made and programmed the board.
Explained any problems and how you fixed them.
Included a ‘hero shot’ of your board.

This week we made an in-circuit programmer known as the hello ISP 44.
The circuit used is this prepared one for the hello ISP 44
http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png
This was saved as a monochrome png.  -->

## Milling a PCB

Two online tools have been made available to us: [MODS](http://mods.cba.mit.edu/) and Fabmodules.
MODs is the more recent tool which uses a machine-like layout where you connect inputs of modules to get to the finished file. I chose this tool because it was open on the machine.
In hindsight fabmodules is perhaps easier for this particular task.


### Setting up data flow from drawing to machine using the [online Mods tool](http://mods.cba.mit.edu/)

The online [Mods tool](http://mods.cba.mit.edu/) consists of several modules that you can connect to calculate parameters from design files to settings for machines.

I selected a preconfigured programme for milling PCBs on the Roland SMR20 by:

* Right clicking on the workspace and selecting PROGRAMS > OPEN SERVER PROGRAM > ROLAND > MILL > SMR20-MDX-20 > PCB png
* To set a place to save my settings I right clicked anywhere in the white space and select MODULE > OPEN SERVE MODULE > SAVE FILE
* To ensure the calculated settings were saved, clicked on OUTPUT of RolandSrm-20milling machine module and clicking again in INPUT of 'save file' module.

### Calculating the toolpath for the trace

#### In the 'Read png' module
I imported my circuit board.png file into the "read png" module.

![Read png mod](assets/pngimport.PNG)

This shows the real dimensions of the circuit as well as the dpi.

#### In the 'Set PCB Default' module
![Set PCB Default](assets/PCBdefaults.PNG)

In the SET PCB Default module **click in MILL TRACES 1/64**


#### In 'Roland SRM-20 milling machine' module

![Roland SRM20 module](assets/Rolandsrm20mod.PNG)

* Set the origin as
  - x0(mm) - 0
  - y0(mm) - 0
  - z0(mm) - 0 These x0,y0,z0 are for setting up an offset from the origin
  - zjopg - 12 - This is the z distance that the mill will go up between the air travellings
* **Set the speed - 4mm/s**
* x/y/x home is the parking position after finishing the cut
* save

#### In 'Mill Raster 2D' module

![Mill Raster 2D](assets/millraster2dmod.PNG)

click in Calculate to calculate the passes of the trace - this will be saved automatically into you saved file



### Outline cut

#### In the 'Read png' module
Imported the outline file and saved as a new file in the computer.

#### In the 'Set PCB Default' module
In the SET PCB Default module **click in MILL TRACES 1/32**

#### In Mill Raster 2D module
Clicked calculate

#### In 'Roland SRM-20 milling machine' module

* Set the origin as before
  - x0(mm) - 0
  - y0(mm) - 0
  - z0(mm) - 0 These x0,y0,z0 are for setting up an offset from the origin
  - zjopg - 12
* **Set the speed -  0.5mm/s**
* x/y/x home is the parking position after finishing the cut
* save

#### In 'Mill Raster 2D' module
Click "calculate"(you will see the path calculating), when the process will finish it will "automatically be saved into your downloads folder"

#### SEND THE FILES TO THE Roland SRM-20

### Setting up the Roland SRM-20

#### Loading the FR1 copper board to Modela.

* Turn on Roland Modela and the computer and open the 'VPanel for SRM-20' software
* go to setup and check RML-1/NC CODE and mm are selected.
* Make sure your FR1 board is clean of the dust and fingerprints. Fingerprints make it harder to solder onto once the board is done.
* Press the “view” button on the Modela to toggle between loading and loaded positions.
* Clean the “sacrificial board” of dust. Make sure it's steady and flat.
* Apply double sided tape across the entire PCB board taking care not to overlap the tape and not trap dust.
* Tape down your board in the Modela onto the sacrificial board avoiding leaving fingerprints.

#### Load the 1/64” endmill for the trace
* keep as much of the endmill in the collect as possible but leave enough for you to hold it with your finger
* gently tighten using an allen key - it does not need much tichtening

#### Set the X/Y/Z zeros

The (0,0) of your file created in fab modules is in the bottom left corner but it is wise to start your file slightly inside your physical board.
* Using the X/Y arrows in VPanel for SRM-20 move the mill head where you want cutting to start
* click on 'Set origin X/Y' in Vpanel to set the origin corresponding to the bottom left hand corner of your file .

#### Set Z zero

* Move the Z slowly with the control panel around 3-5 mm above the PCB plate.
* loosen the collect screw using an allen key and carefully guide the endmill onto the surface of the copper board with your finger as it can break if dropped.
* Press down around 0.5mm on the PCB board, let the mill fall this small distance and tighten the screw on the collet with the allen key.
* save the Z origin in the Vpanel control panel by clicking Z under "Set origin point"
* The z-zero effects milling (horizontal movement of the endmiill) more than drilling (vertical).

#### Milling the copper trace of the PCB
* Click “Cut” on the control panel.
* A new window will appear where you will select your traces cutting file, delete any old ones saved and select the trace.
* click Output and the machine should start.
* If the mill is producing a lot of residues you can press the yellow pause button on the VPanel, brushthe residue away from the bit and click resume when ready to cut again.
*  When cutting stops, Do not remove the board! You can vacuum the board to see the traces.

<center>![](assets/milling.jpg)</center>


#### Cutting the PCB outline from the FR1 board
* Do not change the XY origin on the Vpanel
* change the endmill to 1/32 end mill.
* Reset the Z zero only - as before guiding the endmill onto the board, pushing the board down less than a millimeter and tightening the collect
*click "CUT" again in the control panel but now choose your Outcut/Holes files and click Output

#### Finishing

* Remove board, cut out if desired, cut apart any areas that didn’t make it.
* remove your board with the help of a spatula
* Vacuum away the plastic and metal chips in the area.
* remove your files from the computer


## Soldering components

Below are the components that need soldering to the board:

- 1 ATTiny 44 microcontroller
- 1 Capacitor 1uF
- 2 Capacitor 10 pF
- 2 Resistor 100 ohm
- 1 Resistor 499 ohm
- 1 Resistor 1K ohm
- 1 Resistor 10K
- one 6 pin header
- 1 USB connector
- 2 jumpers - 0 ohm resistors
- 1 Crystal 20MHz
- two Zener Diode 3.3 V


### Finding the correct polarity of components:
Microprocessors and diodes need to be soldered in a correct direction in order to function.

Microprocessors pins have different functionality so they will not perform the task of the circuit if the pins are not connected correctly. **The ATTiny44 has a small dot in one corner which corresponds to PIN 1 which is the VCC line for power**

Diodes will be destroyed by heat if a current runs through them in the wrong direction for long enough.
Different diodes are marked differently to indicate polarity. **Often in electronics [it is the Cathode connection that is marked with a line on the diode](https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity).**

<center>![](assets/soldering.jpg)</center>

# PROBLEMS AND HOW I FIXED THEM....

### Unsoldering the microprocessor

On my first attempt I soldered the microprocessor the wrong way. This was difficult to clean and re-solder.

I used a gentle air heater on the component and quickly lifted it off. Then sucked up the solder  using a copper wick. This was to avoid removing the copper by overheating.


## Hero shot!

<center>![Soldered Hello](assets/helloISP44soldered.jpg)</center>


## Ideas for future circuits:
- [Tattoo-Paper Transfer as a Versatile Platform for All-Printed Tattoo-Paper Transfer as a Versatile Platform for All-Printed Organic Edible Electronics-  Giorgio E. Bonacchini, Caterina Bossio, Francesco Greco, Virgilio Mattoli, Yun Hi Kim, Guglielmo Lanzani, Mario Caironi](https://www.researchgate.net/publication/322886257_Tattoo-Paper_Transfer_as_a_Versatile_Platform_for_All-Printed_Organic_Edible_Electronics)
- [Edible electronics](https://www.technologyreview.com/s/610190/edible-electronics-tattooed-on-your-food-could-help-track-your-health/)
