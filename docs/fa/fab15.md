# <center>Fab Academy Week 15 </center>
# <center>Interfaces</center>
<center>![]()</center>
<center>![]()</center>
<!---     --><!---     --><!---     --><!---     --><!---     -->
##### **Resources:**
* http://fab.academany.org/2019/labs/barcelona/local/wa/week16/
* https://hackmd.io/s/HJvhtz0oV
* https://playground.arduino.cc/Interfacing/Processing/

<!---  
implement and interpret programming protocols
Described your design and fabrication process of your APPLICATION
had fun with visuals!! and buttons,etc
Outlined problems and how you fixed them
Described your problems and how you fixed
included original design files (Eagle, KiCad, Inkscape - whatever)
## assignment
individual assignment:
### write an application that interfaces with an input &/or output device and user!
      that you made
can run on anything - desktop , cloud, a rasberry pi
learn how to make graphics , multimedia vr, maths performance , deploy, build something between user and devices - interafaces for your machine? your final project? or just experiment? take sensor data and do mad stuff witth that.
group assignment:
#### compare as many tool options as possible
  make a chart?
## programming protocols to use
## Design
## Fabrication
## Visuals
## Problems!
## Original design files
# Workshop
What Do I Want to Do ? Ex: A user interface that shows - the value of a sensor in a website
- Which is my input ?
- Which is my output ?
- How is the communication between the input and the output ?
- Wired or wireless ? Bluetooth? Wi-fi?
Bluetooth - uses AT Command
check each connection of the toolpath
### App inventor
http://appinventor.mit.edu/explore/
block coding
### A- Frame, unity
a frame to server to mysql
wifi modules
## Processing
Ardunio
add library
create a key
you can use a wifi key
http://fabacademy.org/archives/2015/eu/students/hisatsune.taichi/classes14.html
## Python
   -->

## Toolchain of available tools

   <iframe
     src="https://embed.kumu.io/20692dbe2f6d14f63009eac6cb7f8e51"
     width="940" height="600" frameborder="0"></iframe>


## Processing

>Processing is where Arduino came from

*https://sites.google.com/site/ncadarduino/serial-communication*

Processing is a language that was designed to be used by artists.
It is quite fitting then that the maker community is using Arduinos and Arduino IDE to be creative. I think technical literacy could be made even more accessible with tools like these.

### Displaying an analogue reading from an Arduino using Firmata code to send to Processing

I thought I would try to visualise analogue sound signals from my [sound sensor from Networking week](fab13.md).


Using [these instructions](https://www.instructables.com/id/Read-analog-data-directly-in-Processing/)

1. Upload the “standardFirmata” example from the Arduino IDE in your Arduino board. If you are using Arduino UNO, there is a standard firmata for UNO.

2. Install Arduino library for processing in your libraries folder in the processing sketchbook
For a detailed instructable for this process go to the Arduino playground example

        import processing.serial.*;
        import cc.arduino.*;

        Arduino arduino; //creates arduino object

        color back = color(64, 218, 255); //variables for the 2 colors

        int sensor= 0;
        int read;

        float value;

        void setup() {
          size(800, 600);
          arduino = new Arduino(this, Arduino.list()[0], 57600); //sets up arduino
            arduino.pinMode(sensor, Arduino.INPUT);//setup pins to be input (A0 =0?)

            background(back);
        }

        void draw() {

          read=arduino.analogRead(sensor);
          background(back);
          println (read);
          value=map(read, 0, 680, 0, width); //use to callibrate  
          ellipse(value, 300,30, 30);

        }

step 3.. Use the “println” command to output your sensor’s minimum and maximum values. They will be different depending on the context.
Plug the min and max values in your map() function.

Step 4: Add Other Variables to Your Code
-Add other variables to manipulate the X and Y axis of the accelerometer. This program tracks down the X and Y values from the accelerometer and displays them altering the position of a graphic in the screen. It needs to be calibrated for both X and Y parameters.

      `import processing.serial.*;
      import cc.arduino.*;

      Arduino arduino; //creates arduino object

      color back = color(64, 218, 255); //variables for the 2 colors

      int xpin= 0;
      int ypin= 1;
      float value=0;

      void setup() {
        size(800, 600);
        arduino = new Arduino(this, Arduino.list()[0], 57600); //sets up arduino


          arduino.pinMode(xpin, Arduino.INPUT);//setup pins to be input (A0 =0?)
          arduino.pinMode(ypin, Arduino.INPUT);
          background(back);
      }

      void draw() {

        noStroke();

        ellipse(arduino.analogRead(xpin)-130, arduino.analogRead(ypin)-130,30, 30);//needs to be calibrated for X and Y separately

        }
`

<iframe width="560" height="315" src="https://www.youtube.com/embed/QTHfjnBumLk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The contributor included processing files



### Displaying distance reading from ultrasound sensor

Here is some code Xavi wrote to turn the output of the ultrasound sensor from last week into a square.

    /**
     * Simple Read
     *
     * Read data from the serial port and change the color of a rectangle
     * when a switch connected to a Wiring or Arduino board is pressed and released.
     * This example works with the Wiring / Arduino program that follows below.
     */

    import processing.serial.*;

    Serial myPort;  // Create object from Serial class
    String val;      // Data received from the serial port
    int number = 0;
    void setup()
    {
      size(200, 200);
      // I know that the first port in the serial list on my mac
      // is always my  FTDI adaptor, so I open Serial.list()[0].
      // On Windows machines, this generally opens COM1.
      // Open whatever port is the one you're using.
      String portName = Serial.list()[0];
      myPort = new Serial(this, "/dev/ttyUSB0", 9600);
    }

    void draw()
    {
      if ( myPort.available() > 0) {  // If data is available,
        val = myPort.readString();         // read it and store it in val
        number = int(val);
        println(number);
      }
      background(255);             // Set background to white
      if (number <= 0) {              // If the serial value is 0,
        fill(0);                   // set fill to black
      }
      else {                       // If the serial value is not 0,
        fill(204);                 // set fill to light gray
      }
      rect(50, 50, 100, 100);
      delay(100);
    }

### How to embed a Processing sketch onto an HTML page


http://doc.gold.ac.uk/CreativeComputing/creativecomputation/?page_id=292
from https://cs.nyu.edu/~kapp/cs101/processing_on_the_web/

Before you start: Preparing your source code
If you've been working in Eclipse you have to make some minor adjustments to your Processing source code before you can render it on the web. Here's how to get started:

Create a new text file to hold your source code. Use any text editor (TextEdit on the Mac or Notepad on the PC).
Copy the code from your main class into this new source code file. Remove any import statements (Processing on the web won't work with any imported Java libraries). Also remove the class block - your code should be "loose" and not contained with a block of execution
If you have multiple classes / files you can simply paste them into your newly created text file AFTER your main class, making sure not to mess up the braces for each class. If you have any classes that were organized into packages you should place them into this file as well. Change all "public class" declarations to just "class" and remove any import statements.
Save your file as plain text using the "pde" file extension - i.e. ."myProgram.pde"
Here's a sample of what your code should look like

Now we can embed the code into a web page by doing the following:

Create a new HTML document
Place a copy of your Processing PDE file into the same folder as your HTML document. In this example our PDE file is named "myProgram.pde"
Place a copy of the processing.js library file into the same folder as your HTML document. You can get a copy of this file from the processingjs.org website
In the head of your document, place the following code to load in the processingjs library:
<script type="text/javascript" src="processing.js"></script>
In the body of your document create a canvas tag that references your PDE file, like this:
<canvas data-processing-sources="myProgram.pde"></canvas>



## Firefly

http://troybaverstock.com/learn/firefly-serial-read-ultrasonic/

connect your sensor directly to 5v and GND, echo to 11 and trigger to 12

in arduino ide
install newping library
go to examples - newping
select your port and board and upload it to the arduino
change the serial monitor baudrate to 115200


download example 1 from this page
http://troybaverstock.com/learn/firefly-serial-read-ultrasonic/
using panels attached to the text separator compnent you can isolate the reading in cm to a number
using list iten you can pick the output from your port only
<!---  
## firefly
analogueoutserial
baud
serialpintln(sensorvalue) - how we talk to processing
take values as a character string
grasshopper - firefly
doesnt work in mac only linux
arduino sketches programm serial in
component = ??
port
timer - sets refresh rate of component - baud

**no delay in the arduino as the grasshopper does that
serail read component
it reads like that arduino serial monitor
component - textsplit to
list item component gives us the latest value
vector 2 poins component
buffer component - stores a certain amount of input data
buffer domain how many valuess you want to have in yuor buffer
multiply each of these with a value
so you can create flows
so it plots points
points component?/
faces componet gives vertices and edges
rotate geometry component
has axis
degrees components - to transform into radians
 number

processing phone data is not tat straight forward

implement and interpret programming protocols
Described your design and fabrication process of your APPLICATION
had fun with visuals!! and buttons,etc
Outlined problems and how you fixed them
Described your problems and how you fixed
included original design files (Eagle, KiCad, Inkscape - whatever)
   -->

1. I downloaded and installed Firefly as per the instructions here:
   http://www.fireflyexperiments.com/resources
2. I opened the Firefly Firmata Sketch included in the Arduino IDE by going to  File > Sketchbook > “Firefly_Firmata”.
3. I made sure that the board and port were assigned correctly and uploaded the sketch to an Arduino. Board number 8 was assigned to t he arduino by the IDE.
4. I set up a test circuit shoen in the tutorial here
5. I opened grasshopper
   firefly - arduino boards-ports available
   check serial port is same as assigned on adruino ide




## IN progress.. X,Y and Average amplitude from sound sensor to processing heatmap




I am trying to create a map of amplitudes of sound picked up from my sound sensor from week 10 in a 2 dimensional plane.
I found some code here:


### Circular pixels
https://sites.google.com/site/ncadarduino/serial-communication which Might be able to take my output code and plot it into an x and y axis using control code

#### For the Arduino

      void setup()
      {
        Serial.begin(9600);
      }

      void loop()
      {
        Serial.print(analogRead(A0)); this would be the absolute x value somehow translated from the plotter
        Serial.print(","); this would be the absolute y value somehow translated from the plotter
        Serial.print(analogRead(A1));
        Serial.print(",");
        Serial.println(analogRead(A2));


#### In Processing

        import processing.serial.*;
        float xValue = 0; // reads the first value as x
        float yValue = 0; // reads second as y
        float toneValue = 0; // reads third as the analogue read
        boolean pressed = false;
        Serial myPort;
        void setup()
        {
        //creates a window 500p x 500p high
        size(500, 500);
        smooth();
        println(Serial.list());
        myPort = new Serial(this, Serial.list()[0], 9600);
        myPort.bufferUntil('\n');
        }
        void draw()
        {
        //Will fill based on the sensor's input
        fill(toneValue);
        //creates an ellipse r = 30 w/ centerpoint at the x and y coordinates
        ellipse(xValue, yValue, 30, 30);
        }
        void serialEvent(Serial myPort)
        {
        String inString = myPort.readStringUntil('\n');
        float[] coordinates = float(split(inString, ","));
        if(coordinates.length >= 3)
        {
        //maps the range of values of the potentiometer to the
        //values of the size of the screen. You
        //may wish to reverse the 0 and 500 to
        //get the knobs to go up and down according to a different //twisting direction.
        xValue = map(coordinates[0], 0, 1023, 0, 500);
        yValue = map(coordinates[1], 0, 1023, 0, 500);
        //maps the range of values of the photoresistor to the
        //values of fill.
        toneValue = map(coordinates[2], 0, 1023, 0, 255);
        }
        }

To make this code work I need to connect the plotter controller to the receiver arduino.


## Other programmes to try using Arduino inputs

* cheap 3d scanning  http://aircconline.com/ijcsit/V9N4/9417ijcsit04.pdf
* wifi field visualisation
* http://www.dustynrobots.com/academia/teaching/seeing-sensors-how-to-visualize-and-save-arduino-sensed-data/
* https://www.creativeapplications.net/objects/making-data-matter-voxel-printing-for-digital-fabrication-of-data/
* https://ccrma.stanford.edu/~mborins/256a/voxelMeter/

## Running Processing on a Raspberry PI


https://projects.raspberrypi.org/en/projects/introduction-to-processing

The first thing I learned is that a raspberry pi is not like an arduino in that it doesnt communicate through a serial port so you can't easily use your laptop screen and keyboard to interface.

You CAN however use an HDMI and USB keyboard and mouse
Apparently the default login for the Raspbian interface is # is username "pi"  with the password "raspberry"
""


plug in the raspberry pi using its microUSB
Download Putty onto laptop

## Voxeltastic 3D voxel visualiser
https://cnlohr.github.io/voxeltastic/#
