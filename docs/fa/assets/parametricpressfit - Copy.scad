thickness = 4;
kerf = 0.1;
size = 70;
nslots = 4;
slot_width = thickness - kerf;


module slotted_circle(size, nslots){
  difference(){
    circle(size);
    for(i = [0:nslots]){
      rotate([0, 0, (360/nslots)*i])
      translate([size, 0, 0])
      square([size, slot_width], center=true);
    }
  }
}

module rounded_square(width, length){
  corner_radius = 2;
  width = width - 2 * corner_radius;
  length = length - 2 * corner_radius;
  hull(){
    translate([ width/2,  length/2]) circle(corner_radius);
    translate([-width/2,  length/2]) circle(corner_radius);
    translate([-width/2, -length/2]) circle(corner_radius);
    translate([ width/2, -length/2]) circle(corner_radius);
  }
}

module slotted_rectangle(width, length, nslots){
  module slot(i){
    translate([
      width/8 + width/4,
      start - length/2 + dist*i
    ])
      square([width/4 + 0.1, slot_width], center=true);
  }
  
  start = width/4 + 2*slot_width;
  dist = (length - 2 * start) / (nslots-1);
  
  difference(){
    rounded_square(width, length);
    for(i = [0:nslots-1]){
      slot(i);
      rotate([0, 0, 180]) slot(i);
    }
    translate([0, length/2]) square([thickness, width/2], center=true);
    translate([0, -length/2]) square([thickness, width/2], center=true);
  }
}

module circles(number, radius, nslots){
  for(i = [0:number-1]){
    translate([(2 + 2 * radius) * i, 0]) slotted_circle(radius, nslots);
  }
}

module rectangles(number, width, length, nslots){
  for(i = [0:number-1]){
    translate([(2 + width) * i, 0]) slotted_rectangle(width, length, nslots);
  }
}

$fn=20;

circles(5, size/2, 8);
translate([0, size + 2]) circles(5, size/2, 8);
translate([0, 2 * (size + 2)]) circles(5, size/2, 8);
translate([0, -1.5 * size - 2]) rectangles(5, size, 2*size, 3);
