


// NRF24 setup off the tutorial here https://hackmd.io/s/B15x5dn9V

// first add libraries
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

// sound level sensor code from https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels
const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample;

// define pins to communicate
const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);
 
// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0x000003;

 //characters for connection test
char data[16]="Hola mundo" ;


// A function with void in front is a void function and so will not return a value. 
// you can use void loop(void) or void loop() to mean means that the function will accept no arguments.

void setup() 
{
   Serial.begin(9600);
    radio.begin();
   radio.openWritingPipe(pipe);
}

void loop() 

// sensor signal definition so it reads the maximum
{
   unsigned long startMillis= millis();  // Start of sample window
   unsigned int peakToPeak = 0;   // peak-to-peak level
   unsigned int signalMax = 0;
   unsigned int signalMin = 1024;

 // the sensor collects data for 50 mS 
   while (millis() - startMillis < sampleWindow)
   {
      sample = analogRead(0);
      if (sample < 1024)  // toss out spurious readings
      {
         if (sample > signalMax)
         {
            signalMax = sample;  // save just the max levels
         }
         else if (sample < signalMin)
         {
            signalMin = sample;  // save just the min levels
         }
      }
   }
   peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
   double volts = (peakToPeak * 3.3) / 1024;  // convert to volts

   Serial.println(volts);

// changing a number into a character string array

char b[16];
String str;
str=String(volts);
str.toCharArray(b,16);

// prints what we are sending out as charcter so we see 2 versions of each reading in the sending serial monitor
Serial.println(b);

// sending to reciever

 radio.write(b, sizeof b);
   delay(500);
 
}
