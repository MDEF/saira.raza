

# <center>Fab Academy Week 7 CNC</center>

<center>![](assets/)</center>

http://academy.cba.mit.edu/classes/computer_machining/index.html

HDPE high density polyethelene is really good for CNCing
Polycarbonate / lexan - hard to laser but you can machine slowly

OSP

a router bit comes in different shapes
big bites from one spiral can pull out more material
multiple spirals the smaller and smoother the surface but not good at pulling out material

shallow cut with a downcut then you finish with the upcut
HDPE is fine with upcuts but wood is bad

flat v balling
ball end mill has a curved endto reduced the jagged cuts
going across
radious feature is good with ball end
ball end doesnt remove material
 plunge down roughly the diatometer of the tool with a stepover of half a diamteer

 screw in wood screw into a brace for anywhere not on the sides
 tape
glue

run a test job and make sure the sides are square

dust collection is important so you dont breathe it.

kerfing machine most of the way through but leave one surave complete - so you can curve a flat surface

kamakura fab lab joinery

avoid the need for screws or glue

reciprocal frames - parts that go into flat structures


toolpath
kerf the space the tool takes away
offset toolpath runout is the margine for tool cutting

rugh cutsgive stripes
finish cuts go across?
ramping is going down at an angle
trochoidal milling - is a looping motion


1. cutting air - move machine above the material to check your settings

2. make a test cut

3. prototype with a lasercutter and cardboard



## software CAM
featurecam - autodesk
sollidworks cam
fusion has cam too. - plugin slicer- to slice into 2d parts

fab modules
- rough cutting module - flat cuts
- finish cut  - 3d paths

## coding
shopbot has an open format .sbp
gcode  .g

always turn off tool before reaching

2.4 x 1.2m board in 15mm

no glu screws or nails

start cutting 2d models


use rhino cam
it takes a while to learn


our machines read g code



## grasshopper short cuts

streamgates to


## Assignment

 make something big!

 ## cnc workshop

 laser cut first

50 digital joints
https://www.flickr.com/photos/satiredun/15868308421/sizes/o/

collet grabs the mill
check no dust between holes you cant close

we will use 7mm
check lines are closed

cnc always rotates clockwise in a conventional machine
and pushes the material up faster

hand drills are not end mills
the ends are different


3D print Ball joint?
https://www.thingiverse.com/thing:3455414

tennon joints for flat edge

dust is too slow - you need chips - eng mill has friction chips take heat out helps to cool.. less fire risk

https://laboeasd.wordpress.com/2017/10/22/catalogo-uniones-para-madera-cnc-y-corte-laser/


http://www.sketchchair.cc/

aluminium melts

shank and end mill sizes
cut length is limited.. longer and thinner end mills more expensive


things to check
- width
cut length


plywoode splits - upcuts are the worst
down cut
you need to define cuts as up or down

straight cut is best

screww material down

hard plastic upcut or flute


round mills to sand not to cut

changing endmills make a difference

bigger the engmill the better

 we use 6mm end mills
more flutes greater accuracy

it screams for chips

microshattering - ratational frequency on the material


## Checklist
* Feed rate - movement in x and y
* cut depth - tool diameter
stepdown - howmuch between each pass
stepover - theoverlap
* Chipload for your tool and settings (varies with machines)
tools have a   chip load chart
chipload - amount of material cut = something or other x feedrate in inches per minute divided by
rpm x xthe number of flutes

you can check the end mill futes

* climb v conventional direction of cutting

climb scoops like a waves cuts and sands down
cutting foam in climb is terrible

convention wood being pushed up

full table used a vaccum needs a lot of maintenance cannot suck if not clean


downcut never makes holes!

aircut first

mdef really bad pocketing - and bad dust

make test in grain direction
test in counter direction

### design tips
drawings in mm
clean
only have what you want to design
everything to origin (not in space)
close polylines
no random points
nestno fields only lines
only contours
explode text if you are using

line thickness have to be traces or rectangles

pocket

cut - profiling

goove

kerf of endmill 0.1 excess of cut
dimensions are always a bit smaller than your drawing


inside angles cannot be sharp

some cam programmes automatically let you select the right drawings

make inner corner a little more than the diameter

check the thickness of the wood - name your wood


  dxf 2004 polyline
  stl for 3d
  svg for 2d

  ## software
  ### vcard

  ###Rhinocam

  1. set up your stock / material

  2400mm x 1220
  origin on the top of the material
  thickness with caliper 15.2
  xy origin on material

2. look for duplicate lines
  clean them
lines need to be closed to cut inside holes or outside cuts (loops)


3. select your mill

stepover 40% of diameter

tabs - cut manually
ramps
name tool paths
incuts outcuts pockets need to be sequenced


strategies in order



you dont want endmill completely inside the cut

the motor might collide into the cut

mould need some gaps for air to escape between fitting parts

a line per rpm v speeds with climb and conventional
straight and downcuts

## rhino CAM

draw the material size

leave 20mm between edge and cut so you can screew it in
in stock window


macining


tool
started with flat 6

pocketing in control geo in 2.5 axi pocketing


advance cut parameters for bridges

profiling for cuts

adv para
entry exit = none to avoid ruini9ng parts

flat tool cos
select zero for z

machine tools set up
post processor options
cnc step vcn - is the big cnc machine
.mc file code for this machine

stock
reference material
xy zero on top of material
z on top of material

machine operations
x axis

stock max z distance  - 10mm
cut parameters
rough depth - 1mm
entry exit  - along path

generate to produce

cut holes for screws


look out for

test 7 x 7 circles and squares

different speeds

1500
2000
2500 etc up to 7000


and spindle speeds
18,000
20,000
22,000


## G Code


http://fab.academany.org/2019/labs/barcelona/local/clubs/makeclub/gcode/

3D printers and CNCs use Go code
e.g slicing programmes use this

usb to connect code to machine


The baud rate is the rate at which information is transferred in a communication channel. In the serial port context, "9600 baud" means that the serial port is capable of transferring a maximum of 9600 bits per second.

we can use the aruino interface

there are other in terminal ways

it remembers last comands so you need to spacify "off" commands

Parameters
[E] The length extruder motor in a 3d printer and the filament to feed into the extruder between the start and end point

[F] The maximum movement rate of the move between the start and end point. The feedrate set here applies to subsequent moves that omit this parameter.

[X] A coordinate on the X axis

[Y] A coordinate on the Y axis

[Z] A coordinate on the Z axis


http://marlinfw.org/meta/gcode/

Go
G1 - controlled motion eg feed rate
g2 - clockwise motion -
G17/G18/G19 (Working Planes)

G20/21 (Inches or Millimeters)

in arduino window

set budrate to machine

you can find gcode as a text file

header sets up the environment

chilipepper visualiser

you can refit machines like 3d printers using g code

or reset machines

you can use rhino cam to make gcodes
you can gocode into it?
