

# <center>Fab Academy Week 0&1</center>

<center>![](assets/)</center>



## Resources:

http://fabacademy.org/2019/

http://fab.academany.org/2019/

http://fab.academany.org/2018/labs/barcelona/local/general/

## Week 1
We missed the first week

Last years lesson is here
http://fabacademy.org/2018/lectures/fab-20180117.html

Learning outcomes were:
Communicate an initial project proposal
Start learning HTML/CSS in order to create a website

http://fabacademy.org/2019/docs/FabAcademy-Assessment/project_management.html

## Week 2
Some students presented their ideas. Students came from a variety of backgrounds including hydraulic engineering, railway engineering, behaviour change,

Prof Gershenfeld used a random generator to pick students to talk about their work. MDEF students will not be picked as we have not got space on the FabAcademy server.

Projects included robotics, wearable tech, internal sensors,
Here are some of last years final projects:

http://academy.cba.mit.edu/classes/project_management/index.html

### Synchronisation

rsync
low level efficient way to work remotely - it only copies over changes.
Used for backup servers.
works in one direction

### Version control
version control is a key skill we need t learn. The history of files is maintained.
Git manages history of files and multiple versions
Gitlab is a git interface
fabcloud is fabacademy's Gitlab repository

dropbox
owncloud is opensource

nginx server

browsers let you look at file lists

mozilla has lots of references on html

html5 lets you handle video
- ctrl U opens raw html in a browser
- div is a contanier <a href  makes a weblink

markdown is more readable but it get converted to popular html

strapdown lets you put java script to convert markdown
handoff converts

hugo and jekyll makes sites from

mkdocs turns markdown into ..?

### Editing

- Macs
- Atom (more visual interfaces)
- Lbreoffice - librewriter - graphic interface like a word processor  - html, markdown
- Dreamweaver
the html tends to be uglier
- bootstrap and jquery enable more interactive webpages

- Gitlab uses these:
  - YAML - describes workflows
  - CI/CD - allows continuous flow of an active website.

### Content management

- Wikis enable file generation that gets pushed to site
- drupal moodle - structured ways to dealing with content
- bluejeans enables video conference which lets you record


### Project management skills

- triaging - what to pursue and what to give up on
- supply-side time management - you build something you know you can do in the time. reverse engineer a project where you can can stop at different stages.
- work in parallel - to remove blocking project
- spiral development - iterative
- debugging - topdown you start removing things (reductive like science), bottom up starting with nothing (additive)
- make your project modular.
- document as you work through the modules


### Gitlab
has wikis built in
.git files contains the whole history of files

Setting up Git
windows has linux subsystem
https://xubuntu.org/ to get git through linux
android uses linux so you can use this on your phone

Windows
        Settings -> Update & Security -> Developer Mode
        Control Panel -> Programs -> Programs and Features
           -> Turn Windows features on or off
           -> Windows Subsystem of Linux (Beta)
        run command -> bash
        apt-get git

Fab academy expects megabytes per person per week
    "du - s" (in git) can check sizes of files
  - pictures need to be compressed reduce resolution to screen size. ImageMagick  is a utility to convert images.
  - videos ffmpeg does this for vids

Git has a lot of help built in

**Issue trackers (on Gitlab?) are used for communicating changes**



> dont worry be happy

Information will form into a cloud which will tighten up as the weeks progresses

## Thursday Git tutorial

Local classes
http://fabacademy.org/2019/labs/barcelona/local/

weekly assignments -choose something you are personally drawn to

gitlab is a tracker

past projects to cross-check

http://archive.fabacademy.org/

Luciana is the coordinator

Final project must be sellable!

**software download list to download this week**

assessment instructions are here:
http://fabacademy.org/2019/docs/FabAcademy-Assessment/
http://fabacademy.org/2019/labs/barcelona/local/documentation/

local instructors will assess us their notes are here:
http://fabacademy.org/2019/labs/barcelona/local/


All your design and fabrication files should be in the archive (Gitlab). Google drive it’s not allowed as a storage service


http://fabacademy.org/2019/labs.html

## Gitlab tutorial
http://fab.academany.org/2019/labs/barcelona/local/wa/week1/

Distributed VCS

we use git because its free

Below is a schematic to describe basic Git commands
![git schematic](assets/gitschematic.jpg)

### in the "terminal" / command prompt / gitbash
Gitbash is basically the command prompt / terminal but with colours.
ls  folders and files in repo
cd FLU change from one repo to another

if you type cd fab it will bring up
ssh-keygen etc is the command to gen a key
a key is a long string of letters  it has your email at the end


an ssh key has a private and public key - your private key is stored in your computer. if you share the private someone can access your computer

the public key looks like ID_

you paste the long key into gitlab
then if you try to clone the repo in your terminal it should come through

* git add - sends changes to a staging area
a commit is a milestone on your changes so adds are like tweaks
* a commit is more of a milestone that you can tag
whan you commit git makes a version of your repo.
* git status compares woking copy to local repo.
* git push sends the repo to online repo
* git fetch goes to local repo
* git pull pulls into working copy

Text changes are very efficient to commit.
Images or video file changes are inefficient because each new file must be stored in the repo so ONLY ADD PICTURES YOU ARE HAPPY WITH.

Commit messages should answer the question "if i apply this commit i will"
* git stash - moves working copy that you dont want to commit yet
* git stash apply is pasting this work

### Static site generators

SSGs takes markup language and will use html css and java using a layout programme. They include:
* mkdocs
* hugo
* jekyll (ruby based)
* docpad
* wintersmith

html is a markup - a language a generic term for  brackets around text
markdown is a markup language

Hackmd.io is also good for documenting (eg https://hackmd.io/ys7lcCDJSeq69meSeGnIXg)

#### Jekyll

Uses ruby a web dev language and can put javascript inside your website.

Gems are executable files for ruby.
Jekyll is a ruby gem
You need to install bundler from your terminal to manage these.

The .yml file runs the script to build your website every time someone asks for it. You add Jekyll in here.

Installing jekyll:

- In a fresh Jekyll upload there is a scripts folder where live links are created. On our template, in Scripts there is a config.yml file
- Sass folder contains formatting parameters

liquid syntax

Site file gets recreated every time you push

Modifying Jekyll
- In your header file in Jekyll you can add a class in html for a new section

## Assignment
- work though a git tutorial here:
http://academy.cba.mit.edu/classes/project_management/archive.html
- Start your personal site
- copy the respective file
- record what you did and what doesnt work.

### Research  / brain dump

<iframe
  src="https://embed.kumu.io/432e90e8aeadf74ebc983a7748de398d"
  width="940" height="600" frameborder="0"></iframe>


#### Soft robotics

Johannes Overvelde
http://www.overvelde.com/research/
www.studioovervelde.com/
http://thijsbiersteker.com/
https://mediatedmattergroup.com/

#### Light interfaces


#### Musical instruments

Hackoustic


#### Attention devices

### Things I'd like to build

* ADD Desk / Bed

* Diatom cell

* Acoustic metamaterial headphones

## Further Reading
