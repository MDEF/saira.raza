

# <center>Fab Academy Week 14 mech
 </center>

<center>![](assets/)</center>

actuation + automation

group project

individual documentation


shipleys - the bible

carr

http://academy.cba.mit.edu/classes/machine_design/index.html


stress is press

pain is strain

the modulus describes the relationship

stress strain curves


elastic averaging - multiple joints constraining - reduces error in one join

kinematic coupling - impose constraints so you can easily add parts to a system




## materials

   plastic
HDPE is nice for making parts - cutting boards are made out of this

   metal

   ceramic
   rubber - vibration dampening
   foam
   garolite
   wood
   cement

## adhesives
avoid because they can fail and you cant reverse them
acrylic adhesives melt the acrylic
hot melt adhesives can be removable

## fasteners
   nuts

   bolts
   washers
   heat-set inserts

## pins

framing
you can buy extrusions
   metal
   plastic
   t-slot
   self-aligning

drive
   gearsflat sharp gears break
   -
   rack and pinion
long straight gear with a round one
you can mill a rack into the machine

   sprockets


   belts
   (with teetth)

   chains
  high forces?

   shafts
   precision shafts define the drive access
guide shafts

   rods
threaded
   nuts

guide
   shafts
   rails
   slides

couplers
   shafts
   joints

bearings
   ball

roller bearings in a track

   thrust
carries load normal to it
turnbines
oil film on a polished surface and thats it!



   linear
   arroys of rings


   rotary
molding and casting projects in the lab


   sleeve

rotary
   wheels
   pulleys
   casters

lubricants

cables
   ties - velco
   carriers
   wire
   fiber

liquids
   pipe
   tubing
   fittings
   pumps

conveyors

springs

##mechanisms
   flexure
   cheap ways of inducing movement?
   http://academy.cba.mit.edu/classes/computer_cutting/56836505.pdf


   linkage
like that Brian eno double pendulumdouble pendulum
https://en.wikipedia.org/wiki/Linkage_(mechanical)

   whippletree
tree of joints to add forces


   pantograph
asymetrical link - for scaling!


   deltabot
used in 3d printing

   hexapod
  6 axes



   CoreXY
beltdrive in bridge  - to scan


   Sarrus
milli a slot to bend
   https://www.thingiverse.com/thing:135017



   SkyCam

passive movement!


   Hoberman



## assignment: design a machine - mechanism - actuation - automation - intelligence
as a group design
build mechanical parts
tem for each element

operate it manually -


## modules
   MTM
   Fellesverkstedet





group assignment
   - design a machine that includes mechanism+actuation+automation
   - build the mechanical parts and operate it manually
   - document the group project and your individual contribution


wildcard week - practice with the process you didnt do before
