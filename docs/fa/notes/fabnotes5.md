

# <center>Fab Academy Week 5</center>

<center>![](assets/)</center>

http://academy.cba.mit.edu/classes/scanning_printing/index.html



stl - format
ply - includes colour

formats for describing geometry plus colour
representing geometry as "distance fields" rather than surfaces.

representing geometry as voxels - vol files from tomography
tif files from images

voxels let you control parameters within the solid not just on the shell

### print somethign that you couldnt make by milling

doesnt have to be paramentrics.

you'll need meshlab. meshmixer - fix

slicer - that takes design and turns it into a path

you need to tell it what size it is

### scan an object

pointcloud to mesh
you might want to tewxture it
lidar - radar with lasers

photogrammetry  to scan a diatom?

light stages are bnot desktop yet

you dont need to pront it


## design a part - but cant be made subtractively

## Blender workshop

converting a model to a real size
the scene panel is on the RHS
click plus button on rhs for transform window
third button on the rhs label - it has 3 solid shapes
- look at units settings change to mm
click s to scale
ctrl z to undo

bottom menu
shading on lhs panel to look at finishes

object mode at bottom
change to edit mode - this will show you the mesh that your file will use - it could be too dense (too much processing) 0.05mm is highest resultion of a 3D printing. anything smaller is inefficient.0.5mm is the line of a printed line!
### Modifers
wrench icon on the rhs - modifer
choose decimate
choose planer for non curved stuff
collapse for organic shapes
shift d to duplicate and put in another layer to see changes
layers at bottom - little boxes in object mode at the bottom

CHANGE MODES WITH TAB


a key selects all
bottom left  - view select add mesh
normals - you have to tell the coomputer what is inside and what is outside
n key
display - mesh display - normals boxes
make normals consister
SPACE BAR to search for commands
flip normals
make normals consistant commands



Sculpting - intuitive - like a brush - pinching

change object mode to sculptmode
changes vertices
optimised meshes may have too few vertices
lhs dyntopo
12px more than good enough - its like a brush size
this CREATES vertices
click on the texture example at top left window to see different options of sculpt
smooth - if you press shift - is the most useful/ powerful
decimate again
optimise mesh again


you cant print in air so you can cut horizontal high up srfaces that have no support


modifer - boolean modifer to create a surface and attach to your model
not sure how to select the objects!

3D printing tab on lhs needs to be added fil>user settings

make manifold - closes holes
checks  - intersections (problematic in printing) and OVERHANGS.. no solution

volume  - for whan you rent a printer

you need to apply modifiers when you export

ctrl l - selects linked vertices


objects - can have modifers applied
edit mode - will go inside the object by vertices

shift and middle button - grabbing hand

create tab
g for grab


Cap Planar Holes - in sold menu in rhino

## Cura for slicing for 3d printing

layer height 0.1-0.3 for first parameter
initial layer
 - rough
 line width - nozzle size - 0.4 we have 0.6 too - affects quality

 shell section - the wall settings

 ;eft hand side button for layers

 red is outer wall
 green is inside

 top layers and bottom layers need definition

 blue
 after 6 layers it starts infilling

 more layers in the top side because you need proper solid (so siz layers) supports

 90% infill should be maximum.. it doesnt increas the strength of the part.

 3d printing a frustule will never do it justice because you cant print radially only orthoganollay

 post processing hcan make things watertight.

 but you couls show a liner hierarchical structure

 experimental material
 metals

 pla
 3mm and 1.75 thickness only
 pushing through a liquid th

 something designed in volxel software - it would be greate if we couls use it to greate a mould?

 ABS needs a heated platform

 ## scanning workshop

 ### Photogrametry

 coda nvidia - is needed from photogrammetry

 mythbusters demo gpu v cpu
 without coda it will take days

 software
 opensource
 photogrametry
 pictures around an object

 - autodesk has Recap
 upload your images to autodeskcloud
 used with drones
 uses ligar (laser data of distance)

-  photoscan
 widely used
 needs a lot of ram
 3000 dollars

- alicevision - opensource
 https://alicevision.github.io/
 meshroom - free to download
 easy to use on windows

 try and keep your hard drive less than half full

- z pro - small piece of metal with a sensor that saves data  - called a digitiser - really slow but accurate scans for small objects (hours_)
the output is pointcloud
you need a software to build a mesh
stl files are triangles
pointcloud > densecloud > mesh > texture

- Colmap
you have to have coda

- Skanect from kinect
uses infrared and standard images - used for gaming
freeware for small models
you have to scan your object really slow
keep the image between green and yellow

with drnes you want to photograph a hemisphere - you might have to move

- Autodesk recap can piece photoes together
https://www.autodesk.com/products/recap/overview

can save in OVG as this can save textures.

- noumena studio  - used in mrac
https://noumena.io/

pictures need to be named in order

- fiji



### post production
- meshmixer
3d models need to watertight

meshlab - old software works well for pointcloud reconstruction
it creates normals on the dots and from this meshes

you erase pointnoise to enable an intelligible shape that can make a mesh

Fluid scanner - milk scanning
http://milkscanner.moviesandbox.net/

 - threshold - ?
 contrast needs to be high
 calibrate with one opaque layer of milk
 space to start recording
