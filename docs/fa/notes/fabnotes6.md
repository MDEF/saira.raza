

# <center>Fab Academy Week 6 Electronics</center>

<center>![](assets/)</center>

http://academy.cba.mit.edu/classes/electronics_design/index.html


Capacitors
little batteries
charge instantly - only AC

inductors
only works in AC - power line filters

diodes
arrow points from anode to cathode ()
symbol (line) usually indicates the cathode
its a valve

zener diodes suddenly conduct backwards at a set voltage

LED
brightness
direction

you can blow the LED withour a resistor in series - to sacrificially protect the LED

mosfets switch power
electrically controlled switch - can handle 30v 1.3 amps - current as big as used for a motor
specified by voltage current and resistance you want to keep this as low as possible
p mosfat - high side goes to power supply
n goes to ground

regulator

amplifier - or op amp
amplifies differences in inputs
there are tiny ampfliers in microprocessors

microcontrollers / processors
can measure time, digitise signal , output analogue signals

The Art of Electronics - horowitz

Kirchoffs law
voltage
sum current at a node = zero
 power = iv and i squared r
so CURRENT has a larger impact than resitance on power needed / used

you get heat sinks for mosfets to dissipate heat

EDA - drawing
is hierarchical so you can change loops easily
is parametric so you can change distanced

you pick the package for microprossesors -liraries of footprints

- place components
- wire
- output for fabrication

limitation on thinness of wires and distance between wires

you can have double surfaced circuit
or with an internal layer for things like power and ground
more layers are more powerfull but harder to make

pcb ground pour - improves electrical properies

you could just draw it by hand  but it would be hard to change
you could physically tape down circuits
use a graphical interface
- tinkercad
- virtual bredboard
- fritzing
but they aren't for high end design

 kicad - opensources connected to cern
 schematic editor
footprints
 export netlist
 artwork - ratsnest - footprint plus wires
 can rotate elements
 rout - by picking trace and rout it
 we use autorouters as they get more complex
 3d cad design tool of circuit to help design the housing

 export svg file to mill
 window has copper layers window and you pick what you want ot include in the trace
 you can use mods for the milling machine or vinyl cutter
 you have a copper file
 upload to mods and invert
 tll it to mill traces and it calculates tool path
 mods can calculate cut out
 can run a design check to make sure your circuits dont touch with components on
 auto routing - is for complex boards.


eagle is a popular alternative
small circuits are free
linked to fusion so thats handy to integrate with your cad


digikey have a kicad library

spice is used for simulation of circuits

Falstad
Circuit simulator applet
easy simulator

VHDL - are programming languages to write circuits
e.g kokompelli
not intuitive
edit in hard code
have to allign the components in the code
rout by listing connections
it draws a schematic / footprint
you run the changes in a third window

 the footprint is hardcoded


## Assignment

individual assignment:
redraw the echo hello-world board,
add (at least) a button and LED (with current-limiting resistor)Resistor calculator
check the design rules, make it, and test it
extra credit: measure its operation
extra extra credit: simulate its operation



- draw circuit


 at tiny 44 processor add a button and an led (and its resistor)
http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.components.jpg

 pb2 and pa7 are free
 make your board do something - push button to make led come on etc

 redraw board from scratch
 place parts minimum to add button and led
  check design rules
  fabricate

  upload echo programme to test it

  its about using design tools

  http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c

  group will test voltages on the pins and look at them through oscilloscope



http://fab.academany.org/2019/labs/barcelona/local/wa/week6/


## Workshop

http://fab.academany.org/2019/labs/barcelona/local/wa/week6extra/electronicsII/

ftdi patented microchip - talks through usb translating info through 6 pins.
needs drivers
check name on the chip

or a chinese ch34ftdi might work but clock time might be  a bit off

boards have 3.3 or 5 v usuall with a jumper that helps you set  the voltage

Pinout attiny Core
specific pins on the ATTiny Core must be connected to specific pin in of the arduino

pull up/ down resistors remove noise?
10k ohms
resistance before the vcc (often built into the microchip)
vcc is a microchip voltage
if it isnt built in you have to add it your selthe board will get a voltage  - when its connected to ground voltage will be low

f
when the microchip is connected to the board

it ensures zero is really zeroed out for the off state going into the mcu

## sync v async - more or less cables
synchronous and serial protocols of connecting signals

more on sparkfun

Parallel asynchronous communication 1 out goes to one in  1 packet of information per line

or lots of packets one after the other in one cable with a clock time delineating where the info packets start and end - in synchronous way

rx and tx in FTDI
a clock signal says how regularly to receive

fab isp - the long one we built first - is synchronous isp with 6 pins

ftdi is asynchonous one
data goes from tx to rx without a clock

in arduino programming windoe you can see the serial speed of the reciever? usually 9600 - if this doesnt match the

## SPI (serial peripheral interface)
sck is the clock
master generates clock whatever is attached is called slave
MOSI connects with MOSI (master out slave in)
MISO master in slave out

voltage
ground
reset - remove the reset to reset connection to prevent it from being reprogrammed

on the 44 board voltage at bottom
reset goes to PA3 on micro

so look for MOSI, MISO, SCK and reset ( in the data sheet)

TX and RX on circuit diags PA0 and PA1

## crystal
crystal sets resonates the signal - when it vibrartes the electrical signal
10% error
internal RC oscillator - if you need faster - then you need externeal.. like for server
resonator has 5
crstal true capaciotor 1%
8 MHz

xtaal vibrates 20 million times a signal
provides output signal

you programme the signal to account for the presence of a crystal

yuo always need capacitor outside of a crystal component.. resonnator has these integrated


https://learn.sparkfun.com/tutorials/serial-communication/all


Makefile


http://highlowtech.org/


version 1.6 is solid
in tool
at attiny board
instructions here? http://highlowtech.org/
in tools
specify programmer USB tiny isp

http://highlowtech.org/?p=1695

http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html

## Eagle workshop

### open a schematic
lhs
projects n
new folder
new project
green button in top right
right click
new
schematic

### add components

download fab.lbr
https://gitlab.fabcloud.org/pub/projects/blob/master/files/fab.lbr
download eagle_fab

go to library tab in schematic
available tab
browse
click use once you have found eagle_fab in your downloads

add component on lhs
find eagle_f which contains all the components we will need
select attiny 44 soic14
add

escape to go back to library

* add vcc and ground to power
add component
search for vvc and ok

* same for gnd

select NET the green angle symbol
click on vcc and connect to vcc on chip

* add programmer port
eagle -fab
avrisp

### add connections
draw a short line out of VCC on port
click name button R2
tag the net you drew
call it VCC in caps press ok
asks you if you want to connect.
the net is now tagged as a vcc
and it has connected it


or you can right click on component
breakout pins
select pins
in label format select pin names
the dialogue box should pop up when pins are connecting


back to the PCB

RST = reset

### Generate board
"Generate/switch to board" button on top line created the connections


add crystal from library
connect to xtal1 and xtal2
one connection excites and the other reads the voltage  coming from crystal

cap-unpolarised for capacitors
c1206fab

one to each end of the crystal


select value tool next to name
click cross on component to add values it doesnt connect to anything

free pins - ADC pins are hard coded and can only connect to some stuff


leds
source combinations - sources voltage from the pcb current flows from pcb glows variable

synch configuration synchs with main voltage?

6mmswitch - for button


## Routing

move IC first
click scroll button to indicate you are soldering on the bottmo of the plate
''

click ratsnest button to untangle

grid button to add a grid

routing is actual physical connections

route button
connect red pads
width needs to be 16
lines
you can use a zero ohm resistor resistor to jump acrosss


layers settings button

for traces
-  hide everything
- select the top and dimensions layers
- file
- export
- image
- name the file
- select monochrome
- dpi resolution has to be 1000


interior
- only dimensions layer
file
export
image
name the file
select monochrome
dpi resolution has to be 1000


DRC
Design Rule Check



## Falstad simulation of circuit
