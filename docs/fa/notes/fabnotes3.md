

# <center>Fab Academy Week 3 lasers and vinyl cutters</center>

<center>![](assets/)</center>



## Resources:

http://fabacademy.org/2019/
http://fab.academany.org/2019/
http://fab.academany.org/2018/labs/barcelona/local/general/
http://academy.cba.mit.edu/classes/computer_design/index.html

lignana - carbonfibres -

### Lasers


### plasma
more affordable
dangerous
waterjets - really expensive can cut through almost anything
hotwire - arch foam
wire edm - expensive - wires made from plasma -

vinyl cutter
laser cutter

this week we cut using parametric cad
laser has a radius so you have to offset the laser.

inkscape extensions can do this

pepakura, flatfab - specific for cutting.

printer drivers allow printing sraight from machines
lots of screens
mods http://mods.cba.mit.edu/?program=programs/frep/gears is something neil wrote to coordinate these and lets you build workflows and talks directly to the machine rather than go through a driver.

vinylcutter task
vinylcutter scores paper and cuts a circuit so you can lay one ver the other.

thermal transfer heat press can melt vinyl into fabric.

origami score

kirigami - origami plus cuts.

you can make electronics on the vinyl cutter - next week in electronics - antennas for radios ( metamaterials?)
circuit for solar panel - to stick onto a glass - titanium oxide blackberry juice




materials
knives
copper
transfer adhesive - lets you turn stuff into a sticker
sandblast stencil

you have to do test cuts first




living hinges

https://www.core77.com/posts/66075/How-to-Design-Living-Hinges
http://web.mit.edu/2.75/resources/random/Living%20Hinge%20Design.pdf

how to make whole mechanisms this way

lasers

laser has a gain medium which sets the wavelength of the laser light - goes to an alpha coupler.
co2 lasers are very efficient
not good for metal cutting
you get some that can raster metal then cut off the board - expensive

WAIT before you open the cutter

never leave it unsupervised


focus on cardboard - heavy duty - good joints

you could put in laser settings as parameters
you could do this in mods


## Assignment

### cut something using vinyl cutter

you can screenprint - image > pattern of hole > can screen print you can illuminate through them
   cut something on the vinylcutter

### press-fit construction
GIK.fcstd - is a freecad file - a parametric design using a spreadsheet opens with freeCAD

slots are weak - chamfers improve them - use them to pin them to improve mechanical properties

http://mtm.cba.mit.edu/
http://mtm.cba.mit.edu/machines/science/

   - design, lasercut, and document a parametric press-fit construction kit,
      - accounting for the lasercutter kerf,
      - which can be assembled in multiple ways (modular kit)


## group assignment
characterise your laser cutter
what is the right feed rate and pulse rate for your material

test with too much and too little
measure beam curve the size you actually got v what you put in.

cut a range of cones

assignment
   group assignment:
      characterize your lasercutter,
      making test part(s)
         that vary cutting settings and dimensions
         kerf is the space left behind by the laser.

         extra credit if you make not just flat surfaces

         living hinges
#check out last years documentation

## week 3  workshop
http://fab.academany.org/2019/labs/barcelona/local/wa/week3/

### lasers
never cut list
vinyl and pvc are not to be laser cut - as they are toxic
abs isfrom petro chem so not to be inhaled

htpi - low density polyethylene

no to GLASS - etch not cut

safe list
mdf lots of glue - formaldehyde - lots of smoke not a clean cut
3-5mm pine plywood ideal
cardboard - kraft type cardboard more resistant  fancy gift boxes
kapton tape - orange tape - heat insulator-  1500 degrees electrical isolator
styrene - flmable
eva foam - like floor mats for kids
leather and suede - shoes! natural  -

PVC is bad so check artificial leathers dont contain this

Magnetic sheet - id no PVC

glass - laser is not powerful - engrave - fine dust and **silicates** sandy destroys machine
safety glass  - layers with glue between layers - can destroy one layer - can break the glass!

cant cut metal without gas assistance
(like tig welding) laser inflames gas

look at slides

a CAM program controls te machine
laser source is a vacumme pipe wih a high power LED (amplified by a ruby?)
- with voltage across it and amperage (current) Power = IV
- frequency of light can be controlled by good laser cutter
20-50 milliamps danger zone for mains voltage

focusing lense is what does the final power concentration from 1cm into 0,25mm

really light mechansim because the weight is small lenses and mirrors only

air compressor at tip to blow away the shit and to help combustion - 200- 800 euros lense can get damaged and cool the surface
smoke should go down not up with a compressor

focus is small and blade is not straight - its therefor not efficient for thick materials

heat  = IVt (energy)
can you programme the speed

 - yes

 fiber lasers   - pcbs


 softwares
 https://svgnest.com/ - optimises cutting layouts - to minimise waste


 #### tolerances and joins
 general offsets of all the shapes - one shape needs to be very very slightly smaller than the other

 kerfing geometries that allow flexibility  - cutting a spring - ALSO the metamaterial pliers - but can you cut them or will they snap. GRAIN matters - it can snap - aximotric  - like that 3d printed curved surface you borrowed

 you can use round corners or cut a circle on the corner

 acrylic  - cnc snds but laser melts so you can use the edge and light it up

 mdef- not structural

 instructables has loads of laser examples https://www.instructables.com/

 pressfir

 stacking  - like topographical maps solid layers-  material heavy - how out shapes and nest parts to save materials

 waffle - metropol sevile - bcn house

 stacking eg paper Michael Hansmeyer

**tesselation**


 Vector  v raster mode

 vector - more common - laser follows line - erase double lines - join the lines as polylines. machines wil cut separately otherwise and not optimise - it will take ages - air travelling

 raster mode  - bng image - mikel knows - it engraves solid areas usually m- usually made by filling with a hatch WITH A BOUNDARY


- always work in mm
- 1000mm x 6000mm  and 600 x 400
- draw a rectangle of you material size
- origin in top left corner
 https://wiki.fablabbcn.org/Category:Machines

 ctrl print choose colour settings
- and power - % of max keep under 80% because frequency is pushed to limimts
- speed 0-100
- ppi resolution fixed to 500
- frequency it is the amount of times the laser is being pulsed  - low frequency can actualy just cut dots and perforate
  - natural  - -lower 500-100,
  - plastics higher frequenccy need more energy conc 5000 -

 switch on compressor and extractor as well as computer and laser


 how to focus laser
 move base of material up and down and we have colibrating tool

low speed high power - thicker materials

plastics allowed:
acrylic
delrin'
mylar (heat blanket - diachromatic)
petg
styrene

ask mikel if you dont know if you should use

you have to do machine test
http://fab.academany.org/2019/labs/barcelona/local/tests/

multicanm big one


Tutorials section here http://fab.academany.org/2019/labs/barcelona/local/wa/week3/

on particular softwares and machines
papermodels - to make 3d maseks and models printing stickers that fit on a 3d paper laser cut model.

Origami:

tt
treemaker
Paper Models How to use a caliper

Other Resources: Massimo Menichinelli - Laser Cutting lecture

HOW TO LASERCUT LIKE A BOSS  - mechanical and geometric turorial
https://lasercutlikeaboss.weebly.com/
https://lasercutlikeaboss.weebly.com/uploads/2/7/8/8/27883957/advancedjoinery_master_web.pdf
best tutorial for press kits  in geometric terms


inkscape does raster to vector transformations
and can hatch a solid for you
