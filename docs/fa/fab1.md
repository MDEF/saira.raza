

# <center>Fab Academy Week 1</center>
# <center>Version Control</center>

<center>![](assets/)</center>


 <!---
<nav class="sidebar show">   explain what css is and how you tweaked -->




##### Resources:

* http://fab.academany.org/2019/labs/barcelona/local/documentation/#project-management-and-principals-and-practices
* http://academy.cba.mit.edu/classes/project_management/archive.html



## My final project

I will be investigating fabrication and testing of acoustic and possibly RF metamaterials.

I would like to build a test 'rig' to scan a plane with a sensor (either microphone or wifi or radio).
I hope to be able to use part of a printer for this.

I will need to build an emitter of sound or wifi or radio as well - so currently I am looking at ultrasound or acoustics - other researchers have used an array of emitters to ensure a flat incident wave. Oscar has recommended that I could try and make a parabola to focus the waves.

<!---  insert sketch here   -->

## Archiving documentation

Part of Fab Academy sharing our experiences of digital fabrication so documentation is important.

Oe the MDEF course we do this by uploading to our Gitlab websites.

I will store my documentation here:

  <a href="https://mdef.gitlab.io/saira.raza/fa/Fabhomepage/" class="btn btn-outline-primary">My Fab Academy Home Page</a>

<!---


Here are a some references on similar projects:


[Gang Yong Song, Bei Huang, Hui Yuan Dong, Qiang Cheng & Tie Jun Cui Broadband Focusing Acoustic Lens Based on Fractal Metamaterials](https://www.nature.com/articles/srep35929?WT.ec_id=SREP-20161101&spMailingID=52661940&spUserID=MzcwNDE0MDA2MjcS1&spJobID=1040395485&spReportId=MTA0MDM5NTQ4NQS2)

-->

## Git

Git is a Source Code Management tool that runs a [Distributed Version Control System](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control).
This is useful for collaboration because it enables you to create branches where you can work on something and push it to a main project. Git takes a picture of what all your files look like whenever you commit or save a change and generally only adds data to a project so every change is stored. If any changes are made to the main project that is not on your local repository Git will flag them and require you to "pull" them to your project so each contributor has a clone of the main project. You can therefore undo mistakes without affecting the rest of the project.

I found this diagram particularly helpful to visualise how Git works.

<center>![](assets/gitschematic.jpg)</center>

It helped me to understand the importance of not committing image changes too often.


For my website:

* **working copies of files** for my site are edited in the web editor [Atom](https://atom.io/).
* My **local repository** is stored on my laptop hard drive
* And my **main repository** is hosted online on [Gitlab.com](https://gitlab.com/)

### Git commands.

Data is transferred between these 3 areas using **Git commands**.
You can run Git commands from a Windows computer's Command Prompt (the black box with white writing that you can call by typing command prompt into search - it's one of the most powerful tools in Windows 10) or a web editor like [Atom](https://atom.io/) but you have to install Git onto your computer first from [here](https://git-scm.com/download/win).


#### To register with Git

To configure the Git commands you first have to **register your name and email address with Git** by entering into your command prompt:

    $ git config --global user.name "<YOUR NAME>"
    $ git config --global user.email "<YOUR EMAIL>"

You can configure a web editor to work with Git too using

    $ git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -nosession"
To list all the settings Git can find you can type:

    $ git config --list command

#### To set up a local repository on your computer

Create a file on your computer. navigate to the file in command prompt

    $ cd /c/user/my_project

Type

    $ git init

And a new subdirectory named .git to contain all your repository files will be created in the file you created

#### To generate an SSH Key

An SHH key links your computer to an online repository.
In command prompt enter

    ssh-keygen -t rsa -C “$your_email”

It should save it in a folder caller .ssh which in windows should be located in the top most level of your computers files where .cache, .gem, .atom are saved because it essentially is only identifying your device.

to check what it is enter in the command prompt

    cat ~/.ssh/id_rsa.pub  

to copy it

    clip < ~/.ssh/id_rsa.pub

#### Connecting a local repository to an online repository hosted by Gitlab

To host a site on Gitlab I :
* Created a new account on Gitlab with my email address
* I signed in to the prepared Gitlab repository on the MDEF website called https://gitlab.com/MDEF/saira.raza.
* I went to PROFILE -> SETTINGS> SSH Keys and added the key in.

#### Cloning a prepared repository in Gitlab onto my laptop

In Gitlab on the project page for my website I copied the Clone with SSH name
and entered this into the command prompt in the following line:

    git clone git@gitlab.MDEF/<REPO NAME>.git

#### Copying changes made online to your local repository

    git pull
  will copy any changes

#### Pushing changes to the online repository from your local repository

    git add
    git commit -m "message"
    git push
This adds the change to the staging area, commits it to your local repository with a message describing the change and pushes it to your online repository.

I can do these commands from the web editor Atom.



### Web Editors
To edit my website locally I have been using the web editor [Atom](https://atom.io/).

#### Connecting Atom to a Gitlab repository





#### To push changes in Atom to Gitlab

Click file> save

The changes then appear as a job in the right hand panel in the "Unstaged Changes" area"

Click "Stage all"

![](assets/save2.png)



The changes will then be saved to the "Staged Changes" area


![](assets/stage2.png)



Enter commit message in the "Commit Message" area and click the "Commit to Master" button.

![](assets/commit2.png)




Then (and this took me a whole day to find!) Click the "Push" command in the bottom right hand corner of the window. It's really small and easy to miss but this is what sends you changes from your local repository saved in your laptop to your central server (in my case the Gitlab server)

![](assets/push2.png)



![](assets/pushing2.png)



## Formatting the site

### Static Site v Dynamic site

[David Walsh's site](https://davidwalsh.name/introduction-static-site-generators) gives this metaphor of a **dynamic web site**

>When a visitor gets to a website (the kiosk) expecting the latest content (the news), a server-side script (the operators) will query one or multiple databases (news agencies) to get the content, pass the results to a templating engine (the scribble) who will format and arrange everything properly and generate an HTML file (the finished newspaper) for the user to consume.

He explains that static sites carry out loading of the entire site only when content changes.

In his news kiosk metaphor, for **static site generators**..

>the news agencies call the kiosk whenever something newsworthy happens. The kiosk operators and scribbles will then compile, format and style the stories and produce a finished newspaper right away, even though nobody ordered one yet. They will print out a huge number of copies (infinite, actually) and pile them up by the store front.
When customers arrive, there's no need to wait for an operator to become available, place the phone call, pass the results to the scribble and wait for the final product. The newspaper is already there, waiting in a pile, so the customer can be served instantly.

*[https://davidwalsh.name/introduction-static-site-generators](https://davidwalsh.name/introduction-static-site-generators)*

There are many static site generators including

* [Mkdocs](https://www.mkdocs.org/) is built on Python and pip specifically for documentation and this is what my Gitlab repository came configured in. It is simple to use and is configured in a single .YAML file which is tidy. It uses the Jinja template engine.
* [Jekyll](https://jekyllrb.com/) - widely used , runs on Ruby and uses a Liquid templating engine which allows you to format different parts of a page in a modular way and is used to power GitHub pages
* [Hexo](https://hexo.io/) - is powered by Node.js (JavaScript ) and is aimed at blogging and has many designed themes you can run
* [Hugo](https://gohugo.io/) is apparently "The world’s fastest framework for building websites" I'm not sure it has much flexibility though
* [Gatsby](https://www.gatsbyjs.org/) - is also powered by Node.js and uses React which allows you to build with components. I like the idea but I would have to learn React.
* [Nuxt](https://nuxtjs.org/) runs of javascript and creates Vue applications  
* [Metalsmith](https://metalsmith.io/) is advertised as extremely simple as everything is a plugin - not many examples of this but it sounds interesting to try

#### HTML, CSS and JS Libraries

These can be used with some static site generators and include:

* Bootstrap - open source toolkit for developing with HTML, CSS, and JS.
* Metro 4 - opensource

#### Just CSS Libraries

Styling libraries you can reference in the header of your HTML page and call up in the body. Popular for animations.

* Material - this came preloaded into my site
* https://teutonic.co/ is an entire CSS toolkit to build a website
* https://equinsuocha.io/blog/hot-tips-css/ has snippets you can copy and paste into your css




#### .gitlab-ci.yml file

This file tells Gitlab what scripts to run when you push a change. We were initially given a site with the following code in YAML for MkDocs material theme.

    image: python:alpine

    before_script:
      - pip install mkdocs
      - pip install mkdocs-material

    pages:
      script:
      - mkdocs build
      - mv site public
      artifacts:
        paths:
        - public
      only:
      - master


#### Mkdocs

[MkDocs](https://www.mkdocs.org/) is a website template system built using .yml, .css, .md and javascript files to manage documentation.

<!---    Explain how you generate and why you created the YML file in Gitlab
  -->
MkDocs runs from the script
     - pip install mkdocs
which is putread from the .gitlab-ci.yml file.

The mkdocs.yml file contains the style configuration for the website including the titles and destinations of the navigation bar links, the file where .css overrides are stored (extra_css: [extra.css]) , favicon and broad colour themes.

I continued using mkdocs and adjusted colours of the background and menu bar using the extra-css extra formatting file.

#### Bootstrap / Bootswatch template
Later I tried the Cosmo [bootstrap theme](http://mkdocs.github.io/mkdocs-bootswatch/)  which required some scripts to be run.

To do this I added into the .gitlab-ci.yml file:

    pip install mkdocs-bootswatch


I add the CSS to where mkdocs stores its formatting overrides. It seemed to work but there were some frustrating features like all links - in the navigation, sidebar and in the body of the text all looking the same.

Eventually by inspecting the code on Google Chrome I managed to hide the messy side navigation bar by tweaking the settings. and making the body background white in stead of transparent.
![Inspecting code](assets/inspection.PNG).

After days of searching I managed to make the sidebar not overlap with the content using
    .container {
      width: 100%;
      padding-right: 0px;
      padding-left: 0px;
      margin-right: 8.1rem;
      margin-left: 4rem;
      display:flex



The toc script in the mkdocs file seemed to be doing something strange so I took this out too.

Also hljs_style in the mkdocs.yml file governs highlighted text so I looked up the options for this here https://highlightjs.org/


It is a work in progress and I hope to make the code lighter by the end of the year.


## Structure of the site


I wanted to make a welcome page with large animated clickable links.
Neil advised us that .gifs are not the a light file to host on Git however I think on this one page if I chose them once only, I think hey will be worth the storage space.


#### HTML welcome page
At the moment I am using basic html to make gifs into clickable links.
I appreciate that this is messay and not very slick but I wanted to pay homage to HTML and how much joy it can give you with very little coding.

![homepage](assets/homepage.gif)


the clickable navigation larger so tried to do this using clickable images. I used some .gifs to simply add some animation. We were warned in our lesson that .gifs are not light files so we should avoid them. However it was good to feel how browsers responded to so many gifs. They require good internet connection.

## Managing the size of the website

#### Minimise tweaks to images

We were advised that the nature of the git commit means that you can never really remove images from Git therefore it is important to use them paringly and well formatted.

#### File formats

We were advised against using .gif files as they take up memory to run when called.


One thing that took me a few weeks to realise is that when coding in markdown (and probably not exclusively so) that capital letters matter and if a file is called a .PNG file in capitals, it should be referred to as a .PNG file not a .png file.  
